import * as gulp from 'gulp';
import * as  clean from 'gulp-clean';
import transpile from './transpile';
import processMarkup from './process-markup';
import processCSS from './process-css';
import preCache from './pre-cache';
import {build} from 'aurelia-cli';
import * as project from '../aurelia.json';

export default gulp.series(
  readProjectConfiguration,
  gulp.parallel(
    transpile,
    processMarkup,
    processCSS
  ),
  removeDirs,
  writeBundles,
  transferFiles,
);

function sleeper() {
  return gulp.dest('/');
}

function removeDirs() {
    return gulp.src('scripts', {read: false})
        .pipe(clean())
        .pipe(gulp.src('dist', {read: false}))
        .pipe(clean())
}

function readProjectConfiguration() {
  return build.src(project);
}

function writeBundles() {
  return build.dest();
}

function transferFiles() {
    return gulp.src('scripts/**/*')
        .pipe(gulp.dest('dist/scripts'))
        .pipe(gulp.src('index.html'))
        .pipe(gulp.dest('dist'))
        .pipe(gulp.src('manifest.json'))
        .pipe(gulp.dest('dist'))
}
