import * as gulp from 'gulp';
import * as swPrecache from 'sw-precache';

export default function preCache() {
    var rootDir = 'dist';

    return swPrecache.write(`${rootDir}/sw.js`, {
        staticFileGlobs: [`${rootDir}/**/*.{js,html}`, `${rootDir}/`, `${rootDir}/index.html`],
        stripPrefix: rootDir
    });
}


