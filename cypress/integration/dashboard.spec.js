describe('Dashboard', () => {

  before(() => {
    cy.visit('https://staging-professionals.vanoapp.com/login')

    cy.get('form').within(($form) => {
      cy.get('input[type="email"]').type('demo@email.com')
      cy.get('input[type="password"]').type('password')
      cy.root().submit()
    })
    cy.wait(1000)
  })

  it('should display stats', () => {
    cy.contains('Appointments left today')
    cy.contains('New appointments this week')
    cy.contains('Booked upcoming seven days')
    cy.contains('New customers previous 30 days')
  })

  it('should allow a user to click an appointment to view its details', () => {
    cy.get('.vano-appointments-table-component .vano-table .body').within(() => {
      cy.get('.vano-row').first().click()
    })
    cy.wait(2000)
    cy.title().should('eq', 'Appointment Details | VANO')
  })
})
