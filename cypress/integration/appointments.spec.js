describe('Appointments', () => {
  before(() => {
    cy.visit('https://staging-professionals.vanoapp.com/login')

    cy.get('form').within(($form) => {
      cy.get('input[type="email"]').type('demo@email.com')
      cy.get('input[type="password"]').type('password')
      cy.root().submit()
    })

    cy.wait(1000)
    cy.visit('https://staging-professionals.vanoapp.com/appointments-v2')
  })

  it('should allow a user to click on a day on the calendar to book an appointment for that date', () => {
   
  })
})

describe('Add Appointment', () => {
  before(() => {
    cy.visit('https://staging-professionals.vanoapp.com/login')

    cy.get('form').within(($form) => {
      cy.get('input[type="email"]').type('demo@email.com')
      cy.get('input[type="password"]').type('password')
      cy.root().submit()
    })
    cy.wait(1000)
    cy.visit('https://staging-professionals.vanoapp.com/appointments-v2')
  })

  it('should allow a user to create an appointment', () => {
    cy.get('#calendar').within(() => {
      cy.get('.vano-week-row').last().click()
    })
    cy.title().should('eq', 'New Appointment | VANO')
    cy.get('vano-input')
  })
})
