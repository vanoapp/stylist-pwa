describe('Reports Page', () => {
  before(() => {
    cy.visit('https://staging-professionals.vanoapp.com/login')

    cy.get('form').within(() => {
      cy.get('input[type="email"]').type('demo@email.com')
      cy.get('input[type="password"]').type('password')
      cy.root().submit()
    })
    cy.wait(1000)
    cy.visit('https://staging-professionals.vanoapp.com/reports')
  })

  it('should display the analytics and work hours tabs', () => {
    cy.contains('Analytics')
    cy.contains('Work Hours')
  })
})
