
describe('Login', () => {
  it('should sucessfully log in', () => {
    cy.visit('https://staging-professionals.vanoapp.com/login')

    cy.get('form').within(() => {
      cy.get('input[type="email"]').type('demo@email.com')
      cy.get('input[type="password"]').type('password')
      cy.root().submit()
    })

    cy.title().should('eq', 'Dashboard | VANO')
  })

  it('should validate the email input', () => {
    cy.visit('https://staging-professionals.vanoapp.com/login')

    cy.get('form').within(() => {
      cy.get('input[type="password"]').type('password')
      cy.root().submit()
    })

    cy.contains('email or phone number cannot be blank.')
  })

  it('should validate the password input', () => {
    cy.visit('https://staging-professionals.vanoapp.com/login')

    cy.get('form').within(() => {
      cy.get('input[type="email"]').type('email@email.com')
      cy.root().submit()
    })

    cy.contains('password cannot be blank.')
  })

  it('should validate the credentials', () => {
    cy.visit('https://staging-professionals.vanoapp.com/login')

    cy.get('form').within(() => {
      cy.get('input[type="email"]').type('invalid@email.com')
      cy.get('input[type="password"]').type('invalid password')
      cy.root().submit()
    })

    cy.contains('invalid credentials')
  })
})

describe('Forgot Password', () => {
  it('should always return a success no matter what email is typed', () => {
    cy.visit('https://staging-professionals.vanoapp.com/forgot-password')
    cy.get('form').within(() => {
      cy.get('input[type="email"]').type('differentemail420@email.com')
      cy.root().submit()
    })
    cy.contains('Awesome!')
    cy.contains('OK').click()

    cy.get('form').within(() => {
      cy.get('input[type="email"]').type('differentemail@email.com')
      cy.root().submit()
    })
    cy.contains('Awesome!')
    cy.contains('OK').click()

    cy.get('form').within(() => {
      cy.get('input[type="email"]').type('differentemail1@email.com')
      cy.root().submit()
    })
    cy.contains('Awesome!')
    cy.contains('OK').click()

    cy.get('form').within(($form) => {
      cy.get('input[type="email"]').type('differentemail2@email.com')
      cy.root().submit()
    })
    cy.contains('Awesome!')
    cy.contains('OK').click()
  })

  it('should validate the email input', () => {
    cy.visit('https://staging-professionals.vanoapp.com/forgot-password')
    cy.get('form').within(() => {
      cy.root().submit()
    })
    cy.contains('email or phone number cannot be blank.')
  })
})



