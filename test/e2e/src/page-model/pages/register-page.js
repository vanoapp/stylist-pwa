import { Selector } from 'testcafe';
import aurelia from 'testcafe-aurelia-selectors';

export default {
    fullNameInput:    aurelia.byValueBind('fullName & validate'),
    emailInput:       aurelia.byValueBind('email & validate'),
    passwordInput:    aurelia.byValueBind('password & validate'),
    companyNameInput: aurelia.byValueBind('name & validate'),
    phoneNumberInput: aurelia.byValueBind('phoneNumber & validate'),
    addressInput:     aurelia.byValueBind('address & validate'),
    cityInput:        aurelia.byValueBind('city & validate'),
    zipInput:         aurelia.byValueBind('zip & validate'),
    stateSelect:      aurelia.byValueBind('state & validate'),

    startBtn:         Selector('button').withText('Start Registration'),
    completeBtn:      Selector('button').withText('Complete Registration!'),
    goBackLink:       Selector('.prev-step').find('a')
};