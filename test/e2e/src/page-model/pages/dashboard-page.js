import { Selector, t } from 'testcafe';

class Stats {
    constructor(text) {
        this.box = Selector('.stat-box').withText(text);
    }

    async getValue () {
        await t.expect(this.box.visible).ok();

        return (await this.box.find('.stat-number').textContent).trim();
    }
}

export default {
    stats: {
        newCustomers: new Stats('New customers this month!')
    },

    appointment: Selector('panel[title="Appointments"]').find('.panel')
}
