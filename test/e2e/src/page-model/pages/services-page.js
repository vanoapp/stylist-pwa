import { Selector } from 'testcafe';
import aurelia from 'testcafe-aurelia-selectors';

class ServiceItem {
    constructor(name) {
        this.el = aurelia.byClickDelegate('showUpdateService(s.id)').withText(name);
        this.title = this.el.find('.title');
        this.length = this.el.find('.length').child('span');
        this.description = this.el.find('.description');
    }
}

export default class ServicesPage {
    constructor() {
        this.addService = aurelia.byClickDelegate('showCreateService()');
        this.services = aurelia.byClickDelegate('showUpdateService(s.id)');

        this.updatePanel = Selector('.add-service-component,.update-service-component').find('.panel');
        this.panelTitle = this.updatePanel.find('h1.panel-title');

        this.createInputs = {
            name: aurelia.byValueBind('name & validate', this.updatePanel),
            description: aurelia.byValueBind('description & validate', this.updatePanel),
            length: aurelia.byValueBind('length & validate', this.updatePanel),
            price: aurelia.byValueBind('price & validate', this.updatePanel)
        };

        this.updateInputs = {
            name: aurelia.byValueBind('service.name', this.updatePanel),
            description: aurelia.byValueBind('service.description', this.updatePanel),
            length: aurelia.byValueBind('service.length', this.updatePanel),
            price: aurelia.byValueBind('service.price', this.updatePanel)
        };

        this.submitBtn = this.updatePanel.find('button[type="submit"]');
        this.deleteBtn = this.updatePanel.find('button').withText('Delete');
        this.closeBtn = this.updatePanel.find('.panel-heading .right-header');
    }

    getItem(name) {
        return new ServiceItem(name);
    }
}
