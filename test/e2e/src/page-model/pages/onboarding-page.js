import { Selector } from 'testcafe';
import aurelia from 'testcafe-aurelia-selectors';

export default {
    header:        Selector('h1'),
    getStartedBtn: Selector('button').withText('Get Started'),
    nextBtn:       aurelia.byClickDelegate('next()'),
    prevBtn:       aurelia.byClickDelegate('prev()'),
    completeBtn:   aurelia.byClickDelegate("complete('dashboard')"),

    service: function (nth) {
        const el = aurelia.byClickDelegate('toggleService(s)').nth(nth);
        
        return {
            el: el,
            value: el.textContent,
            disabled: el.hasClass('disbaled')
        }
    }
}