import { Selector } from 'testcafe';
import aurelia from 'testcafe-aurelia-selectors';

export default {
    registerNowLink: Selector('a').withText('Register Now'),
    authenticatorInput: aurelia.byValueBind('authenticator & validate'),
    passwordInput: aurelia.byValueBind('password & validate'),
    errMsgInput: aurelia.byValueBind('errMsg & validate'),
    loginBtn: Selector('button').withText('Login'),
    showPasswordCheckBox: aurelia.byCheckedBind('showPwdInputTextType')
};