import { Selector, t } from 'testcafe';
import aurelia from 'testcafe-aurelia-selectors';
import Alert from '../components/alert';

const alert = new Alert();

const newClientForm = {
    firstNameInput: aurelia.byValueBind('firstName & validate'),
    lastNameInput: aurelia.byValueBind('lastName & validate'),
    emailInput: aurelia.byValueBind('email & validate'),
    phoneInput: aurelia.byValueBind('phoneNumber & validate'),
    submitBtn: Selector('button').withText('SUBMIT')
}

class Note {
    constructor(el) {
        this.el = el;

        this.editIcon = aurelia.byClickDelegate('edit(n, true)', this.el);

        this.updateForm = Selector('.update-note-form');
        this.editInput = aurelia.byValueBind('n.value', this.updateForm);
        this.submitBtn = this.updateForm.find('button').withAttribute('type', 'submit');
        this.cancelBtn = aurelia.byClickDelegate('edit(n, false)', this.updateForm);
        this.deleteBtn = aurelia.byClickDelegate('delete(n)', this.updateForm);
    }

    getText() {
        return this.el.find('p').textContent;
    }
}

class Client {
    constructor(fullName) {
        this.item = Selector('.members-list').find('.item').find('.full-name').withText(fullName);

        this.body = Selector('.panel-body');
        this.fullNameTitle = this.body.find('.full-name');

        this.customerInfo = this.body.find('.customer-info');
        this.customerInfoItem = this.customerInfo.find('.item');

        this.fullName = this.customerInfoItem.nth(0);
        this.phone = this.customerInfoItem.nth(1);
        this.email = this.customerInfoItem.nth(2);

        this.tabs = {
            notes: aurelia.byClickDelegate("routeTo('customer-notes')")
        };

        this.notesContainer = Selector('.notes');
        this.notes = this.notesContainer.find('.note');
        this.noteInput = aurelia.byValueBind('note');
    }

    get visible() {
        return this.item.visible;
    }

    async closeInfo() {
        // for mobile view
        const closeBtn = Selector('.mobile-click-icon');

        if (await closeBtn.exists)
            await t.click(closeBtn);
    }

    async deleteAllNotes() {
        let firstNote = new Note(this.notes.nth(0));
        let firstNoteExists = await firstNote.el.exists;

        while (firstNoteExists) {
            await t
                .click(firstNote.editIcon)
                .click(firstNote.deleteBtn)
                .wait(500);
                
            await alert.checkAndClose(null, null, { isWarning: true });
            await alert.checkAndClose(null, null, { isSuccess: true });

            firstNoteExists = await firstNote.el.exists;
        }
    }

    note(text) {
        return new Note(this.notes.withText(text));
    }
}

export default class ClientsPage {
    constructor() {
        this.addBtn = Selector('div.add-new-wrapper').withText('ADD NEW CLIENT');
        this.newClientForm = newClientForm;
        this.searchInput = aurelia.byValueBind('searchQuery');
        this.clientItems = Selector('.members-list').find('.item');
    }

    getClient(fullName) {
        return new Client(fullName);
    }
}
