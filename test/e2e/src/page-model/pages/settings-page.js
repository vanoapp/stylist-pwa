import { Selector } from 'testcafe';
import aurelia from 'testcafe-aurelia-selectors';

export default {
    firstNameInput: aurelia.byValueBind('authProfessional.first_name | capitalize'),
    lastNameInput: aurelia.byValueBind('authProfessional.last_name | capitalize'),
    emailInput: aurelia.byValueBind('authProfessional.email'),
    phoneInput: aurelia.byValueBind('authProfessional.phone_number'),
    companyInput: aurelia.byValueBind('company.name'),
    addressInput: aurelia.byValueBind('company.address_line1'),
    cityInput: aurelia.byValueBind('company.city'),
    stateSelect: aurelia.byValueBind('company.state'),
    zipInput: aurelia.byValueBind('company.zip_code'),

    title: name => Selector('.titles .list-titles').find('a').find('.title').withText(name)
}
