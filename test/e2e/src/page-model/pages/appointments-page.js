import { Selector, t } from 'testcafe';
import aurelia from 'testcafe-aurelia-selectors';

class Calendar {
    constructor() {
        this.el = Selector('#calendar');
        this.today = this.el.find('.fc-day.fc-today');
        this.goBack = aurelia.byClickDelegate('prev()');
        this.goForward = aurelia.byClickDelegate('next()');
        this.threeDay = aurelia.byClickDelegate('showThreeDay()');

        this.threeDayView = {
            row: time => Selector(`tr[data-time="${time}"]`)
        }

    }

    getDayDate(day) {
        return day.getAttribute('data-date');
    }

    async openDay(day) {
        await t
            .click(this.today, { offsetY: -10 })
            .wait(500); // wait for animation
    }
}

class SelectedService {
    constructor(el) {
        this.el = el;
        this.name = el.find('.name');
        this.length = el.find('.length .number-text');
        this.price = el.find('.price');
        this.remove = el.find('.remove');
    }
}

class UpdatePanel {
    constructor() {
        this.el = Selector('.add-appointment-component,.update-appointment-component').find('.panel');
        this.title = this.el.find('h1.panel-title');

        this.clientDropdown = Selector('.customer-dropdown-component');
        this.serviceDropdown = Selector('.service-dropdown-component');

        this.createDateInput = aurelia.byValueBind('date & validate');
        this.notes = aurelia.byValueBind('notes');

        this.submitBtn = this.el.find('button[type="submit"]');
        this.closeBtn = this.el.find('.panel-heading .right-header');
        this.cancelBtn = this.el.find('button').withAttribute('click.trigger', 'cancelAppointment()');

        this.selectedService = idx => new SelectedService(Selector('.selected-services .service').nth(idx));
        this.selectedClient = this.clientDropdown.find('.select2-selection__rendered');

        this.openTimeSelector = aurelia.byClickDelegate('toggleMenu(h)');
    }

    async setTime(hour, time) {
        await t
            .click(this.openTimeSelector.withText(hour))
            .click(aurelia.byClickDelegate('setSelectedTime(t, h)').withText(time));
    }
}


export default class AppointmentsPage {
    constructor() {
        this.calendar = new Calendar();
        this.updatePanel = new UpdatePanel();
        this.appointment = Selector('.fc-event');
    }
}