import { Selector, t } from 'testcafe';

export default class Alert {
    constructor() {
        this.mainElement = Selector('.sweet-alert');
        this.title = this.mainElement.child('h2');
        this.message = this.mainElement.child('p');
        this.okBtn = this.mainElement.find('button.confirm');
        this.cancelBtn = this.mainElement.find('button.cancel');
    }

    async checkAndClose(title, message, { isWarning, isSuccess, isError } = {}, cancel) {
        await t.expect(this.mainElement.visible).ok();

        if (title)
            await t.expect(this.title.textContent).eql(title);

        if (message)
            await t.expect(this.message.textContent).contains(message);

        if (isWarning)
            await t.expect(this.mainElement.find('.sa-icon.sa-warning').visible).ok();

        if (isSuccess)
            await t.expect(this.mainElement.find('.sa-icon.sa-success').visible).ok();

        if (isError)
            await t.expect(this.mainElement.find('.sa-icon.sa-error').visible).ok();

        if (cancel)
            await t.click(this.cancelBtn)
        else
            await t.click(this.okBtn);
    }
}