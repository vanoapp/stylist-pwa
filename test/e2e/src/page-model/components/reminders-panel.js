import { Selector, t } from 'testcafe';
import aurelia from 'testcafe-aurelia-selectors';

class Reminder {
    constructor(el) {
        this.text = el.find('td').nth(0);
        this.complete = aurelia.byClickDelegate('completeReminder(r)', el);
        this.uncomplete = aurelia.byClickDelegate('uncompleteReminder(r)', el);
    }

    completed () {
        return this.text.hasClass('completed');
    }

    static async getUncompletedRemindersCount() {
        return await aurelia.byClickDelegate('completeReminder(r)').count;
    }

    static async completeAllReminders() {
        let uncompletedRemindersCount = await Reminder.getUncompletedRemindersCount();
        
        if (!uncompletedRemindersCount)
            return false;

        while (uncompletedRemindersCount--)
            await t.click(aurelia.byClickDelegate('completeReminder(r)'));

        return true;
    }
}

export default class RemindersPanel {
    constructor() {
        this.panel = Selector('panel').withAttribute('title', 'Reminders').child('.panel');
        this.body = this.panel.find('.panel-body');
        this.addRow = this.body.find('.add-row');
        this.reminders = this.body.find('tr').filter((el, idx) => idx !== 0);

        this.addInput = aurelia.byValueBind('reminderText & validate', this.addRow);
        this.errorMsg = this.addInput.parent(0).find('span.danger');
    }

    reminder(nth) {
        return new Reminder(this.body.find('tr').nth(nth + 1));
    }

    static async completeAllReminders() {
        return await Reminder.completeAllReminders();
    }
}