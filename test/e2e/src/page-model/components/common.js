import { Selector } from 'testcafe';

export function inputMessageBox(input) {
    return input.parent(0).find('span.danger');
}

export function inputMessageBoxVano(input) {
    return input.parent('.vano-input-group').find('span.err-msg');
}

