import { Selector } from 'testcafe';

export default class Toast {
    constructor () {
        this.mainElement = Selector('.toast');
        this.success = this.mainElement.hasClass('toast-success');
        this.message = this.mainElement.find('.toast-message');
    }
}