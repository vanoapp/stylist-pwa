import { Selector, t } from 'testcafe';
import aurelia from 'testcafe-aurelia-selectors';
import days from '../../helpers/days';

class Hour {
    constructor(mainElement, isOnboardingPage) {
        this.mainElement = mainElement;
        this.day = this.mainElement.find('.day');
        this.fullDay = null;
        this.isOnboardingPage = isOnboardingPage;
    }

    async _fillFullDay() {
        if (!this.fullDay) {
            const dayShort = await this.day.textContent;

            this.fullDay = days[dayShort];
        }

        return this.fullDay;
    }

    _getOpenInput() {
        // NOTE: ensure that this.fullDay is set before call this
        return aurelia.byValueBind(`${this.isOnboardingPage ? '' : 'company.'}hours.${this.fullDay}.open`);
    }

    _getCloseInput() {
        // NOTE: ensure that this.fullDay is set before call this
        return aurelia.byValueBind(`${this.isOnboardingPage ? '' : 'company.'}hours.${this.fullDay}.close`);
    }

    _getOpenedCheckBox() {
        // NOTE: ensure that this.fullDay is set before call this
        return aurelia.byCheckedBind(`${this.isOnboardingPage ? '' : 'company.'}hours.${this.fullDay}.operating`);
    }

    async getData() {
        await this._fillFullDay();

        return {
            open: this._getOpenInput().value,
            close: this._getCloseInput().value,
            opened: this._getOpenedCheckBox().checked
        };
    }

    async setData(open, close, opened) {
        await this._fillFullDay();

        const data = await this.getData();

        if (open !== null && data.open !== open)
            await t.typeText(this._getOpenInput(), open);

        if (close !== null && data.close !== close)
            await t.typeText(this._getCloseInput(), close)

        if (opened !== null && data.opened !== opened)
            await t.click(this._getOpenedCheckBox().parent('.checkbox-slide'));
    }
}

export default class Hours {
    constructor(isOnboardingPage) {
        this.hours = Selector('div.hours');
        this.isOnboardingPage = isOnboardingPage;
    }

    getHourByDay(day) {
        const el = this.hours.find('div.day').withText(day).parent('.hour-wrapper');

        return new Hour(el, this.isOnboardingPage);
    }

    getUncheckedCheckboxes() {
        return Selector('.checkbox-slide').child('input').filter(el => !el.checked).parent(0).find('label');
    }

    getCheckedCheckboxes() {
        return Selector('.checkbox-slide').child('input').filter(el => el.checked).parent(0).find('label');
    }
}
