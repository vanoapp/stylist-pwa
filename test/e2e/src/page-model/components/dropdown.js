import { Selector } from 'testcafe';

export default class Dropdown {
    constructor() {
        this.options = Selector('.select2-results').find('li');
    }
}