import { Selector, t } from 'testcafe';
import aurelia from 'testcafe-aurelia-selectors';

export default class Sidebar {
    constructor() {
        this.mainElement = Selector('.sb-sidebar');
        this.item = name => this.mainElement.find('.sb-item').withText(name);
        this.toggleMenu = aurelia.byClickDelegate('toggleMenu()');
    }

    expanded () {
        return this.mainElement.visible;
    }

    async expand() {
        if (!(await this.expanded())) {
            await t.click(this.toggleMenu);
        }
    }

    async swithItem (itemName) {
        await this.expand();
        await t.click(this.item(itemName));
    }
}
