// !IMPORTANT: all days should be available for appoitments creating when tests are started.

import { baseUrl } from './config.json';
import ServicesPage from './page-model/pages/services-page';
import ClientsPage from './page-model/pages/clients-page';
import AppointmentsPage from './page-model/pages/appointments-page';
import dashboardPage from './page-model/pages/dashboard-page';
import { inputMessageBox } from './page-model/components/common'
import Sidebar from './page-model/components/sidebar';
import Alert from './page-model/components/alert';
import Dropdown from './page-model/components/dropdown';
import login from './helpers/login';
import { enableAll, disableAll } from './helpers/set-working-days';

const sidebar = new Sidebar();
const alert = new Alert();
const dropdown = new Dropdown();
const servicesPage = new ServicesPage();
const clientsPage = new ClientsPage();
const appointmentsPage = new AppointmentsPage();
const { calendar, updatePanel } = appointmentsPage;

fixture`Appointments`
    .page`${baseUrl}`
    .beforeEach(async t => {
        await login();

        await sidebar.swithItem('Appointments');
        await t.expect(appointmentsPage.calendar.today.visible).ok();
    });

test('Validate', async t => {
    // Collect available services and clients
    await sidebar.swithItem('Services');
    await t.expect(servicesPage.services.count).gt(0);

    const servicesCount = await servicesPage.services.count;

    await sidebar.swithItem('Clients');
    await t.expect(clientsPage.clientItems.count).gt(0);

    const clientsCount = await clientsPage.clientItems.count;

    await sidebar.swithItem('Appointments');

    const todayDate = await calendar.getDayDate(calendar.today);

    // Validate fields
    await calendar.openDay(calendar.today);
    await t
        .expect(updatePanel.el.visible).ok()
        .click(updatePanel.submitBtn)
        .expect(inputMessageBox(updatePanel.clientDropdown.parent(0)).textContent).contains('Customer is required')
        .expect(inputMessageBox(updatePanel.serviceDropdown.parent(0)).textContent).contains('Service is required')
        .click(updatePanel.closeBtn)
        .expect(updatePanel.el.exists).notOk();

    await calendar.openDay(calendar.today);
    await t.expect(updatePanel.el.visible).ok()

        // Check all clients and services are availabe
        .click(updatePanel.clientDropdown)
        .expect(dropdown.options.count).eql(clientsCount)
        .click(updatePanel.clientDropdown)
        .click(updatePanel.serviceDropdown)
        .expect(dropdown.options.count).eql(servicesCount)
        .click(updatePanel.serviceDropdown)
        .expect(updatePanel.createDateInput.value).eql(todayDate);
});

test('Check unavailable days', async t => {
    // Check the past
    await t
        .click(calendar.goBack)
        .click(calendar.el);

    await alert.checkAndClose('Silly You!', "you can't book an appointment for a day that already passed.", { isError: true });

    // Check weekends
    await disableAll('Appointments');
    await t
        .click(calendar.goForward)
        .click(calendar.el);

    await alert.checkAndClose('Bummer!', "Salon is closed on ", { isError: true });
}).after(async () => {
    await enableAll();
});

test('Create an appointment', async t => {
    const customer = 'Albert Last';
    const service = {
        name: 'One Hour',
        length: '60',
        price: '100'
    };

    // TODO: remove all appointments in this view before test

    // There are no Month view in mobile
    await t
        .click(calendar.threeDay)
        .click(calendar.threeDayView.row('09:00:00'), { offsetX: -50 })
        .wait(500)  // Wait for animation
        .click(updatePanel.clientDropdown)
        .click(dropdown.options.withText(customer))
        .click(updatePanel.serviceDropdown)
        .click(dropdown.options.withText(service.name));

    const firstSelectedService = updatePanel.selectedService(0);
    const secondSelectedService = updatePanel.selectedService(1);

    await t
        .expect(firstSelectedService.el.visible).ok()
        .expect(secondSelectedService.el.exists).notOk()
        .expect(firstSelectedService.name.textContent).eql(service.name)
        .expect(firstSelectedService.length.textContent).eql(service.length)
        .expect(firstSelectedService.price.textContent).contains(service.price)
        .click(updatePanel.serviceDropdown)
        .click(dropdown.options.withText(service.name))
        .expect(secondSelectedService.el.visible).ok()
        .click(secondSelectedService.remove)
        .wait(500) //wait for animation
        .expect(secondSelectedService.el.exists).notOk();

    const selectedClient = await updatePanel.selectedClient.textContent;
    const selectedClientShort = selectedClient.split(' ')[0] + ' ' + selectedClient.split(' ')[1][0] + '.';

    // BUG: works wrong for other timezones
    await updatePanel.setTime('9am', '9:00am');
    await t.click(updatePanel.submitBtn);

    // BUG: can be failed because it seems removed appointments still block this time
    await alert.checkAndClose('Awesome!', "Appointment was created!", { isSuccess: true });

    const createdAppointment = appointmentsPage.appointment.withText('Albert L.');
    await t
        .expect(createdAppointment.visible).ok()
        .expect(createdAppointment.textContent).contains('12:30');

    // Check on dashboard
    await sidebar.swithItem('Dashboard');
    await t
        .expect(dashboardPage.appointment.withText(customer).visible).ok()
        .expect(dashboardPage.appointment.withText(service.name).visible).ok()
        .click(dashboardPage.appointment.withText(customer).find('a').withText('View'))
        .expect(updatePanel.selectedClient.textContent).eql(customer)
        .click(updatePanel.cancelBtn);

    await alert.checkAndClose('Are you sure?', 'You will not be able to recover this appointment', { isWarning: true });
    await alert.checkAndClose('Cancelled!', 'Your appointment has been cancelled.', { isSuccess: true });

    await t
        .click(calendar.threeDay)
        .expect(createdAppointment.exists).notOk();
});
