import { baseUrl } from './config.json';
import ServicesPage from './page-model/pages/services-page';
import { inputMessageBox } from './page-model/components/common'
import Sidebar from './page-model/components/sidebar';
import Alert from './page-model/components/alert';
import login from './helpers/login';

const sidebar = new Sidebar();
const alert = new Alert();
const servicesPage = new ServicesPage();

fixture`Services`
    .page`${baseUrl}`
    .beforeEach(async () => {
        await login();
        await sidebar.swithItem('Services');
    });

test('Validation', async t => {
    await t
        .click(servicesPage.addService)
        .expect(servicesPage.updatePanel.visible).ok()
        .wait(500) // wait for animation
        .click(servicesPage.closeBtn)
        .expect(servicesPage.updatePanel.exists).notOk()
        .click(servicesPage.addService)
        .wait(500)  // wait for animation
        .click(servicesPage.submitBtn)
        .expect(inputMessageBox(servicesPage.createInputs.name).textContent).contains('name cannot be blank')
        .expect(inputMessageBox(servicesPage.createInputs.description).textContent).contains('description cannot be blank')
        .expect(inputMessageBox(servicesPage.createInputs.price).textContent).contains('price is required')
        .expect(inputMessageBox(servicesPage.createInputs.length).textContent).contains('length cannot be blank')
        .typeText(servicesPage.createInputs.name, 'name')
        .typeText(servicesPage.createInputs.description, 'description')
        .typeText(servicesPage.createInputs.price, '1.00')
        .typeText(servicesPage.createInputs.length, 'a')
        .click(servicesPage.createInputs.price)
        .click(servicesPage.submitBtn);

    await alert.checkAndClose('Oops!', 'length is required', { isError: true });
});

test('Manage services', async t => {
    const id = Date.now();
    const serviceData = {
        name: `S-${id}`,
        description: `d-${id}`,
        price: '10.00',
        length: '90'
    };

    const createdService = servicesPage.getItem(serviceData.name);

    // Create
    await t
        .expect(createdService.el.exists).notOk()
        .click(servicesPage.addService)
        .expect(servicesPage.panelTitle.textContent).contains('Add Service')
        .typeText(servicesPage.createInputs.name, serviceData.name)
        .typeText(servicesPage.createInputs.description, serviceData.description)
        .typeText(servicesPage.createInputs.price, serviceData.price)
        .typeText(servicesPage.createInputs.length, serviceData.length)
        .click(servicesPage.createInputs.price)
        .click(servicesPage.submitBtn);

    await alert.checkAndClose('Awesome!', 'Service was created!', { isSuccess: true });
    await t
        .expect(createdService.el.exists).ok()
        .expect(createdService.description.textContent).contains(serviceData.description)
        .expect(createdService.length.textContent).contains(serviceData.length);

    // Update
    let updatedServiceData = {
        name: 'U' + serviceData.name,
        description: 'u' + serviceData.description,
        length: '1' + serviceData.length,
        price: serviceData.price
    };

    await t
        .click(createdService.el)
        .expect(servicesPage.panelTitle.textContent).contains(`Update ${serviceData.name}`)
        .typeText(servicesPage.updateInputs.name, serviceData.name, { replace: true })
        .typeText(servicesPage.updateInputs.description, serviceData.description, { replace: true })
        .typeText(servicesPage.updateInputs.length, serviceData.length, { replace: true })
        .click(servicesPage.submitBtn);

    await alert.checkAndClose('Awesome!', 'Service was updated!', { isSuccess: true });

    const updatedService = servicesPage.getItem(serviceData.name);

    await t
        .expect(updatedService.el.exists).ok()
        .expect(updatedService.description.textContent).contains(serviceData.description)
        .expect(updatedService.length.textContent).contains(serviceData.length);

    // Delete
    await t
        .click(updatedService.el)
        .click(servicesPage.deleteBtn)
        .wait(500);

    await alert.checkAndClose('Are you sure?', 'You will not be able to recover this service', { isWarning: true });

    // BUG: It seems it should not be `Your service has beed deleted` instead of `appoitment`
    await alert.checkAndClose('Deleted!', 'Your appointment has been deleted.', { isSuccess: true });
    await t.expect(updatedService.el.exists).notOk();
});