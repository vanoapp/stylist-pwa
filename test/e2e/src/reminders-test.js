import { t } from 'testcafe';
import { baseUrl } from './config.json';
import login from './helpers/login';
import RemindersPanel from './page-model/components/reminders-panel';
import Sidebar from './page-model/components/sidebar';

const remindersPanel = new RemindersPanel();
const firstReminder = remindersPanel.reminder(0);
const secondReminder = remindersPanel.reminder(1);

const sidebar = new Sidebar();

async function refreshReminders() {
    await sidebar.swithItem('Services');
    await sidebar.swithItem('Dashboard');
    await t.expect(remindersPanel.panel.visible).ok();
}

async function clearReminders() {
    await t.expect(remindersPanel.panel.visible).ok();

    // Clean all reminders from prev runs
    let counter = 20;

    while (counter-- && await RemindersPanel.completeAllReminders())
        await refreshReminders();

    await t.expect(remindersPanel.reminders.count).eql(0, "Can't clean reminders");
}

fixture`Reminders`
    .page(baseUrl)
    .beforeEach(async t => {
        await login();
        await clearReminders();
    });

test('Manage reminders', async t => {
    const firstText = Date.now().toString();
    const secondText = 's-' + firstText;

    await t

        // Check validation
        .click(remindersPanel.addInput)
        .pressKey('enter')
        .expect(remindersPanel.errorMsg.visible).ok()
        .expect(remindersPanel.errorMsg.textContent).contains('reminder is required')

        // Create a reminder
        .typeText(remindersPanel.addInput, firstText)
        .pressKey('enter')
        .expect(remindersPanel.reminders.count).eql(1)
        .expect(firstReminder.text.textContent).eql(firstText)
        .expect(firstReminder.completed()).eql(false)

        // Check complete/uncomplete button
        .click(firstReminder.complete)
        .expect(firstReminder.completed()).eql(true)
        .click(firstReminder.uncomplete)
        .expect(firstReminder.completed()).eql(false)
        .click(firstReminder.complete)

        // Create one more reminder
        .typeText(remindersPanel.addInput, secondText)
        .pressKey('enter')
        .expect(remindersPanel.reminders.count).eql(2)
        .expect(secondReminder.text.textContent).eql(secondText);

    // Check completed reminders are remove after reload
    await refreshReminders();
    await t.expect(remindersPanel.reminders.count).eql(1)
        .expect(firstReminder.text.textContent).eql(secondText)
        .expect(firstReminder.completed()).eql(false)
        .click(firstReminder.complete);
});
