import { ClientFunction } from 'testcafe';
import { baseUrl } from './config.json';
import credentials from './data/credentials.json';

import loginPage from './page-model/pages/login-page';
import { inputMessageBoxVano } from './page-model/components/common'
import Sidebar from './page-model/components/sidebar';
import Toast from './page-model/components/toast';

import login from './helpers/login';
import getLocation from './helpers/get-location';

fixture`Login`.page`${baseUrl}/login`;

test('Validation', async t => {
    await t
        // check empty fields
        .click(loginPage.loginBtn)
        .expect(inputMessageBoxVano(loginPage.authenticatorInput).textContent).contains('email or phone number cannot be blank')
        .expect(inputMessageBoxVano(loginPage.authenticatorInput).visible).ok()
        .expect(inputMessageBoxVano(loginPage.passwordInput).textContent).contains('password cannot be blank')
        .expect(inputMessageBoxVano(loginPage.passwordInput).visible).ok()

        // check incorrect arguments
        .typeText(loginPage.authenticatorInput, '1')
        .typeText(loginPage.passwordInput, '1')
        .pressKey('enter')

        .expect(inputMessageBoxVano(loginPage.authenticatorInput).visible).notOk()
        .expect(inputMessageBoxVano(loginPage.passwordInput).visible).notOk()
        .expect(inputMessageBoxVano(loginPage.errMsgInput).textContent).contains('invalid credentials')
        .expect(inputMessageBoxVano(loginPage.errMsgInput).visible).ok()

        // check `Show password` checkbox
        .expect(loginPage.passwordInput.getAttribute('type')).eql('password')
        .click(loginPage.showPasswordCheckBox)
        .expect(loginPage.passwordInput.getAttribute('type')).eql('text')
        .click(loginPage.showPasswordCheckBox);
});

test('Login and logout', async t => {
    const sidebar = new Sidebar();
    const toast = new Toast();

    await login();
    await t
        .expect(toast.success).ok()
        .expect(toast.message.textContent).eql('Successfully logged in!')
        .expect(getLocation()).contains('dashboard');
    
    await sidebar.expand();
    await t
        .click(sidebar.item('Logout'))
        .expect(toast.success).ok()
        .expect(toast.message.textContent).eql('Successfully logged out!')
        .expect(getLocation()).contains('login');

    await login(true);
    await t
        .expect(getLocation()).contains('dashboard');

    // Check sidebar
    const sidebarExpanded = await sidebar.expanded();

    await t
        .click(sidebar.toggleMenu)
        .expect(sidebar.expanded()).notEql(sidebarExpanded);
});