import { Selector } from 'testcafe';
import { baseUrl } from './config.json';

import dashboardPage from './page-model/pages/dashboard-page';
import ClientsPage from './page-model/pages/clients-page';
import Sidebar from './page-model/components/sidebar';
import Alert from './page-model/components/alert';
import login from './helpers/login';
import { inputMessageBox } from './page-model/components/common'

const sidebar = new Sidebar();
const alert = new Alert();
const clientsPage = new ClientsPage();
const newClientForm = clientsPage.newClientForm;

fixture`Clients`
    .page(baseUrl)
    .beforeEach(login);

test('Create new client', async t => {
    const custId = Date.now().toString();
    const customerInfo = {
        firstName: 'Testcafe',
        lastName: `Cust-${custId}`,
        email: `cust-${custId}@mail.com`,
        phone: custId.substring(3)
    };

    await sidebar.swithItem('Clients');
    await t
        .click(clientsPage.addBtn)

        // New client form
        // - validate
        .click(newClientForm.submitBtn)
        .expect(inputMessageBox(newClientForm.firstNameInput).textContent).contains('first name cannot be blank')
        .expect(inputMessageBox(newClientForm.firstNameInput).visible).ok()
        .expect(inputMessageBox(newClientForm.lastNameInput).textContent).contains('last name cannot be blank')
        .expect(inputMessageBox(newClientForm.lastNameInput).visible).ok()
        .expect(inputMessageBox(newClientForm.phoneInput).textContent).contains('phone number cannot be blank')
        .expect(inputMessageBox(newClientForm.phoneInput).visible).ok()
        .typeText(newClientForm.phoneInput, '1')
        .pressKey('tab')
        .expect(inputMessageBox(newClientForm.phoneInput).textContent).contains('phone number is invalid')
        .expect(inputMessageBox(newClientForm.phoneInput).visible).ok()

        // - fill
        .typeText(newClientForm.firstNameInput, customerInfo.firstName)
        .typeText(newClientForm.lastNameInput, customerInfo.lastName)
        .typeText(newClientForm.emailInput, customerInfo.email)
        .typeText(newClientForm.phoneInput, customerInfo.phone, { replace: true })
        .click(newClientForm.submitBtn);

    await alert.checkAndClose('Awesome!', 'Client was created!')

    // Check clients list
    await sidebar.swithItem('Clients');

    const clientFullName = `${customerInfo.firstName} ${customerInfo.lastName}`;
    const phoneParts = [
        customerInfo.phone.substring(0, 3),
        customerInfo.phone.substring(3, 6),
        customerInfo.phone.substring(6),
    ];

    const client = clientsPage.getClient(clientFullName);

    await t
        .click(client.item)
        .expect(client.fullNameTitle.textContent).contains(clientFullName)
        .expect(client.fullName.textContent).contains(clientFullName)
        .expect(client.email.textContent).contains(customerInfo.email)
        .expect(client.phone.textContent).contains(`(${phoneParts[0]}) ${phoneParts[1]}-${phoneParts[2]}`);

    await client.closeInfo();

    // Search
    await t
        .typeText(clientsPage.searchInput, customerInfo.lastName)
        .expect(client.visible).ok()
        .typeText(clientsPage.searchInput, 'u', { caretPos: 0 })
        .expect(client.item.exists).notOk()
        .typeText(clientsPage.searchInput, customerInfo.lastName, { replace: true })
        .expect(client.visible).ok();
});

test('Check client notes', async t => {
    // This test can fail because of the found bug: https://www.screencast.com/t/9Nu0fP9vgH
    await sidebar.swithItem('Clients');

    const firstClientName = (await clientsPage.clientItems.nth(0).textContent).trim();
    const client = clientsPage.getClient(firstClientName);

    let firstNoteText = `note-${Date.now()}`;
    const secondNoteText = `note-${Date.now() + 1}`;

    // Go to the Notes tab
    await t
        .click(client.item)
        .click(client.tabs.notes)
        .expect(client.notesContainer.exists).ok();

    // Clear notes
    await client.deleteAllNotes();

    // Create two notes
    await t
        .typeText(client.noteInput, firstNoteText)
        .pressKey('enter');

    await alert.checkAndClose('Awesome!', `${firstClientName}'s note was created!`, { isSuccess: true });
    await t
        .typeText(client.noteInput, secondNoteText)
        .pressKey('enter');

    await alert.checkAndClose('Awesome!', `${firstClientName}'s note was created!`, { isSuccess: true });

    let firstNote = client.note(firstNoteText);
    let secondNote = client.note(secondNoteText);

    // Modify note
    await t
        .expect(firstNote.el.exists).ok()
        .expect(secondNote.el.exists).ok()
        .expect(firstNote.editInput.exists).notOk()
        .doubleClick(firstNote.el)
        .expect(firstNote.editInput.exists).ok()
        .click(firstNote.cancelBtn)
        .expect(firstNote.editInput.exists).notOk()
        .click(firstNote.editIcon)
        .expect(firstNote.editInput.exists).ok();

    firstNoteText += ' - updated';

    await t
        .typeText(firstNote.editInput, firstNoteText, { replace: true })
        .click(firstNote.submitBtn);
    
    await alert.checkAndClose('Awesome!', `${firstClientName}'s note was updated!`, { isSuccess: true });

    firstNote = client.note(firstNoteText);

    // Remove notes
    await t
        .expect(firstNote.el.exists).ok()
        .click(firstNote.editIcon)
        .click(firstNote.deleteBtn)
        .wait(500);

    await alert.checkAndClose('Are you sure?', 'You will not be able to recover this note', { isWarning: true }, true);
    await t
        .click(firstNote.deleteBtn)
        .wait(500)
        .click(alert.okBtn);

    await alert.checkAndClose('Deleted!', 'Your note has been deleted.', { isSuccess: true });
    await t
        .expect(firstNote.el.exists).notOk()
        .click(secondNote.editIcon)
        .click(secondNote.deleteBtn)
        .wait(500)
        .click(alert.okBtn)
        .expect(secondNote.el.exists).notOk();
});