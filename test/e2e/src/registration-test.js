import { Role, t, ClientFunction } from 'testcafe';
import { baseUrl } from './config.json';
import generateCredentials from './data/generate-credentials';

import loginPage from './page-model/pages/login-page';
import registerPage from './page-model/pages/register-page';
import onboardingPage from './page-model/pages/onboarding-page';
import ServicesPage from './page-model/pages/services-page';
import settingsPage from './page-model/pages/settings-page';

import { inputMessageBoxVano } from './page-model/components/common';
import Hours from './page-model/components/hours';
import Sidebar from './page-model/components/sidebar';
import Alert from './page-model/components/alert';


const credentials = generateCredentials(Date.now());
const sidebar = new Sidebar();
const alert = new Alert();
const servicesPage = new ServicesPage();

fixture`Registration`.page`${baseUrl}`;

// TODO: const justRegisteredUser = Role(baseUrl, async t => { // (see explanation below)
async function registerUser() {
    await t
        .click(loginPage.registerNowLink)

        // fill the form and go ahead
        .typeText(registerPage.fullNameInput, credentials.fullName, { replace: true })
        .typeText(registerPage.emailInput, credentials.email, { replace: true })
        .typeText(registerPage.passwordInput, credentials.password, { replace: true })
        .click(registerPage.startBtn)

        // go back and check an input
        .click(registerPage.goBackLink)
        .expect(registerPage.fullNameInput.value).eql(credentials.fullName)
        .click(registerPage.startBtn)

        // fill the form and go ahead
        .typeText(registerPage.companyNameInput, credentials.companyName)
        .typeText(registerPage.phoneNumberInput, credentials.phoneNumber, { replace: true })
        .typeText(registerPage.addressInput, credentials.address)
        .typeText(registerPage.cityInput, credentials.city)
        .typeText(registerPage.zipInput, credentials.zip)
        .click(registerPage.stateSelect)
        .click(registerPage.stateSelect.find('option').withText(credentials.state))
        .click(registerPage.completeBtn);
};

test('Validation', async t => {
    await t
        .click(loginPage.registerNowLink)

        // check validation
        .click(registerPage.startBtn)
        .expect(inputMessageBoxVano(registerPage.fullNameInput).textContent).contains('full name cannot be blank')
        .expect(inputMessageBoxVano(registerPage.emailInput).textContent).contains('email address cannot be blank')
        .expect(inputMessageBoxVano(registerPage.passwordInput).textContent).contains('password cannot be blank')

        .typeText(registerPage.fullNameInput, 'name')
        .pressKey('tab')
        .expect(inputMessageBoxVano(registerPage.fullNameInput).textContent).contains('please provide a full name')

        .typeText(registerPage.emailInput, 'email')
        .pressKey('tab')
        .expect(inputMessageBoxVano(registerPage.emailInput).textContent).contains('email address is invalid')

        .typeText(registerPage.passwordInput, 'ppp')
        .pressKey('tab')
        .expect(inputMessageBoxVano(registerPage.passwordInput).textContent).contains('password must be at least 6 characters')

        // fill the form and go ahead
        .typeText(registerPage.fullNameInput, credentials.fullName, { replace: true })
        .typeText(registerPage.emailInput, credentials.email, { replace: true })
        .typeText(registerPage.passwordInput, credentials.password, { replace: true })
        .click(registerPage.startBtn)

        // check validation
        .click(registerPage.completeBtn)
        .expect(inputMessageBoxVano(registerPage.companyNameInput).textContent).contains('company name cannot be blank')
        .expect(inputMessageBoxVano(registerPage.phoneNumberInput).textContent).contains('Phone Number cannot be blank')
        .expect(inputMessageBoxVano(registerPage.addressInput).textContent).contains('address cannot be blank')
        .expect(inputMessageBoxVano(registerPage.cityInput).textContent).contains('city cannot be blank')
        .expect(inputMessageBoxVano(registerPage.stateSelect).textContent).contains('state cannot be blank')
        // BUG:
        //.expect(inputMessageBox(registerPage.zipInput).textContent).contains('zip code cannot be blank')

        .typeText(registerPage.phoneNumberInput, '1')
        .pressKey('tab')
        .expect(inputMessageBoxVano(registerPage.phoneNumberInput).textContent).contains('Phone Number is invalid');
});

test('Register a new user', async t => {
    // Register a new user and go to the onboarding page
    // TODO: await t.useRole(justRegisteredUser); //(see explanation below)
    await registerUser();

    // Test the Onboarding page
    await t
        .expect(onboardingPage.header.textContent).eql('Welcome to VANO', 'Registration completed', { timeout: 10000 })

        // Check navigation
        .click(onboardingPage.getStartedBtn)
        .expect(onboardingPage.header.textContent).eql('What services do you offer?')
        .click(onboardingPage.prevBtn)
        .expect(onboardingPage.header.textContent).eql('Welcome to VANO')
        .click(onboardingPage.nextBtn)
        .expect(onboardingPage.header.textContent).eql('What services do you offer?');

    // Check Services

    // check the 'Please select at least one service to continue' dialog
    await t.click(onboardingPage.nextBtn);
    await alert.checkAndClose('Hold it There!', 'Please select at least one service to continue')

    const service = onboardingPage.service(0);
    const selectedServiceName = (await service.value).trim();

    await t
        .expect(service.disabled).ok()
        .click(service.el)
        .expect(service.disabled).notOk()
        .click(onboardingPage.nextBtn)
        .expect(onboardingPage.header.textContent).eql('When are you available?');

    // Check Hours
    const onboardingPageHours = new Hours(true);
    const monday = onboardingPageHours.getHourByDay('Mon');
    let mondayData = await monday.getData();

    await t
        .expect(mondayData.open).eql('09:00')
        .expect(mondayData.close).eql('17:00')
        .expect(mondayData.opened).ok();

    await monday.setData('10:00', '18:00', false);

    await t
        .expect(mondayData.open).eql('10:00')
        .expect(mondayData.close).eql('18:00')
        .expect(mondayData.opened).notOk()

        // Check your are registered
        .click(onboardingPage.nextBtn)
        .expect(onboardingPage.header.textContent).eql('Congratulations!')
        .click(onboardingPage.completeBtn)
        .expect(ClientFunction(() => document.location.href)()).contains('dashboard');

    // Check the settings are applied
    await sidebar.swithItem('Settings');

    const fullNameParts = credentials.fullName.split(' ');

    await t
        .expect(settingsPage.firstNameInput.value).eql(fullNameParts[0])
        .expect(settingsPage.lastNameInput.value).eql(`${fullNameParts[1]}`)
        .expect(settingsPage.emailInput.value).eql(credentials.email)
        .expect(settingsPage.phoneInput.value).eql(credentials.phoneNumber)

        .click(settingsPage.title('Company'))
        .expect(settingsPage.companyInput.value).eql(credentials.companyName)
        .expect(settingsPage.addressInput.value).eql(credentials.address)
        .expect(settingsPage.cityInput.value).eql(credentials.city)
        .expect(settingsPage.zipInput.value).eql(credentials.zip)
        .expect(settingsPage.stateSelect.value).eql(credentials.state.substring(0, 2).toUpperCase());

    const settingsHours = new Hours();

    const settingsMonday = settingsHours.getHourByDay('Mon');
    const settingsMondayData = await settingsMonday.getData();

    await t
        .expect(settingsMondayData.open).eql('10:00')
        .expect(settingsMondayData.close).eql('18:00')
        .expect(settingsMondayData.opened).notOk();

    // Check services are applied
    await sidebar.swithItem('Services');
    await t
        .expect(servicesPage.addService.count).eql(1)
        .expect(servicesPage.services.count).eql(1)
        .expect(servicesPage.services.withText(selectedServiceName).exists).ok();
});


// ----------------------------------------------------------------------
// TODO: I'll move the onboarding page testing to a separate test once the
// https://github.com/DevExpress/testcafe/issues/1454 is fixed (is should happen soon).
// I will use Roles and it will work in the following way:
// - you can run both tests, the the first test register a new user
// and the second test open the onboarding page as a just registered user and test it;
// - you can run only the first test to check the registration process;
// - you can run the second test to check the onboarding page. In this case it registers a new user first
// (because it's required by design) and that check the onboarding page. 
/*test.page(`${baseUrl}/onboarding/welcome`)('Test onboarding page', async t => {
    await t.useRole(justRegisteredUser);
})*/