import { t } from 'testcafe';
import aurelia from 'testcafe-aurelia-selectors';
import Sidebar from '../page-model/components/sidebar';
import settingsPage from '../page-model/pages/settings-page';
import Hours from '../page-model/components/hours';
import Alert from '../page-model/components/alert';

const sidebar = new Sidebar();
const hours = new Hours();
const alert = new Alert();

async function clickHoursCheckboxes(hoursCheckboxes, prevSidebarItem) {
    await sidebar.swithItem('Settings');
    await t.click(settingsPage.title('Company'));

    while (await hoursCheckboxes.exists)
        await t.click(hoursCheckboxes);;

    await t.click(aurelia.byClickDelegate('update()'));
    await alert.checkAndClose();

    if (prevSidebarItem)
        await sidebar.swithItem(prevSidebarItem);
}

export async function enableAll(prevSidebarItem) {
    await clickHoursCheckboxes(hours.getUncheckedCheckboxes(), prevSidebarItem);
}

export async function disableAll(prevSidebarItem) {
    await clickHoursCheckboxes(hours.getCheckedCheckboxes(), prevSidebarItem);
}