import { ClientFunction } from 'testcafe';

export default function () {
    return ClientFunction(() => window.location.href.toString())();
}
