// TODO: replace it with Roles when https://github.com/DevExpress/testcafe/issues/1454 is fixed
import { t } from 'testcafe';
import credentials from '../data/credentials.json';
import loginPage from '../page-model/pages/login-page';

export default async function login(byPhone) {
    await t
        .typeText(loginPage.authenticatorInput, byPhone ? credentials.phoneNumber : credentials.email)
        .typeText(loginPage.passwordInput, credentials.password)
        .click(loginPage.loginBtn);
}