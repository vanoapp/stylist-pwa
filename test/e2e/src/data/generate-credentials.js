import baseCredentials from './credentials.json';

export default function generateCredentials(id) {
    return {
        fullName: `${baseCredentials.fullName} ${id}`,
        email: `${id}-${baseCredentials.email}`,
        password: baseCredentials.password,
        companyName: baseCredentials.companyName,
        phoneNumber: Date.now().toString().substring(3),
        address: baseCredentials.address,
        city: baseCredentials.city,
        zip: baseCredentials.zip,
        state: baseCredentials.state
    };
};