import { professionalTypes } from '../actions/professionals';

const initialState = {
  professional: {},
  professionals: [],
  isFetching: false,
}

export const professionalsReducer = (state = initialState, action) => {
  console.log(professionalTypes.SUCCESS_REQUEST_PROFESSIONAL);
  
  switch (action.type) {
    case professionalTypes.REQUEST_PROFESSIONAL:
      return {
        ...state,
        isFetching: true
      }
    case professionalTypes.SUCCESS_REQUEST_PROFESSIONAL:
      alert('yes')
      const newState =  {
        ...state,
        professional: action.payload,
        isFetching: false
      }
      return newState
    default:
      return state
  }
}
