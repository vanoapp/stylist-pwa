import { analyticsTypes } from '../actions/analytics';

const initialState = {
  analytics: {},
  isFetching: false,
}

export const analyticsReducer = (state = initialState, action) => {
  switch (action.type) {
    case analyticsTypes.REQUEST_PROFESSIONAL_ANALYTICS:
      return {
        ...state,
        isFetching: true
      }
    case analyticsTypes.SUCCESS_REQUEST_PROFESSIONAL_ANALYTICS:
      const newState =  {
        ...state,
        analytics: action.payload,
        isFetching: false
      }
      return newState
    default:
      return state
  }
}
