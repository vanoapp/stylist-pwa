// Redux
import {combineReducers} from 'redux';

// Reducers
import { analyticsReducer  } from './analytics';
import { professionalsReducer } from './professionals';

export default combineReducers({
  analyticsReducer,
  professionalsReducer
})
