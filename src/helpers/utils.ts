import env from '../environment';

export function inArray(list, item): boolean {
    return (list.indexOf(item) != -1);
}

export function formatPhoneNumber(num: string): string {
    switch (num.length) {
        case 1:
            return num.replace(/(\d{1})/, '($1');
        case 2:
            return num.replace(/(\d{2})/, '($1');
        case 3:
            return num.replace(/(\d{3})/, '($1');
        case 4:
            return num.replace(/(\d{3})\-?(\d{1})/, '($1) $2');
        case 5:
            return num.replace(/(\d{3})\-?(\d{2})/, '($1) $2');
        case 6:
            return num.replace(/(\d{3})\-?(\d{3})/, '($1) $2');
        case 7:
            return num.replace(/(\d{3})\-?(\d{3})\-?(\d{1})/, '($1) $2-$3');
        case 8:
            return num.replace(/(\d{3})\-?(\d{3})\-?(\d{2})/, '($1) $2-$3');
        case 9:
            return num.replace(/(\d{3})\-?(\d{3})\-?(\d{3})/, '($1) $2-$3');
        case 10:
            return num.replace(/(\d{3})\-?(\d{3})\-?(\d{4})/, '($1) $2-$3');
    }
}

export function capitalize(value: string): string {
    var words = value.split(' ');
    if (words.length > 1) {
        var res = '';
        for (var i = 0; i < words.length; i++) {
            if (i != 0) {
                res += ' ';
            }
            res += words[i].charAt(0).toUpperCase() + words[i].slice(1)
        }

        return res;
    }

    return value.charAt(0).toUpperCase() + value.slice(1)
}

const DEV_HOSTNAME = "localhost"
const DEV_IP = "192.168"
export function apiHost(): string {
    if (window.location.hostname.includes(DEV_HOSTNAME)) {
        return "http://localhost:8000/v1"
    } else if (window.location.hostname.includes(DEV_IP)) {
        return `http://${window.location.hostname}:8000/v1`
    }
    if(env.debug) {
      return "https://vano-api-staging.herokuapp.com/v1"
    }
    return "https://api.vanoapp.com/v1"
    
}


export const validRoutingNumber = function (routing): boolean {
    // all valid routing numbers are 9 numbers in length
    if (routing.length !== 9) {
        return false;
    }

    // if it aint a number, it aint a routing number
    if (isNaN(parseFloat(routing)) && isFinite(routing)) {
        return false;
    }
    // routing numbers starting with 5 are internal routing numbers
    // usually found on bank deposit slips
    if (routing[0] == '5') {
        return false;
    }

    // http://en.wikipedia.org/wiki/Routing_transit_number#MICR_Routing_number_format
    var checksumTotal = (7 * (parseInt(routing.charAt(0), 10) + parseInt(routing.charAt(3), 10) + parseInt(routing.charAt(6), 10))) +
        (3 * (parseInt(routing.charAt(1), 10) + parseInt(routing.charAt(4), 10) + parseInt(routing.charAt(7), 10))) +
        (9 * (parseInt(routing.charAt(2), 10) + parseInt(routing.charAt(5), 10) + parseInt(routing.charAt(8), 10)));

    var checksumMod = checksumTotal % 10;
    if (checksumMod !== 0) {
        return false;
    } else {
        return true;
    }
};

export const googleApiKey = () => {
    return "AIzaSyDP-j8eE1Q5noGzwZ1Jo7B7T9xp6IwT0dg"
}

export const shuffleArray = (a) => {
  let newArray = a;
  const arrayLen = newArray.length;
  for (let i = arrayLen; i; i--) {
      let j = Math.floor(Math.random() * i);
      [newArray[i - 1], newArray[j]] = [newArray[j], newArray[i - 1]];
  }
  return newArray;
}

export const randomClass = (): string => {
    let text = "";
    const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    for (let i = 0; i < 9; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text
}
