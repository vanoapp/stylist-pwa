import { Aurelia } from 'aurelia-framework'
import environment from './environment';

if (!String.prototype.includes) {
  String.prototype.includes = (search, start) => {
    'use strict';
    if (typeof start !== 'number') {
      start = 0;
    }

    if (start + search.length > this.length) {
      return false;
    } else {
      return this.indexOf(search, start) !== -1;
    }
  };
}

// const  isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
// if(isIE){
//   alert("do not use IE"); 
// }



//Configure Bluebird Promises.
//Note: You may want to use environment-specific configuration.
(<any>Promise).config({
  warnings: {
    wForgottenReturn: false
  }
});

export const configure = (aurelia: Aurelia) => {
  aurelia.use
    .standardConfiguration()
    .plugin('aurelia-validation')
    .plugin('aurelia-animator-css')
    .feature('resources');

  aurelia.use.globalResources([
    'components/vano-float-button/vano-float-button',
  ]);


  if (environment.debug) {
    aurelia.use.developmentLogging();
  }

  if (environment.testing) {
    aurelia.use.plugin('aurelia-testing');
  }

  aurelia.start().then(() => aurelia.setRoot());
}
