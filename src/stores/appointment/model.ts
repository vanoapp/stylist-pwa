import {Service} from '../service/model';

export class Appointment {
    id: string;
    professional_id: string;
    customer_id: string;
    notes: string;
    start_time: number;
    end_time: number;
    services: Service[];
    is_selected: boolean;
    is_blockoff: boolean;
}

export class PostAppointment {
  id: string;
  appointment_id: string;
  total_service_length: string;
  color_formula: string;
  highlight_formula: string;
  notes: string;
}

export class UIAppointment {
    id: string;
    professional_id: string;
    customer_id: string;
    notes: string;
    start_time: number;
    services: Service[];
    is_selected: boolean;
}

export class UpdateAppointment {
    id: string;
    professional_id: string;
    customer_id: string;
    notes: string;
    start_time: number;
    end_time: number;
    services: Service[];
}

export class NewAppointment {
    professional_id: string;
    customer_id: string;
    notes: string;
    start_time: number;
    end_time: number;
    services: Service[];
    is_blockoff: boolean;
}
