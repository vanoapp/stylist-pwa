import { HttpResponseMessage } from 'aurelia-http-client';

import { NewAppointment, UpdateAppointment, PostAppointment } from './model';

import {BaseService} from '../store';


export class AppointmentService extends BaseService {

    create(payload: NewAppointment): Promise<HttpResponseMessage> {
        let p = {
            "appointment": payload
        }
        return this.http.post('appointments', p).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    get(id: string): Promise<HttpResponseMessage> {
        return this.http.get(`appointments/${id}`).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    listAvailableProfessionalAppointmentTimes(professionalId: string, length: number, date: number): Promise<HttpResponseMessage> {
        return this.http.get(`appointments/times?professional_id=${professionalId}&length=${length}&date=${date}`).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    cancel(id: string): Promise<HttpResponseMessage> {
        return this.http.delete(`/appointments/${id}`).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    listProfessionalAppointments(professionalId: string, startTime: string, endTime: string, limit: string): Promise<HttpResponseMessage> {
        return this.http.get(`/professionals/${professionalId}/appointments?start_time=${startTime}&end_time=${endTime}&limit=${limit}`).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    listProfessionalBlockoffs(professionalId: string): Promise<HttpResponseMessage> {
      return this.http.get(`/professionals/${professionalId}/blockoffs`).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
  }

    listCustomerAppointments(cutomerId: string, startTime: string, endTime: string, limit: string): Promise<HttpResponseMessage> {
        return this.http.get(`/customers/${cutomerId}/appointments?start_time=${startTime}&end_time=${endTime}&limit=${limit}`);
    }

    update(payload: UpdateAppointment): Promise<HttpResponseMessage> {
        let p = {
            "appointment": payload
        }
        return this.http.put(`appointments/${payload.id}`, p).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    createPostAppointment(payload: PostAppointment): Promise<HttpResponseMessage> {
      const p = {
        post_appointment: payload
      }
      return this.http.post(`/appointments/${payload.appointment_id}/post-appointments`, p).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }

    updatePostAppointment(payload: PostAppointment): Promise<HttpResponseMessage> {
      const p = {
        post_appointment: payload
      }
      return this.http.put(`/appointments/${payload.appointment_id}/post-appointments/${payload.id}`, p).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }
}
