export class Analytics {
    day: Stat;
    week: Stat;
    month: Stat;
    year: Stat;
}

export class Stat {
    new_customers: number;
    percentage_booked: number;
    new_appointments: number;
    appointments_left: number;
}