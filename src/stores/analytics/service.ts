// Aurelia 
import { HttpResponseMessage } from 'aurelia-http-client';


// Home Brewed
import { BaseService } from '../store';

export class AnalyticsService extends BaseService {

    getProfessionalAnalytics(id: string): Promise<HttpResponseMessage> {
        return this.http.get(`/professionals/${id}/analytics`).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }
}

