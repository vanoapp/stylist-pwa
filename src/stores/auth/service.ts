// Aurelia
import { autoinject } from 'aurelia-framework';
import { HttpClient, HttpResponseMessage } from 'aurelia-http-client';
import { Router } from 'aurelia-router';

// Services
import { ProfessionalService } from '../professional/service';
import { CompanyService } from '../company/service';

// Models
import { Professional } from '../professional/model';

// Third Party
import * as jwt_decode from 'jwt-decode';
import * as swal from 'sweetalert';

// First Party
import { apiHost } from '../../helpers/utils';

let REFRESH_INTERVAL = 1296000 // refresh token 15 days  before it expires
let StatusOK = 200;

@autoinject
export class AuthService {
    token: string;
    router: Router;
    jwt: any;
    professional: Professional;

    constructor(
        private http: HttpClient,
        router: Router,
        private companyService: CompanyService,
        private professionalService: ProfessionalService) {
        this.router = router;
        this.http = http.configure(x => {
            x.withBaseUrl(apiHost())
        });
    }

    login(authenticator: string, password: string, redirect?: string) {
        if (!redirect) {
            redirect = '/dashboard';
        }

        let payload = {
            "authenticator": authenticator,
            "role": "professional",
            "password": password,
        };

        return this.http.post('/authenticate', payload).then(data => {
            if (data.statusCode == StatusOK) {
                this.token = data.content.token;

                try {
                    localStorage.setItem('token', this.token);
                } catch (err) {
                    swal('Oops!', 'It seems you are running this website while in private/incognito mode. This is not supported at the time in mobile devices', 'error');
                    return;
                }
                this.jwt = jwt_decode(this.token);
                this.calculateNextRefreshTime();
                return this.setUser().then(() => {
                    return this.setCompany().then(() => {
                        this.router.navigate(redirect);
                    });
                });
            }
        });
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('professional');
        localStorage.removeItem('company');
        localStorage.clear();
        this.router.navigate('login');
    }

    requestForgotPassword(authenticator: string): Promise<HttpResponseMessage> {
      const payload = {
        "authenticator": authenticator,
        "role": "professional",
      }
      return this.http.post('/reset-password-request', payload);
    }

    resetPassword(id: string, newPassword: string): Promise<HttpResponseMessage> {
      const payload = {
        "id": id,
        "role": "professional",
        "password": newPassword,
      }
      return this.http.post('/reset-password', payload);
    }

    calculateNextRefreshTime(): void {
        let tokenExp: any = this.jwt.exp;
        let refreshIn: number = ((tokenExp - Date.now() / 1000 | 0) - REFRESH_INTERVAL) * 1000;

        setTimeout(() => {
            this.refreshToken();
        }, refreshIn);
    }

    refreshToken(): void {
        let payload = {
            "token": this.token,
        };

        this.http.createRequest('/auth/refresh-token')
            .asPost()
            .withHeader('Authorization', 'Bearer ' + this.token)
            .withContent(payload)
            .send()
            .then(data => {
                if (data.statusCode == 200) {
                    this.token = data.content.token;
                    localStorage.setItem('token', this.token);

                    this.calculateNextRefreshTime();
                }
            })
            .catch(err => {
                this.logout();
            });
    }

    isAuthenticated(): boolean {
        if (localStorage.getItem('token')) {
            this.setUser();
            this.token = localStorage.getItem('token');
            let jwt = jwt_decode(this.token);

            let currentTime = Date.now() / 1000 | 0;

            if (currentTime > jwt.exp) {
                this.logout();
                return false;
            }

            this.calculateNextRefreshTime();
            return true;
        }

        this.logout();
        return false;
    }

    setUser() {
        let self = this;
        if (localStorage.getItem('professional')) {
            return;
        }

        return this.professionalService.get(this.jwt.sub).then(resp => {
            if (resp.statusCode === 200) {
                this.professional = resp.content.professional;
                localStorage.setItem('professional', JSON.stringify(this.professional));
            }
        });
    }

    setCompany() {
        return this.companyService.get(this.professional.company_id).then(resp => {
            if (resp.statusCode === 200) {
                let company = resp.content.company;
                localStorage.setItem('company', JSON.stringify(company));
            }
        })
    }
}
