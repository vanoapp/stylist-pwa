export class Event {
    id: string;
    customerId: number;
    employeeId: number;
    serviceId: number;
    startTime: number;
    endTime: number;
    title: string;
}

export class NewEvent {
    customerId: number;
    employeeId: number;
    serviceId: number;
    start: number;
    end: number;
    title: string;
}

export class UpdateEvent {
    id: string;
    customerId: number;
    employeeId: number;
    serviceId: number;
    start: number;
    end: number;
    title: string;
}