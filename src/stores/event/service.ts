// libs
import { inject } from 'aurelia-framework';
import { HttpClient, HttpResponseMessage } from 'aurelia-http-client';

// models
import {NewEvent, Event, UpdateEvent} from './model';
import {apiHost} from '../../helpers/utils';

@inject(HttpClient)
export class EventService {
    constructor(private http: HttpClient) {
        this.http = http.configure(x => {
            x.withBaseUrl(apiHost()).withHeader("Authorization", 'Bearer ' + localStorage.getItem('token'));
        });
    }

    getEmployeeEvents(employeeId: number, limit?: string): Promise<HttpResponseMessage> {
        var url: string = '/employees/' + employeeId + '/events'
        if(limit) {
            url = '/employees/' + employeeId + '/events?limit=' + limit
        }
        return this.http.get(url);
    }

    create(payload: NewEvent): Promise<HttpResponseMessage> {
        return this.http.post('/events', payload);
    }

    delete(id: number): Promise<HttpResponseMessage> {
        return this.http.delete('/events/' + id);
    }

    getEmployeeEventBetweenDates(employeeId: number, start: string, end: string, limit: string, status?: string): Promise<HttpResponseMessage> {
        if (!status) {
            status = 'all';
        }
        if (limit) {
            return this.http.get('/employees/' + employeeId + '/events?start=' + start + '&end=' + end + '&limit=' + limit + '&status='+status);
        }
        return this.http.get('/employees/' + employeeId + '/events?start=' + start + '&end=' + end + '&status='+status);
    }

    getCustomerEventBetweenDates(customerId: string, start: string, end: string, limit: string, status?: string): Promise<HttpResponseMessage> {
        if (!status) {
            status = 'all';
        }
        if (limit) {
            return this.http.get('/customers/' + customerId + '/events?start=' + start + '&end=' + end + '&limit=' + limit + '&status='+status);
        }
        return this.http.get('/customers/' + customerId + '/events?start=' + start + '&end=' + end + '&status='+status);
    }

    get(id: number): Promise<HttpResponseMessage> {
        return this.http.get('/events/' + id);
    }

    update(payload: UpdateEvent): Promise<HttpResponseMessage> {
        return this.http.put('/events/' + payload.id, payload);
    }
}