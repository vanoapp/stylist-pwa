import {Company} from '../company/model';

export class Customer {
    id: string;
    companies: CustomerCompany[];
    selected_company_id: number;
    first_name: string;
    last_name: string;
    username: string;
    email: string;
    phone_number: string;
    createdAt: string;
    updatedAt: string;
    is_selected: boolean;
    notes: CustomerNote[]
    notification: Notification;
}

export class CustomerNote {
    id: string;
    customer_id: string;
    value: string;
    is_editing: boolean;
    company_id: string;
}

export class NewCustomer {
    id: number;
    company_id: string;
    professional_id: string;
    first_name: string;
    last_name: string;
    username: string;
    email: string;
    phone_number: string;
    password: string;
}

export class UpdateCustomer {
    id: string;
    first_name: string;
    last_name: string;
    email: string;
    phone_number: string;
    notification: Notification;
}

class CustomerReminder {
    id: number;
    text: string;
    createdAt: string;
    updatedAt: string;
}

class CustomerCompany{
    id: number;
    customerId: number;
    companyId: number;
    createdAt: string;
    updatedAt: string;
}

class Notification {
    sms: boolean;
    email: boolean;
    phoneCall: boolean;
}

export class Card {
    id: string;
    holder: string;
    customer_id: string;
    nonce: string;
    last_four: number;
    expiration: string;
    zipCode: number;
}

export class NewCard {
    holder: string;
    customer_id: string;
    nonce: string;
    last_four: number;
    expiration: string;
    zip_code: number;
}
