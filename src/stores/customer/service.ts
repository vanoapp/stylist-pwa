import { HttpResponseMessage } from 'aurelia-http-client';
import { UpdateCustomer, NewCustomer, Customer, NewCard } from './model';
import { Company } from '../company/model';

import { BaseService } from '../store';

export class CustomerService extends BaseService {
  checkExists(email: string, phoneNumber: string): Promise<HttpResponseMessage> {
    const number = phoneNumber.replace(/\D/g, '');
    return this.http.get(`/customers/check-exists?phone_number=${number}&email=${email}`);
  }

  create(payload: NewCustomer): Promise<HttpResponseMessage> {
    var p = {
      "customer": payload
    }
    p.customer.phone_number = p.customer.phone_number.replace(/\D/g, '');
    return this.http.post('/customers', p).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  createNote(customer_id: string, company_id: string, value: string): Promise<HttpResponseMessage> {
    let p = {
      "note": {
        customer_id,
        value,
        company_id
      }
    }

    return this.http.post('/notes', p).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  deleteNote(id: string): Promise<HttpResponseMessage> {
    return this.http.delete(`/notes/${id}`).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  updateNote(id: string, value: string): Promise<HttpResponseMessage> {
    let p = {
      "note": {
        "id": id,
        "value": value
      }
    }

    return this.http.put('/notes/' + id, p).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  createCard(payload: NewCard): Promise<HttpResponseMessage> {
    return this.http.post('/customers/' + payload.customer_id + '/cards', payload).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  deleteCard(id: string): Promise<HttpResponseMessage> {
    return this.http.delete(`/cards/${id}`).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  get(id: string): Promise<HttpResponseMessage> {
    return this.http.get('/customers/' + id).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  getCompanyCustomers(companyId: string, page?: string): Promise<HttpResponseMessage> {
    if (!page) page = '1'
    return this.http.get('/companies/' + companyId + '/customers?page=' + page).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  getProfessionalCustomers(professionalID: string, page?: string): Promise<HttpResponseMessage> {
    if (!page) page = '1'
    return this.http.get('/professionals/' + professionalID + '/customers?page=' + page).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  getCards(id: string): Promise<HttpResponseMessage> {
    return this.http.get('/customers/' + id.toString() + '/cards').catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  all(): Promise<HttpResponseMessage> {
    return this.http.get('/customers').catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  removeFromCompany(companyId: string, customerId: string): Promise<HttpResponseMessage> {
    return this.http.delete(`/companies/${companyId}/customers/${customerId}`).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  update(payload: UpdateCustomer): Promise<HttpResponseMessage> {
    let p = {
      "customer": payload
    }
    p.customer.phone_number = p.customer.phone_number.replace(/\D/g, '');
    return this.http.put('/customers/' + payload.id, p).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  updatePassword(id: string, password: string, oldpassword: string): Promise<HttpResponseMessage> {
    let payload = {
      "id": id,
      "oldPassword": oldpassword,
      "password": password,
    };

    return this.http.put('/customers/' + id + '/update-password', payload).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  addCustomerToCompany(customerID: string, companyID: string): Promise<HttpResponseMessage> {
    let payload = {
      "company_id": companyID,
      "customer_id": customerID,
    }
    return this.http.post(`/companies/${companyID}/customers`, payload).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  addProfessionalCustomer(customerID: string, professional_id: string): Promise<HttpResponseMessage> {
    let payload = {
      professional_id,
      "customer_id": customerID,
    }
    return this.http.post(`/professionals/${professional_id}/customers/${customerID}`, payload).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  addCustomerService(customerID: string, service_id: string, length: number): Promise<HttpResponseMessage> {
    let payload = {
      service_id,
      "customer_id": customerID,
      length
    }
    return this.http.post(`/customers/${customerID}/services`, payload).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  removeCustomerService(customerID: string, customer_service_id: string): Promise<HttpResponseMessage> {
    return this.http.delete(`/customers/${customerID}/services/${customer_service_id}`).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  listCustomerServices(customerID: string): Promise<HttpResponseMessage> {
    return this.http.get(`/customers/${customerID}/services`).catch(err => {
      if(err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }
}
