import { HttpResponseMessage } from 'aurelia-http-client';
import { Service, NewService, UpdateService } from './model';

import {apiHost} from '../../helpers/utils';

import {BaseService} from '../store';


export class ServiceService extends BaseService {
    create(payload: NewService): Promise<HttpResponseMessage> {
        return this.http.post('/services', payload).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    all(profId: string): Promise<HttpResponseMessage> {
        return this.http.get('/professionals/' + profId + '/services').catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    get(id: string): Promise<HttpResponseMessage> {
        return this.http.get('/services/' + id).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    delete(id: string): Promise<HttpResponseMessage> {
        return this.http.delete('/services/' + id).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    update(payload: UpdateService): Promise<HttpResponseMessage> {
        let p = {
            "service": payload,
        }

        return this.http.put('/services/' + payload.id, p).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }
}
