export class Service {
    id: string;
    name: string;
    professional_id: string;
    length: number;
    description: string;
    price: number;
    is_editing: boolean;
    is_selected: boolean;
    require_phone_call: boolean;
    steps: Service[];
    is_gap: boolean;
    service_id: string;
    is_master: boolean;
}

export class NewService {
    name: string;
    professional_id: string;
    length: number;
    description: string;
    price: number;
    require_phone_call: boolean;
    steps: Service[];
    is_gap: boolean;
    service_id: string;
}

export class UpdateService {
    id: string;
    professional_id: string;
    name: string;
    companyId: number;
    length: number;
    description: string;
    price: number;
    require_phone_call: boolean;
    steps: Service[];
    is_gap: boolean;
    service_id: string;
}
