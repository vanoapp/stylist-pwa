export class Reminder {
    id: string;
    professional_id: string;
    text: string;
    due_date: number;
    completed: boolean;
    description: string;
}

export class CreateReminder {
    professional_id: string;
    text: string;
    due_date: number;
    completed: boolean;
    description: string;
}

export class UpdateReminder {
    id: string;
    text: string;
    due_date: number;
    completed: boolean;
    description: string;
}

//******************************//
// REQUEST MODELS  //
//******************************//

export class CreateReminderRequest {
    reminder: CreateReminder;
}

export class UpdateReminderRequest {
    reminder: UpdateReminder;
}
