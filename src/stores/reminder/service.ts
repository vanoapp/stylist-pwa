import { HttpResponseMessage } from 'aurelia-http-client';
import { CreateReminderRequest, UpdateReminderRequest } from './model';

import { apiHost } from '../../helpers/utils';

import { BaseService } from '../store';

export class ReminderService extends BaseService {
  create(payload: CreateReminderRequest): Promise<HttpResponseMessage> {
    return this.http.post('/reminders', payload).catch(err => {
      if (err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  update(payload: UpdateReminderRequest): Promise<HttpResponseMessage> {
    return this.http.put(`/reminders/${payload.reminder.id}`, payload).catch(err => {
      if (err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  get(id: string): Promise<HttpResponseMessage> {
    return this.http.get('/reminders/' + id).catch(err => {
      if (err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  all(limit: string, done: string): Promise<HttpResponseMessage> {
    if (limit) {
      if (done) {
        return this.http.get('/reminders?limit=' + limit + '&done=' + done).catch(err => {
          if (err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
      }
      return this.http.get('/reminders?limit=' + limit).catch(err => {
        if (err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }
    if (done) {
      return this.http.get('/reminders?done=' + done).catch(err => {
        if (err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }

    return this.http.get('/reminders').catch(err => {
      if (err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  getProfessionalReminders(professionalId: string, limit: string, completed: string): Promise<HttpResponseMessage> {
    return this.http.get('/professionals/' + professionalId + '/reminders?limit=' + limit + '&completed=' + completed).catch(err => {
      if (err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  remove(id: number): Promise<HttpResponseMessage> {
    return this.http.delete('/reminders/' + id).catch(err => {
      if (err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }
}
