import { BaseService } from './../store';
import { inject } from 'aurelia-framework';
import { HttpClient, HttpResponseMessage } from 'aurelia-http-client';

import { Company, UpdateCompany, NewCompany } from './model';
import {apiHost} from '../../helpers/utils';

export class CompanyService extends BaseService {
    create(payload: NewCompany): Promise<HttpResponseMessage> {
        let p = {
            "company": payload,
        }
        return this.http.post('/companies', p).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    get(id: string): Promise<HttpResponseMessage> {
        return this.http.get('/companies/' + id).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    update(payload: UpdateCompany): Promise<HttpResponseMessage> {
        let p = {
            "company": payload,
        }
        return this.http.put('/companies/' + payload.id, p).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    all(): Promise<HttpResponseMessage> {
        return this.http.get('/companies').catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    uploadImage(company_id: string, url: string): Promise<HttpResponseMessage> {
      let p = {
        "company_image": {
          url,
          company_id,
        }
      }

      return this.http.post(`/companies/${company_id}/images`, p).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }

    deleteImage(company_id: string, image_id: string): Promise<HttpResponseMessage> {
      return this.http.delete(`/companies/${company_id}/images/${image_id}`).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }

    listCompanyImages(company_id: string): Promise<HttpResponseMessage> {
      return this.http.get(`/companies/${company_id}/images`).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }
}
