export class Company {
    id: string;
    name: string;
    hours: Hours;
    address_line1: string;
    address_line2: string;
    city: string;
    state: string;
    country: string;
    zip_code: number;
    logo_url: string;
    cover_url: string;
    description: string;
    merchant_integrated: boolean;
    phone_number: string;
    email: string;
    created_at: string;
}

export class CompanyImage {
  id: string;
  company_id: string;
  url: string;
}

export class NewCompany {
    name: string;
    address_line1: string;
    address_line2: string;
    city: string;
    state: string;
    country: string;
    phone_number: string;
    email: string;
    zip_code: number;
}

export class UpdateCompany {
    id: string;
    name: string;
    hours: Hours;
    address_line1: string;
    address_line2: string;
    city: string;
    state: string;
    country: string;
    zip_code: number;
    logo_url: string;
    cover_url: string;
    description: string;
    merchant_integrated: boolean;
    phone_number: string;
    email: string;
}

export class Address {
    address1: string;
    address2: string;
    city: string;
    state: string;
    country: string;
    zipCode: number;
}

export class Hours {
    monday: Time;
    tuesday: Time;
    wednesday: Time;
    thursday: Time;
    friday: Time;
    saturday: Time;
    sunday: Time;
}

export class Time { 
    open: string;
    close: string;
    operating: boolean;
}
