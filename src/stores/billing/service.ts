// libs
import { HttpResponseMessage } from 'aurelia-http-client';

import { BaseService } from '../store';
import { NewTransaction } from './model';

export class BillingService extends BaseService {
  createCard(customerID: string, name: string, number: string, exp: string, method: string): Promise<HttpResponseMessage> {
    var payload = {
      "card": {
        "customer_id": customerID,
        number,
        exp,
        name,
        method
      }
    };
    return this.http.post('/cards', payload).catch(err => {
      if (err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  getCustomerCards(customerId: string): Promise<HttpResponseMessage> {
    return this.http.get('/customers/' + customerId + '/cards').catch(err => {
      if (err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  getTransaction(id: string): Promise<HttpResponseMessage> {
    return this.http.get(`/transactions/${id}`).catch(err => {
      if (err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  charge(t: NewTransaction) {
    var payload = {
      "transaction": t
    };
    return this.http.post('/transactions', payload).catch(err => {
      if (err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  getCompanyTransactions(companyId: string, fromDate?: number): Promise<HttpResponseMessage> {
    let url = `/companies/${companyId}/transactions`;
    if (fromDate) {
      url = `/companies/${companyId}/transactions?from_date=${fromDate}`;
    }
    return this.http.get(url).catch(err => {
      if (err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  getProfessionalTransactions(professionalId: string, fromDate?: number): Promise<HttpResponseMessage> {
    let url = `/professionals/${professionalId}/transactions`;
    if (fromDate) {
      url = `/professionals/${professionalId}/transactions?from_date=${fromDate}`;
    }
    return this.http.get(url).catch(err => {
      if (err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }


  removeCard(id: string): Promise<HttpResponseMessage> {
    return this.http.delete(`/cards/${id}`).catch(err => {
      if (err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }

  refund(id: string): Promise<HttpResponseMessage> {
    return this.http.delete(`/transactions/${id}`).catch(err => {
      if (err.statusCode === 403) {
        this.logout();
        return
      }
      return Promise.reject(err)
    });
  }
}
