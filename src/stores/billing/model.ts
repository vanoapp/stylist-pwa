export class Card {
    id: string;
    customer_id: string;
    nonce: string;
    last_four: number;
    exp: string;
    is_selected: boolean;
    token: string;
}

export class NewTransaction {
    total: number;
    professional_id: string;
    customer_id: string;
    appointment_id: string;
    card: TransactionCard;
    token: string;
}

export class Transaction {
    id: string;
    total: number;
    customer_id: string;
    professional_id: string;
    appointment_id: string;
    last_four: number;
    card_method: string;
    created_at: string;
}


export class TransactionCard {
    id: string;
    number: string;
    cvv: string;
    year: string;
    month: string;
    name: string;
    token: string;
    method: string;
}
