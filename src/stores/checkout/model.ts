import { Professional } from './../professional/model';
import {Service} from '../service/model';
import {Customer} from '../customer/model';
import {Card} from '../billing/model';
import {TransactionCard} from '../billing/model';
import {Appointment} from '../appointment/model';

export class Checkout{
    type: string;
    card: TransactionCard;
    appointment: Appointment;
    total: number;
    customer: Customer;
    saveCard: boolean;
    professional: Professional;
}
