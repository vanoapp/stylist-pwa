import { NewMerchant } from './model';

import { HttpResponseMessage } from 'aurelia-http-client';

import { BaseService } from '../store';

export class MerchantService extends BaseService {
    create(payload: NewMerchant): Promise<HttpResponseMessage> {
        const p = {
            "merchant": payload,
        }
        return this.http.post('/merchants', p).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }
}
