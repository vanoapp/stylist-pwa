export class Merchant {
    id: string;
    company_id: string;
    account_number: string;
    routing_number: string;
    ein: string;
    account_type: string;
    annual_cc_sales: number;
    dl_number: string;
    dl_state: string;
    professional_id: string;
}

export class NewMerchant {
    company_id: string;
    account_number: string;
    routing_number: string;
    ein: string;
    ssn: string;
    account_type: string;
    annual_cc_sales: number;
    dl_number: string;
    dl_state: string;
    professional_id: string;
}
