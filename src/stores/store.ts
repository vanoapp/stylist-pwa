import { Router } from 'aurelia-router';
import { autoinject } from 'aurelia-framework';
import { AuthService } from './auth/service';
import { HttpClient } from 'aurelia-http-client';

import { apiHost } from '../helpers/utils';

@autoinject
export class BaseService {
    public http: HttpClient;
    public router: Router;

    constructor(http: HttpClient, router: Router) {
        this.router = router;
        this.http = http.configure(x => {
            x.withBaseUrl(apiHost()).withHeader("Authorization", 'Bearer ' + localStorage.getItem('token'));
        });
    }

    logout(){
      localStorage.removeItem('token');
      localStorage.removeItem('professional');
      localStorage.removeItem('company');
      localStorage.clear();
      this.router.navigate('login');
      window.location.reload();
    }
}
