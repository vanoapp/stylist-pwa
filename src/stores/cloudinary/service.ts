// Aurelia
import {NewInstance, inject} from 'aurelia-framework';
import {HttpClient, HttpResponseMessage} from 'aurelia-http-client';

const CLOUDINARY_BASE_URL = 'https://api.cloudinary.com/v1_1'

@inject(NewInstance.of(HttpClient))
export class CloudinaryService{
  constructor(private http: HttpClient){
    this.http = http.configure(x => {
      x.withBaseUrl(CLOUDINARY_BASE_URL)
    });
  }

  upload(file): Promise<HttpResponseMessage> {
    let payload = {
      file,
      tags: 'browser_upload',
      upload_preset: 'woxvsbgh'
    }
    return this.http.post('/solabuddy/upload', payload);
  }
}
