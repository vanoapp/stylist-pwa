// Aurelia
import { inject, NewInstance } from 'aurelia-framework';
import { HttpClient } from 'aurelia-http-client';

const DEV_BASE_URL = 'https://lobanov.botscrew.com/Vano'

@inject(NewInstance.of(HttpClient))
export class ChatbotService {
  constructor(private http: HttpClient) {}

  sendMessage(id: string, msg: string) {
    let payload = {
      "chatId": {
        id
      },
      "message": {
        "text": msg,
      }
    }
    return this.http.post(`${DEV_BASE_URL}/messages`, payload);
  }
}

export class MessageResponse {
  from: string;
  to: string;
  action: string;
  messages: Message[];
}

class Message {
  text: string;
}
