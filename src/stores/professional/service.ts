import { ManagedProfessional } from './model';
// Aurelia 
import { HttpResponseMessage } from 'aurelia-http-client';

//models
import { UpdateProfessional, NewProfessional, ProfessionalPermission } from './model';

// Home Brewed
import { BaseService } from '../store';

export class ProfessionalService extends BaseService {
    create(professional: NewProfessional): Promise<HttpResponseMessage> {
        let p = {
            "professional": professional
        }

        p.professional.phone_number = p.professional.phone_number.replace(/\D/g,'');

        return this.http.post('/professionals', p).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    activate(id: string, password: string): Promise<HttpResponseMessage> {
        let p = {
            id,
            password
        }
        return this.http.post(`/professionals/${id}/activate`, p).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    checkExists(email: string, phoneNumber: string): Promise<HttpResponseMessage> {
        phoneNumber = phoneNumber.replace(/\D/g,'');
        return this.http.get(`/professionals/check-exists?phone_number=${phoneNumber}&email=${email}`).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    get(id: string): Promise<HttpResponseMessage> {
        return this.http.get('/professionals/' + id).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    getCompanyProfessionals(companyId: string): Promise<HttpResponseMessage> {
        return this.http.get(`/companies/${companyId}/professionals`).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    update(payload: UpdateProfessional): Promise<HttpResponseMessage> {
        let p = {
            "professional": payload
        }
        p.professional.phone_number = p.professional.phone_number.replace(/\D/g,'');
        return this.http.put(`/professionals/${payload.id}`, p).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

     resetPassword(id: string, password: string, newPassword: string): Promise<HttpResponseMessage> {
        let p = {
            "id": id,
            "password": password,
            "new_password": newPassword
        }
        return this.http.put(`/professionals/${id}/reset-password`, p).catch(err => {
          if(err.statusCode === 403) {
            this.logout();
            return
          }
          return Promise.reject(err)
        });
    }

    createManageProfessional(manager: string, managee: string, permission: ProfessionalPermission): Promise<HttpResponseMessage> {
      const p = {
        managee,
        manager,
        permission
      }

      return this.http.post('managed-professionals', p).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }

    getManagedProfessionals(professionalId: string): Promise<HttpResponseMessage> {
      return this.http.get(`/professionals/${professionalId}/managed-professionals`).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }

    getManageeInvitations(professionalId: string, status?: string): Promise<HttpResponseMessage> {
      if(!status) status = ''
      return this.http.get(`/professionals/${professionalId}/managee-invitations?status=${status}`).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }

    updateManagedProfessional(mp: ManagedProfessional): Promise<HttpResponseMessage> {
      let payload = {
        "managed_professional": mp
      }

      return this.http.put('/managed-professionals', payload).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }

    delete(id: string ): Promise<HttpResponseMessage> {
      return this.http.delete(`/professionals/${id}`).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }

    deleteManagedProfessional(id: string): Promise<HttpResponseMessage> {
      return this.http.delete(`/managed-professionals/${id}`).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }

    updateProfessionalFeature(spId: string, featureId: string, enabled: boolean): Promise<HttpResponseMessage> {
      let p = {
        enabled,
        professional_id: spId,
        feature_id: featureId,
      }

      return this.http.put(`/professionals/${spId}/features/${featureId}`, p).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }

    clocIn(id: string): Promise<HttpResponseMessage> {
      return this.http.post(`/professionals/${id}/cloc-ins`, {}).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }

    clocOut(spID: string, clocID: string): Promise<HttpResponseMessage> {
      return this.http.put(`/professionals/${spID}/cloc-ins/${clocID}`, {}).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }

    listProfessionalClocIns(id: string): Promise<HttpResponseMessage> {
      return this.http.get(`/professionals/${id}/cloc-ins`).catch(err => {
        if(err.statusCode === 403) {
          this.logout();
          return
        }
        return Promise.reject(err)
      });
    }
}
