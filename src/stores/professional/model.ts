import { Service } from '../service/model';

export class Professional {
  id: string;
  company_id: string;
  first_name: string;
  last_name: string;
  email: string;
  phone_number: string;
  services: Service[];
  notification: Notification;
  onboarded: boolean;
  permission: ProfessionalPermission;
  password_set: boolean;
  admin_generated: boolean;
  features: Feature[];
  clock_ins: ClockIns[];
  independent:boolean;
  merchant_integrated: boolean;
}

export class Feature {
  id: string;
  name: string;
  description: string;
  enabled: boolean;
  price: number;
  sale_price: number;
  order: number;
  benefits: string;
  for_who: string;
}

export class ManagedProfessional {
  id: string;
  managee: string;
  manager: string;
  status: string;
  permission: ProfessionalPermission;
}

export class NewProfessional {
  company_id: string;
  first_name: string;
  last_name: string;
  email: string;
  phone_number: string;
  password: string;
  permission: ProfessionalPermission;
  admin_generated: boolean;
  password_set: boolean;
  independent: boolean;
}

export class ProfessionalPermission {
  manage_appointments: boolean;
  manage_professionals: boolean;
  manage_analytics: boolean;
  manage_company: boolean;
  manage_customers: boolean;
  manage_checkouts: boolean;
}

export class UpdateProfessional {
  id: string;
  first_name: string;
  last_name: string;
  email: string;
  phone_number: string;
  notification: Notification;
  permission: ProfessionalPermission;
  merchant_integrated: boolean;
}

export class Notification {
  sms: boolean;
  email: boolean;
  phone: boolean;
}

export class ClockIns {
  id: string;
  created_at: string;
  cloc_in_time: string;
  cloc_out_time: string;
  professional_id: string;
}
