import { Redirect, Router } from 'aurelia-router';
import { autoinject } from 'aurelia-framework';

@autoinject
export class AuthorizeStep {
    constructor(private router: Router) {

    }

    run(navigationInstruction, next) {
        let professional = JSON.parse(localStorage.getItem('professional'));
        if (navigationInstruction.getAllInstructions().some(i => i.config.auth)) {
            if (!professional) {
                return next.cancel(new Redirect('login'));
            } else {
                // if(!professional.onboarded && !navigationInstruction.fragment.includes('/onboarding')) {
                //     return next.cancel(new Redirect('onboarding'));
                // }
                if(professional.onboarded && navigationInstruction.fragment.includes('/onboarding')) {
                    return next.cancel(new Redirect('dashboard'));
                }
            }
        } else {
            if (professional) {
                return next.cancel(new Redirect('dashboard'));
            }
        }
        return next();
    }
}

export class PreActivateStep {

    run(navigationInstruction, next) {
        if(!navigationInstruction.fragment.includes('/checkout')) {
            sessionStorage.removeItem('checkout');
        }
        if(!navigationInstruction.fragment.includes('/merchant-onboarding')) {
            sessionStorage.removeItem('onboarding');
        }
        window.scrollTo(0, 0);
        return next();
    }
}