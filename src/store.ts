// Redux
import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';

// Reducer
import reducer from './reducers/index';

const loggerMiddleware = createLogger();

let state = {
  error: null,
  loggedIn: false,
  analytics: {}
}

const indexDBMiddleware = store => next => action => {
  
}

const configureStore = () => {
  return createStore(
    reducer, // or combine some reducers
    compose(
      applyMiddleware(
        thunkMiddleware,
        loggerMiddleware,
        indexDBMiddleware,
      )
    )
  )
}

export const reduxStore = configureStore();
