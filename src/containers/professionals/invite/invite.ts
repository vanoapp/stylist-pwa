// Aurelia
import {autoinject} from 'aurelia-framework';
import {ValidationController, ValidationControllerFactory, ValidationRules} from 'aurelia-validation';

// Services
import { ProfessionalService } from './../../../stores/professional/service';

// Models
import { Professional } from './../../../stores/professional/model';

// First Party
import { VanoValidationRenderer } from './../../../validation/vano-renderer';

// Third Party
import * as swal from 'sweetalert';

@autoinject
export class Invite{
  // authProfessional: Professional;
  // email: string;
  // manageAnalytics: boolean;
  // manageAppointments: boolean;
  // serverErr: string;
  // controller: ValidationController;
  // accordionCount: number = 1;
  // isFetching: boolean;
  // opened: boolean;

  // constructor(private professionalService: ProfessionalService, controller: ValidationControllerFactory) {
  //   this.authProfessional = JSON.parse(localStorage.getItem('professional'));
  //   this.controller = controller.createForCurrentScope();
  //   this.controller.addRenderer(new VanoValidationRenderer());
  // }

  // validate() {
  //   this.controller.validate().then(res => {
  //     if(res.valid) {
  //       if(!this.manageAnalytics && !this.manageAppointments) {
  //         this.controller.addError('Please select at least one permission', this, 'serverErr');
  //         return
  //       }
  //       this.sendRequest();
  //     }
  //   })
  // }

  // sendRequest() {
  //   this.isFetching = true;
  //   this.professionalService.checkExists(this.email, '000000000').then(resp => {
  //     if(resp.statusCode === 200) {
  //       const p = resp.content.professional;
  //       if(p.company_id === this.authProfessional.company_id) {
  //         this.controller.addError('professional already belongs to your company.', this, 'serverErr');
  //         this.isFetching = false;
  //         return;        
  //       }
  //       return this.professionalService.getManagedProfessionals(this.authProfessional.id).then(resp => {
  //         if(resp.statusCode === 200) {
  //           const mp = resp.content.managed_professionals;
  //           const sps = mp.filter(item => item.managee === p.id);
  //           if(sps.length > 0) {
  //             this.controller.addError('You have already invited this professional.', this, 'serverErr');
  //             this.isFetching = false;
  //             return;
  //           }
  //           const permission = {
  //             "manage_company": false,
  //             "manage_professionals": false,
  //             "manage_analytics": this.manageAnalytics,
  //             "manage_appointments": this.manageAppointments,
  //             "manage_customers": false,
  //           }
  //           return this.professionalService.createManageProfessional(this.authProfessional.id, p.id, permission).then(resp => {
  //             if(resp.statusCode === 201) {
  //               this.isFetching = false;
  //               swal('Awesome!', 'request to manage professional has been sent.', 'success');
  //               window.dispatchEvent(new CustomEvent('vano-create-managed-professional', 
  //                 {detail: {
  //                 managedProfessional: resp.content.managed_professional,
  //               }}));
  //               this.email = '';
  //               this.manageAnalytics = false;
  //               this.manageAppointments  = false;
  //               this.opened = false;
  //             }
  //           })
  //         }
  //       })
  //     }
  //   }).catch(err => {
  //     this.isFetching = false;
  //     if(err.statusCode === 404) {
  //       this.controller.addError('professional not found, is the email correct?', this, 'serverErr')
  //     }
  //   })
  // }
}

ValidationRules
.ensure('email').displayName('email').email().required()
.on(Invite)
