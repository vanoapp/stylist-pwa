// Aurelia
import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

// Services
import { ProfessionalService } from '../../../stores/professional/service';

// Models
import { Professional, UpdateProfessional } from '../../../stores/professional/model';

// Third Party
import * as swal from 'sweetalert';

@autoinject
export class Company {
  professionals: Professional[];
  authProfessional: Professional;
  router: Router;

  constructor(private professionalService: ProfessionalService, router: Router) {
    this.router = router;
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.handleUpdatePermission = this.handleUpdatePermission.bind(this);
  }

  activate() {
    return this.getCompanyProfessioanls();
  }

  attached() {
    window.addEventListener('vano-professional-permission-change', this.handleUpdatePermission)
  }

  detached() {
    window.removeEventListener('vano-professional-permission-change', this.handleUpdatePermission)
  }

  getCompanyProfessioanls() {
    return this.professionalService.getCompanyProfessionals(this.authProfessional.company_id).then(resp => {
      if (resp.statusCode === 200) {
        this.professionals = resp.content.professionals.filter(p => p.id !== this.authProfessional.id && !p.independent);
      }
    });
  }

  handleUpdatePermission(e) {
    let p = e.detail.professional;
    this.update(p);
  }

  update(professional: Professional) {
    let updated = new UpdateProfessional();
    updated.id = professional.id;
    updated.email = professional.email;
    updated.phone_number = professional.phone_number;
    updated.first_name = professional.first_name;
    updated.last_name = professional.last_name;
    updated.permission = professional.permission;
    updated.notification = professional.notification;

    this.professionalService.update(updated).then(resp => {
      if (resp.statusCode === 200) {
        swal("Awesome!", "Permissions were updated!", "success");
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }
}
