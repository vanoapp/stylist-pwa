// Aurelia
import {autoinject} from 'aurelia-framework';

// Models
import { Professional, ManagedProfessional } from './../../../stores/professional/model';

// Services
import { ProfessionalService } from './../../../stores/professional/service';

// Third Party
import * as swal from 'sweetalert';

@autoinject
export class Individuals{
  authProfessional: Professional;
  professionals: Professional[];
  allManagedProfessionals: ManagedProfessional[];
  acceptedProfessionals: ManagedProfessional[];
  pendingProfessionals: ManagedProfessional[];

  constructor(private professionalService: ProfessionalService) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.handleAddManagedProfessional = this.handleAddManagedProfessional.bind(this);
  }

  activate() {
    return this.getManagedProfessionals().then(() => {
      return this.getProfessionals();
    })
    .catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("Well this is emberassing!", "Having trouble talking to our servers!", "error");
      }
    });
  }

  attached() {
    window.addEventListener('vano-create-managed-professional', this.handleAddManagedProfessional)
  }

  detached() {
    window.removeEventListener('vano-create-managed-professional', this.handleAddManagedProfessional)
  }

  handleAddManagedProfessional(e) {
    const p = e.detail.managedProfessional;
    this.pendingProfessionals.push(p);
  }

  getManagedProfessionals() {
    return this.professionalService.getManagedProfessionals(this.authProfessional.id).then(resp => {
      if(resp.statusCode === 200) {
        const m = resp.content.managed_professionals;
        this.allManagedProfessionals = m;
        this.acceptedProfessionals = m.filter(item => item.status === 'accepted');
        this.pendingProfessionals = m.filter(item => item.status === 'pending');
      }
    })
  }

  delete(id: string) {
    let self = this;
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this appointment",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, cancel it!",
      closeOnConfirm: false
    },
      () => {
        this.professionalService.deleteManagedProfessional(id).then(resp => {
          if(resp.statusCode === 200) {
            swal('Awesome!', 'individual professional was deleted', 'success');
            this.acceptedProfessionals = this.acceptedProfessionals.filter(item => item.id !== id);
            this.pendingProfessionals = this.pendingProfessionals.filter(item => item.id !== id);
          }  
        }).catch(err => {
          if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
            swal("Well this is emberassing!", "Having trouble talking to our servers!", "error");
          }
        });
      });
  }

  getProfessionals(): Promise<any> {
    this.professionals = [];
    if(this.allManagedProfessionals.length === 0) return Promise.resolve();
    const profLen = this.allManagedProfessionals.length;
    let profIndex = 0;

    return new Promise((resolve, reject) => {
      this.allManagedProfessionals.map(p => {
        this.professionalService.get(p.managee).then(resp => {
          profIndex++;
          if (resp.statusCode === 200) {
            const p = resp.content.professional;
            this.professionals.push(p);
            if(profIndex === profLen) {
              resolve()
            }
          }
        }).catch(err => reject(err))
      });
    });
  }

  fullName(p: ManagedProfessional) {
    const sp = this.professionals.filter(item => item.id === p.managee);
    return `${sp[0].first_name} ${sp[0].last_name}`;
  }

  email(p: ManagedProfessional) {
    const sp = this.professionals.filter(item => item.id === p.managee);
    return sp[0].email;
  }
}
