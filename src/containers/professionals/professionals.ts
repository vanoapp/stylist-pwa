// Aurelia
import { autoinject } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';

// Models
import { Professional } from '../../stores/professional/model';


@autoinject
export class Professioanls {
  router: Router;
  authProfessional: Professional;

  constructor() {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
  }

  configureRouter(config: RouterConfiguration, router: Router) {
      config.map([
        { route: [''], redirect: 'salon' },
          { route: ['/salon'], name: 'app.professionals.salon', moduleId: './company/company', title: 'Salon' },
          { route: ['/independents'], name: 'app.professionals.independents', moduleId: './independents/independents', title: 'Independents' },
      ]);

      config.mapUnknownRoutes('salon');

      this.router = router;
  }

  goTo(route: string): void {
      this.router.navigateToRoute(route);
  }

  get feature() {
    return {
      merchant_services: this.authProfessional.features.filter(i => i.name === 'credit_card_processing')[0].enabled,
      manage_employees: this.authProfessional.features.filter(i => i.name === 'manage_professionals')[0].enabled,
      require_credit_card: this.authProfessional.features.filter(i => i.name === 'require_credit_card')[0].enabled,
    }
  }
}
