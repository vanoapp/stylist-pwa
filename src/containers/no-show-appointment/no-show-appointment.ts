import { autoinject } from 'aurelia-dependency-injection';
import { Router } from 'aurelia-router';
import { TransactionCard } from './../../stores/billing/model';
import { Card } from './../../stores/customer/model';
import { Appointment } from './../../stores/appointment/model';
import { CustomerService } from './../../stores/customer/service';
import { AppointmentService } from './../../stores/appointment/service';
import { Professional } from './../../stores/professional/model';
import { BillingService } from './../../stores/billing/service';
import { observable } from 'aurelia-framework';

// Third Party
import * as swal from 'sweetalert';
import { NewTransaction } from '../../stores/billing/model';

@autoinject
export class NoShowAppointment {
  authProfessional: Professional;
  isFetching: boolean;
  total: number;
  appointment: Appointment;
  card: TransactionCard;
  router: Router;

  constructor(router: Router, private billingService: BillingService, private appointmentService: AppointmentService, private customerService: CustomerService) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.createTransaction = this.createTransaction.bind(this);
    this.router = router;
  }

  activate(params) {
    return this.getAppointment(params.id).then(() => {
      return this.getCustomerCards()
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  attached() {
    window.addEventListener('vano-dial-pad-submit', this.createTransaction);
  }

  detached() {
    window.removeEventListener('vano-dial-pad-submit', this.createTransaction);
  }

  getAppointment(id: string) {
    return this.appointmentService.get(id).then(resp => {
      if(resp.statusCode === 200) {
        this.appointment = resp.content.appointment;
      }
    })
  }

  getCustomerCards() {
    return this.customerService.getCards(this.appointment.customer_id).then(resp => {
      if(resp.statusCode === 200) {
        this.card = resp.content.cards[0];
      }
    })
  }

  createTransaction() {
    this.isFetching = true;
    if(!this.card) {
      swal('Oops!', 'This customer does not have any saved credit cards', 'warning');
      this.isFetching = false;
      return
    }

    if (this.total === 0) {
      swal('Oops', 'Total must be greater than 0', 'error');
      return;
    }

    let t = new NewTransaction;
    t.professional_id = this.authProfessional.id;
    t.total = this.total * 100;
    t.token = this.card.token;
    t.customer_id = this.appointment.customer_id;
    t.appointment_id = this.appointment.id;

    this.billingService.charge(t).then(resp => {
      this.isFetching = false;
      if (resp.statusCode === 201) {
        swal("Done!", "No show fee has been charged.", "success");
        this.router.navigate('/appointments-v2');
        return;
      }
    }).catch(err => {
      this.isFetching = false;
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }
}
