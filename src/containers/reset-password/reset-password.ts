import { Router } from 'aurelia-router';
import { autoinject } from 'aurelia-dependency-injection';
import { AuthService } from './../../stores/auth/service';
import { VanoValidationRenderer } from './../../validation/vano-renderer';
// Aurelia
import { inject, NewInstance } from 'aurelia-framework';
import { ValidationRules, ValidationController, ValidationControllerFactory } from 'aurelia-validation';

import * as swal from 'sweetalert';

@autoinject
export class ResetPassword{
  controller: ValidationController;
  password: string;
  id: string;
  router: Router

  constructor(controller: ValidationControllerFactory, private authService: AuthService, router: Router) {
    this.controller = controller.createForCurrentScope();
    this.controller.addRenderer(new VanoValidationRenderer());
    this.router = router;
  }

  activate(params) {
    this.id = params.id;
  }

  validate() {
    this.controller.validate().then(res => {
      if(res.valid) {
        this.resetPassword();
      }
    });
  }

  resetPassword() {
    this.authService.resetPassword(this.id, this.password).then(resp => {
      if(resp.statusCode === 200) {
        swal('Awesome!', 'password reset successfully, please log in to continue', 'success');
        this.password = '';
        this.router.navigate('/login');
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("Well this is emberassing!", "Having trouble talking to our servers!", "error");
      } else if (err.statusCode === 404) {
        swal("Oops!", err.content.error, "error");
      }
    });
  }
}

ValidationRules
.ensure('password').displayName('password').required().withMessage(`\${$displayName} cannot be blank.`)
.on(ResetPassword);
