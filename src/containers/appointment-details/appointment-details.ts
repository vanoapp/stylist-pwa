import { ProfessionalService } from './../../stores/professional/service';
// Aurelia
import { autoinject, observable } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { ValidationController, ValidationControllerFactory, ValidationRules, ValidateResult } from 'aurelia-validation';

// Services
import { CustomerService } from '../../stores/customer/service';
import { AppointmentService } from '../../stores/appointment/service';

// Models
import { Appointment, UpdateAppointment } from '../../stores/appointment/model';
import { Customer } from '../../stores/customer/model';
import { Professional } from '../../stores/professional/model';
import { Service } from '../../stores/service/model';
import { Company } from '../../stores/company/model';
import { Option } from '../../components/vano-forms/vano-search-dropdown/vano-search-dropdown';
import { VanoSelectOption } from '../../components/vano-forms/vano-select/vano-select';

// First Party
import { VanoValidationRenderer } from '../../validation/vano-renderer';
import { capitalize } from '../../helpers/utils';
import env from '../../environment';
import { ALGOLIA_APP_ID, ALGOLIA_API_KEY } from '../../const/const';

// Third Party
import * as swal from 'sweetalert';
import * as moment from 'moment';
import * as algoliasearch from 'algoliasearch';

const NO_SERVICE_SELECTED_PLACEHOLDER = 'Select service(s)';
const SERVICE_SELECTED_PLACEHOLDER = 'Select another service';

@autoinject
export class AppointmentDetails {
  authProfessional: Professional;
  router: Router;
  customers: Customer[];
  services: Service[];
  controller: ValidationController;
  customerOptions: Option[];
  serviceOptions: Option[];
  company: Company;
  appointment: Appointment;
  @observable customerSelected: string;
  servicesSelected: Service[];
  servicesSearchPlaceholder: string = 'Select another service';
  @observable serviceSelected: string;
  totalServicesLength: number;
  timeOptions: VanoSelectOption[];
  availableTimes: string[];
  selectedTime;
  timeDisabled: boolean = true;
  invalidResults: ValidateResult[];
  date: number;
  @observable stringDate: string;
  notes: string;
  limit: number = 5;
  professionals: Professional[];
  professionalName: string;
  @observable customerSearchQuery: string;
  initialDate: number;
  customerServices: any[];

  constructor(
    private customerService: CustomerService,
    private appointmentService: AppointmentService,
    private professionalService: ProfessionalService,
    controller: ValidationControllerFactory,
    router: Router
  ) {
    this.company = JSON.parse(localStorage.getItem('company'));
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.services = this.authProfessional.services;
    this.router = router;
    this.controller = controller.createForCurrentScope();
    this.controller.addRenderer(new VanoValidationRenderer());

    this.removeService = this.removeService.bind(this);
    this.handleServiceLengthChange = this.handleServiceLengthChange.bind(this);
  }

  getBackRouteURL(): string {
    return `/appointments-v2?date=${this.initialDate}`;
  }

  activate(params: any) {
    return this.getAppointment(params.id).then(() => {
      return this.getCompanyProfessionals().then(() => {
        return this.getCompanyCustomers().then(() => {
          return this.getCustomer().then(() => {
            this.generateCustomerOptions();
            this.generateServiceOptions();
            this.setUpdateableProperties();
          });
        })
      })
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  attached() {
    window.addEventListener('vano-appointment-remove-service', this.removeService);
    window.addEventListener('vano-appointment-service-length-change', this.handleServiceLengthChange);
  }

  detached() {
    window.removeEventListener('vano-appointment-remove-service', this.removeService);
    window.removeEventListener('vano-appointment-service-length-change', this.handleServiceLengthChange);
  }

  getCustomer() {
   return this.customerService.get(this.appointment.customer_id).then(resp => {
      if(resp.statusCode === 200) {
        this.customers.push(resp.content.customer);
      }
    })
  }

  goToNoShow() {
    const url = `/appointments/${this.appointment.id}/no-show`
    this.router.navigate(url);
  }

  handleServiceLengthChange(e) {
    const svc = e.detail.service;
    const newLength = e.detail.length;
    this.servicesSelected = this.servicesSelected.map(s => {
      if (s.id === svc.id) {
        s.length = parseInt(newLength);
      }
      return s;
    });

    this.getAvailableTimes();
  }

  validate() {
    this.controller.validate().then(res => {
      if (res.valid) {
        this.updateAppointment();
      }
    })
  }

  updateAppointment() {
    let updatedAppointment = new UpdateAppointment();
    updatedAppointment.id = this.appointment.id;
    updatedAppointment.customer_id = this.customerSelected;
    updatedAppointment.professional_id = this.appointment.professional_id;
    updatedAppointment.notes = this.notes;
    updatedAppointment.services = this.servicesSelected;
    updatedAppointment.start_time = parseInt(this.selectedTime);
    updatedAppointment.end_time = (60 * this.totalServicesLength) + updatedAppointment.start_time;

    this.appointmentService.update(updatedAppointment).then(resp => {
      if (resp.statusCode === 200) {
        swal("Awesome!", "Appointment was updated!", "success");
        this.router.navigate(this.getBackRouteURL());
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("Well this is emberassing!", "Having trouble talking to our servers!", "error");
      }
    });

  }

  cancelAppointment() {
    let self = this;
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this appointment",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, cancel it!",
      closeOnConfirm: false
    },
      () => {
        self.appointmentService.cancel(self.appointment.id).then(resp => {
          if (resp.statusCode === 200) {
            swal("Cancelled!", "Your appointment has been cancelled.", "success");
            self.router.navigate(this.getBackRouteURL());
          }
        }).catch(err => {
          if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
            swal("Well this is emberassing!", "Having trouble talking to our servers!", "error");
          }
        });
      });
  }

  customerSelectedChanged() {

    console.log(this.customerSelected);
    
    if(!this.customerSelected) return
    this.customerService.listCustomerServices(this.customerSelected).then(resp => {
      if(resp.statusCode === 200) {
        this.customerServices = resp.content.customer_services;
        this.checkForCustomerServicesLengths();
      }
    });
  }

  checkForCustomerServicesLengths() {
    if(this.servicesSelected.length > 0 && this.customerServices.length > 0) {
      this.servicesSelected = this.servicesSelected.map(svc => {
        this.customerServices.map(customerSvc => {
          if(customerSvc.service_id === svc.id) {
            svc.length = customerSvc.length;
          }
        });
        return svc;
      })
    }
  }

  getCompanyProfessionals() {
    return this.professionalService.getCompanyProfessionals(this.authProfessional.company_id).then(resp => {
      if (resp.statusCode === 200) {
        const p = resp.content.professionals;
        this.professionals = p.length > 0 ? p : [];
      }
    });
  }

  getAppointment(id: string) {
    return this.appointmentService.get(id).then(resp => {
      if (resp.statusCode === 200) {
        this.appointment = resp.content.appointment;
      }
    });
  }

  idToProf(id: string): Professional {
    const p = this.professionals.filter(item => item.id === id);
    return p[0];
  }

  setUpdateableProperties() {
    this.date = this.appointment.start_time;
    this.initialDate = this.appointment.start_time;
    this.customerSelected = this.appointment.customer_id;
    this.servicesSelected = this.appointment.services;
    this.stringDate = moment(this.appointment.start_time * 1000).format('MM/DD/YYYY');
    this.selectedTime = this.appointment.start_time;
    this.notes = this.appointment.notes;
    const p = this.idToProf(this.appointment.professional_id);
    this.professionalName = capitalize(`${p.first_name} ${p.last_name}`);
    this.getAvailableTimes(true);
  }

  getCompanyCustomers() {
    return this.customerService.getProfessionalCustomers(this.authProfessional.id).then(resp => {
      if (resp.statusCode === 200) {
        this.customers = resp.content.customers;
      }
    })
  }

  removeService(e) {
    const service = e.detail.service;
    let selectedServicesLen = this.servicesSelected.length;
    for (let i = 0; i < selectedServicesLen; i++) {
      let s = this.servicesSelected[i];
      if (service.id == s.id) {
        this.servicesSelected.splice(i, 1);
        break;
      }
    }

    this.serviceSelected = '';

    if (this.servicesSelected.length === 0) {
      this.servicesSearchPlaceholder = NO_SERVICE_SELECTED_PLACEHOLDER;
    }

    this.getAvailableTimes();
  }

  getAvailableTimes(first?: boolean) {
    this.totalServicesLength = 0;
    if (this.servicesSelected.length > 0) {
      for (let i = 0; i < this.servicesSelected.length; i++) {
        this.totalServicesLength += this.servicesSelected[i].length;
      }

      this.appointmentService.listAvailableProfessionalAppointmentTimes(this.authProfessional.id, this.totalServicesLength, this.date).then(resp => {
        if (resp.statusCode === 200) {
          this.availableTimes = resp.content.times;
          this.generateTimeOptions(first);
        }
      });
    }
  }

  generateTimeOptions(first?: boolean) {
    this.timeOptions = [];
    this.timeOptions = this.availableTimes.map(h => {
      return {
        value: h,
        label: moment(parseInt(h) * 1000).format('h:mm a'),
      }
    });
    this.timeDisabled = false;
    
    if (moment(this.date * 1000).format('MM/DD/YYYY') === moment(this.initialDate * 1000).format('MM/DD/YYYY')) {
      let opt = {
        value: this.selectedTime,
        label: moment(this.selectedTime * 1000).format('h:mm a'),
      }
      this.timeOptions.unshift(opt);
    }
  }

  handleKeyup(e) {
    if (e.which == 13 && !e.shiftKey) {
      e.preventDefault();
      this.validate();
    }
  }

  stringDateChanged() {
    if (!this.stringDate) this.stringDate = moment(this.date * 1000).format('MM/DD/YYYY');
    this.date = moment(this.stringDate).unix();
    this.getAvailableTimes();
  }

  serviceSelectedChanged() {
    this.services.map(s => {
      if (s.id === this.serviceSelected) {
        this.servicesSelected.push(s);
        this.servicesSearchPlaceholder = SERVICE_SELECTED_PLACEHOLDER;
      }
    });
    this.getAvailableTimes();
    this.serviceSelected = '';
    this.checkForCustomerServicesLengths();
  }

  removeError(propertyName: string): void {
    if (this.invalidResults) {
      const resLen = this.invalidResults.length;
      for (let i = 0; i < resLen; i++) {
        let r = this.invalidResults[i];
        if (r.propertyName === propertyName) {
          this.controller.removeError(r);
        }
      }
    }
  }

  generateCustomerOptions() {
    let options = [];
    let customersLen = this.customers.length;
    for (let i = 0; i < customersLen; i++) {
      let customer = this.customers[i];
      let opt = {
        value: customer.id,
        label: capitalize(`${customer.first_name} ${customer.last_name}`)
      }
      options.push(opt);
    }
    this.customerOptions = options;
  }

  generateServiceOptions() {
    let options = [];
    let servicesLen = this.services.length;
    for (let i = 0; i < servicesLen; i++) {
      let service = this.services[i];
      let opt = {
        value: service.id,
        label: service.name
      }
      options.push(opt);
    }

    this.serviceOptions = options;
  }

  customerSearchQueryChanged() {
    if (!this.customerSearchQuery) return
    let client = algoliasearch(ALGOLIA_APP_ID, ALGOLIA_API_KEY);
    let indexName = 'vano_customers';
    if (env.debug) {
      indexName = 'staging_vano_customers';
    }
    if (env.debug && env.testing) {
      indexName = 'dev_vano_customers';
    }
    const index = client.initIndex(indexName);
    index.search({
      query: this.customerSearchQuery,
      filters: `companies:${this.authProfessional.company_id} OR professionals:${this.authProfessional.id}`,
    }).then(resp => {
      this.customers = resp.hits.map(c => {
        c.id = c.objectID;
        return c;
      });
      this.generateCustomerOptions();
    });
  }

  get feature() {
    return {
      merchant_services: this.authProfessional.features.filter(i => i.name === 'credit_card_processing')[0].enabled,
      manage_employees: this.authProfessional.features.filter(i => i.name === 'manage_professionals')[0].enabled,
      require_credit_card: this.authProfessional.features.filter(i => i.name === 'require_credit_card')[0].enabled,
      clock_in: this.authProfessional.features.filter(i => i.name === 'clock_in_clock_out')[0].enabled,
      customer_service_times: this.authProfessional.features.filter(i => i.name === 'client_service_times')[0].enabled,
    }
  }
}

ValidationRules
  .ensure('customerSelected').displayName('Customer').required()
  .ensure('servicesSelected').displayName('Service').satisfies((services: Service[]) => {
    if (services.length > 0) return true;
    return false;
  }).withMessage('Service is required')
  .ensure('stringDate').displayName('Date').required()
  .ensure('selectedTime').displayName('Time').required()
  .on(AppointmentDetails)
