// Aurelia
import { autoinject, observable } from 'aurelia-framework';
import {Router} from 'aurelia-router';
import { 
    ValidationRules, 
    ValidationController, 
    ValidateResult, 
    ValidationControllerFactory
} from 'aurelia-validation';

// Services
import {ProfessionalService} from '../../stores/professional/service';

// Models
import {NewProfessional, Professional} from '../../stores/professional/model';
import {Company} from '../../stores/company/model';

// First Party
import {VanoValidationRenderer} from '../../validation/vano-renderer';

// Third Party
import * as swal from 'sweetalert';

@autoinject
export class AddProfessional{
    authProfessional: Professional;
    controller: ValidationController;
    firstName: string;
    lastName: string;
    email: string;
    phoneNumber: string;
    router: Router;
    serverError; string;
    isFetching: boolean;
    independent: boolean;

    constructor(
        controller: ValidationControllerFactory, 
        private professionalService: ProfessionalService,
        router: Router) {
        this.router = router;
        this.authProfessional = JSON.parse(localStorage.getItem('professional'));
        this.controller = controller.createForCurrentScope();
        this.controller.addRenderer(new VanoValidationRenderer());
    }

    validate() {
        this.controller.validate().then(res => {
            if(res.valid) {
                this.create();
            }
        })
    }

    handleKeyup(e) {
        if(e.which == 13 && !e.shiftKey) {  
            e.preventDefault();
            this.validate();
        }
    }

    create() {
        this.isFetching = true;
        let phoneNumber = this.phoneNumber.replace(/\D/g, '');
        this.professionalService.checkExists(this.email, phoneNumber).then(resp => {
            this.isFetching = false;
            if(resp.statusCode === 200) {
                const p = resp.content.professional;
                if(p.email === this.email) {
                  this.controller.addError('email address is already taken', this, 'serverError');
                }
                if(p.phone_number === phoneNumber) {
                  this.controller.addError('phone number is already taken', this, 'serverError');
                }
                return;
            }
        }).catch(resp => {
            if (resp.statusCode === 400) {
                this.isFetching = false;
                swal("Oops!", resp.content.error, "error");
            } else if (resp.statusCode === 500) {
                this.isFetching = true;
                swal("This is emberassing!", "Could not process request at this time", "error");
            }
            if(resp.statusCode === 404) {
                let sp = new NewProfessional();
                sp.first_name = this.firstName;
                sp.last_name = this.lastName;
                sp.email = this.email;
                sp.phone_number = phoneNumber;
                sp.company_id = this.authProfessional.company_id;
                sp.permission = {
                    "manage_appointments": false,
                    "manage_professionals": false,
                    "manage_analytics": false,
                    "manage_company": false,
                    "manage_customers": true,
                    "manage_checkouts": false,
                }
                sp.admin_generated = true;
                sp.password_set = false;
                sp.independent = this.independent;
                this.professionalService.create(sp).then(resp => {
                    this.isFetching = false;
                    if (resp.statusCode === 201) {
                        swal("Awesome!", "Professional was created!", "success");
                        this.router.navigateBack();
                    }
                }).catch(err => {
                    this.isFetching = false;
                    if (err.statusCode === 400) {
                        swal("Oops!", err.content.error, "error");
                    } else if (err.statusCode === 500) {
                        swal("This is emberassing!", "Could not process request at this time", "error");
                    }
                });  
            }
        })    
    }
}

ValidationRules
.ensure('firstName').displayName('first name').required().withMessage(`\${$displayName} cannot be blank.`)
.ensure('lastName').displayName('last name').required().withMessage(`\${$displayName} cannot be blank.`)
.ensure('email').displayName('email address').email().required().withMessage(`\${$displayName} cannot be blank.`)
.ensure('phoneNumber').displayName('phone number').matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/).withMessageKey('invalid phone number').required().withMessage(`\${$displayName} cannot be blank.`)
.on(AddProfessional);
