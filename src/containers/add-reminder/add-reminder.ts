// Aurelia
import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { ValidationController, ValidationControllerFactory, ValidationRules } from 'aurelia-validation';

// Services
import { ReminderService } from '../../stores/reminder/service';

// Models
import { VanoSelectOption } from '../../components/vano-forms/vano-select/vano-select';
import { CreateReminderRequest, CreateReminder } from '../../stores/reminder/model';
import { Professional } from '../../stores/professional/model';

// First Party
import { VanoValidationRenderer } from '../../validation/vano-renderer';

// Third Party
import * as swal from 'sweetalert';
import * as moment from 'moment';

@autoinject
export class AddReminder {
  authProfessional: Professional;
  router: Router;
  options: VanoSelectOption[];
  controller: ValidationController;
  title: string;
  date: string;
  time: string;
  description: string;
  formEl = undefined;
  isFetching: boolean;

  constructor(
    router: Router,
    controller: ValidationController,
    private reminderService: ReminderService) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.router = router;
    this.generateOptions();
    this.controller = controller;
    this.controller.addRenderer(new VanoValidationRenderer());
    this.handleKeyup = this.handleKeyup.bind(this);
  }

  attached() {
    this.formEl = document.querySelector('.submissionForm');
    this.formEl.addEventListener('keyup', this.handleKeyup);
  }

  detached() {
    this.formEl.removeEventListener('keyup', this.handleKeyup);
  }

  validate() {
    this.controller.validate().then(res => {
      if (res.valid) {
        this.createReminder();
      }
    })
  }

  handleKeyup(e) {
    console.log('hit');
    if (e.which == 13 && !e.shiftKey) {
      e.preventDefault();
      this.validate();
    }
  }

  createReminder() {
    this.isFetching = true;
    let hour = parseInt(this.time.split(':')[0]);
    let minutes = parseInt(this.time.split(':')[1]);
    let meridiem = this.time.split(' ')[1];
    if (meridiem === 'pm') {
      hour += 12;
    }
    if (meridiem === 'am' && hour === 12) {
      hour = 0;
    }
    let due_date = moment(this.date).add(hour, 'hour').add(minutes, 'minute').unix();

    let newReminder: CreateReminder = {
      text: this.title,
      professional_id: this.authProfessional.id,
      description: this.description,
      due_date: due_date,
      completed: false,
    }

    let req: CreateReminderRequest = {
      reminder: newReminder
    }

    this.reminderService.create(req).then(resp => {
      this.isFetching = false;
      if (resp.statusCode === 201) {
        swal("Awesome!", "Reminder was created!", "success");
        this.router.navigateBack();
      }
    }).catch(err => {
      this.isFetching = false;
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  generateOptions() {
    let meridiem;
    let cleanHour;
    let time;
    this.options = [];
    for (let h = 0; h < 24; h++) {
      for (let t = 0; t < 4; t++) {
        let option;
        switch (t) {
          case 0:
            meridiem = 'am'
            cleanHour = h
            if (h > 12) {
              cleanHour = h - 12;
              meridiem = 'pm'
            } else if (h === 0) {
              cleanHour = 12;
            }
            time = `${cleanHour}:00 ${meridiem}`
            option = {
              label: time,
              value: time
            }
            this.options.push(option);
            break;
          case 1:
            meridiem = 'am'
            cleanHour = h
            if (h > 12) {
              cleanHour = h - 12;
              meridiem = 'pm'
            } else if (h === 0) {
              cleanHour = 12;
            }
            time = `${cleanHour}:15 ${meridiem}`
            option = {
              label: time,
              value: time
            }
            this.options.push(option);
            break;
          case 2:
            meridiem = 'am'
            cleanHour = h
            if (h > 12) {
              cleanHour = h - 12;
              meridiem = 'pm'
            } else if (h === 0) {
              cleanHour = 12;
            }
            time = `${cleanHour}:30 ${meridiem}`
            option = {
              label: time,
              value: time
            }
            this.options.push(option);
            break;
          case 3:
            meridiem = 'am'
            cleanHour = h
            if (h > 12) {
              cleanHour = h - 12;
              meridiem = 'pm'
            } else if (h === 0) {
              cleanHour = 12;
            }
            time = `${cleanHour}:45 ${meridiem}`
            option = {
              label: time,
              value: time
            }
            this.options.push(option);
            break;
        }
      }
    }
  }
}

ValidationRules
  .ensure('title').displayName('Title').required()
  .ensure('date').displayName('Date').required()
  .ensure('time').displayName('Time').required()
  .on(AddReminder)
