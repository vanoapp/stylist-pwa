import { Router } from 'aurelia-router';
// Aurelia
import { bindable, autoinject, observable } from 'aurelia-framework';

// Services
import { AppointmentService } from '../../../stores/appointment/service';
import { CustomerService } from '../../../stores/customer/service';

// Models
import { Customer, CustomerNote, Card } from '../../../stores/customer/model';
import { Appointment } from '../../../stores/appointment/model';
import { Professional } from './../../../stores/professional/model';

// Third Party
import * as moment from 'moment';
import * as swal from 'sweetalert';
import { capitalize } from '../../../helpers/utils';

@autoinject
export class DesktopView {
  isTicking: boolean = false;
  scrollBuffer = 50;
  authProfessional: Professional;
  @bindable customers: Customer[];
  @bindable customer: Customer;
  @bindable search: string;
  @bindable notes: CustomerNote[];
  @bindable cards: Card[];
  @bindable isFetchingMoreClients: boolean;
  appointments: Appointment[];
  lastLetterSent: string;
  @observable noteValue: string;
  @bindable router: Router;

  constructor(private appointmentService: AppointmentService, private customerService: CustomerService) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
  }

  attached() {
    console.log(this.customer.id);
    
    this.getCustomerAppointments();
  }

  noteValueChanged() {
    if (!this.noteValue) return;
    this.customerService.createNote(this.customer.id, this.authProfessional.company_id, this.noteValue).then(resp => {
      if (resp.statusCode === 201) {
        swal("Awesome!", "Note was created!", "success");
        this.notes.push(resp.content.note);
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  selectCustomer(c: any) {
    if (c.objectID) c.id = c.objectID;
    this.customer = c;
    this.getCustomerAppointments();
    window.scrollTo(0, 0);
  }

  updateCustomer() {
    const url = `/customers/${this.customer.id}/update`;
    this.router.navigateToRoute('app.update.customer', { id: this.customer.id });
  }

  idToCustomerName(id: string): string {
    const c = this.customers.filter(item => item.id === id);
    if(c.length === 0) {
      return 'name unavailbale';
    }
    return capitalize(`${c[0].first_name} ${c[0].last_name}`)
  }

  getCustomerAppointments() {
    this.appointments = [];
    const thirtyDaysAgo = moment().subtract('day', 30).unix().toString();
    const thirtyDaysFromToday = moment().add('day', 30).unix().toString();
    this.appointmentService.listCustomerAppointments(this.customer.id, thirtyDaysAgo, thirtyDaysFromToday, '10').then(resp => {
      if (resp.statusCode === 200) {
        this.appointments = resp.content.appointments.filter((a: Appointment) => a.professional_id === this.authProfessional.id);
      }
    });
  }

  get feature() {
    return {
      merchant_services: this.authProfessional.features.filter(i => i.name === 'credit_card_processing')[0].enabled,
      manage_employees: this.authProfessional.features.filter(i => i.name === 'manage_professionals')[0].enabled,
      require_credit_card: this.authProfessional.features.filter(i => i.name === 'require_credit_card')[0].enabled,
      clock_in: this.authProfessional.features.filter(i => i.name === 'clock_in_clock_out')[0].enabled,
      customer_service_times: this.authProfessional.features.filter(i => i.name === 'client_service_times')[0].enabled,
    }
  }

  fullName(fname: string, lname: string) {
    return fname + ' ' + lname;
  }

  shouldSendLetter(letter: string): string {
    if (letter != this.lastLetterSent) {
      this.lastLetterSent = letter;
      return letter;
    }

    return null;
  }
}
