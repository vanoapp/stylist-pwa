// Aurelia
import {bindable} from 'aurelia-framework';
import {Router} from 'aurelia-router';

// Models
import {Customer} from '../../../stores/customer/model';

export class MobileView{
    @bindable customers: Customer;
    @bindable router: Router;
    @bindable isFetchingMoreClients: boolean;
    @bindable search: string;
    lastLetterSent: string;

    fullName(fname: string, lname: string) {
        return fname + ' ' + lname;
    }

    viewCustomerDetail(c: any) {
        if(c.objectID) c.id = c.objectID;
        this.router.navigate(`/customers/${c.id}`)
    }

    shouldSendLetter(letter: string): string {
        if(letter != this.lastLetterSent) {
            this.lastLetterSent = letter;
            return letter;
        }
        return null;
    }
}
