// Aurelia 
import { autoinject, observable } from 'aurelia-framework';
import { Router } from 'aurelia-router';

// Models
import { Professional } from '../../stores/professional/model';
import { Customer, CustomerNote, Card } from '../../stores/customer/model';

// Serices
import { CustomerService } from '../../stores/customer/service';

// Third Party
import * as swal from 'sweetalert';
import * as algoliasearch from 'algoliasearch';

// First Party
import env from '../../environment';
import { ALGOLIA_APP_ID, ALGOLIA_API_KEY } from '../../const/const';
import { inArray } from '../../helpers/utils';

@autoinject
export class Customers {
  @observable searchQuery: string;
  authProfessional: Professional;
  customers: Customer[] = [];
  notes: CustomerNote[] = [];
  @observable selectedCustomer: Customer;
  filteredCustomers: Customer[] = [];
  letters: string[] = [];
  router: Router;
  cards: Card[];
  page: number = 1;
  lastPage: number;
  customersCompleted: boolean;
  isFetchingMoreClients: boolean;
  customerListEl;
  index: algoliasearch.AlgoliaIndex;
  isMobile: boolean;

  constructor(private customerService: CustomerService, router: Router) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.router = router;
    this.handleDeleteCC = this.handleDeleteCC.bind(this);
    this.handleListScroll = this.handleListScroll.bind(this);
    this.handleUpdateCustomerNote = this.handleUpdateCustomerNote.bind(this);
    this.handleDeleteCustomerNote = this.handleDeleteCustomerNote.bind(this);
  }

  activate(params) {
    if (window.innerWidth > 960) {
      return this.getCustomers().then(_ => {
        if (this.customers.length === 0) {
          this.router.navigateToRoute('app.add.customer', { first_customer: true });
          return;
        }
        this.selectedCustomer = this.customers[0];
        if (this.selectedCustomer.notes) {
          this.notes = this.selectedCustomer.notes.filter(n => n.company_id === this.authProfessional.company_id);
        }
      }).catch(err => {
        if (err.statusCode === 400) {
          swal("Oops!", err.content.error, "error");
        } else if (err.statusCode === 500) {
          swal("This is emberassing!", "Could not process request at this time", "error");
        } else if (err.statusCode === 403) {
          swal("Oops!", "You do not have permission to do that!", "error");
        }
      });
    }

    this.isMobile = true

    return this.getCustomers().then(() => {
      if (this.customers.length === 0) {
        this.router.navigateToRoute('app.add.customer', { first_customer: true });
        return;
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }

  attached() {
    window.addEventListener('vano-delete-cc', this.handleDeleteCC);
    window.addEventListener('vano-edit-customer-note', this.handleUpdateCustomerNote)
    window.addEventListener('vano-delete-customer-note', this.handleDeleteCustomerNote)
    this.customerListEl = document.querySelector('.customers-list');
    if(this.customerListEl) this.customerListEl.addEventListener('scroll', this.handleListScroll);
  }

  detached() {
    window.removeEventListener('vano-edit-customer-note', this.handleUpdateCustomerNote)
    window.removeEventListener('vano-delete-customer-note', this.handleDeleteCustomerNote)
    window.removeEventListener('vano-delete-cc', this.handleDeleteCC);
    if(this.customerListEl) this.customerListEl.removeEventListener('scroll', this.handleListScroll);
  }

  handleDeleteCustomerNote(e) {
    const id = e.detail.id;
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this note",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
      () => {
        this.customerService.deleteNote(id).then(resp => {
          if(resp.statusCode === 200) {
            swal('Awesome!', 'Note was deleted sucessfully', 'success');
            this.notes = this.notes.filter(n => n.id !== id);
          }
        }).catch(err => {
          if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
            swal("This is emberassing!", "Could not process request at this time", "error");
          } else if (err.statusCode === 403) {
            swal("Oops!", "You do not have permission to do that!", "error");
          }
        });
      });
  }

  handleUpdateCustomerNote(e) {
    const id = e.detail.id;
    const value = e.detail.value;

    this.customerService.updateNote(id, value).then(resp => {
      if(resp.statusCode === 200) {
        swal('Awesome!', 'Note was updated sucessfully', 'success');
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }
  
  handleListScroll(e) {
    let elemHeight = this.customerListEl.offsetHeight;
    if (this.customersCompleted || this.searchQuery) return;
    if (!this.isFetchingMoreClients && this.customerListEl.scrollTop + this.customerListEl.clientHeight >= this.customerListEl.scrollHeight) {
      this.isFetchingMoreClients = true;
      this.customerService.getProfessionalCustomers(this.authProfessional.id, this.page.toString()).then(resp => {
        if (resp.statusCode === 200) {
          if (resp.content.customers.length === 0) {
            this.customersCompleted = true;
            this.isFetchingMoreClients = false;
            return;
          }
          this.page++
          this.isFetchingMoreClients = false;
          this.customers = this.customers.concat(resp.content.customers);
          this.filteredCustomers = this.customers.sort(this.sortCustomers);
        }
      })
    }
  }

  handleDeleteCC(e) {
    const id = e.detail.id;
    let self = this;
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this card",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
      () => {
        self.customerService.deleteCard(id).then(resp => {
          if (resp.statusCode === 200) {
            self.cards = self.cards.filter(item => item.id !== id);
            swal('Awesome!', 'Card was deleted', 'success');
          }
        }).catch(err => {
          if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
            swal("This is emberassing!", "Could not process request at this time", "error");
          } else if (err.statusCode === 403) {
            swal("Oops!", "You do not have permission to do that!", "error");
          }
        });
      });
  }

  selectedCustomerChanged() {
    this.notes = [];
    this.getCustomerCards(this.selectedCustomer.id);
    if (this.selectedCustomer.notes) this.notes = this.selectedCustomer.notes;
  }

  searchQueryChanged() {
    if (this.searchQuery.length > 0) {
      this.filterCustomers();
      return
    }

    this.getCustomers();
  }

  getCustomers() {
    this.page = 1
    return this.customerService.getProfessionalCustomers(this.authProfessional.id, this.page.toString()).then(resp => {
      if (resp.statusCode === 200) {
        if (resp.content.customers) {
          const c = resp.content.customers;
          this.customers = c.length > 0 ? c : [];
          if (this.customers.length > 0) {
            this.customers = this.customers.sort(this.sortCustomers);
          }
          this.page++;
          this.filteredCustomers = this.customers;
        }
      }
    });
  }

  getCustomerCards(id: string) {
    return this.customerService.getCards(id).then(resp => {
      if (resp.statusCode === 200) {
        const c = resp.content.cards;
        this.cards = c.length > 0 ? c : [];
      }
    });
  }

  sortCustomers(a: Customer, b: Customer) {
    if (a.first_name.toLowerCase() < b.first_name.toLowerCase()) return -1;
    if (a.first_name.toLowerCase() > b.first_name.toLowerCase()) return 1;
    return 0;
  }

  filterCustomers() {
    if (!this.searchQuery) return
    let client = algoliasearch(ALGOLIA_APP_ID, ALGOLIA_API_KEY);
    let indexName = 'vano_customers';
    if (env.debug) {
      indexName = 'staging_vano_customers';
    }
    if (env.debug && env.testing) {
      indexName = 'dev_vano_customers';
    }
    this.index = client.initIndex(indexName);
    this.index.search({
      query: this.searchQuery,
      filters: `companies:${this.authProfessional.company_id} OR professionals:${this.authProfessional.id}`,
    }).then(resp => {
      this.filteredCustomers = resp.hits.sort(this.sortCustomers);
    });
  }
}
