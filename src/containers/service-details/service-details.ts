// Aurelia
import { autoinject, bindable } from 'aurelia-framework';
import { ValidationRules, ValidationController, ValidationControllerFactory } from 'aurelia-validation';
import { Router } from 'aurelia-router';

// Third Party
import * as swal from 'sweetalert';
import * as VMasker from 'vanilla-masker';

// First Party
import { VanoValidationRenderer } from './../../validation/vano-renderer';

// services
import { ServiceService } from '../../stores/service/service';

// models
import { Service, UpdateService as UpdateServiceModel } from '../../stores/service/model';
import { Professional } from '../../stores/professional/model';

@autoinject
export class ServiceDetails {
  @bindable id: string;
  authProfessional: Professional;
  service: Service;
  controller: ValidationController;
  name: string;
  description: string;
  length: string;
  price: string;
  requirePhoneCall: boolean;

  constructor(private serviceService: ServiceService, private router: Router, controller: ValidationControllerFactory) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.controller = controller.createForCurrentScope();
    this.controller.addRenderer(new VanoValidationRenderer());
  }

  activate(params) {
    return this.getService(params.id).then(() => {
      this.setUpdateableProperties();
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }

  attached() {
    console.log(document.querySelector(".price-input"))
    // VMasker(document.querySelector(".price-input")).maskMoney({
    //   separator: '.',
    //   unit: '$'
    // });
  }

  setUpdateableProperties() {
    this.name = this.service.name;
    this.description = this.service.description;
    this.price = this.service.price.toString();
    this.length = this.service.length.toString();
    this.requirePhoneCall = this.service.require_phone_call;
  }

  getService(id: string) {
    return this.serviceService.get(id).then(resp => {
      this.service = resp.content.service;
    })
  }

  handleKeyup(e) {
    if(e.which == 13 && !e.shiftKey) {  
        e.preventDefault();
        this.validate();
    }
}

  validate() {
    this.controller.validate().then(res => {
      if (res.valid) {
        this.update();
      }
    });
  }

  delete() {
    let self = this;
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this service",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
      function () {
        self.serviceService.delete(self.service.id).then(resp => {
          swal("Deleted!", "Your appointment has been deleted.", "success");
          self.router.navigateBack();
          self.authProfessional.services = self.authProfessional.services.filter(item => item.id != self.service.id);
          localStorage.setItem('professional', JSON.stringify(self.authProfessional))
        }).catch(err => {
          if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
            swal('could not process the request at this time');
          }
        });
      });

  }

  update() {
    let us = new UpdateServiceModel();


    let price = this.price
    if(this.price.includes('$')) {
      price = this.price;
      price = price.split('$ ')[1];
      price = price.replace('.', '');
    }

    us.id = this.service.id;
    us.professional_id = this.service.professional_id;
    us.description = this.description;
    us.name = this.name;
    us.price = parseInt(price);
    us.length = parseInt(this.length);
    us.require_phone_call = this.requirePhoneCall;

    this.serviceService.update(us).then(resp => {
      this.router.navigateBack();
      swal("Awesome!", "Service was updated!", "success");
      let sLen = this.authProfessional.services.length;
      for (let i = 0; i < sLen; i++) {
        if (this.authProfessional.services[i].id == this.service.id) {
          this.authProfessional.services[i] = resp.content.service;
          localStorage.setItem('professional', JSON.stringify(this.authProfessional));
          break;
        }
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal('could not process the request at this time');
      }
    });
  }
}

ValidationRules
  .ensure('name').displayName('Name').required().withMessage(`\${$displayName} cannot be blank.`)
  .ensure('length').displayName('Length').required().withMessage(`\${$displayName} cannot be blank.`)
  .ensure('price').displayName('Error').satisfies((x: string) => {
    if(!x) return false;
    if (!x.includes('.') && parseInt(x) > 0) {
      return true
    }
    const strPrice = x.split('$')[1];
    const price = parseFloat(strPrice);

    if (price > 0.00) {
      return true
    }

    return false
  }).withMessage('price is required')
  .on(ServiceDetails);
