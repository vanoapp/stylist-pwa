import {autoinject} from 'aurelia-framework';

import {AuthService} from '../../stores/auth/service';

@autoinject
export class Logout {
    constructor(private auth: AuthService) {
        this.auth.logout();
    }
}