import { CalendarEvent } from './../../libs/calendarjs/calendar';
import { ManagedProfessional } from './../../stores/professional/model';
// Aurelia
import { autoinject, observable } from 'aurelia-framework';
import { Router } from 'aurelia-router';

// models
import { Professional } from '../../stores/Professional/model';
import { Customer } from '../../stores/customer/model';
import { Appointment } from '../../stores/appointment/model';
import { VanoCalendarEvent } from '../../components/vano-calendar/vano-calendar';

// services
import { AppointmentService } from '../../stores/appointment/service';
import { CustomerService } from '../../stores/customer/service';
import { CompanyService } from '../../stores/company/service';
import { ProfessionalService } from '../../stores/professional/service';

// First Party
import { capitalize } from '../../helpers/utils';

// Third Party
import * as moment from 'moment';
import * as swal from 'sweetalert';

const MONTH_PAGE_TITLE = 'Your Monthly Appointments';
const MONTH_PAGE_SUB_TITLE = 'Click any appointment to view or edit.';
const WEEK_PAGE_TITLE = 'Your Weekly Appointments';
const WEEK_PAGE_SUB_TITLE = 'Click any appointment to view or edit.';

const AGENDA_PAGE_TITLE = 'Your Appointments';
const AGENDA_PAGE_SUB_TITLE = 'Click any appointment to view or edit.';


@autoinject
export class Appointments {
  @observable selectedDisplay: string = 'month';

  router: Router;
  pageTitle: string = MONTH_PAGE_TITLE;
  pageSubTitle: string = MONTH_PAGE_SUB_TITLE;

  authProfessional: Professional;
  professionals: Professional[];
  customers: Customer[];
  appointments: Appointment[] = [];
  upcomingAppointments: Appointment[] = [];
  companyAppointments: Appointment[] = [];
  companyUpcomingAppointments: Appointment[] = [];
  events: CalendarEvent[] = [];
  upcomingEvents: CalendarEvent[] = [];
  companyEvents: CalendarEvent[] = [];
  companyUpcomingEvents: CalendarEvent[] = [];
  dataLoaded: boolean = false;
  upcomingDataLoaded: boolean;
  companyDataLoaded: boolean = false;
  companyUpcomingDataLoaded: boolean;
  @observable selectedProfessional: string = 'all';
  managedProfessionals: ManagedProfessional[];

  lastTitleDate: string;

  constructor(
    private appointmentService: AppointmentService,
    private customerService: CustomerService,
    private professionalService: ProfessionalService,
    router: Router
  ) {
    this.router = router;
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));

    this.calendayDayClick = this.calendayDayClick.bind(this);
    this.calendayEventClick = this.calendayEventClick.bind(this);
  }

  adminActivate() {
    return this.getCompanyProfessionals().then(() => {
      return this.getManagedSps().then(() => {
        return this.getCompanyAppointments().then(() => {
          return this.getCompanyUpcomingAppointments().then(() => {
            return this.getCustomers().then(() => {
              this.companyDataLoaded = true;
              this.companyEvents = this.transformToEvents(this.companyAppointments);
              this.companyUpcomingDataLoaded = true;
              this.companyUpcomingEvents = this.transformToEvents(this.companyUpcomingAppointments).sort(this.sortUpcomingEvents);
            })
          });
        })
      })
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }

  professionalActivate() {
    this.getAppointments(this.authProfessional.id).then(() => {
      return this.getUpcomingAppointments(this.authProfessional.id).then(() => {
        return this.getCustomers().then(() => {
          console.log('hey', this.appointments);
          
          this.dataLoaded = true;
          this.events = this.transformToEvents(this.appointments);
          this.upcomingEvents = this.transformToEvents(this.upcomingAppointments);
          this.upcomingDataLoaded = true;

          console.log('data loaded', this.dataLoaded);
          console.log('events', this.events);
          console.log('upcoming events loaded', this.upcomingDataLoaded);
          console.log('upcoming events', this.upcomingEvents);
          
          
          
          
        });
      });
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }

  attached() {
    this.addEventListeners();
    if (this.authProfessional.permission.manage_appointments) {
      this.adminActivate();
    }
    this.professionalActivate();
  }

  detached() {
    this.removeEventListeners();
  }

  addEventListeners() {
    window.addEventListener('vano-calendar-day-click', this.calendayDayClick);
    window.addEventListener('vano-calendar-event-click', this.calendayEventClick);
  }

  removeEventListeners() {
    window.removeEventListener('vano-calendar-day-click', this.calendayDayClick);
    window.removeEventListener('vano-calendar-event-click', this.calendayEventClick);
  }

  selectedDisplayChanged() {
    switch (this.selectedDisplay) {
      case 'month':
        this.pageTitle = MONTH_PAGE_TITLE;
        this.pageSubTitle = MONTH_PAGE_SUB_TITLE;
        break;
      case 'week':
        this.pageTitle = WEEK_PAGE_TITLE;
        this.pageSubTitle = WEEK_PAGE_SUB_TITLE;
        break;
      case 'day':
        this.pageTitle = AGENDA_PAGE_TITLE;
        this.pageSubTitle = AGENDA_PAGE_SUB_TITLE;
        break;
    }
  }

  calendayDayClick(e) {
    let date = moment(e.detail.date).unix();
    let midnighttoday = moment().subtract(moment().second(), 'second').subtract(moment().hour(), 'hour').subtract(moment().minute(), 'minute').unix();
    if (date >= midnighttoday) {
      this.router.navigateToRoute('app.add.appointment', { date });
      return;
    }
    swal('Silly You!', 'You can\'t book an appointment for earlier than today.', 'error');
  }

  calendayEventClick(e) {
    let id = e.detail.event.id;
    this.router.navigateToRoute('app.appointment.details', { id });
  }

  getAppointments(id: string) {
    let begginingOfMonth = moment().date(0).subtract(10, 'day').unix();
    let endOfMonth = moment().add('months', 2).date(0).unix();
    return this.appointmentService.listProfessionalAppointments(id, begginingOfMonth.toString(), endOfMonth.toString(), "200").then(resp => {
      if (resp.statusCode === 200) {
        this.appointments = resp.content.appointments
      }
    });
  }

  getUpcomingAppointments(id: string) {
    let midnight = moment().subtract('hour', moment().hour()).subtract('minute', moment().minute()).unix().toString();
    let endOfMonth = moment().add('months', 2).date(0).unix();
    return this.appointmentService.listProfessionalAppointments(id, midnight, endOfMonth.toString(), "10").then(resp => {
      if (resp.statusCode === 200) {
        this.upcomingAppointments = resp.content.appointments
      }
    });
  }

  getCompanyProfessionals() {
    return this.professionalService.getCompanyProfessionals(this.authProfessional.company_id).then(resp => {
      if (resp.statusCode === 200) {
        const profs = resp.content.professionals;
        this.professionals = profs.length > 0 ? profs : [];
      }
    })
  }

  getManagedSps() {
    let professionals = this.professionals.map(p => p);
    return new Promise((resolve, reject) => {
      this.professionalService.getManagedProfessionals(this.authProfessional.id).then(resp => {
        if (resp.statusCode === 200) {
          const profs = resp.content.managed_professionals;
          if(profs.length === 0) resolve();
          const msLen = profs.length;
          let msIndex = 0;
          profs.map(p => {
            this.professionalService.get(p.managee).then(resp => {
              if (resp.statusCode === 200) {
                msIndex++
                professionals.push(resp.content.professional);
                if(msIndex === msLen) {
                  this.professionals = professionals;
                  resolve();
                }
              }
            }).catch(err => {
              reject(err)
            })
          })
        }
      }).catch(err => reject(err))
    });
  }

  getCompanyAppointments() {
    const begginingOfMonth = moment().date(0).subtract(10, 'day').unix().toString();
    const endOfMonth = moment().add('months', 2).date(0).unix().toString();

    const profLen = this.professionals.length;
    let mapIndex = 0;

    return new Promise((resolve, reject) => {
      this.professionals.map(p => {
        this.appointmentService.listProfessionalAppointments(p.id, begginingOfMonth, endOfMonth, '200').then(resp => {
          mapIndex++;
          if (resp.statusCode === 200) {
            const apps = resp.content.appointments;
            if (this.companyAppointments.length === 0) {
              this.companyAppointments = apps.length > 0 ? apps : [];
              if (mapIndex === profLen) return resolve();
              return
            }
            this.companyAppointments = this.companyAppointments.concat(apps);
            if (mapIndex === profLen) return resolve();
          }
        })
      });
    });
  }

  getCompanyUpcomingAppointments() {
    let midnight = moment().subtract('hour', moment().hour()).subtract('minute', moment().minute()).unix().toString();
    const endOfMonth = moment().add('months', 2).date(0).unix().toString();

    const profLen = this.professionals.length;
    let mapIndex = 0;

    return new Promise((resolve, reject) => {
      this.professionals.map(p => {
        this.appointmentService.listProfessionalAppointments(p.id, midnight, endOfMonth, '200').then(resp => {
          mapIndex++;
          if (resp.statusCode === 200) {
            const apps = resp.content.appointments;
            if (this.companyUpcomingAppointments.length === 0) {
              this.companyUpcomingAppointments = apps.length > 0 ? apps : [];
              if (mapIndex === profLen) return resolve();
              return
            }
            this.companyUpcomingAppointments = this.companyUpcomingAppointments.concat(apps);
            if (mapIndex === profLen) return resolve();
          }
        })
      });
    });
  }

  getCustomers(): Promise<any> {
    let appointments = this.companyUpcomingAppointments;
    if (!this.authProfessional.permission.manage_appointments) {
      appointments = this.upcomingAppointments;
    }
  
    this.customers = [];
    const appsLen = appointments.length;
    if (appsLen === 0) return Promise.resolve();
    let appsIndex = 0;
    return new Promise((resolve, reject) => {
      appointments.map(t => {
        this.customerService.get(t.customer_id).then(resp => {
          if (resp.statusCode === 200) {
            this.customers.push(resp.content.customer);
            appsIndex++;
            if (appsIndex === appsLen) {
              resolve();
            }
          }
        }).catch(err => {
          reject(err)
        });
      });
    });
  }

  selectedProfessionalChanged() {
    if (!this.professionals || !this.selectedProfessional) return;
    if (this.selectedProfessional === 'all') {
      this.getAllProfessionalAppointments();
      return
    }

    this.getProfessionalAppointments(this.selectedProfessional);
  }

  getProfessionalAppointments(id: string) {
    this.appointments = [];
    this.upcomingAppointments = [];
    this.events = [];
    this.upcomingEvents = [];
    this.companyAppointments = [];
    this.companyUpcomingAppointments = [];
    this.companyEvents = [];
    this.companyUpcomingEvents = [];
    this.companyDataLoaded = false;
    this.companyUpcomingDataLoaded = false;
    this.getAppointments(id).then(() => {
      this.companyEvents = this.transformToEvents(this.appointments);
      this.companyDataLoaded = true;
      return this.getUpcomingAppointments(id).then(() => {
        this.companyUpcomingEvents = this.transformToEvents(this.upcomingAppointments);
        this.companyUpcomingDataLoaded = true;
      })
    });
  }

  getAllProfessionalAppointments() {
    this.companyAppointments = [];
    this.companyUpcomingAppointments = [];
    this.companyEvents = [];
    this.companyUpcomingEvents = [];
    this.companyDataLoaded = false;
    this.companyUpcomingDataLoaded = false;
    this.getCompanyAppointments().then(() => {
      this.companyEvents = this.transformToEvents(this.companyAppointments);
      this.companyDataLoaded = true;
      return this.getCompanyUpcomingAppointments().then(() => {
        this.companyUpcomingEvents = this.transformToEvents(this.companyUpcomingAppointments).sort(this.sortUpcomingEvents);
        this.companyUpcomingDataLoaded = true;
      })
    });
  }

  sortUpcomingEvents(a: CalendarEvent, b: CalendarEvent) {
    if (a.start < b.start)
      return -1;
    if (a.start > b.start)
      return 1;
    return 0;
  }

  profIdToName(id) {
    const p = this.professionals.filter(p => p.id === id)[0];
    return capitalize(`${p.first_name} ${p.last_name}`)
  }

  transformToEvents(appointments: Appointment[]): CalendarEvent[] {
    let events = [];
    const appsLength = appointments.length;

    if(appsLength === 0) return events;
    
    for (let i = 0; i < appsLength; i++) {
      const app = appointments[i];
      let event = new CalendarEvent;
      let professional = capitalize(`${this.authProfessional.first_name} ${this.authProfessional.last_name}`)
      if(this.authProfessional.permission.manage_appointments) {
        professional = this.profIdToName(app.professional_id)
      }
      event.id = app.id;
      event.start = moment(app.start_time * 1000).unix();
      event.end = moment(app.end_time * 1000).unix();
      event.title = `${this.convertCustomerIdToName(app.customer_id)} with ${professional}`;
      events.push(event);
    }
    return events;
  }

  convertCustomerIdToName(id: string) {
    let customersLen = this.customers.length;
    for (let i = 0; i < customersLen; i++) {
      if (id == this.customers[i].id) {
        let name = `${this.customers[i].first_name} ${this.customers[i].last_name[0]}.`
        return capitalize(name);
      }
    }
  }

  randomColor() {
    const colors = ['red', 'blue', 'green'];
    const color = colors[Math.floor(Math.random() * colors.length)];
    switch (color) {
      case 'red':
        return 'red';
      case 'blue':
        return 'blue';
      case 'green':
        return 'green';
    }
    return 'blue';

  }

  randomEventColor() {
    let e = {
      color: '',
      textColor: '',
      className: ''
    };
    switch (this.randomColor()) {
      case 'red':
        e.color = '#F40842';
        e.textColor = '#fff';
        e.className = 'vano-calendar-event vano-calendar-red-event';
        break;
      case 'blue':
        e.color = '#4786FF';
        e.textColor = '#fff';
        e.className = 'vano-calendar-event vano-calendar-blue-event';
        break;
      case 'green':
        e.color = '#4BC856';
        e.textColor = '#fff';
        e.className = 'vano-calendar-event vano-calendar-green-event';
        break;
    }
    return e;
  }
}
