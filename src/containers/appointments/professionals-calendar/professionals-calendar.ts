// Aurelia
import {bindable} from 'aurelia-framework';
import {Router} from 'aurelia-router';

// Models
import { VanoCalendarEvent } from '../../../components/vano-calendar/vano-calendar';

// Third Party
import * as moment from 'moment';
import { CalendarEvent } from '../../../libs/calendarjs/calendar';

export class ProfessionalsCalendar{
  @bindable events: CalendarEvent[];
  @bindable upcomingEvents: CalendarEvent[];
  @bindable selectedDisplay: string;
  @bindable eventDataLoaded: boolean;
  @bindable upcomingEventDataLoaded: boolean;
  @bindable router: Router;
  lastTitleDate: string;

  navigate(id: string): void {
    this.router.navigateToRoute('app.appointment.details', { id });
  }

  randomColor() {
    const colors = ['red', 'blue', 'green'];
    const color = colors[Math.floor(Math.random() * colors.length)];
    switch (color) {
      case 'red':
        return 'red';
      case 'blue':
        return 'blue';
      case 'green':
        return 'green';
    }
    return 'blue';
  }

  shouldDisplayDate(e): boolean {
    let title = moment(e.start).format('dddd DD');
    if (title != this.lastTitleDate) {
      this.lastTitleDate = title;
      return true;
    }
    return false;
  }

  shouldDisplayWrapper(e, i): boolean {
    let date = moment(e.start).unix();
    if (date > moment().unix() && i < 20) {
      return true;
    }
    return false;
  }

  getDateTitle(e): string {
    return moment(e.start).format('dddd DD');
  }
}
