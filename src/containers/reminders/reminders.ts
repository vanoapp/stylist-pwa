// Aurelia
import { autoinject } from 'aurelia-framework';
import {Router} from 'aurelia-router';

// Services
import { ReminderService } from './../../stores/reminder/service';

// Models
import { Reminder, UpdateReminderRequest, UpdateReminder } from './../../stores/reminder/model';
import { Professional } from './../../stores/professional/model';

// Third Party
import * as swal from 'sweetalert';

@autoinject
export class Reminders {
  authProfessional: Professional;
  reminders: Reminder[];
  completedReminders: Reminder[];
  uncompletedReminders: Reminder[];
  remindersLoaded: boolean;
  router: Router;

  constructor(private reminderService: ReminderService, router: Router) {
    this.router = router;
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.handleDeleteReminder = this.handleDeleteReminder.bind(this);
    this.checkedReminderChanged = this.checkedReminderChanged.bind(this);
  }

  activate() {
    this.getUncompletedReminders().then(() => {
      return this.getCompletedReminders().then(() => {
        this.reminders = this.uncompletedReminders.concat(this.completedReminders);
        this.reminders = this.reminders.sort(this.sortReminders);
        this.remindersLoaded = true;
      })
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }

  attached() {
    window.addEventListener('vano-delete-reminder', this.handleDeleteReminder);
    window.addEventListener('vano-reminder-toggle', this.checkedReminderChanged);
  }

  detached() {
    window.addEventListener('vano-delete-reminder', this.handleDeleteReminder);
    window.removeEventListener('vano-reminder-toggle', this.checkedReminderChanged);
  }

  checkedReminderChanged(e) {
    const reminder = this.reminders.filter(item => item.id === e.detail.reminderId)[0];
    if (e.detail.checked === true) {
      this.completeReminder(reminder);
      return;
    }
    this.uncompleteReminder(reminder);
  }

  completeReminder(reminder: Reminder) {
    // update for UI
    reminder.completed = true;

    let updated = new UpdateReminder();
    updated.completed = true;
    updated.id = reminder.id;
    updated.text = reminder.text;
    updated.due_date = reminder.due_date;

    this.updateReminder(updated);
  }

  uncompleteReminder(reminder: Reminder) {
    // update for UI
    reminder.completed = false;

    let updated = new UpdateReminder();
    updated.completed = false;
    updated.id = reminder.id;
    updated.text = reminder.text;
    updated.due_date = reminder.due_date;

    this.updateReminder(updated);
  }

  updateReminder(reminder: UpdateReminder) {
    let req = new UpdateReminderRequest();
    req.reminder = reminder;

    this.reminderService.update(req).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }

  handleDeleteReminder(e) {
    const id = e.detail.reminderId;
    let self = this;
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this reminder",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
      () => {
        this.reminderService.remove(id).then(resp => {
          if (resp.statusCode === 200) {
            this.reminders = this.reminders.filter(item => item.id !== id);
            swal('Awesome!', 'reminder was deleted', 'success');
          }
        }).catch(err => {
          if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
            swal("This is emberassing!", "Could not process request at this time", "error");
          } else if (err.statusCode === 403) {
            swal("Oops!", "You do not have permission to do that!", "error");
          }
        });
      });
  }

  getUncompletedReminders() {
    return this.reminderService.getProfessionalReminders(this.authProfessional.id, '25', 'false').then(resp => {
      if (resp.statusCode === 200) {
        this.uncompletedReminders = resp.content.reminders;
      }
    })
  }

  getCompletedReminders() {
    return this.reminderService.getProfessionalReminders(this.authProfessional.id, '25', 'true').then(resp => {
      if (resp.statusCode === 200) {
        this.completedReminders = resp.content.reminders;
      }
    })
  }

  sortReminders(a: Reminder, b: Reminder) {
    if (a.due_date < b.due_date)
      return -1;
    if (a.due_date > b.due_date)
      return 1;
    return 0;
  }
}
