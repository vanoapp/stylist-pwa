import { getProfessional } from './../../actions/professionals';
// Aurelia
import { inject } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';

// Models
import { Company } from '../../stores/company/model';
import { Professional } from '../../stores/professional/model';

// Services
import { ProfessionalService } from './../../stores/professional/service';
import { CompanyService } from '../../stores/company/service';

// Third Party
import * as moment from 'moment';
import { reduxStore } from '../../store';

const NEW_MANAGEE_INVITATION_TEXT = `<p>You have a new request to get your account managed. <a href="/manage-professionals/invitations">&nbsp;Click Here to find out more</a></p>`
const MERCHANT_ONBOARDING_PITCH = `<p>To get the most out of VANO we recommend doing your merchant on-boarding. <a href="/merchant-onboarding">&nbsp;Click here to get started</p>`

@inject(ProfessionalService, CompanyService, reduxStore)
export class VanoApp {
  router: Router;
  sidebarOpen: boolean = false;
  company: Company;
  authProfessional: Professional;
  hasNotification: boolean;
  notificationText: string;
  vanoAppEl;
  showProfessionalsLink: boolean;
  displayCheckout: boolean;
  displayMerchantOnboarding: boolean;
  store;

  constructor(private professionalService: ProfessionalService, private companyService: CompanyService, store) {
    this.company = JSON.parse(localStorage.getItem('company'));
    this.store = reduxStore
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));

    if (window.innerWidth > 960) {
      this.sidebarOpen = true;
    }
    this._clickEvent = this._clickEvent.bind(this);
    this._resizeEvent = this._resizeEvent.bind(this);
    this.handleMerchantOnboard = this.handleMerchantOnboard.bind(this);
    this.handleAuthCompanyChange = this.handleAuthCompanyChange.bind(this);
    this.handleTopbarNotificationClick = this.handleTopbarNotificationClick.bind(this);
    this.handleVanoFeatureChanged = this.handleVanoFeatureChanged.bind(this);
  }

  attached() {
    this.store.subscribe(this.update.bind(this));
    this.showCheckout();
    this.showMerchantOnboarding();
    this.vanoAppEl = document.getElementById('vano-app');
    this.addEventListeners();
    this.updateStorageAuths();
    this.checkIfMerchantIsOnboarderd();
    this.displayProfessionalsLink();
  }

  detached() {
    this.removeEventListeners()
  }


  addEventListeners() {
    this.vanoAppEl.addEventListener('click', this._clickEvent);
    window.addEventListener('resize', this._resizeEvent);
    window.addEventListener('vano-feature-changed', this.handleVanoFeatureChanged)
    window.addEventListener('vano-topbar-notification-click', this.handleTopbarNotificationClick);
    window.addEventListener('vano-merchant-onboarded', this.handleMerchantOnboard);
    window.addEventListener('vano-auth-company-change', this.handleAuthCompanyChange);
  }

  removeEventListeners() {
    window.removeEventListener('vano-feature-changed', this.handleVanoFeatureChanged)
    this.vanoAppEl.removeEventListener('click', this._clickEvent);
    window.removeEventListener('resize', this._resizeEvent);
    window.removeEventListener('vano-merchant-onboarded', this.handleMerchantOnboard);
    window.removeEventListener('vano-auth-company-change', this.handleAuthCompanyChange);
  }

  update() {
    const state = this.store.getState().professionalsReducer;
    if (!state.isFetching) {
      console.log(state.professional);
      
      this.authProfessional = state.professional;
      localStorage.setItem('professional', JSON.stringify(this.authProfessional));
    }
  }

  updateStorageAuths() {
    this.updateProfAuth();
    
    this.companyService.get(this.authProfessional.company_id).then(resp => {
      if(resp.statusCode === 200) {
        this.company = resp.content.company;
        localStorage.setItem('company', JSON.stringify(this.company));
      }
    })
  }

  updateProfAuth() {
    return this.professionalService.get(this.authProfessional.id).then(resp => {
      if(resp.statusCode === 200) {
        this.authProfessional = resp.content.professional;
        localStorage.setItem('professional', JSON.stringify(this.authProfessional));
      }
    })
  }

  handleMerchantOnboard(e) {
    if (this.authProfessional.independent) {
      this.authProfessional.merchant_integrated = true;
      localStorage.setItem('professional', JSON.stringify(this.authProfessional))
      this.showCheckout();
      this.showMerchantOnboarding();
      return
    }
    this.company.merchant_integrated = true;
    localStorage.setItem('company', JSON.stringify(this.company));
    this.showCheckout();
    this.showMerchantOnboarding();
  }

  get fragment() {
    return this.router.currentInstruction.fragment;
  }

  get checkoutTitle() {
    if (this.fragment.includes('merchant')) {
      return 'Onboarding';
    }

    return 'Checkout';
  }

  configureRouter(config: RouterConfiguration, router: Router): void {
    config.map([
      { route: '/dashboard', moduleId: 'containers/dashboard/dashboard', title: 'Dashboard', name: 'app.dashboard' },
      { route: '/services', moduleId: '../services/services', title: 'Services', name: 'app.services' },
      { route: '/appointments-v2', moduleId: 'containers/appointments-v2/appointments-v2', title: 'Appointments', name: 'app.appointments.v2' },
      { route: '/settings', moduleId: 'containers/settings/settings', title: 'Settings', name: 'app.settings' },
      { route: '/merchant-onboarding', moduleId: 'containers/merchant-onboarding/merchant-onboarding', title: 'Merchant', auth: true, name: 'app.merchant.onboarding' },
      { route: '/checkout', moduleId: 'containers/checkout/checkout', title: 'Checkout', name: 'app.checkout' },
      { route: '/reports', moduleId: 'containers/reports/reports', title: 'Reports', name: 'app.reports' },
      { route: '/professionals', moduleId: 'containers/professionals/professionals', title: 'Professionals', name: 'app.professionals' },
      { route: 'customers', moduleId: 'containers/customers/customers', title: 'Customers', name: 'app.customers' },
      { route: 'reminders', moduleId: 'containers/reminders/reminders', title: 'Reminders', name: 'app.reminders' },
      { route: 'customers/:id', moduleId: 'containers/customer-detail/customer-detail', title: 'Details', name: 'app.customer.detial' },
      { route: 'transactions/:id', moduleId: 'containers/transaction-details/transaction-details', title: 'Details', name: 'app.transaction.details' },
      { route: 'manage-professionals/invitations', moduleId: 'containers/managee-invitation/managee-invitation', title: 'Invitations', name: 'app.managee.invitations' },
      { route: ['/power-ups'], name: 'app.power.ups', moduleId: 'containers/features/features', title: 'Power Ups' },
      { route: ['/add-service-v2'], name: 'app.add.service.v2', moduleId: 'containers/add-service-v2/add-service-v2', title: 'Add Service' },
      { route: ['/services-v2/:id'], name: 'app.service.details.v2', moduleId: 'containers/service-details-v2/service-details-v2', title: 'Service Details' },
    ]);

    config.fallbackRoute('dashboard');

    this.router = router;
  }

  checkIfMerchantIsOnboarderd() {
    // if(this.company.merchant_integrated || this.authProfessional.admin_generated || window.innerWidth < 650) return;

    // const companyCreateDate = moment(this.company.created_at).unix();
    // const tenDaysAgo = moment().subtract('days', 10).unix();

    // if(tenDaysAgo > companyCreateDate) return;

    // this.hasNotification = true;
    // this.notificationText = MERCHANT_ONBOARDING_PITCH;
  }

  get feature() {
    return {
      merchant_services: this.authProfessional.features.filter(i => i.name === 'credit_card_processing')[0].enabled,
      manage_employees: this.authProfessional.features.filter(i => i.name === 'manage_professionals')[0].enabled,
      require_credit_card: this.authProfessional.features.filter(i => i.name === 'require_credit_card')[0].enabled,
    }
  }

  handleVanoFeatureChanged(e) {
    this.updateProfAuth().then(() => {
      this.displayProfessionalsLink();
      this.showCheckout()
      this.showMerchantOnboarding();
    })
  }

  displayProfessionalsLink() {
    let res = true;
    if (!this.authProfessional.permission.manage_professionals) res = false;
    if (!this.feature.manage_employees) res = false;
    this.showProfessionalsLink = res;
  }

  handleTopbarNotificationClick(e) {
    this.hasNotification = false;
  }

  handleAuthCompanyChange() {
    this.company = JSON.parse(localStorage.getItem('company'));
  }

  showCheckout(): boolean {
    if (this.authProfessional.independent) {
      let result = true;
      if (!this.feature.merchant_services) result = false
      if (!this.authProfessional.merchant_integrated) result = false;
      this.displayCheckout = result
      return 
    }
    let result = true;
    if (!this.feature.merchant_services) result = false
    if (!this.company.merchant_integrated) result = false
    this.displayCheckout = result
    return 
  }

  showMerchantOnboarding(): boolean {
    if (this.authProfessional.independent) {
      let result = true;
      if (!this.feature.merchant_services) result = false
      if (this.authProfessional.merchant_integrated) result = false
      this.displayMerchantOnboarding = result
      return 
    }
    let result = true;
    if (!this.feature.merchant_services) result = false
    if (this.company.merchant_integrated) result = false
    if (this.authProfessional.admin_generated) result = false
    this.displayMerchantOnboarding = result
    return 
  }

  _clickEvent(e) {
    let target = e.target;
    if (target && target.hasAttribute('vano-menu-toggle')) {
      this.sidebarOpen = !this.sidebarOpen;
      window.dispatchEvent(new CustomEvent('vano-sidebar-change', {
        detail: {
          sidebar: {
            open: this.sidebarOpen
          }
        }
      }));
    }
  }

  _resizeEvent(e) {
    if (window.innerWidth > 960) {
      this.sidebarOpen = true;
      window.dispatchEvent(new CustomEvent('vano-sidebar-change', {
        detail: {
          sidebar: {
            open: this.sidebarOpen
          }
        }
      }));
      return;
    }
    this.sidebarOpen = false;
    window.dispatchEvent(new CustomEvent('vano-sidebar-change', {
      detail: {
        sidebar: {
          open: this.sidebarOpen
        }
      }
    }));
  }

  goTo(route: string) {
    this.router.navigateToRoute(route);
  }
}
