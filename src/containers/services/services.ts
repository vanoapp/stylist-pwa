// libs
import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

// services
// import { ServiceService } from '../../stores/service/service';

// models
import { Service } from '../../stores/service/model';
import { Professional } from '../../stores/professional/model';
// import { Canvas } from '../off-canvas/model';

@autoinject()
export class Services {
  services: Service[] = [];
  authProfessional: Professional;
  lastColor: string;
  hasServices: boolean = false;
  router: Router;


  constructor(router: Router) {
    this.router = router;
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
  }

  activate() {
    this.services = this.authProfessional.services.filter(s => !s.is_master);
  }
}
