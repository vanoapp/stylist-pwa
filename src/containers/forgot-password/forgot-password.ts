import { autoinject } from 'aurelia-dependency-injection';
import { AuthService } from './../../stores/auth/service';
import { VanoValidationRenderer } from './../../validation/vano-renderer';
// Aurelia
import { inject, NewInstance } from 'aurelia-framework';
import { ValidationRules, ValidationController, ValidationControllerFactory } from 'aurelia-validation';

import * as swal from 'sweetalert';

@autoinject
export class ForgotPassword{
  controller: ValidationController;
  authenticator: string;

  constructor(controller: ValidationControllerFactory, private authService: AuthService) {
    this.controller = controller.createForCurrentScope();
    this.controller.addRenderer(new VanoValidationRenderer());
  }

  validate() {
    this.controller.validate().then(res => {
      if(res.valid) {
        this.resetPassword();
      }
    });
  }

  resetPassword() {
    this.authService.requestForgotPassword(this.authenticator).then(resp => {
      if(resp.statusCode === 200) {
        swal('Awesome!', 'password reset was requested', 'success');
        this.authenticator = '';
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("Well this is emberassing!", "Having trouble talking to our servers!", "error");
      } else if (err.statusCode === 404) {
        swal('Awesome!', 'password reset was requested', 'success');
        this.authenticator = '';
      }
    });
  }
}

ValidationRules
.ensure('authenticator').displayName('email or phone number').required().withMessage(`\${$displayName} cannot be blank.`)
.on(ForgotPassword);
