// Aurelia
import {autoinject} from 'aurelia-framework';
import {Router} from 'aurelia-router';

// Services
import { ReminderService } from '../../stores/reminder/service';
import { CustomerService } from '../../stores/customer/service';
import { AppointmentService } from '../../stores/appointment/service';
import { AnalyticsService } from '../../stores/analytics/service';

// Models
import { Reminder, UpdateReminder, CreateReminderRequest, UpdateReminderRequest } from '../../stores/reminder/model';
import { Professional } from '../../stores/professional/model';
import { Customer } from '../../stores/customer/model';
import { Appointment } from '../../stores/appointment/model';
import { Analytics } from '../../stores/analytics/model';

// Third Party
import * as moment from 'moment';
import * as swal from 'sweetalert';

// First Party
import { inArray } from '../../helpers/utils';

@autoinject
export class Dashboard {
    authProfessional: Professional;
    reminders: Reminder[] = [];
    reminder: Reminder;
    reminderText: string;
    customers: Customer[] = [];
    appointments: Appointment[];
    analytics: Analytics;
    statDirection: string = 'column';
    router: Router;

    constructor(
        private reminderService: ReminderService, 
        private customerService: CustomerService, 
        private appointmentService: AppointmentService, 
        private analyticsService: AnalyticsService,
        router: Router) {
        this.router = router;
        this.authProfessional = JSON.parse(localStorage.getItem('professional'));

        this.checkedReminderChanged = this.checkedReminderChanged.bind(this);
        this.resizeEvent = this.resizeEvent.bind(this);
    }

    activate(params) {
        return this.getProfessionalAppointments().then(() => {
            return this.getProfessionalReminders().then(() => {
                return this.getProfessionalCustomers().then(() => {
                    return this.getProfessionalAnalytics();
                });
            });
        }).catch(err => {
            if (err.statusCode === 400) {
                swal("Oops!", err.content.error, "error");
            } else if (err.statusCode === 500) {
                swal("This is emberassing!", "Could not process request at this time", "error");
            } else if (err.statusCode === 403) {
                swal("Oops!", "You do not have permission to do that!", "error");
            }
        });
    }

    attached() {
        if(window.innerWidth > 960) {
            this.statDirection = 'vano-row';
        }

        window.addEventListener('vano-reminder-change', this.checkedReminderChanged);
        window.addEventListener('resize', this.resizeEvent);
    }

    detached() {
      window.removeEventListener('vano-reminder-change', this.checkedReminderChanged);
      window.removeEventListener('resize', this.resizeEvent);
    }

    resizeEvent(e) {
        if(window.innerWidth > 960) {
            this.statDirection = 'vano-row';
            return;
        }
        this.statDirection = 'column';
    }


    getProfessionalAppointments() {
        let now = moment().unix().toString();
        let tenDaysFromToday = moment().add('day', 40).unix().toString();
        return this.appointmentService.listProfessionalAppointments(this.authProfessional.id, now, tenDaysFromToday, "4").then(resp => {
            if (resp.content.appointments) {
                this.appointments = resp.content.appointments.filter(a => !a.is_blockoff).sort(this.sortAppointments);
            }
        });
    }

    getProfessionalCustomers(): Promise<any> {
        const appsLen = this.appointments.length;
        if(appsLen === 0) return Promise.resolve();
        let appsIndex = 0;
        return new Promise((resolve, reject) => {
          this.appointments.map(a => {
            this.customerService.get(a.customer_id).then(resp => {
              if(resp.statusCode === 200) {
                this.customers.push(resp.content.customer);
                appsIndex++;
                if(appsIndex === appsLen) {
                  resolve();
                }
              }
            }).catch(err => {
              reject(err)
            });
          });
        });
    }

    getProfessionalReminders() {
        return this.reminderService.getProfessionalReminders(this.authProfessional.id, "5", "false").then(resp => {
            let reminders = resp.content.reminders;
            if (reminders) {
                this.reminders = reminders.sort(this.sortReminders);
            }
        });
    }

    roundPercentage(percent: number) {
        return Math.round(percent * 100);
    }

    getProfessionalAnalytics() {
        return this.analyticsService.getProfessionalAnalytics(this.authProfessional.id).then(resp => {
            if(resp.statusCode === 200) {
                this.analytics = resp.content.analytics;
            }
        });
    }

    checkedReminderChanged(e) {
        if (e.detail.checked === true) {
            this.completeReminder(e.detail.reminder);
            return;
        }
        this.uncompleteReminder(e.detail.reminder);
    }

    completeReminder(reminder: Reminder) {
        // update for UI
        reminder.completed = true;

        let updated = new UpdateReminder();
        updated.completed = true;
        updated.id = reminder.id;
        updated.text = reminder.text;
        updated.due_date = reminder.due_date;

        this.updateReminder(updated);
    }

    uncompleteReminder(reminder: Reminder) {
        // update for UI
        reminder.completed = false;

        let updated = new UpdateReminder();
        updated.completed = false;
        updated.id = reminder.id;
        updated.text = reminder.text;
        updated.due_date = reminder.due_date;

        this.updateReminder(updated);
    }

    updateReminder(reminder: UpdateReminder) {
        let req = new UpdateReminderRequest();
        req.reminder = reminder;

        this.reminderService.update(req).catch(err => {
            if (err.statusCode === 400) {
                swal("Oops!", err.content.error, "error");
            } else if (err.statusCode === 500) {
                swal("This is emberassing!", "Could not process request at this time", "error");
            } else if (err.statusCode === 403) {
                swal("Oops!", "You do not have permission to do that!", "error");
            }
        });
    }

    sortReminders(a: Reminder, b: Reminder) {
        if (a.due_date < b.due_date)
            return -1;
        if (a.due_date > b.due_date)
            return 1;
        return 0;
    }

    sortAppointments(a: Appointment, b: Appointment) {
        if (a.start_time < b.start_time)
            return -1;
        if (a.start_time > b.start_time)
            return 1;
        return 0;
    }

    get feature() {
      return {
        merchant_services: this.authProfessional.features.filter(i => i.name === 'credit_card_processing')[0].enabled,
        manage_employees: this.authProfessional.features.filter(i => i.name === 'manage_professionals')[0].enabled,
        require_credit_card: this.authProfessional.features.filter(i => i.name === 'require_credit_card')[0].enabled,
        clock_in: this.authProfessional.features.filter(i => i.name === 'clock_in_clock_out')[0].enabled,
      }
    }
}
