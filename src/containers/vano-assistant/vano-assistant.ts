// Aurlelia
import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

// Services
import { ChatbotService, MessageResponse } from '../../stores/chatbot/service';

// Models
import { Professional } from '../../stores/professional/model';

// Third Party
declare var firebase;
import * as moment from 'moment';

import env from '../../environment';

@autoinject
export class VanoAssistant {
  router: Router;
  authProfessional: Professional;
  message: string;
  messages: BubbleMessage[] = [];

  constructor(router: Router, private chatborService: ChatbotService) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.router = router;
    this.handleKeyPress = this.handleKeyPress.bind(this);
    let b: BubbleMessage = {
      fromBot: true,
      text: "Hello, how can I help?",
      timestamp: moment().unix().toString()
    };
    this.messages.push(b)
  }

  activate() {
    // let self = this;
    // var config = {
    //   apiKey: "AIzaSyB0fjr3nhDloemlXJQg5Qkomw6yPiekUWA",
    //   authDomain: "vano-chatbot.firebaseapp.com",
    //   databaseURL: "https://vano-chatbot.firebaseio.com",
    //   projectId: "vano-chatbot",
    //   storageBucket: "vano-chatbot.appspot.com",
    //   messagingSenderId: "891624913839"
    // };
    // firebase.initializeApp(config);

    // let db = firebase.database();
    // let ref = db.ref(this.authProfessional.id);
    // if(env.debug) {
    //   ref = db.ref('28f5c060-cea8-4885-90c0-3b24143d68d8');
    // }
    // ref.off();
    // ref.on('child_added', function (msg) {
    //   let resp = msg.val()
    //   console.log('resp', resp)
    //   let msgResp: string = '';
    //   let i = 0
    //   resp.messages.map(m => {
    //     if(i === 0) {
    //       msgResp += m.text 
    //       return
    //     }
    //     msgResp += "\n"
    //     msgResp += m.text 
    //   });
    //   let b: BubbleMessage = {
    //     fromBot: true,
    //     text: msgResp,
    //     timestamp: resp.timestamp,
    //   };
    //   self.messages.unshift(b)
    // });
  }

  attached() {
    window.addEventListener('keypress', this.handleKeyPress)
  }

  detached() {
    window.removeEventListener('keypress', this.handleKeyPress)
  }

  handleKeyPress(e) {
    if (e.which == 13 && !e.shiftKey) {
      e.preventDefault();
      this.submit();
    }
  }

  submit() {
    let b = {
      fromBot: false,
      text: this.message,
      timestamp: moment().unix().toString()
    }
    this.messages.unshift(b);
    
    setTimeout(() => {
      let b = {
        fromBot: true,
        text: "I'm currently being developed. Soon enough I'll be able to help you manage all task with the sound of your voice or by chatting with me in this window. We will keep you updated when I'll be ready!",
        timestamp: moment().unix().toString()
      }
      this.messages.unshift(b);
    }, 1500)

    this.message = '';
  }

  navigateBack() {
    this.router.navigateBack();
  }
}

export class BubbleMessage {
  fromBot: boolean;
  text: string;
  timestamp: string;
}
