// Libs
import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

// Services
import { CustomerService } from '../../../stores/customer/service';
import { BillingService } from '../../../stores/billing/service';

// Models
import { Professional } from '../../../stores/professional/model';
import { Checkout } from '../../../stores/checkout/model';
import { Service } from '../../../stores/service/model';
import { Customer } from '../../../stores/customer/model';
import { NewTransaction, TransactionCard } from '../../../stores/billing/model';

// Third Party
import * as swal from 'sweetalert';
import * as Hammer from 'hammerjs';

@autoinject
export class Review {
  checkout: Checkout;
  router: Router;
  authProfessional: Professional;
  total: number = 0.00;
  hammerEl = undefined;
  priceEditing: boolean;
  isFetching: boolean;

  constructor(
    router: Router,
    private customerService: CustomerService,
    private billingService: BillingService) {
    this.router = router;
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));

    this.prev = this.prev.bind(this);
    this.handlePriceChange = this.handlePriceChange.bind(this);
    this.handleWindowClick = this.handleWindowClick.bind(this);
  }

  activate() {
    if (!sessionStorage.getItem('checkout')) {
      this.router.navigate('/checkout/type');
      return;
    }

    this.checkout = JSON.parse(sessionStorage.getItem('checkout'));
    this.calcTotal();
  }

  attached() {
    const elem = document.querySelector('.checkout-review-component');
    this.hammerEl = new Hammer(elem, {});
    this.hammerEl.on('swiperight', this.prev);

    window.addEventListener('vano-checkout-service-price-change', this.handlePriceChange);
    window.addEventListener('click', this.handleWindowClick);
  }


  detached() {
    this.hammerEl.off('swiperight', this.prev);
    window.removeEventListener('vano-checkout-service-price-change', this.handlePriceChange);
    window.removeEventListener('click', this.handleWindowClick);
  }

  handleWindowClick(e) {
    if (e.target.classList.contains('pencil-icon')) return
    window.dispatchEvent(new CustomEvent('vano-checkout-price-editing-change', { detail: { editing: false, event: e } }));
  }

  handlePriceChange() {
    const check = this.checkout;
    // SUPER HACK!!!!!
    // FIX!!!!!
    // NOTE: the binding system isn't as fast as the event system so this.checkout is out of date when setting local storage.
    setTimeout(() => {
      localStorage.setItem('checkout', JSON.stringify(this.checkout));
      this.calcTotal();
    }, 300)

  }

  prev() {
    this.router.navigateToRoute('app.checkout.appointment');
  }

  get fullName() {
    return `${this.checkout.customer.first_name} ${this.checkout.customer.last_name}`;
  }

  calcTotal() {
    this.total = 0.00;
    const servicesLen = this.checkout.appointment.services.length;
    for (let i = 0; i < servicesLen; i++) {
      const service = this.checkout.appointment.services[i];
      // Input values are always strings. So we do this to ensure we always get an int
      this.total += parseInt(service.price.toString());
    }
  }

  route(name: string) {
    this.router.navigate(`/checkout/${name}`);
  }

  submit() {
    this.createTransaction();
  }

  createTransaction() {
    const editing = this.checkout.appointment.services.filter(s => s.is_editing === true);
    // This right here seems like it wouldn't work because we just keep calling the createTransaction method without chaning any of the service.is_editing state. The reason this works is because of the click event that removes the editing state of the price
    if (editing.length > 0) {
      swal({
        title: "Just Checking",
        text: "You have not finished editing the service price. Do you wish to continue with the transaction?",
        showCancelButton: true,
        confirmButtonColor: "#97f13c",
        confirmButtonText: "Confirm",
        closeOnConfirm: false
      }, () => this.createTransaction());
      return;
    }

    this.isFetching = true;
    let t = new NewTransaction;
    t.professional_id = this.checkout.professional.id;
    t.total = this.total;
    t.token = this.checkout.card.token;
    t.customer_id = this.checkout.customer.id;
    t.appointment_id = this.checkout.appointment.id;

    this.billingService.charge(t).then(resp => {
      this.isFetching = false;
      if (resp.statusCode === 201) {
        if (!this.checkout.saveCard) {
          this.deleteCC();
        }
        swal("Thank You!", "Your transaction has been created.", "success");
        sessionStorage.removeItem('checkout');
        this.router.navigateToRoute('app.checkout');
        return;
      }
    }).catch(err => {
      this.isFetching = false;
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }

  deleteCC() {
    return this.billingService.removeCard(this.checkout.card.id).then(resp => {
      if (resp.statusCode === 200) {

      }
    }).catch(err => {
      if (err.statusCode === 400) {
        console.log("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        console.log("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        console.log("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }

}
