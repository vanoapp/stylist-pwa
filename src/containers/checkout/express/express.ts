// libs
import { autoinject, observable } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { ValidationRules, ValidationController, ValidationControllerFactory } from 'aurelia-validation';

// Services
import { BillingService } from '../../../stores/billing/service';

// Models
import { Card } from '../../../stores/billing/model';
import { Checkout } from '../../../stores/checkout/model';
import { Professional } from '../../../stores/professional/model';
import { NewTransaction, TransactionCard } from '../../../stores/billing/model';

// Third Party
import * as swal from 'sweetalert';
import * as creditcardutils from 'creditcardutils';

// First Party
import { VanoValidationRenderer } from '../../../validation/vano-renderer';

@autoinject
export class Express {
    authProfessional: Professional;
    router: Router
    @observable total: number;
    checkout: Checkout;
    controller: ValidationController;
    displayDialpad: boolean;
    cardNumber: string;
    cardHolder: string;
    cardExp: string;
    cardCvv: string;
    card: Card;
    isFetching: boolean;

    constructor(router: Router, private billingService: BillingService, controller: ValidationControllerFactory) {
        this.authProfessional = JSON.parse(localStorage.getItem('professional'));
        this.router = router;
        this.controller = controller.createForCurrentScope();
        this.controller.addRenderer(new VanoValidationRenderer());
        this.createTransaction = this.createTransaction.bind(this)
    }

    activate() {
        this.checkout = JSON.parse(sessionStorage.getItem('checkout'));

        if (!this.checkout || this.checkout.type != 'express') {
            this.router.navigateToRoute('app.checkout.type');
            return;
        }

        if (this.checkout.total) {
            this.checkout.total = 0;
            sessionStorage.setItem('checkout', JSON.stringify(this.checkout));
        }
    }

    attached() {
        window.addEventListener('vano-dial-pad-submit', this.createTransaction);
    }

    detached() {
        window.removeEventListener('vano-dial-pad-submit', this.createTransaction);
    }

    totalChanged() {
        let stringTotal = this.total.toString();
        let total = this.total * 100;
        if (stringTotal.includes('.')) {
            total = parseInt(`${stringTotal.replace('.', '')}`)
            let cents = stringTotal.split('.')[1];
            if (cents.length < 2) {
                total = parseInt(`${stringTotal.replace('.', '')}`) * 10;
            }
        }

        this.checkout.total = total;
        sessionStorage.setItem('checkout', JSON.stringify(this.checkout));
    }

    confirm() {
        this.createTransaction();
    }

    createTransaction() {
        this.isFetching = true;
        if (this.total === 0) {
          this.isFetching = false;
            swal('Oops', 'Total must be greater than 0', 'error');
            return;
        }

        const expMonth = this.cardExp.split('/')[0].trim();
        const expYear = `20${this.cardExp.split('/')[1].trim()}`;

        let card = new TransactionCard();
        card.number = this.cardNumber.replace(/ /g, '');
        card.cvv = this.cardCvv;
        card.year = expYear;
        card.month = expMonth;
        card.name = this.cardHolder;
        card.method = creditcardutils.parseCardType(this.cardNumber);

        let t = new NewTransaction;
        t.professional_id = this.checkout.professional.id;
        t.total = this.total * 100;
        t.card = card;

        this.billingService.charge(t).then(resp => {
            if (resp.statusCode === 201) {
                this.isFetching = false;
                swal("Thank You!", "Your transaction has been created.", "success");
                sessionStorage.removeItem('checkout');
                this.router.navigateToRoute('app.checkout');
                return;
            }
        }).catch(err => {
            this.isFetching = false;
            if (err.statusCode === 400) {
                swal("Oops!", err.content.error, "error");
            } else if (err.statusCode === 500) {
                swal("This is emberassing!", "Could not process request at this time", "error");
            } else if (err.statusCode === 403) {
                swal("Oops!", "You do not have permission to do that!", "error");
            }
        });
    }

    validate() {
        this.controller.validate().then(res => {
            if (res.valid) {
                this.displayDialpad = true;
            }
        })
    }
}

ValidationRules
    .ensure('cardHolder').displayName('name on card').required()
    .ensure('cardNumber').displayName('card number').required().satisfies((cardNumber: string) => {
        if (cardNumber === '4111 1111 1111 1111') return true;
        return creditcardutils.validateCardNumber(cardNumber)
    }).withMessage("invalid card number")
    .ensure('cardExp').displayName('expiration').required().satisfies((cardExp: string) => {
        if (cardExp) {
            return creditcardutils.validateCardExpiry(cardExp.split('/')[0], cardExp.split('/')[1])
        }
        return false;
    }).withMessage("invalid expiration")
    .ensure('cardCvv').displayName('cvv').required().satisfies((cardCvv: string) => {
        return creditcardutils.validateCardCVC(cardCvv)
    }).withMessage("invalid cvv")
    .on(Express)
