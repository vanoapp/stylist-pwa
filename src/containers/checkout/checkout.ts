// libs
import { autoinject } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';
import { EventAggregator } from 'aurelia-event-aggregator';

// Models
import { Professional } from '../../stores/professional/model';
import { Company } from '../../stores/company/model';

// services
import { CompanyService } from '../../stores/company/service';

// Third Party
import * as Hammer from 'hammerjs';
import * as swal from 'sweetalert';

@autoinject
export class Checkout {
    router: Router;
    active: string;
    authProfessional: Professional;
    company: Company;
    initialDisplay: string;
    fabMenuEl = undefined;
    activeStep: number = 0;
    stepsLength: number = 4;
    typeDesc: string = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    customerDesc: string = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    selectServiceDesc: string = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    reviewDesc: string = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    cardDesc: string = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    tipDesc: string = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
    appointmentDesc: string = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';

    constructor(private companyService: CompanyService, private ea: EventAggregator) {
        this.authProfessional = JSON.parse(localStorage.getItem('professional'));
        this.company = JSON.parse(localStorage.getItem('company'));
    }

    attached() {
        this.updateActiveStep();
        this.ea.subscribe('router:navigation:complete', e => this.updateActiveStep())
        this.fabMenuEl = document.querySelector('.vano-fab-menu-component');
        this.initialDisplay = this.fabMenuEl.style.display;
        this.fabMenuEl.style.display = 'none';
        const elem = document.querySelector('.checkout-component');
        const hammer = new Hammer(elem, {});
        hammer.on('swiperight', e => this.prev());
    }

    prev() {
        this.router.navigateBack();
    }

    detached() {
        this.fabMenuEl.style.display = this.initialDisplay;
    }

    updateActiveStep() {
        this.stepsLength = 4;
        switch (this.router.currentInstruction.fragment) {
            case '':
                this.activeStep = 0;
                break;
            case 'card':
                this.activeStep = 2;
                this.stepsLength = 4;
                break;
            case 'express':
                this.activeStep = 1;
                this.stepsLength = 2;
                break;
            case 'appointment':
                this.activeStep = 1;
                break;
            case 'review':
                this.activeStep = 3;
                break;
        }
    }

    getTitle() {
        switch (this.router.currentInstruction.fragment) {
            case 'type':
                return "Checkout Process"
            case 'customer':
                return "Select Customer"
            case 'services':
                return "Select Services"
            case 'review':
                return "Order Review"
        }
    }

    configureRouter(config: RouterConfiguration, router: Router) {
        config.map([
            { route: ['', '/type'], name: 'app.checkout.type', moduleId: 'containers/checkout/type/type', title: 'Type' },
            { route: ['/appointment'], name: 'app.checkout.appointment', moduleId: 'containers/checkout/appointment/appointment', title: 'Appointment' },
            { route: ['/express'], name: 'app.checkout.express', moduleId: 'containers/checkout/express/express', title: 'Express' },
            { route: ['/review'], name: 'app.checkout.review', moduleId: 'containers/checkout/review/review', title: 'Review' },
            { route: ['/card'], name: 'app.checkout.card', moduleId: 'containers/checkout/card/card', title: 'Card' },
        ]);
        this.router = router;
        if (!this.company.merchant_integrated) {
            router.navigateToRoute('app.merchant.onboarding');
        }
    }
}
