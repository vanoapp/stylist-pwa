import { ProfessionalService } from './../../../stores/professional/service';
import { Professional } from './../../../stores/professional/model';
// Libs
import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

// Models
import { Checkout } from '../../../stores/checkout/model';
import { capitalize } from '../../../helpers/utils';

@autoinject
export class Type {
  checkout: Checkout;
  authProfessional: Professional;
  professionals: Professional[];
  menuOpen: boolean;
  dropdownText: string = 'Select a Different Professional';

  constructor(private router: Router, checkout: Checkout, private professionalService: ProfessionalService) {
    this.checkout = checkout;
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.handleWindowClick = this.handleWindowClick.bind(this);
  }

  attached() {
    this.checkout.professional = this.authProfessional;
    if(!this.authProfessional.permission.manage_checkouts) return;
    window.addEventListener('click', this.handleWindowClick);

    this.professionalService.getCompanyProfessionals(this.authProfessional.company_id).then(resp => {
      if (resp.statusCode === 200) {
        const ps: Professional[] = resp.content.professionals;
        this.professionals = ps.filter(p => {
          if (p.independent && p.merchant_integrated) return true
          return false
        })

      }
    })
  }
  
  detached() {
    window.removeEventListener('click', this.handleWindowClick);
  }

  handleWindowClick(e) {
    if (e.target.classList.contains('vano-menu-dont-close')) return;
    this.menuOpen = false;
  }

  selectProfessional(p: Professional) {
    this.checkout.professional = p;
    this.dropdownText = this.fullName(p)
    this.menuOpen = false;
  }

  fullName(p: Professional): string {
    return capitalize(`${p.first_name} ${p.last_name}`)
  }

  select(type: string) {
    this.checkout.type = type;
    sessionStorage.setItem('checkout', JSON.stringify(this.checkout));

    switch (type) {
      case 'appointment':
        this.router.navigate('/checkout/appointment');
        return;
      case 'express':
        this.router.navigate('/checkout/express');
        return;
    }
  }
}
