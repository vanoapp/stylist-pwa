// libs
import { autoinject } from 'aurelia-framework';
import { ValidationRules, ValidationController, ValidationControllerFactory } from 'aurelia-validation';
import { Router } from 'aurelia-router';

// Models
import { Card as CardModel } from '../../../stores/billing/model';
import { Checkout } from '../../../stores/checkout/model';

// Services
import { BillingService } from '../../../stores/billing/service';

// Third Party
import * as creditcardutils from 'creditcardutils';

// First Party
import {VanoValidationRenderer} from '../../../validation/vano-renderer';

@autoinject
export class Card {
    cards: CardModel[];
    router: Router;
    checkout: Checkout;
    selectedCard: CardModel;
    addingCard: boolean = false;
    cardNumber: string;
    cardHolder: string;
    cardExp: string;
    cardCvv: string;
    controller: ValidationController;
    saveCard: boolean = true;
    isFetching: boolean;

    constructor(private billingService: BillingService, router: Router, controller: ValidationControllerFactory) {
        this.router = router;
        this.controller = controller.createForCurrentScope();
        this.controller.addRenderer(new VanoValidationRenderer());
    }

    activate() {
        this.checkout = JSON.parse(sessionStorage.getItem('checkout'));
        if (!this.checkout.appointment) {
            this.router.navigateToRoute('app.checkout.appointment');
        }
        return this.getCustomerCards();
    }

    attached() {   
        if(this.cards.length > 0  && this.checkout.card) {
            const checkoutCard = this.checkout.card.id;
            const selectedCard = this.cards.filter(item => item.id === checkoutCard)[0];
            this.toggleSelected(selectedCard);
        }
        
    }

    toggleSaveCard(){
        this.saveCard = !this.saveCard;
    }

    validate() {
        this.controller.validate().then(result => {
            if (result.valid) {
                this.createCard();
            }
        })
    }

    getCustomerCards() {
        return this.billingService.getCustomerCards(this.checkout.appointment.customer_id).then(resp => {
            if (resp.statusCode === 200) {
                this.cards = resp.content.cards;
            }
        })
    }

    toggleSelected(c: CardModel) {
        c.is_selected = !c.is_selected;

        this.clearSelected(c);
        this.removeSelectedCard();

        if (c.is_selected) {
            this.setSelectedCard(c);
        }
    }

    setSelectedCard(c) {
        this.selectedCard = c;
        this.checkout.card = c;
        this.checkout.saveCard = this.saveCard;
        sessionStorage.setItem('checkout', JSON.stringify(this.checkout));
    }

    removeSelectedCard() {
        this.selectedCard = null;
        this.checkout.card = null;
        this.checkout.saveCard = null;

        sessionStorage.setItem('checkout', JSON.stringify(this.checkout));
    }

    clearSelected(sc: CardModel) {
        this.cards.map(c => {
            if (sc.id != c.id) {
                c.is_selected = false;
            }
            return c;
        });
    }

    createCard() {
        this.isFetching = true;
        const cardNumber = this.cardNumber.replace(/ /g, '');
        const cardExp = this.cardExp.replace(/ /g, '');
        const method = creditcardutils.parseCardType(this.cardNumber);

        this.billingService.createCard(this.checkout.appointment.customer_id, this.cardHolder, cardNumber, cardExp, method).then(resp => {
          this.isFetching = false;
            if (resp.statusCode === 201) {
                const c = resp.content.card;
                this.cards.push(c);
                this.toggleSelected(c);
                this.next();
                this.addingCard = false;
            }
        }).catch(err => {
          this.isFetching = false;
          if (err.statusCode === 400) {
              swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
              swal("This is emberassing!", "Could not process request at this time", "error");
          } else if (err.statusCode === 403) {
              swal("Oops!", "You do not have permission to do that!", "error");
          }
      });
    }

    next() {
        this.router.navigateToRoute('app.checkout.review');
    }

}


ValidationRules
    .ensure('cardHolder').displayName('name on card').required()
    .ensure('cardNumber').displayName('card number').required().satisfies((cardNumber: string) => {
        if(cardNumber === '4111 1111 1111 1111') return true;
        return creditcardutils.validateCardNumber(cardNumber)
    }).withMessage("invalid card number")
    .ensure('cardExp').displayName('expiration').required().satisfies((cardExp: string) => {
        if (cardExp) {
            return creditcardutils.validateCardExpiry(cardExp.split('/')[0], cardExp.split('/')[1])
        }
        return false;
    }).withMessage("invalid expiration")
    .ensure('cardCvv').displayName('cvv').required().satisfies((cardCvv: string) => {
        return creditcardutils.validateCardCVC(cardCvv)
    }).withMessage("invalid cvv")
    .on(Card)
