// Libs
import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

// Services
import { AppointmentService } from '../../../stores/appointment/service';
import { CustomerService } from '../../../stores/customer/service';

// Models
import { Professional } from '../../../stores/professional/model';
import { Customer } from '../../../stores/customer/model';
import { Checkout } from '../../../stores/checkout/model';
import { Appointment as AppointmentModel } from '../../../stores/appointment/model';

// Third Party
import * as swal from 'sweetalert';
import * as Hammer from 'hammerjs';

// First Party
import { inArray } from '../../../helpers/utils';

@autoinject
export class Appointment {
    authProfessional: Professional;
    appointments: AppointmentModel[];
    customers: Customer[];
    router: Router;
    selectedAppointment: AppointmentModel;
    checkout: Checkout;

    constructor(private appointmentService: AppointmentService, private customerService: CustomerService, router: Router) {
        this.router = router;
        this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    }

    activate() {
        if(!JSON.parse(sessionStorage.getItem('checkout'))) {
            this.router.navigate('/checkout/type');
            return;
        }

        this.checkout = JSON.parse(sessionStorage.getItem('checkout'));

        return this.getProfessionalAppointments().then(() => {
            return this.getCompanyCustomers();
        }).catch(err => {
            if (err.statusCode === 400) {
                swal("Oops!", err.content.error, "error");
            } else if (err.statusCode === 500) {
                swal("This is emberassing!", "Could not process request at this time", "error");
            } else if (err.statusCode === 403) {
                swal("Oops!", "You do not have permission to do that!", "error");
            }
        });
    }

    getProfessionalAppointments() {
        let now = Math.round(Date.now() / 1000);
        const twoHours = 60 * (10 * 60);
        const tenHours = 60 * (100 * 60);
        const startTime = now - twoHours;
        const endTime = now + tenHours;

        return this.appointmentService.listProfessionalAppointments(this.checkout.professional.id, startTime.toString(), endTime.toString(), '3').then(resp => {
            if (resp.statusCode === 200) {
                this.appointments = resp.content.appointments.map(a => {
                    if(this.checkout.appointment && this.checkout.appointment.id === a.id) {
                        a.is_selected = true;
                        this.selectedAppointment = a;
                    }
                    return a;
                });
            }
        });
    }

    getCompanyCustomers(): Promise<any> {
      this.customers = [];
      const appsLen = this.appointments.length;
      if(appsLen === 0) return Promise.resolve();
      let appsIndex = 0;
      return new Promise((resolve, reject) => {
        this.appointments.map(a => {
          this.customerService.get(a.customer_id).then(resp => {
            if(resp.statusCode === 200) {
              this.customers.push(resp.content.customer);
              appsIndex++;
              if(appsIndex === appsLen) {
                resolve();
              }
            }
          }).catch(err => {
            reject(err)
          });
        });
      })
    }

    customerIdToCustomer(id: string): Customer {
        const customerLen = this.customers.length;
        for(let i = 0; i < customerLen; i++) {
            const customer = this.customers[i];
            if(id === customer.id) {
                return customer
            }
        }
    }

    customerIdToFullName(id: string): string {
        const customerLen = this.customers.length;
        for(let i = 0; i < customerLen; i++) {
            const customer = this.customers[i];
            if(id === customer.id) {
                return `${customer.first_name} ${customer.last_name}`;
            }
        }
        return '';
    }

    toggleSelected(a: AppointmentModel) {
        a.is_selected = !a.is_selected;

        this.clearSelected(a);
        this.removeSelectedAppointment();

        if (a.is_selected) {
            this.setSelectedAppointment(a);
            return;
        }
    }

    setSelectedAppointment(a: AppointmentModel) {
        this.selectedAppointment = a;
        this.checkout.appointment = a;
        this.checkout.customer = this.customerIdToCustomer(a.customer_id);
        sessionStorage.setItem('checkout', JSON.stringify(this.checkout));
    }

    removeSelectedAppointment() {
        this.selectedAppointment = null;
        this.checkout.customer = null;
        this.checkout.appointment = null;

        sessionStorage.setItem('checkout', JSON.stringify(this.checkout));
    }

    clearSelected(a: AppointmentModel) {
        this.appointments.map(ma => {
            if (a.id != ma.id) {
                ma.is_selected = false;
            }
            return ma;
        });
    }

    next() {
        this.router.navigateToRoute('app.checkout.card')
    }
}
