import { Professional } from './../../stores/professional/model';
// Aurelia
import { autoinject,observable } from 'aurelia-framework';
import { Router } from 'aurelia-router';

// Services
import { CustomerService } from '../../stores/customer/service';
import { AppointmentService } from '../../stores/appointment/service';

// Models
import { Customer, CustomerNote, Card } from '../../stores/customer/model';
import { Appointment } from '../../stores/appointment/model';

// Third Party
import * as swal from 'sweetalert';
import * as moment from 'moment';

// First Party
import {capitalize} from '../../helpers/utils';

@autoinject
export class CustomerDetail {
    customer: Customer;
    notes: CustomerNote[] = [];
    recentAppointments: Appointment[] = [];
    cards: Card[];
    router: Router;
    authProfessional: Professional;
    @observable noteValue: string;

    constructor(router: Router, private customerService: CustomerService, private appointmentService: AppointmentService) {
      this.authProfessional = JSON.parse(localStorage.getItem('professional'));
      this.router = router;
      this.handleDeleteCC = this.handleDeleteCC.bind(this);
      this.handleUpdateCustomerNote = this.handleUpdateCustomerNote.bind(this);
      this.handleDeleteCustomerNote = this.handleDeleteCustomerNote.bind(this);
    }

    activate(params) {
        return this.getCustomer(params.id).then(_ => {
            return this.getCustomerAppointments().then(() => {
              return this.getCustomerCards();
            });
        }).catch(err => {
            if (err.statusCode === 400) {
                swal("Oops!", err.content.error, "error");
            } else if (err.statusCode === 500) {
                swal("This is emberassing!", "Could not process request at this time", "error");
            } else if (err.statusCode === 403) {
                swal("Oops!", "You do not have permission to do that!", "error");
            }
        });
    }

    get feature() {
      return {
        merchant_services: this.authProfessional.features.filter(i => i.name === 'credit_card_processing')[0].enabled,
        manage_employees: this.authProfessional.features.filter(i => i.name === 'manage_professionals')[0].enabled,
        require_credit_card: this.authProfessional.features.filter(i => i.name === 'require_credit_card')[0].enabled,
        clock_in: this.authProfessional.features.filter(i => i.name === 'clock_in_clock_out')[0].enabled,
        customer_service_times: this.authProfessional.features.filter(i => i.name === 'client_service_times')[0].enabled,
      }
    }

    attached() {
      window.addEventListener('vano-delete-cc', this.handleDeleteCC);
      window.addEventListener('vano-edit-customer-note', this.handleUpdateCustomerNote)
      window.addEventListener('vano-delete-customer-note', this.handleDeleteCustomerNote)
    }
  
    detached() {
      window.removeEventListener('vano-delete-cc', this.handleDeleteCC);
      window.removeEventListener('vano-edit-customer-note', this.handleUpdateCustomerNote)
      window.removeEventListener('vano-delete-customer-note', this.handleDeleteCustomerNote)
    }

    handleDeleteCustomerNote(e) {
      const id = e.detail.id;
      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this note",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
        () => {
          this.customerService.deleteNote(id).then(resp => {
            if(resp.statusCode === 200) {
              swal('Awesome!', 'Note was deleted sucessfully', 'success');
              this.notes = this.notes.filter(n => n.id !== id);
            }
          }).catch(err => {
            if (err.statusCode === 400) {
              swal("Oops!", err.content.error, "error");
            } else if (err.statusCode === 500) {
              swal("This is emberassing!", "Could not process request at this time", "error");
            } else if (err.statusCode === 403) {
              swal("Oops!", "You do not have permission to do that!", "error");
            }
          });
        });
    }

    handleUpdateCustomerNote(e) {
      const id = e.detail.id;
      const value = e.detail.value;
  
      this.customerService.updateNote(id, value).then(resp => {
        if(resp.statusCode === 200) {
          swal('Awesome!', 'Note was updated sucessfully', 'success');
        }
      }).catch(err => {
        if (err.statusCode === 400) {
          swal("Oops!", err.content.error, "error");
        } else if (err.statusCode === 500) {
          swal("This is emberassing!", "Could not process request at this time", "error");
        } else if (err.statusCode === 403) {
          swal("Oops!", "You do not have permission to do that!", "error");
        }
      });
    }

    noteValueChanged() {
      if (!this.noteValue) return;
      this.customerService.createNote(this.customer.id, this.authProfessional.company_id, this.noteValue).then(resp => {
        if (resp.statusCode === 201) {
          swal("Awesome!", "Note was created!", "success");
          this.notes.push(resp.content.note);
        }
      }).catch(err => {
        if (err.statusCode === 400) {
          swal("Oops!", err.content.error, "error");
        } else if (err.statusCode === 500) {
          swal("This is emberassing!", "Could not process request at this time", "error");
        }
      });
    }

    getCustomerCards() {
      return this.customerService.getCards(this.customer.id).then(resp => {
        if (resp.statusCode === 200) {
          const c = resp.content.cards;
          this.cards = c.length > 0 ? c : [];
        }
      });
    }

    updateCustomer() {
      this.router.navigate(`/customers/${this.customer.id}/update`);
    }
  
    handleDeleteCC(e) {
      const id = e.detail.id;
      let self = this;
      swal({
        title: "Are you sure?",
        text: "You will not be able to recover this card",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        closeOnConfirm: false
      },
        () => {
          self.customerService.deleteCard(id).then(resp => {
            if(resp.statusCode === 200) {
              self.cards = self.cards.filter(item => item.id !== id);
              swal('Awesome!', 'Card was deleted', 'success');
            }
          }).catch(err => {
            if (err.statusCode === 400) {
              swal("Oops!", err.content.error, "error");
            } else if (err.statusCode === 500) {
              swal("This is emberassing!", "Could not process request at this time", "error");
            } else if (err.statusCode === 403) {
              swal("Oops!", "You do not have permission to do that!", "error");
            }
          });
        });
    }

    getCustomer(id: string) {
        return this.customerService.get(id).then(resp => {
            if (resp.statusCode === 200) {
                this.customer = resp.content.customer;
                if(this.customer.notes) {
                    this.notes = this.customer.notes.filter(n => n.company_id === this.authProfessional.company_id);
                }
            }
        });
    }

    getCustomerAppointments() {
        const thirtyDaysAgo = moment().subtract('day', 30).unix().toString();
        const thirtyDaysFromToday = moment().add('day', 30).unix().toString();
        return this.appointmentService.listCustomerAppointments(this.customer.id, thirtyDaysAgo, thirtyDaysFromToday, '10').then(resp => {
            if(resp.statusCode === 200) {
                this.recentAppointments = resp.content.appointments.filter((a: Appointment) => a.professional_id === this.authProfessional.id);
            }
        })
    }

    get fullName(): string {
        return `${this.customer.first_name} ${this.customer.last_name}`;
    }
}
