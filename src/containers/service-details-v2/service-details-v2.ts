import { autoinject } from 'aurelia-dependency-injection';
import { ServiceService } from './../../stores/service/service';
import { Professional } from './../../stores/professional/model';
import { Service, UpdateService } from '../../stores/service/model';
import { Router } from 'aurelia-router';

import toastr from '../../libs/toastr/toastr';

// Third Party
import * as swal from 'sweetalert';
import * as VMasker from 'vanilla-masker';
import { VanoValidationRenderer } from '../../validation/vano-renderer';
import { ValidationController, ValidationControllerFactory } from 'aurelia-validation';
import { randomClass } from '../../helpers/utils';

@autoinject
export class ServiceDetailsV2{
  authProfessional: Professional;
  service: Service;
  router: Router;
  serviceName: string;
  steps: Service[];
  serviceDescription: string;
  servicePrice: string;
  serviceLength: string;
  requiresConsultation: boolean;
  stepName: string;
  stepIsGap: boolean;
  stepLength: string;
  isFetching: boolean;
  modalOpened: boolean;
  controller: ValidationController;
  editingStepName: string;
  editingStepDescription: string;
  editingStepLength: string;
  editingStepGap: boolean;
  editingStepId: string;

  constructor(private serviceService: ServiceService, router: Router, controller: ValidationControllerFactory){
    this.router = router;
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.controller = controller.createForCurrentScope();
    this.controller.addRenderer(new VanoValidationRenderer());
  }

  activate(params) {
    return this.getService(params.id).then(() => {
      this.setUpdateableProps();
    });
  }

  attached() {
    VMasker(document.querySelector(".price-input").querySelector('input')).maskMoney({
      separator: '.',
      unit: '$'
    });
  }

  setUpdateableProps() {
    this.serviceName = this.service.name;
    this.serviceDescription = this.service.description;
    this.servicePrice = this.service.price.toString();
    this.serviceLength = this.service.length.toString();
    this.requiresConsultation = this.service.require_phone_call;
    this.steps = this.service.steps;
  }

  addStep() {
    this.controller.reset();
    let hasErr = false;
    if (!this.stepName || this.stepName == '') {
      this.controller.addError('step name is required', this, 'stepName')
      hasErr = true
    }
    if (!this.stepLength || this.stepLength == '') {
      this.controller.addError('step length is required', this, 'stepLength')
      hasErr = true
    }
    if(this.stepLength && this.stepLength.replace(/\D/g,'') === ''){
      this.controller.addError('step length must be a number in minutes', this, 'stepLength')
      hasErr = true
    }
    if (hasErr) return

    let step = new Service;
    step.length = parseInt(this.stepLength);
    step.name = this.stepName;
    step.is_gap = this.stepIsGap ? true : false;
    step.id = randomClass();
    this.steps = this.steps.concat(step);
    this.stepName = '';
    this.stepLength = '';
    this.stepIsGap = false;
    toastr.success('Added step sucessfully');
  }

  updateStep() {
    let hasErr = false;
    if (!this.editingStepName || this.editingStepName == '') {
      this.controller.addError('step name is required', this, 'editingStepName')
      hasErr = true
    }
    if (!this.editingStepLength || this.editingStepLength == '') {
      this.controller.addError('step length is required', this, 'editingStepLength')
      hasErr = true
    }
    if (hasErr) return

    const step = this.steps.find(s => s.id === this.editingStepId);
    if(!step) return;
    step.name = this.editingStepName;
    step.length = parseInt(this.editingStepLength);
    step.is_gap = this.editingStepGap;
    this.modalOpened = false;
    toastr.success('Step updated successfully!');
  }

  removeStep(id: string) {
    this.steps = this.steps.filter(s => s.id !== id);
    this.modalOpened = false;
    toastr.success('Step removed successfully');
  }

  edit(step: Service) {
    this.editingStepGap = step.is_gap;
    this.editingStepLength = step.length.toString();
    this.editingStepName = step.name;
    this.editingStepId = step.id;
    this.modalOpened = true;
  }

  validate() {
    let hasErr = false;
    switch (true) {
      case !this.serviceName || this.serviceName == '':
        this.controller.addError('service name is required', this, 'serviceName')
        hasErr = true
      case !this.servicePrice || this.servicePrice == '':
        this.controller.addError('service price is required', this, 'servicePrice')
        hasErr = true
    }
    if(hasErr) return
    this.update();
  }

  update() {
    let us = new UpdateService();

    let price = this.servicePrice
    if(this.servicePrice.includes('$')) {
      price = this.servicePrice;
      price = price.split('$ ')[1];
      price = price.replace('.', '');
    }

    let totalSvcLen = parseInt(this.serviceLength);
    if(this.steps && this.steps.length > 0) {
      totalSvcLen = 0;
      this.steps.map(s => {
        totalSvcLen += s.length;
      })
    }

    us.id = this.service.id;
    us.professional_id = this.service.professional_id;
    us.description = this.serviceDescription;
    us.name = this.serviceName;
    us.price = parseInt(price);
    us.length = totalSvcLen;
    us.require_phone_call = this.requiresConsultation;

    this.serviceService.update(us).then(resp => {
      swal("Awesome!", "Service was updated!", "success");
      let sLen = this.authProfessional.services.length;
      for (let i = 0; i < sLen; i++) {
        if (this.authProfessional.services[i].id == this.service.id) {
          this.authProfessional.services[i] = resp.content.service;
          localStorage.setItem('professional', JSON.stringify(this.authProfessional));
          break;
        }
      }
      this.router.navigateBack();
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal('could not process the request at this time');
      }
    });
  }

  delete() {
    let self = this;
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this service",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
       () => {
        self.serviceService.delete(self.service.id).then(resp => {
          swal("Deleted!", "Your appointment has been deleted.", "success");
          self.router.navigateBack();
          self.authProfessional.services = self.authProfessional.services.filter(item => item.id != self.service.id);
          localStorage.setItem('professional', JSON.stringify(self.authProfessional))
        }).catch(err => {
          if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
            swal('could not process the request at this time');
          }
        });
      });
  }

  getService(id: string) {
    return this.serviceService.get(id).then(resp => {
      if(resp.statusCode === 200) {
        this.service = resp.content.service;
      }
    })
  }
}
