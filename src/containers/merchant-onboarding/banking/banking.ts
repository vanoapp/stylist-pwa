// Aurelia
import { bindable, autoinject } from 'aurelia-framework';
import { ValidationController, ValidationControllerFactory, ValidationRules } from 'aurelia-validation';

// First Party
import { VanoValidationRenderer } from '../../../validation/vano-renderer';
import { validRoutingNumber } from '../../../helpers/utils';

@autoinject
export class Banking {
    @bindable state: string;
    @bindable isFetching: boolean;
    controller: ValidationController;
    accountType: string = 'checking';
    accountNumber: string;
    routingNumber: string;

    constructor(controller: ValidationControllerFactory) {
        this.controller = controller.createForCurrentScope();
        this.controller.addRenderer(new VanoValidationRenderer());
    }

    validate() {
        this.controller.validate().then(res => {
            if (res.valid) {
                this.continue();
            }
        })
    }

    continue() {
        window.dispatchEvent(new CustomEvent('vano-merchant-onboarding-next-click', {
            detail: {
                step: 'banking', payload: {
                    account_type: this.accountType,
                    account_number: this.accountNumber,
                    routing_number: this.routingNumber,
                }
            }
        }));
    }
}

ValidationRules
    .ensure('accountType').displayName('account type').required()
    .ensure('accountNumber').displayName('account number').required()
    .ensure('routingNumber').displayName('routing number').satisfies((routing: string) => {
        if(!routing) return false;
        return validRoutingNumber(routing);
    }).required()
    .on(Banking)