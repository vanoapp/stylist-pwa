// libs
import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

// Models
import { Professional } from '../../../stores/professional/model';


@autoinject
export class Congratulations {
    authProfessional: Professional;
    fromOnboarding: boolean = false;
    router: Router;

    constructor(router: Router) {
        this.router = router;
        this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    }

    activate() {
        sessionStorage.removeItem('merchant-personal');

        if (sessionStorage.getItem('onboarding') && sessionStorage.getItem('onboarding') === 'true') {
            this.fromOnboarding = true
        }
    }

    complete(route: string) {
        sessionStorage.removeItem('onboarding');
        this.router.navigate(`/${route}`);
    }
}