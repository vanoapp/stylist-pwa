// Aurelia
import { autoinject } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';

// Models
import { Professional } from '../../stores/professional/model';
import { Company } from '../../stores/company/model';
import { NewMerchant } from '../../stores/merchant/model';

// Services
import { CompanyService } from '../../stores/company/service';
import { MerchantService } from '../../stores/merchant/service';

// Third Party
import * as swal from 'sweetalert';

@autoinject
export class MerchantOnboarding {
  authProfessional: Professional;
  authCompany: Company;
  infoState: string = 'active';
  personalState: string = 'disabled';
  bankingState: string = 'disabled';
  annualCCSales: string;
  ein: string;
  ssn: string;
  indenType: string;
  dlNumber: string;
  dlState: string;
  accountType: string;
  accountNumber: string;
  routingNumber: string;
  fabMenuEl = undefined;
  fabMenuInitDisplay: string;
  isFetching: boolean = false;
  router: Router;

  constructor(private merchantService: MerchantService, router: Router) {
    this.router = router;
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.authCompany = JSON.parse(localStorage.getItem('company'));
    this.handleNextClick = this.handleNextClick.bind(this);
  }

  attached() {
    window.addEventListener('vano-merchant-onboarding-next-click', this.handleNextClick);
    this.fabMenuEl = document.querySelector('.vano-fab-menu-component');
    this.fabMenuInitDisplay = this.fabMenuEl.style.display;
    this.fabMenuEl.style.display = 'none';
  }

  detached() {
    this.fabMenuEl.style.display = this.fabMenuInitDisplay;
    window.removeEventListener('vano-merchant-onboarding-next-click', this.handleNextClick);
  }

  handleNextClick(e) {
    const payload = e.detail.payload;
    switch (e.detail.step) {
      case 'info':
        this.infoState = 'completed';
        this.personalState = 'active';
        this.annualCCSales = payload.annual_cc_sales;
        break;
      case 'personal':
        this.personalState = 'completed';
        this.bankingState = 'active';
        this.ein = payload.ein;
        this.ssn = payload.ssn;
        this.indenType = payload.iden_type;
        this.dlNumber = payload.dl_number;
        this.dlState = payload.dl_state;
        break;
      case 'banking':
        this.personalState = 'completed';
        this.accountType = payload.account_type;
        this.accountNumber = payload.account_number;
        this.routingNumber = payload.routing_number;
        this.complete();
        break;
    }
  }

  complete() {
    if (this.isFetching) return
    this.isFetching = true;
    let newMerchant = new NewMerchant();
    newMerchant.account_number = this.accountNumber;
    newMerchant.company_id = this.authProfessional.company_id;
    newMerchant.routing_number = this.routingNumber;
    newMerchant.account_type = this.accountType;
    newMerchant.annual_cc_sales = parseInt(this.annualCCSales);
    if(this.authProfessional.independent){
      newMerchant.professional_id = this.authProfessional.id;
    }
    if (this.ein) {
      newMerchant.ein = this.ein.replace(/-/g, '');
    }
    if (this.ssn) {
      newMerchant.ssn = this.ssn.replace(/-/g, '');
    }
    newMerchant.dl_number = this.dlNumber;
    newMerchant.dl_state = this.dlState;

    this.merchantService.create(newMerchant).then(resp => {
      if (resp.statusCode === 201) {
        this.isFetching = false;
        window.dispatchEvent(new CustomEvent('vano-merchant-onboarded'));
        this.router.navigateToRoute('app.checkout');
      }
    }).catch(err => {
      this.isFetching = false;
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }

}
