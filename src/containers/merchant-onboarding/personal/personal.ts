// Aurelia
import { autoinject, bindable } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { ValidationController, ValidationControllerFactory, ValidationRules } from 'aurelia-validation';

// First Party
import { VanoValidationRenderer } from '../../../validation/vano-renderer';

// Models
import { Professional } from '../../../stores/professional/model';

@autoinject
export class Personal {
    authProfessional: Professional;
    @bindable state: string;
    indentificationType: string = 'ein';
    dlState: string;
    dlNumber: string;
    ein: string;
    ssn: string;
    controller: ValidationController;

    constructor(controller: ValidationControllerFactory) {
        this.authProfessional = JSON.parse(localStorage.getItem('professional'));
        this.controller = controller.createForCurrentScope();
        this.controller.addRenderer(new VanoValidationRenderer());
    }

    validate() {
        this.controller.validate().then(res => {
            if (res.valid) {
                this.continue();
            }
        })
    }

    continue() {
        window.dispatchEvent(new CustomEvent('vano-merchant-onboarding-next-click', {
            detail: {
                step: 'personal',
                payload: {
                    ssn: this.ssn,
                    ein: this.ein,
                    iden_type: this.indentificationType,
                    dl_number: this.dlNumber,
                    dl_state: this.dlState,
                }
            }
        }));
    }
}

ValidationRules
    .ensure('ssn').displayName('SSN').satisfies((ssn: string) => {
        const trimmed = ssn.replace(/-/g, '');
        if(trimmed.length !== 9) return false
        return true
    }).required()
    .ensure('ein').displayName('EIN').satisfies((ssn: string) => {
        const trimmed = ssn.replace(/-/g, '');
        if(trimmed.length !== 9) return false
        return true
    }).required()
    .ensure('dlNumber').displayName('Driver license number').required()
    .ensure('dlState').displayName('Driver license state').required()
    .on(Personal)
