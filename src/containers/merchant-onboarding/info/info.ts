// Aurelia
import { bindable, autoinject } from 'aurelia-framework';
import { ValidationController, ValidationControllerFactory, ValidationRules } from 'aurelia-validation';

// First Party
import { VanoValidationRenderer } from '../../../validation/vano-renderer';

@autoinject
export class Info {
    @bindable state: string;
    options;
    annualCCSales: string;
    controller: ValidationController;

    constructor(controller: ValidationControllerFactory) {
        this.generateOptions();
        this.controller = controller.createForCurrentScope();
        this.controller.addRenderer(new VanoValidationRenderer());
    }

    continue() {
        this.controller.validate().then(res => {
            if (res.valid) {
                window.dispatchEvent(new CustomEvent('vano-merchant-onboarding-next-click', {
                    detail: {
                        step: 'info', payload: {
                            annual_cc_sales: this.annualCCSales
                        }
                    }
                }));
            }
        })
    }

    generateOptions() {
        this.options = [
            { label: '$0 - $10,000', value: '1000000' },
            { label: '$10,000 - $25,000', value: '2500000' },
            { label: '$25,000 - $100,000', value: '10000000' },
            { label: '$100,000 - $250,000', value: '25000000' },
            { label: '$250,000 - $750,000', value: '75000000' },
            { label: 'over $1,000,000', value: '100000000' },
        ]
    }
}

ValidationRules
    .ensure('annualCCSales').displayName('annual sales').required()
    .on(Info)