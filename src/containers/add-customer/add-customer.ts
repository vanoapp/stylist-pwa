// Aurelia
import { inject, NewInstance } from 'aurelia-framework';
import { ValidationRules, ValidationController } from 'aurelia-validation';
import { Router } from 'aurelia-router';

// Third Party
import * as swal from 'sweetalert';

// First Party
import { VanoValidationRenderer } from '../../validation/vano-renderer';
import { capitalize, inArray } from '../../helpers/utils';

// Services
import { CustomerService } from '../../stores/customer/service';

// Models
import { NewCustomer, Customer } from '../../stores/customer/model';
import { Professional } from '../../stores/professional/model';

@inject(CustomerService, Router, NewInstance.of(ValidationController))
export class AddCustomer {
  authProfessional: Professional;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  serverErr: string;
  controller: ValidationController;
  fromAppointment: boolean = false;
  redirect: string;
  displayExistingCustomer: boolean;
  existingCustomer: Customer;
  closeRoute: string;
  canvasTitle: string = 'New Client';
  router: Router;
  isFetching: boolean;
  isPrivate: boolean;

  constructor(private customerService: CustomerService, router: Router, controller: ValidationController) {
    this.router = router;
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.controller = controller;
    this.controller.addRenderer(new VanoValidationRenderer());
  }

  activate(params) {
    if (params.first_customer) {
      this.closeRoute = '/dashboard';
      this.canvasTitle = 'Create Your First Client!'
    }
  }

  handleKeyup(e) {
    if (e.which == 13 && !e.shiftKey) {
      e.preventDefault();
      this.validate();
    }
  }

  validate() {
    this.controller.validate().then(res => {
      if (res.valid) {
        this.create();
      }
    });
  }

  decline() {
    this.displayExistingCustomer = false;
    setTimeout(() => {
      if (this.existingCustomer.phone_number === this.phoneNumber.replace(/\D/g, '')) {
        this.controller.addError('phone number already taken', this, 'serverErr');
      }
      if (this.existingCustomer.email === this.email) {
        this.controller.addError('email address already taken', this, 'serverErr');
      }
    }, 100);
  }

  addCustomerToCompany() {
    this.isFetching = true;
    if (!this.isPrivate) {
      this.customerService.addCustomerToCompany(this.existingCustomer.id, this.authProfessional.company_id).then(resp => {
        this.isFetching = false;
        if (resp.statusCode === 201) {
          let name = `${this.existingCustomer.first_name} ${this.existingCustomer.last_name}`
          swal('Awesome!', `${capitalize(name)} was added to your client list`, 'success');
          this.router.navigate('/customers');
        }
      }).catch(err => {
        this.isFetching = false;
        if (err.statusCode === 400) {
          swal("Oops!", err.content.error, "error");
        } else if (err.statusCode === 500) {
          swal("This is emberassing!", "Could not process request at this time", "error");
        }
      });
      return;
    }

    this.customerService.addProfessionalCustomer(this.existingCustomer.id, this.authProfessional.id).then(resp => {
      this.isFetching = false;
      if (resp.statusCode === 201) {
        let name = `${this.existingCustomer.first_name} ${this.existingCustomer.last_name}`
        swal('Awesome!', `${capitalize(name)} was added to your client list`, 'success');
        this.router.navigate('/customers');
      }
    }).catch(err => {
      this.isFetching = false;
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  create() {
    this.isFetching = true;
    let newCustomer = new NewCustomer();
    newCustomer.first_name = this.firstName.toLowerCase();
    newCustomer.last_name = this.lastName.toLowerCase();
    newCustomer.email = this.email;
    newCustomer.phone_number = this.phoneNumber;
    newCustomer.company_id = this.authProfessional.company_id;
    if (this.isPrivate) {
      newCustomer.company_id = '';
      newCustomer.professional_id = this.authProfessional.id;
    }
    this.customerService.checkExists(newCustomer.email, newCustomer.phone_number).then(resp => {
      if (resp.statusCode === 200) {
        this.existingCustomer = resp.content.customer;
        if (this.existingCustomer.companies) {
          if (inArray(this.existingCustomer.companies, this.authProfessional.company_id)) {
            this.isFetching = false;
            this.controller.addError('Client is already part of your salon', this, 'serverErr');
            return;
          }
        }
        this.isFetching = false;
        this.displayExistingCustomer = true;
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        this.isFetching = false;
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        this.isFetching = false;
        swal('Oops!', 'could not process the request at this time');
      } else if (err.statusCode === 404) {
        this.customerService.create(newCustomer).then(resp => {
          this.isFetching = false;
          if (resp.statusCode === 201) {
            swal("Awesome!", "Client was created!", "success");
            if (this.redirect) {
              this.router.navigate(this.redirect);
              return;
            }
            this.router.navigateBack();
          }
        }).catch(err => {
          this.isFetching = false;
          if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
            swal('could not process the request at this time');
          }
        });;
      }
    });
  }

  closeCanvas() {
    this.router.navigateBack();
  }

}

ValidationRules
  .ensure('firstName').displayName('first name').required().withMessage(`\${$displayName} cannot be blank.`)
  .ensure('lastName').displayName('last name').required().withMessage(`\${$displayName} cannot be blank.`)
  .ensure('email').displayName('email').email().withMessage(`\${$displayName} cannot be blank.`)
  .ensure('phoneNumber').displayName('phone number').matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/).withMessageKey('invalid phone number').required().withMessage(`\${$displayName} cannot be blank.`)
  .on(AddCustomer);



  // focus on customer retiion to then close the salons. For example promote vano as a "directory" of salons that you can book any salon. 

  // Turn the client portal into a social media platform. Friends can share photos of cuts. Pintrest style haircuts inspirations. And more ideas to come.

  // Augmented reality pre seeing how haircuts could look
