import { CalendarOption } from './../../components/vano-calendar/vano-calendar';
import { Appointment } from './../../stores/appointment/model';
import { CustomerService } from './../../stores/customer/service';
import { AppointmentService } from './../../stores/appointment/service';
import { Professional } from './../../stores/professional/model';
import { ProfessionalService } from './../../stores/professional/service';
// Aurelia
import { autoinject, observable } from 'aurelia-framework';
import { Customer } from '../../stores/customer/model';

// Third Party
import * as moment from 'moment';
import { CalendarEvent } from '../../libs/calendarjs/calendar';
import { capitalize } from '../../helpers/utils';
import { Router } from 'aurelia-router';

@autoinject
export class AppointmentsV2{
  authProfessional: Professional;
  personalAppointments: Appointment[];
  currentEvents: CalendarEvent[];
  companyAppointments: Appointment[];
  companyEvents: CalendarEvent[];
  companyProfessionals: Professional[];
  isFetchingPersonalData: boolean;
  fetchedCompanyData: boolean;
  calOptions: CalendarOption[];
  router: Router;
  initialDate: number;

  constructor(private professionalService: ProfessionalService, private appointmentService: AppointmentService, private customerService: CustomerService, router: Router){
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.handleSelectProfessional = this.handleSelectProfessional.bind(this);
    this.calendayDayClick = this.calendayDayClick.bind(this);
    this.calendayEventClick = this.calendayEventClick.bind(this);
    this.router = router;
  }

  activate(params) {
    if(params.date) {
      this.initialDate = params.date;
    }
  }

  calendayDayClick(e) {
    let date = moment(e.detail.date).unix();
    let midnighttoday = moment().subtract(moment().second(), 'second').subtract(moment().hour(), 'hour').subtract(moment().minute(), 'minute').unix();
    if (date >= midnighttoday) {
      this.router.navigateToRoute('app.add.appointment', { date });
      return;
    }
    swal('Silly You!', 'You can\'t book an appointment for earlier than today.', 'error');
  }

  calendayEventClick(e) {
    let id = e.detail.event.id;
    this.router.navigateToRoute('app.appointment.details', { id });
  }

  removeEventListeners() {
    window.removeEventListener('vano-calendar-day-click', this.calendayDayClick);
    window.removeEventListener('vano-calendar-event-click', this.calendayEventClick);
  }

  attached() {
    this.isFetchingPersonalData = true;
    window.addEventListener('vano-calendar-selected-professional', this.handleSelectProfessional);
    window.addEventListener('vano-calendar-day-click', this.calendayDayClick);
    window.addEventListener('vano-calendar-event-click', this.calendayEventClick);
    this.getPersonalAppointments().then(() => {
      this.isFetchingPersonalData = false;
      this.currentEvents = this.transformToEvents(this.personalAppointments, true);
      if(this.authProfessional.permission.manage_appointments) {
        return this.getCompanyProfessionals().then(() => {
          if(this.companyProfessionals.length === 0) {
            this.companyEvents = this.currentEvents;
            return;
          }
          return this.getCompanyAppointments().then(() => {
            this.companyEvents = this.transformToEvents(this.companyAppointments);
            this.calOptions = this.genCalOptions()
          });
        })
      }
    });
  }

  detached() {
    window.removeEventListener('vano-calendar-selected-professional', this.handleSelectProfessional);
    this.removeEventListeners();
  }

  private handleSelectProfessional(e) {
    this.currentEvents = [];
    if(e.detail.value === 'all') {
      this.currentEvents = this.companyEvents;
      return;
    }
    this.currentEvents = this.companyEvents.filter(ev => ev.payload.professional_id === e.detail.value);
  }

  private getCompanyAppointments(): Promise<any> {
    const spLen = this.companyProfessionals.length;
    let requstMade: number = 0;
    return new Promise((resolve, reject) => {
      const threeMonthsAgo = moment().subtract(3, 'months').unix().toString();
      const threeMonthsAhead = moment().add(3, 'months').unix().toString();
      this.companyProfessionals.map(p => {
        this.appointmentService.listProfessionalAppointments(p.id, threeMonthsAgo, threeMonthsAhead, '250').then(resp => {
          requstMade++;
          if(resp.statusCode === 200) {
            if(!this.companyAppointments || this.companyAppointments === []) {
              this.companyAppointments = resp.content.appointments;
              if(requstMade === spLen) return resolve();
              return
            }
            this.companyAppointments = this.companyAppointments.concat(resp.content.appointments);
            if(requstMade === spLen) return resolve();
          }
        }).catch(err => {
          requstMade++;
        })
      })
    })
  }

  private genCalOptions(): CalendarOption[] {
    let opts = this.companyProfessionals.map(p => {
      return {
        label: capitalize(`${p.first_name} ${p.last_name}`),
        value: p.id,
      }
    });

    opts.unshift({
      label: 'All',
      value: 'all'
    })

    return opts;
  }

  private getCompanyProfessionals() {
    return this.professionalService.getCompanyProfessionals(this.authProfessional.company_id).then(resp => {
      if(resp.statusCode === 200) {
        this.companyProfessionals = resp.content.professionals;
      }
    })
  }

  private getPersonalAppointments() {
    const oneMonthsAgo = moment().subtract(1, 'months').unix().toString();
    const sixMonthsAhead = moment().add(6, 'months').unix().toString();
    return this.appointmentService.listProfessionalAppointments(this.authProfessional.id, oneMonthsAgo, sixMonthsAhead, '250').then(resp => {
      if(resp.statusCode === 200) {
        this.personalAppointments = resp.content.appointments.filter(a => !a.is_blockoff);
      }
    }).catch(err => {
      if(err.statusCode === 500){
        swal('Opps!', 'Sorry! We seem to be having some issues on our end. The app may or may not continue working the same. Refreshing the page may fix all your problems.')
      }
    });
  }

  private profIdToName(id: string): string {
    const p = this.companyProfessionals.filter(p => p.id === id)[0];
    return capitalize(`${p.first_name} ${p.last_name}`)
  }

  get authFullName(): string {
    return capitalize(`${this.authProfessional.first_name} ${this.authProfessional.last_name}`)
  }

  private transformToEvents(appointments: Appointment[], initial: boolean = false): CalendarEvent[] {
    if(!appointments) return [];
    return appointments.map(a => {
      let name = this.authFullName;
      if(!initial) {
        if(this.authProfessional.permission.manage_appointments && a.professional_id != this.authProfessional.id) {
          name = this.profIdToName(a.professional_id);
        }
      }
      let title = `Service Booking - ${name}`;
      if(a.services && a.services.length > 0) {
        let service = this.authProfessional.services.find(s => s.id === a.services[0].id);
        if(service) {
          title = `${service.name} - ${name}`;
        }
      }
      
      return {
        id: a.id,
        start: moment(a.start_time * 1000).unix(),
        end: moment(a.end_time * 1000).unix(),
        title: title,
        payload: {
          professional_id: a.professional_id,
        },
      }
    })
  }

}
