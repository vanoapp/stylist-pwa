import { Router } from 'aurelia-router';
// Aurelia
import { inject, NewInstance } from 'aurelia-framework';
import { ValidationRules, ValidationController } from 'aurelia-validation';

// Services
import { AuthService } from '../../stores/auth/service';

// First Party
import {VanoValidationRenderer} from '../../validation/vano-renderer';

// Third Party
import * as swal from 'sweetalert';

@inject(AuthService, NewInstance.of(ValidationController), Router)
export class Login {
    authenticator: string;
    password: string;
    controller: ValidationController;
    isFetching: boolean = false;
    showPwdInputTextType: boolean = false;
    errMsg: string;
    router: Router;

    constructor(private auth: AuthService, controller: ValidationController, router: Router) {
      this.router = router;
        this.controller = controller;
        this.controller.addRenderer(new VanoValidationRenderer());
    }

    login() {
        this.isFetching = true;
        var self = this;
        this.auth.login(this.authenticator.toLowerCase(), this.password).catch(err => {
            this.isFetching = false;
            if (err.statusCode === 400) {
                this.controller.addError('invalid credentials', this, 'errMsg')
            } else if (err.statusCode === 500) {
                swal("This is emberassing!", "Could not process request at this time", "error");
            } else if (err.statusCode === 403) {
                swal("Oops!", "You do not have permission to do that!", "error");
            }
        });
    }

    validate() {
        this.controller.validate().then(res => {
            if (res.valid) {
                this.login();
            }
        });
    }

    navigate(route: string): void {
      this.router.navigateToRoute(route);
    }
}

ValidationRules
    .ensure('authenticator').displayName('email or phone number').required().withMessage(`\${$displayName} cannot be blank.`)
    .ensure('password').displayName('password').required().withMessage(`\${$displayName} cannot be blank.`)
    .on(Login);
