// Aurelia
import { observable, autoinject } from 'aurelia-framework';

// Services
import { BillingService } from './../../../stores/billing/service';

// Models
import { Transaction } from './../../../stores/billing/model';
import { Professional } from './../../../stores/professional/model';

// Third Party
import * as moment from 'moment';
import * as swal from 'sweetalert';


const WEEK_TITLE = 'Total revenue past 7 days';
const MONTH_TITLE = 'Total revenue past 30 days';
const THREE_MONTH_TITLE = 'Total revenue past 3 months';
const SIX_MONTH_TITLE = 'Total revenue past 6 months';
const YEAR_TITLE = 'Total revenue past year';

@autoinject
export class VanoRevenueStat {
  authProfessional: Professional;
  @observable statView: string = 'month';
  statTitle: string = MONTH_TITLE;
  statSubTitle: string;
  isFetchingData: boolean;
  totalRevenue: number = 0.00;
  weekTransactions: Transaction[];
  monthTransactions: Transaction[];
  threeMonthTransactions: Transaction[];
  sixMonthTransactions: Transaction[];
  yearTransactions: Transaction[];

  constructor(private billingService: BillingService) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
  }

  attached() {
    this.isFetchingData = true;
    this.getMonthTransactions().then(() => {
      this.totalRevenue = this.calcTotalRevenue(this.monthTransactions);
      this.statSubTitle = this.genStatSubTitle();
      this.isFetchingData = false;
      this.getYearTransactions().then(() => {
        this.genAllTransactions();
      });
    }).catch(err => {
      this.isFetchingData = false;
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }

  statViewChanged(newVal: string, oldVal: string) {
    if (!oldVal) return;
    switch (this.statView) {
      case 'week':
        this.statTitle = WEEK_TITLE;
        this.statSubTitle = this.genStatSubTitle();
        this.totalRevenue = this.calcTotalRevenue(this.weekTransactions);
        break;
      case 'month':
        this.statTitle = MONTH_TITLE;
        this.statSubTitle = this.genStatSubTitle();
        this.totalRevenue = this.calcTotalRevenue(this.monthTransactions);
        break;
      case 'three-month':
        this.statTitle = THREE_MONTH_TITLE;
        this.statSubTitle = this.genStatSubTitle();
        this.totalRevenue = this.calcTotalRevenue(this.threeMonthTransactions);
        break;
      case 'six-month':
        this.statTitle = SIX_MONTH_TITLE;
        this.statSubTitle = this.genStatSubTitle();
        this.totalRevenue = this.calcTotalRevenue(this.sixMonthTransactions);
        break;
      case 'year':
        this.statTitle = YEAR_TITLE;
        this.statSubTitle = this.genStatSubTitle();
        this.totalRevenue = this.calcTotalRevenue(this.yearTransactions);
        break;
    }
  }

  calcTotalRevenue(transactions: Transaction[]): number {
    if (!transactions || transactions.length === 0) return 0.00;
    let total = 0.00;
    transactions.map(t => {
      total += t.total;
    });
    return total;
  }

  genStatSubTitle() {
    const tomorrow = moment().add(1, 'day').format("MMM Do");
    const tmrwYear = moment().add(1, 'day').format("YYYY");
    switch (this.statView) {
      case 'week':
        const weekAgo = moment().subtract(1, 'week').format("MMM Do");
        const weekAgoYear = moment().subtract(1, 'week').format("YYYY");
        if (weekAgoYear === tmrwYear) {
          return `${weekAgo} - ${tomorrow}, ${tmrwYear}`
        }
        return `${weekAgo} ${weekAgoYear} - ${tomorrow} ${tmrwYear}`
      case 'month':
        const monthAgo = moment().subtract(1, 'month').format("MMM Do")
        const monthAgoYear = moment().subtract(1, 'month').format("YYYY");
        if (monthAgoYear === tmrwYear) {
          return `${monthAgo} - ${tomorrow}, ${tmrwYear}`
        }
        return `${monthAgo} ${monthAgoYear} - ${tomorrow} ${tmrwYear}`
      case 'three-month':
        const threeMonthsAgo = moment().subtract(3, 'month').format("MMM Do")
        const threeMonthAgoYear = moment().subtract(3, 'month').format("YYYY");
        if (threeMonthAgoYear === tmrwYear) {
          return `${threeMonthsAgo} - ${tomorrow}, ${tmrwYear}`
        }
        return `${threeMonthsAgo} ${threeMonthAgoYear} - ${tomorrow} ${tmrwYear}`
      case 'six-month':
        const sixMonthsAgo = moment().subtract(6, 'month').format("MMM Do")
        const sixMonthsYear = moment().subtract(6, 'month').format("YYYY");
        if (sixMonthsYear === tmrwYear) {
          return `${sixMonthsAgo} - ${tomorrow}, ${tmrwYear}`
        }
        return `${sixMonthsAgo} ${sixMonthsYear} - ${tomorrow} ${tmrwYear}`
      case 'year':
        const yearAgo = moment().subtract(1, 'year').format("MMM Do")
        const yearAgoYear = moment().subtract(1, 'year').format("YYYY");
        if (yearAgoYear === tmrwYear) {
          return `${yearAgo} - ${tomorrow}, ${tmrwYear}`
        }
        return `${yearAgo} ${yearAgoYear} - ${tomorrow} ${tmrwYear}`
    }
  }

  getMonthTransactions() {
    const monthAgo = moment().subtract(1, 'month').unix();
    return this.billingService.getProfessionalTransactions(this.authProfessional.id, monthAgo).then(resp => {
      if (resp.statusCode === 200) {
        const t = resp.content.transactions;
        this.monthTransactions = t.length > 0 ? t.sort(this.sortTransactions) : [];
      }
    });
  }

  getYearTransactions() {
    const yearAgo = moment().subtract(1, 'year').unix();
    return this.billingService.getProfessionalTransactions(this.authProfessional.id, yearAgo).then(resp => {
      if (resp.statusCode === 200) {
        const t = resp.content.transactions;
        this.yearTransactions = t.length > 0 ? t.sort(this.sortTransactions) : [];
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }

  genAllTransactions() {
    if (this.yearTransactions.length === 0) {
      this.weekTransactions = [];
      this.monthTransactions = [];
      this.threeMonthTransactions = [];
      this.sixMonthTransactions = [];
      return;
    }

    this.weekTransactions = this.monthTransactions.filter(t => {
      const weekAgo = moment().subtract(1, 'week').unix();
      const today = moment().unix();
      const createdAt = moment(t.created_at).unix();
      if (createdAt > weekAgo && createdAt < today) return true
      return false
    });

    this.threeMonthTransactions = this.yearTransactions.filter(t => {
      const threeMonthsAgo = moment().subtract(90, 'days').unix();
      const today = moment().unix();
      const createdAt = moment(t.created_at).unix();
      if (createdAt > threeMonthsAgo && createdAt < today) return true
      return false
    });

    this.sixMonthTransactions = this.yearTransactions.filter(t => {
      const sixMonthsAgo = moment().subtract(180, 'days').unix();
      const today = moment().unix();
      const createdAt = moment(t.created_at).unix();
      if (createdAt > sixMonthsAgo && createdAt < today) return true
      return false
    });
  }

  sortTransactions(a, b) {
    if (a.created_at > b.created_at)
      return -1;
    if (a.created_at < b.created_at)
      return 1;
    return 0;
  }
}
