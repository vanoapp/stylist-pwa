// Aurelia
import { autoinject, observable } from 'aurelia-framework';
import { Router } from 'aurelia-router';

// Services
import { BillingService } from '../../../stores/billing/service';

// Models
import { Transaction } from '../../../stores/billing/model';
import { Professional } from './../../../stores/professional/model';

// Third Party
import * as swal from 'sweetalert';
import * as moment from 'moment';

@autoinject
export class VanoSalesGraph {
  authProfessional: Professional;
  weekTransactions: Transaction[];
  monthTransactions: Transaction[];
  threeMonthTransactions: Transaction[];
  sixMonthTransactions: Transaction[];
  yearTransactions: Transaction[];
  currentTransactions: Transaction[];
  @observable salesView: string = 'month';
  labels: string[] = [];
  data: number[] = [];
  isFetchingData: boolean;

  constructor(private billingService: BillingService) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
  }

  attached() {
    this.isFetchingData = true;
    this.getTransactions().then(() => {
      this.generateLabels();
      this.generateData();
      
      this.isFetchingData = false;
      this.generateYearTransactions().then(() => {
        this.genAllTransactions();
      });
    }).catch(err => {
      this.isFetchingData = false;
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    })
  }

  salesViewChanged(newView, oldView) {
    if (!oldView) return;
    this.generateLabels();
    this.generateData();
  }

  generateLabels() {
    switch (this.salesView) {
      case 'month':
        this.labels = this.genMonthLabels();
        break
      case 'week':
        this.labels = this.genWeekLabels();
        break
      case 'three-month':
        this.labels = this.genThreeMonthLabels();
        break
      case 'six-month':
        this.labels = this.genSixMonthLabels();
        break
      case 'year':
        this.labels = this.genYearLabels();
        break
    }
  }

  generateData() {
    let transactions: Transaction[];
    switch (this.salesView) {
      case 'week':
        transactions = this.weekTransactions;
        this.data = this.genWeekData(this.weekTransactions);
        break;
      case 'month':
        this.data = this.genMonthData(this.monthTransactions)
        break;
      case 'three-month':
        this.data = this.genThreeMonthData(this.threeMonthTransactions);
        break;
      case 'six-month':
      this.data = this.genSixMonthData(this.sixMonthTransactions);
        break;
      case 'year':
      this.data = this.genYearData(this.yearTransactions);
        break;
    }
  }

  genWeekLabels() {
    let labels = [];
    const weekAgo = moment().subtract(1, 'week');
    for (let i = 0; i < 7; i++) {
      const label = moment(weekAgo).add(i, 'days').format("MMM Do");
      labels.push(label);
    }
    return labels;
  }

  genMonthLabels(): string[] {
    let labels = [];
    const monthAgo = moment().subtract(30, 'days');
    let daysToAdd = 0;
    for (let i = 0; i < 7; i++) {
      const label = moment(monthAgo).add(daysToAdd, 'days').format("MMM Do");
      labels.push(label);
      daysToAdd += 5
    }
    return labels;
  }

  genThreeMonthLabels(): string[] {
    let labels = [];
    const monthAgo = moment().subtract(90, 'days');
    let daysToAdd = 0;
    for (let i = 0; i < 7; i++) {
      const label = moment(monthAgo).add(daysToAdd, 'days').format("MMM Do");
      labels.push(label);
      daysToAdd += 15
    }
    return labels;
  }

  genSixMonthLabels(): string[] {
    let labels = [];
    const monthAgo = moment().subtract(180, 'days');
    let daysToAdd = 0;
    for (let i = 0; i < 7; i++) {
      const label = moment(monthAgo).add(daysToAdd, 'days').format("MMM Do");
      labels.push(label);
      daysToAdd += 30
    }
    return labels;
  }

  genYearLabels(): string[] {
    let labels = [];
    const monthAgo = moment().subtract(360, 'days');
    let daysToAdd = 0;
    for (let i = 0; i < 7; i++) {
      const label = moment(monthAgo).add(daysToAdd, 'days').format("MMM Do");
      labels.push(label);
      daysToAdd += 60
    }
    return labels;
  }

  genWeekData(transactions: Transaction[]): number[] {
    let sales = [0, 0, 0, 0, 0, 0, 0];
    if(!transactions || transactions.length === 0) return sales;

    const tLen = transactions.length;
    let dayIndex = 0;
    for (let i = 0; i < tLen; i++) {
      let t = transactions[i];
      let day = moment(t.created_at).format("MMM Do");
      const index = this.labels.indexOf(day);
      sales[index] += (t.total / 100);
    }

    return sales;
  }

  genMonthData(transactions: Transaction[]): number[] {
    let sales = [0, 0, 0, 0, 0, 0, 0];
    if(!transactions || transactions.length === 0) return sales;
    const tLen = transactions.length;
    const monthAgo = moment().subtract(30, 'days');

    const labelOneStamp = monthAgo.unix();
    const labelTwoStamp = monthAgo.add(5, 'days').unix();
    const labelThreeStamp = monthAgo.add(5, 'days').unix();
    const labelFourStamp = monthAgo.add(5, 'days').unix();
    const labelFiveStamp = monthAgo.add(5, 'days').unix();
    const labelSixStamp = monthAgo.add(5, 'days').unix();
    const labelSevenStamp = monthAgo.add(5, 'days').unix();

    for (let i = 0; i < tLen; i++) {
      let t = transactions[i];
      let createdStamp = moment(t.created_at).unix();
      switch (true) {
        case createdStamp < labelTwoStamp:
          sales[0] = t.total / 100;
          break
        case createdStamp > labelTwoStamp && createdStamp < labelThreeStamp:
          sales[1] = t.total / 100;
          break
        case createdStamp > labelThreeStamp && createdStamp < labelFourStamp:
          sales[2] = t.total / 100;
          break
        case createdStamp > labelFourStamp && createdStamp < labelFiveStamp:
          sales[3] = t.total / 100;
          break
        case createdStamp > labelFiveStamp && createdStamp < labelSixStamp:
          sales[4] = t.total / 100;
          break
        case createdStamp > labelSixStamp && createdStamp < labelSevenStamp:
          sales[5] = t.total / 100;
          break
        case createdStamp > labelSevenStamp:
          sales[6] = t.total / 100;
          break
      }
    }
    return sales;
  }

  genThreeMonthData(transactions: Transaction[]): number[] {
    let sales = [0, 0, 0, 0, 0, 0, 0];
    if(!transactions || transactions.length === 0) return sales;
    const tLen = transactions.length;
    const threeMonthsAgo = moment().subtract(90, 'days');
    const labelOneStamp = threeMonthsAgo.unix();
    const labelTwoStamp = threeMonthsAgo.add(15, 'days').unix();
    const labelThreeStamp = threeMonthsAgo.add(15, 'days').unix();
    const labelFourStamp = threeMonthsAgo.add(15, 'days').unix();
    const labelFiveStamp = threeMonthsAgo.add(15, 'days').unix();
    const labelSixStamp = threeMonthsAgo.add(15, 'days').unix();
    const labelSevenStamp = threeMonthsAgo.add(15, 'days').unix();

    for (let i = 0; i < tLen; i++) {
      let t = transactions[i];
      let createdStamp = moment(t.created_at).unix();
      switch (true) {
        case createdStamp < labelTwoStamp:
          sales[0] = t.total / 100;
          break
        case createdStamp > labelTwoStamp && createdStamp < labelThreeStamp:
          sales[1] = t.total / 100;
          break
        case createdStamp > labelThreeStamp && createdStamp < labelFourStamp:
          sales[2] = t.total / 100;
          break
        case createdStamp > labelFourStamp && createdStamp < labelFiveStamp:
          sales[3] = t.total / 100;
          break
        case createdStamp > labelFiveStamp && createdStamp < labelSixStamp:
          sales[4] = t.total / 100;
          break
        case createdStamp > labelSixStamp && createdStamp < labelSevenStamp:
          sales[5] = t.total / 100;
          break
        case createdStamp > labelSevenStamp:
          sales[6] = t.total / 100;
          break
      }
    }
    return sales;
  }

  genSixMonthData(transactions: Transaction[]): number[] {
    let sales = [0, 0, 0, 0, 0, 0, 0];
    if(!transactions || transactions.length === 0) return sales;
    const tLen = transactions.length;
    const threeMonthsAgo = moment().subtract(180, 'days');
    const labelOneStamp = threeMonthsAgo.unix();
    const labelTwoStamp = threeMonthsAgo.add(30, 'days').unix();
    const labelThreeStamp = threeMonthsAgo.add(30, 'days').unix();
    const labelFourStamp = threeMonthsAgo.add(30, 'days').unix();
    const labelFiveStamp = threeMonthsAgo.add(30, 'days').unix();
    const labelSixStamp = threeMonthsAgo.add(30, 'days').unix();
    const labelSevenStamp = threeMonthsAgo.add(30, 'days').unix();

    for (let i = 0; i < tLen; i++) {
      let t = transactions[i];
      let createdStamp = moment(t.created_at).unix();
      switch (true) {
        case createdStamp < labelTwoStamp:
          sales[0] = t.total / 100;
          break
        case createdStamp > labelTwoStamp && createdStamp < labelThreeStamp:
          sales[1] = t.total / 100;
          break
        case createdStamp > labelThreeStamp && createdStamp < labelFourStamp:
          sales[2] = t.total / 100;
          break
        case createdStamp > labelFourStamp && createdStamp < labelFiveStamp:
          sales[3] = t.total / 100;
          break
        case createdStamp > labelFiveStamp && createdStamp < labelSixStamp:
          sales[4] = t.total / 100;
          break
        case createdStamp > labelSixStamp && createdStamp < labelSevenStamp:
          sales[5] = t.total / 100;
          break
        case createdStamp > labelSevenStamp:
          sales[6] = t.total / 100;
          break
      }
    }
    return sales;
  }

  genYearData(transactions: Transaction[]): number[] {
    let sales = [0, 0, 0, 0, 0, 0, 0];
    if(!transactions || transactions.length === 0) return sales;
    const tLen = transactions.length;
    const threeMonthsAgo = moment().subtract(360, 'days');
    const labelOneStamp = threeMonthsAgo.unix();
    const labelTwoStamp = threeMonthsAgo.add(60, 'days').unix();
    const labelThreeStamp = threeMonthsAgo.add(60, 'days').unix();
    const labelFourStamp = threeMonthsAgo.add(60, 'days').unix();
    const labelFiveStamp = threeMonthsAgo.add(60, 'days').unix();
    const labelSixStamp = threeMonthsAgo.add(60, 'days').unix();
    const labelSevenStamp = threeMonthsAgo.add(60, 'days').unix();

    for (let i = 0; i < tLen; i++) {
      let t = transactions[i];
      let createdStamp = moment(t.created_at).unix();
      switch (true) {
        case createdStamp < labelTwoStamp:
          sales[0] = t.total / 100;
          break
        case createdStamp > labelTwoStamp && createdStamp < labelThreeStamp:
          sales[1] = t.total / 100;
          break
        case createdStamp > labelThreeStamp && createdStamp < labelFourStamp:
          sales[2] = t.total / 100;
          break
        case createdStamp > labelFourStamp && createdStamp < labelFiveStamp:
          sales[3] = t.total / 100;
          break
        case createdStamp > labelFiveStamp && createdStamp < labelSixStamp:
          sales[4] = t.total / 100;
          break
        case createdStamp > labelSixStamp && createdStamp < labelSevenStamp:
          sales[5] = t.total / 100;
          break
        case createdStamp > labelSevenStamp:
          sales[6] = t.total / 100;
          break
      }
    }
    return sales;
  }

  getTransactions() {
    const monthAgo = moment().subtract(1, 'month').unix();
    return this.billingService.getProfessionalTransactions(this.authProfessional.id, monthAgo).then(resp => {
      if (resp.statusCode === 200) {
        const t = resp.content.transactions;
        this.monthTransactions = t.length > 0 ? t.sort(this.sortTransactions) : [];
      }
    });
  }

  generateYearTransactions() {
    const yearAgo = moment().subtract(1, 'year').unix();
    return this.billingService.getProfessionalTransactions(this.authProfessional.id, yearAgo).then(resp => {
      if (resp.statusCode === 200) {
        const t = resp.content.transactions;
        this.yearTransactions = t.length > 0 ? t.sort(this.sortTransactions) : [];
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }

  genAllTransactions() {
    if(this.yearTransactions.length === 0) {
      this.weekTransactions = [];
      this.monthTransactions = [];
      this.threeMonthTransactions = [];
      this.sixMonthTransactions = [];
      return;
    }

    this.weekTransactions = this.monthTransactions.filter(t => {
      const weekAgo = moment().subtract(1, 'week').unix();
      const today = moment().unix();
      const createdAt = moment(t.created_at).unix();
      if (createdAt > weekAgo && createdAt < today) return true
      return false
    });

    this.threeMonthTransactions = this.yearTransactions.filter(t => {
      const threeMonthsAgo = moment().subtract(90, 'days').unix();
      const today = moment().unix();
      const createdAt = moment(t.created_at).unix();
      if (createdAt > threeMonthsAgo && createdAt < today) return true
      return false
    });
    
    this.sixMonthTransactions = this.yearTransactions.filter(t => {
      const sixMonthsAgo = moment().subtract(180, 'days').unix();
      const today = moment().unix();
      const createdAt = moment(t.created_at).unix();
      if (createdAt > sixMonthsAgo && createdAt < today) return true
      return false
    });
  }

  sortTransactions(a, b) {
    if (a.created_at > b.created_at)
      return -1;
    if (a.created_at < b.created_at)
      return 1;
    return 0;
  }
}
