// Aurelia
import { autoinject, observable } from 'aurelia-framework';

// Services
import { AppointmentService } from './../../../stores/appointment/service';

// Models
import { Appointment } from './../../../stores/appointment/model';
import { Professional } from './../../../stores/professional/model';

// Third Party
import * as swal from 'sweetalert';
import * as moment from 'moment';

const bgColors = ['#DBECF8', '#4786FF', '#76DDFB', '#3D71D4', '#DBECF8', '#4786FF', '#76DDFB', '#3D71D4', '#DBECF8', '#4786FF', '#76DDFB', '#3D71D4', '#DBECF8', '#4786FF', '#76DDFB', '#3D71D4', '#DBECF8', '#4786FF', '#76DDFB', '#3D71D4', '#DBECF8', '#4786FF', '#76DDFB', '#3D71D4', '#DBECF8', '#4786FF', '#76DDFB', '#3D71D4', '#DBECF8', '#4786FF', '#76DDFB', '#3D71D4', '#DBECF8', '#4786FF', '#76DDFB', '#3D71D4', '#DBECF8', '#4786FF', '#76DDFB', '#3D71D4', '#DBECF8', '#4786FF', '#76DDFB', '#3D71D4', '#DBECF8', '#4786FF', '#76DDFB', '#3D71D4', '#DBECF8', '#4786FF', '#76DDFB', '#3D71D4'];

@autoinject
export class VanoServicesGraph {
  authProfessional: Professional;
  weekAppointments: Appointment[];
  monthAppointments: Appointment[];
  threeMonthAppointments: Appointment[];
  sixMonthAppointments: Appointment[];
  yearAppointments: Appointment[];
  totalServices: number;
  data: number[];
  labels: string[];
  isFetchingData: boolean;
  @observable graphView: string = 'month';
  backgroundColors: string[] = bgColors;

  constructor(private appointmentService: AppointmentService) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
  }

  attached() {
    this.isFetchingData = true;
    this.getProfessionalAppointments().then(() => {
      this.generateAppointmentLabels(this.monthAppointments);
      this.data = this.generateAppointmentData(this.monthAppointments);
      this.isFetchingData = false;
      this.getYearProfessionalAppointments();
    }).catch(err => {
      this.isFetchingData = false;
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    })
  }

  getProfessionalAppointments() {
    const begginingOfMonth = moment().subtract(1, 'month').unix().toString();
    const tomorrow = moment().add(1, 'days').unix().toString();
    return this.appointmentService.listProfessionalAppointments(this.authProfessional.id, begginingOfMonth, tomorrow, '500').then(resp => {
      if (resp.statusCode === 200) {
        this.monthAppointments = resp.content.appointments;
      }
    });
  }

  getYearProfessionalAppointments() {
    const begginingOfYear = moment().subtract(1, 'year').unix().toString();
    const tomorrow = moment().add(1, 'days').unix().toString();
    return this.appointmentService.listProfessionalAppointments(this.authProfessional.id, begginingOfYear, tomorrow, '5000').then(resp => {
      if (resp.statusCode === 200) {
        this.yearAppointments = resp.content.appointments;
        this.genAllAppointments();
      }
    });
  }

  genAllAppointments() {
    const weekAgo = moment().subtract(7, 'days').unix();
    const threeMonthsAgo = moment().subtract(3, 'months').unix();
    const sixMonthsAgo = moment().subtract(6, 'months').unix();
    const tomorrow = moment().add(1, 'days').unix();

    this.weekAppointments = this.monthAppointments.filter(a => {
      if (a.start_time > weekAgo && a.start_time < tomorrow) return true
      return false
    });

    this.threeMonthAppointments = this.yearAppointments.filter(a => {
      if (a.start_time > threeMonthsAgo && a.start_time < tomorrow) return true
      return false
    });

    this.sixMonthAppointments = this.yearAppointments.filter(a => {
      if (a.start_time > sixMonthsAgo && a.start_time < tomorrow) return true
      return false
    });
  }

  generateAppointmentLabels(appointments: Appointment[]) {
    this.labels = [];
    if (appointments.length === 0) return [];
    this.labels = this.authProfessional.services.map(s => {
      return s.name;
    });
  }

  graphViewChanged(newVal, oldVal) {
    if (!oldVal) return;
    switch (this.graphView) {
      case 'week':
        this.data = this.generateAppointmentData(this.weekAppointments);
        break;
      case 'month':
        this.data = this.generateAppointmentData(this.monthAppointments);
        break;
      case 'three-month':
        this.data = this.generateAppointmentData(this.threeMonthAppointments);
        break;
      case 'six-month':
        this.data = this.generateAppointmentData(this.sixMonthAppointments);
        break;
      case 'year':
        this.data = this.generateAppointmentData(this.yearAppointments);
        break;
    }
  }

  generateAppointmentData(appointments: Appointment[]): number[] {
    let data = [];
    let services = [];
    appointments.map(a => {
      if(!a.services) return
      if (services.length === 0) {
        services = a.services
        return
      }
      services = services.concat(a.services);
    });

    this.totalServices = services.length;

    this.labels.map(l => {
      let svcs = [];
      for (let i = 0; i < this.totalServices; i++) {
        const svc = services[i];
        if(svc.name === l) {
          svcs.push(1)
        }
      }
      data.push(svcs.length);
    });

    return data;
  }

  calcPercentage(qty: number): number {
    return Math.round((qty / this.totalServices) * 100)
  }

}
