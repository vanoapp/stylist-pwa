// Aurelia
import { autoinject } from 'aurelia-framework';
import { Router, RouterConfiguration } from 'aurelia-router';

// Models
import { Professional } from './../../stores/professional/model';

export class Reports {
  authProfessional: Professional;
  router: Router;

  constructor() {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
  }

  configureRouter(config: RouterConfiguration, router: Router): void {
    config.map([
      { route: [''], redirect: 'analytics' },
      { route: '/analytics', moduleId: './analytics/analytics', title: 'Analytics', name: 'app.reports.analytics' },
      { route: '/times', moduleId: './times/times', title: 'Times', name: 'app.reports.times' },
    ]);

    config.fallbackRoute('dashboard');

    this.router = router;
  }

  goTo(route: string): void {
    this.router.navigateToRoute(route);
  }

  get feature() {
    return {
      merchant_services: this.authProfessional.features.filter(i => i.name === 'credit_card_processing')[0].enabled,
      manage_employees: this.authProfessional.features.filter(i => i.name === 'manage_professionals')[0].enabled,
      require_credit_card: this.authProfessional.features.filter(i => i.name === 'require_credit_card')[0].enabled,
      cloc_in: this.authProfessional.features.filter(i => i.name === 'clock_in_clock_out')[0].enabled,
    }
  }
}
