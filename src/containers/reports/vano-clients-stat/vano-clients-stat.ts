// Aurelia
import { observable, autoinject } from 'aurelia-framework';

// Models
import { Professional } from '../../../stores/professional/model';
import { Analytics } from './../../../stores/analytics/model';

// Services
import { AnalyticsService } from './../../../stores/analytics/service';

// Third Party
import * as moment from 'moment';
import * as swal from 'sweetalert';

const WEEK_TITLE = 'New clients past 7 days';
const MONTH_TITLE = 'New clients past 30 days';
const THREE_MONTH_TITLE = 'New clients past 3 months';
const SIX_MONTH_TITLE = 'New clients past 6 months';
const YEAR_TITLE = 'New clients past year';


@autoinject
export class VanoClientsStat {
  authProfessional: Professional;
  analytics: Analytics;
  @observable statView: string = 'month';
  statTitle: string = MONTH_TITLE;
  statSubTitle: string;
  isFetchingData: boolean;
  newCustomers: number = 0;

  constructor(private analyticsService: AnalyticsService){
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
  }

  attached() {
    this.isFetchingData = true;
    this.getAnalytics().then(() => {
      this.statSubTitle = this.genStatSubTitle();
      this.newCustomers = this.analytics.month.new_customers;
      this.isFetchingData = false;
    }).catch(err => {
      this.isFetchingData = false;
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });
  }

  statViewChanged(newVal, oldVal) {
    if(!oldVal) return;
    switch (this.statView) {
      case 'week':
        this.statTitle = WEEK_TITLE;
        this.statSubTitle = this.genStatSubTitle()
        this.newCustomers = this.analytics.week.new_customers;
        break;
      case 'month':
        this.statTitle = MONTH_TITLE;
        this.statSubTitle = this.genStatSubTitle()
        this.newCustomers = this.analytics.month.new_customers;
        break;
      case 'year':
        this.statTitle = YEAR_TITLE;
        this.statSubTitle = this.genStatSubTitle()
        this.newCustomers = this.analytics.year.new_customers;
        break;
    }
  }

  getAnalytics() {
    return this.analyticsService.getProfessionalAnalytics(this.authProfessional.id).then(resp => {
      if(resp.statusCode === 200) {
        this.analytics = resp.content.analytics;
      }
    })
  }

  genStatSubTitle() {
    const tomorrow = moment().add(1, 'day').format("MMM Do");
    const tmrwYear = moment().add(1, 'day').format("YYYY");
    switch (this.statView) {
      case 'week':
        const weekAgo = moment().subtract(1, 'week').format("MMM Do");
        const weekAgoYear = moment().subtract(1, 'week').format("YYYY");
        if (weekAgoYear === tmrwYear) {
          return `${weekAgo} - ${tomorrow}, ${tmrwYear}`
        }
        return `${weekAgo} ${weekAgoYear} - ${tomorrow} ${tmrwYear}`
      case 'month':
        const monthAgo = moment().subtract(1, 'month').format("MMM Do")
        const monthAgoYear = moment().subtract(1, 'month').format("YYYY");
        if (monthAgoYear === tmrwYear) {
          return `${monthAgo} - ${tomorrow}, ${tmrwYear}`
        }
        return `${monthAgo} ${monthAgoYear} - ${tomorrow} ${tmrwYear}`
      case 'three-month':
        const threeMonthsAgo = moment().subtract(3, 'month').format("MMM Do")
        const threeMonthAgoYear = moment().subtract(3, 'month').format("YYYY");
        if (threeMonthAgoYear === tmrwYear) {
          return `${threeMonthsAgo} - ${tomorrow}, ${tmrwYear}`
        }
        return `${threeMonthsAgo} ${threeMonthAgoYear} - ${tomorrow} ${tmrwYear}`
      case 'six-month':
        const sixMonthsAgo = moment().subtract(6, 'month').format("MMM Do")
        const sixMonthsYear = moment().subtract(6, 'month').format("YYYY");
        if (sixMonthsYear === tmrwYear) {
          return `${sixMonthsAgo} - ${tomorrow}, ${tmrwYear}`
        }
        return `${sixMonthsAgo} ${sixMonthsYear} - ${tomorrow} ${tmrwYear}`
      case 'year':
        const yearAgo = moment().subtract(1, 'year').format("MMM Do")
        const yearAgoYear = moment().subtract(1, 'year').format("YYYY");
        if (yearAgoYear === tmrwYear) {
          return `${yearAgo} - ${tomorrow}, ${tmrwYear}`
        }
        return `${yearAgo} ${yearAgoYear} - ${tomorrow} ${tmrwYear}`
    }
  }

}
