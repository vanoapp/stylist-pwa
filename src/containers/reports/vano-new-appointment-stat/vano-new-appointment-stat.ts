
// Aurelia
import { observable, autoinject, inject } from 'aurelia-framework';

// Models
import { Professional } from '../../../stores/professional/model';
import { Analytics } from './../../../stores/analytics/model';

// Services
import { AnalyticsService } from './../../../stores/analytics/service';

// Third Party
import * as moment from 'moment';
import * as swal from 'sweetalert';

// Redux
import { reduxStore } from './../../../store';
import { StoreCreator } from 'redux';
import { getProfessionalAnalytics } from './../../../actions/analytics';

const WEEK_TITLE = 'New appointments this week';
const MONTH_TITLE = 'New appointments this month';
const THREE_MONTH_TITLE = 'New appointments past 3 months';
const SIX_MONTH_TITLE = 'New appointments past 6 months';
const YEAR_TITLE = 'New appointments this year';

@inject(AnalyticsService, reduxStore)
export class VanoNewAppointmentStat {
  authProfessional: Professional;
  analytics: Analytics;
  @observable statView: string = 'month';
  statTitle: string = MONTH_TITLE;
  statSubTitle: string;
  isFetchingData: boolean;
  newAppointments: number = 0;
  store;

  constructor(private analyticsService: AnalyticsService, store) {
    this.store = store;
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
  }

  attached() {
    this.store.subscribe(this.update.bind(this));
    this.store.dispatch(getProfessionalAnalytics(this.authProfessional.id));
  }

  update() {
    const state = this.store.getState().analyticsReducer;
    this.analytics = state.analytics;
    this.isFetchingData = state.isFetching;
    if (!this.isFetchingData) {
      this.statSubTitle = this.genStatSubTitle();
      this.newAppointments = this.analytics.month.new_appointments;
    }
  }

  statViewChanged(newVal, oldVal) {
    if (!oldVal) return;
    switch (this.statView) {
      case 'week':
        this.statTitle = WEEK_TITLE;
        this.statSubTitle = this.genStatSubTitle()
        this.newAppointments = this.analytics.week.new_appointments;
        break;
      case 'month':
        this.statTitle = MONTH_TITLE;
        this.statSubTitle = this.genStatSubTitle()
        this.newAppointments = this.analytics.month.new_appointments;
        break;
      case 'year':
        this.statTitle = YEAR_TITLE;
        this.statSubTitle = this.genStatSubTitle()
        this.newAppointments = this.analytics.year.new_appointments;
        break;
    }
  }

  getAnalytics() {
    return this.analyticsService.getProfessionalAnalytics(this.authProfessional.id).then(resp => {
      if (resp.statusCode === 200) {
        this.analytics = resp.content.analytics;
      }
    })
  }

  genStatSubTitle() {
    switch (this.statView) {
      case 'week':
        const startOfWeek = moment().startOf('week').format("MMM Do");
        const endOfWeek = moment().endOf('week').format("MMM Do");
        const startOfWeekYear = moment().startOf('week').format("YYYY");
        const endOfWeekYear = moment().endOf('week').format("YYYY");
        if (startOfWeekYear === endOfWeekYear) {
          return `${startOfWeek} - ${endOfWeek}, ${startOfWeekYear}`
        }
        return `${startOfWeek} ${startOfWeekYear} - ${endOfWeek} ${endOfWeekYear}`
      case 'month':
        const startOfMonth = moment().startOf('month').format("MMM Do");
        const endOfMonth = moment().endOf('month').format("MMM Do");
        const startOfMonthYear = moment().startOf('month').format("YYYY");
        const endOfMonthYear = moment().endOf('month').format("YYYY");
        if (startOfMonthYear === endOfMonthYear) {
          return `${startOfMonth} - ${endOfMonth}, ${startOfMonthYear}`
        }
        return `${startOfMonth} ${startOfMonthYear} - ${endOfMonth} ${endOfMonthYear}`
      case 'year':
        const startOfYear = moment().startOf('year').format("MMM Do");
        const endOfYear = moment().endOf('year').format("MMM Do");
        const startOfYearYear = moment().startOf('year').format("YYYY");
        const endOfYearYear = moment().endOf('year').format("YYYY");
        if (startOfYearYear === endOfYearYear) {
          return `${startOfYear} - ${endOfYear}, ${startOfYearYear}`
        }
        return `${startOfYear} ${startOfYearYear} - ${endOfYear} ${endOfYearYear}`
    }
  }
}
