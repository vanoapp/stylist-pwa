// Aurelia
import {autoinject} from 'aurelia-framework';

// Services
import { ProfessionalService } from './../../../stores/professional/service';

// Models
import { Professional } from './../../../stores/professional/model';
import { ClockIns } from './../../../stores/professional/model';
import { capitalize } from '../../../helpers/utils';

@autoinject
export class Times{
  authProfessional: Professional;
  professionals: Professional[];
  authClocIns: ClockIns[];

  constructor(private professionalService: ProfessionalService) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
  }

  attached() {
    this.getProfessionalService();
    if(this.authProfessional.permission.manage_company) {
      this.getCompanyProfessionals().then(() => {
        this.getSpsClockIns();
      })
    }
  }

  getSpsClockIns() {
    if(!this.professionals || this.professionals.length === 0) return
    this.professionals.map(p => {
      this.professionalService.listProfessionalClocIns(p.id).then(resp => {
        if(resp.statusCode === 200) {
          p.clock_ins = resp.content.cloc_ins;
        }
      })
    })
  }

  getProfessionalService() {
    return this.professionalService.listProfessionalClocIns(this.authProfessional.id).then(resp => {
      if(resp.statusCode === 200) {
        this.authClocIns = resp.content.cloc_ins;
      }
    })
  }

  getCompanyProfessionals() {
    return this.professionalService.getCompanyProfessionals(this.authProfessional.company_id).then(resp => {
      if(resp.statusCode === 200) {
        this.professionals = resp.content.professionals.filter(i => i.id !== this.authProfessional.id)
      }
    })
  }

  fullName(f, l) {
    return capitalize(`${f} ${l}`);
  }
}
