import { Router } from 'aurelia-router';
// Aurelia
import {autoinject} from 'aurelia-framework';

// Services
import { BillingService } from "../../../stores/billing/service";
import { CustomerService } from "../../../stores/customer/service";

// Models
import { Transaction } from "../../../stores/billing/model";
import { Customer } from "../../../stores/customer/model";
import { Professional } from "../../../stores/professional/model";

// First Party
import { capitalize } from "../../../helpers/utils";

// Third Party
import * as swal from 'sweetalert';

@autoinject
export class VanoTransactionsHistory {
  authProfessional: Professional;
  selectedTabItem: string = 'all';
  transactions: Transaction[];
  deposits: any[];
  customers: Customer[];
  length: number = 4;
  customerDataLoaded: boolean;
  router: Router;

  constructor(private billingService: BillingService, private customerService: CustomerService, router: Router) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.router = router;
  }

  attached() {
    this.getTransactions().then(() => {
      return this.getCustomers();
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    });

  }

  getTransactions() {
    return this.billingService.getProfessionalTransactions(this.authProfessional.id).then(resp => {
      if (resp.statusCode === 200) {
        const t = resp.content.transactions;
        this.transactions = t.length > 0 ? t.sort(this.sortTransactions) : [];
      }
    });
  }


  getCustomers(): Promise<any> {
    this.customers = [];
    const tranLen = this.transactions.length;
    if (tranLen === 0) return Promise.resolve();
    let tranIndex = 0;
    return new Promise((resolve, reject) => {
      this.transactions.map(t => {
        if (!t.customer_id) { tranIndex++; return; }
        this.customerService.get(t.customer_id).then(resp => {
          if (resp.statusCode === 200) {
            tranIndex++;
            this.customers.push(resp.content.customer);
            if (tranIndex === tranLen) {
              this.customerDataLoaded = true;
              resolve();
            }
          }
        }).catch(err => {
          reject(err)
        });
      });
    });
  }


  sortTransactions(a, b) {
    if (a.created_at > b.created_at)
      return -1;
    if (a.created_at < b.created_at)
      return 1;
    return 0;
  }

  idToCustomer(id: string): string {
    if (id === '') return 'Express'
    if (!this.customers) return '';
    let c = this.customers.filter(item => item.id === id);
    return capitalize(`${c[0].first_name} ${c[0].last_name}`);
  }

  navigate(route: string, id: string) {
    this.router.navigateToRoute(route, {id})
  }

}
