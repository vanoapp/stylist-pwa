// Aurelia
import { autoinject, NewInstance } from 'aurelia-framework';
import { ValidationRules, ValidationController, ValidationControllerFactory } from 'aurelia-validation';
import { Router } from 'aurelia-router';

// Models
import { Customer, UpdateCustomer as UpdateCustomerModel } from './../../stores/customer/model';
import { Professional } from '../../stores/professional/model';

// Services
import { CustomerService } from './../../stores/customer/service';

// First Party
import { VanoValidationRenderer } from './../../validation/vano-renderer';

// Third Party
import * as swal from 'sweetalert';

@autoinject
export class UpdateCustomer {
  controller: ValidationController;
  router: Router;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  initialEmail: string;
  initialPhoneNumber: string;
  customer: Customer;
  authProfessional: Professional;

  constructor(
    controller: ValidationControllerFactory,
    router: Router,
    private customerService: CustomerService) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.controller = controller.createForCurrentScope();
    this.controller.addRenderer(new VanoValidationRenderer());
    this.router = router;
  }

  activate(params) {
    return this.getCustomer(params.id).then(() => {
      this.setUpdateableProps();
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  validate() {
    this.controller.validate().then(res => {
      if (res.valid) {
        this.updateCustomer();
      }
    })
  }

  update(uc: UpdateCustomerModel) {
    return this.customerService.update(uc).then(resp => {
      if (resp.statusCode === 200) {
        swal('Awesome!', 'customer was updated', 'success');
        this.router.navigateBack();
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  updateCustomer() {
    let uc = new UpdateCustomerModel();
    uc.id = this.customer.id;
    uc.first_name = this.firstName;
    uc.last_name = this.lastName;
    uc.notification = this.customer.notification;
    uc.phone_number = this.phoneNumber.replace(/\D/g, '');
    uc.email = this.email;

    console.log(uc.email);
    console.log(uc.phone_number);
    
    
    const pnChanged = this.initialPhoneNumber !== uc.phone_number;
    const emailChanged = this.initialEmail !== uc.email;

    if (!pnChanged && !emailChanged) return this.update(uc);

    let completedRequest = 0;
    if (pnChanged) {
      this.checkPhoneNumber(uc).then(() => {
        if(!emailChanged) return this.update(uc);
        this.checkEmail(uc).then(() => {
          return this.update(uc);
        }).catch(() => {
          this.controller.addError('Email address is already being used by another customer.', this, 'serverErr');
          return;
        });
      }).catch(() => {
        this.controller.addError('Phone number is already being used by another customer.', this, 'serverErr');
        return;
      });
      return
    }

    if(emailChanged) {
      this.checkEmail(uc).then(() => {
        return this.update(uc);
      }).catch(() => {
        this.controller.addError('Email address is already being used by another customer.', this, 'serverErr');
        return;
      });
    }
  }

  checkPhoneNumber(uc: UpdateCustomerModel) {
    return new Promise((resolve, reject) => {
      this.customerService.checkExists('invalidemail', uc.phone_number).then(resp => {
        if (resp.statusCode === 200) {
          const customer: Customer = resp.content.customer;
          if (customer.id !== uc.id) {
            reject('phone number taken');
          }
          resolve();
        }
      }).catch(err => {
        if (err.statusCode === 400) {
          swal("Oops!", err.content.error, "error");
        } else if (err.statusCode === 500) {
          swal("This is emberassing!", "Could not process request at this time", "error");
        } else if (err.statusCode === 404) {
          resolve();
        }
      });
    });
  }

  checkEmail(uc: UpdateCustomerModel) {
    return new Promise((resolve, reject) => {
      this.customerService.checkExists(uc.email, '00000000').then(resp => {
        if (resp.statusCode === 200) {
          const customer: Customer = resp.content.customer;
          if (customer.id !== uc.id) {
            reject()
          }
          resolve();
        }
      }).catch(err => {
        if (err.statusCode === 400) {
          swal("Oops!", err.content.error, "error");
        } else if (err.statusCode === 500) {
          swal("This is emberassing!", "Could not process request at this time", "error");
        } else if (err.statusCode === 404) {
          resolve();
        }
      });
    })
  }

  delete() {
    let self = this;
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this customer",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
      () => {
        self.customerService.removeFromCompany(self.authProfessional.company_id, this.customer.id).then(resp => {
          if (resp.statusCode === 200) {
            swal("Deleted!", "Your customer has been deleted.", "success");
            self.router.navigateBack();
          }
        }).catch(err => {
          if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
            swal("Well this is emberassing!", "Having trouble talking to our servers!", "error");
          }
        });
      });
  }

  getCustomer(id: string) {
    return this.customerService.get(id).then(resp => {
      if (resp.statusCode === 200) {
        this.customer = resp.content.customer;
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  setUpdateableProps() {
    this.firstName = this.customer.first_name;
    this.lastName = this.customer.last_name;
    this.email = this.customer.email;
    this.phoneNumber = this.customer.phone_number;
    this.initialEmail = this.email;
    this.initialPhoneNumber = this.customer.phone_number;
  }
}

ValidationRules
  .ensure('firstName').displayName('first name').required().withMessage(`\${$displayName} cannot be blank.`)
  .ensure('lastName').displayName('last name').required().withMessage(`\${$displayName} cannot be blank.`)
  .ensure('email').displayName('email').email().withMessage(`\${$displayName} cannot be blank.`)
  .ensure('phoneNumber').displayName('phone number').matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/).withMessageKey('invalid phone number').required().withMessage(`\${$displayName} cannot be blank.`)
  .on(UpdateCustomer);
