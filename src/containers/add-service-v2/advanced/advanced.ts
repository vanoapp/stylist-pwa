import { randomClass } from './../../../helpers/utils';
import { NewService } from './../../../stores/service/model';
// Aurelia
import { autoinject } from 'aurelia-dependency-injection';
import { ValidationController, ValidationControllerFactory } from 'aurelia-validation';

// Models
import { Service } from "../../../stores/service/model";

// First Party
import { VanoValidationRenderer } from './../../../validation/vano-renderer';
import { ServiceService } from '../../../stores/service/service';
import toastr from '../../../libs/toastr/toastr';

// Third Party
import * as VMasker from 'vanilla-masker';

@autoinject
export class Advanced {
  steps: Service[] = [];
  serviceName: string;
  serviceDescription: string;
  servicePrice: string;
  requiresConsultation: boolean;
  stepName: string;
  stepLength: string;
  isGap: boolean;
  controller: ValidationController;
  displaySecondStep: boolean;
  isFetching: boolean;
  modalOpened: boolean;
  editingStepName: string;
  editingStepLength: string;
  editingStepId: string;
  editingStepGap: boolean;

  constructor(controller: ValidationControllerFactory, private svcService: ServiceService) {
    this.controller = controller.createForCurrentScope();
    this.controller.addRenderer(new VanoValidationRenderer)
  }

  attached() {
    VMasker(document.querySelector(".price-input").querySelector('input')).maskMoney({
      separator: '.',
      unit: '$'
    });
  }

  updateStep() {
    let hasErr = false;
    if (!this.editingStepName || this.editingStepName == '') {
      this.controller.addError('step name is required', this, 'editingStepName')
      hasErr = true
    }
    if (!this.editingStepLength || this.editingStepLength == '') {
      this.controller.addError('step length is required', this, 'editingStepLength')
      hasErr = true
    }
    if (hasErr) return

    const step = this.steps.find(s => s.id === this.editingStepId);
    if(!step) return;
    step.name = this.editingStepName;
    step.length = parseInt(this.editingStepLength);
    step.is_gap = this.editingStepGap;
    this.modalOpened = false;
    toastr.success('Step updated successfully!');
  }

  removeStep(id: string) {
    this.steps = this.steps.filter(s => s.id !== id);
    this.modalOpened = false;
    toastr.success('Step removed successfully');
  }

  edit(step: Service) {
    this.editingStepGap = step.is_gap;
    this.editingStepLength = step.length.toString();
    this.editingStepName = step.name;
    this.editingStepId = step.id;
    this.modalOpened = true;
  }

  showSecondStep() {
    let hasErr = false;
    switch (true) {
      case !this.serviceName || this.serviceName == '':
        this.controller.addError('service name is required', this, 'serviceName')
        hasErr = true
      case !this.servicePrice || this.servicePrice == '':
        this.controller.addError('service price is required', this, 'servicePrice')
        hasErr = true
    }
    if(hasErr) return
    this.displaySecondStep = true;
  }

  showFirstStep() {
    this.displaySecondStep = false;
  }

  addStep() {
    let hasErr = false;
    if (!this.stepName || this.stepName == '') {
      this.controller.addError('step name is required', this, 'stepName')
      hasErr = true
    }
    if (!this.stepLength || this.stepLength == '') {
      this.controller.addError('step length is required', this, 'stepLength')
      hasErr = true
    }
    if(this.stepLength && this.stepLength.replace(/\D/g,'') === ''){
      this.controller.addError('step length must be a number in minutes', this, 'stepLength')
      hasErr = true
    }
    if (hasErr) return

    let step = new Service;
    step.length = parseInt(this.stepLength);
    step.name = this.stepName;
    step.is_gap = this.isGap ? true : false;
    step.id = randomClass();
    this.steps = this.steps.concat(step);
    this.stepName = '';
    this.stepLength = '';
    this.isGap = false;
    console.log(this.steps)
    toastr.success('Added step sucessfully');
  }

  clearInputs() {
    this.stepName = '';
    this.stepLength = '';
    this.serviceName = '';
    this.servicePrice = '';
    this.steps = [];
    this.serviceDescription = '';
    this.isGap = false;
  }

  addService() {
    let totalSvcLen = 0;
    this.steps.map(s => {
      totalSvcLen += s.length;
    })

    let price: any = this.servicePrice.split('$')[1];
    price = parseInt(price.replace('.', ''));
    this.isFetching = true;
    const newSvc = new NewService;
    newSvc.steps = this.steps;
    newSvc.length = totalSvcLen;
    newSvc.name = this.serviceName;
    newSvc.description = this.serviceDescription;
    newSvc.price = price;
    newSvc.professional_id = JSON.parse(localStorage.getItem('professional')).id;
    newSvc.require_phone_call = this.requiresConsultation;

    this.svcService.create(newSvc).then(resp => {
      this.isFetching = false;
      if (resp.statusCode === 201) {
        swal('Awesome!', 'Service was added sucessfully!', 'success');
        this.clearInputs();
        this.displaySecondStep = false;
      }
    }).catch(err => {
      this.isFetching = false;
      console.log(err);

    })
  }
}
