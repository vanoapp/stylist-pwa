// libs
import { Router, RouterConfiguration } from 'aurelia-router';

// models
import { Customer } from '../../stores/customer/model';
import { Professional } from './../../stores/professional/model';

export class AddServiceV2 {
    authProfessional: Professional;
    router: Router;

    constructor() {
      this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    }

    configureRouter(config: RouterConfiguration, router: Router) {
        config.map([
            { route: [''], redirect: 'simple' },
            { route: ['/simple'], name: 'app.add.service.simple', moduleId: './simple/simple', title: 'Simple Service' },
            { route: ['/advanced'], name: 'app.add.service.advanced', moduleId: './advanced/advanced', title: 'Advanced Service' },
        ]);

        config.mapUnknownRoutes('not-found');

        this.router = router;
    }

    goTo(route: string): void {
        this.router.navigateToRoute(route);
    }
}
