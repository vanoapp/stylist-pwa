import { inject, NewInstance } from 'aurelia-framework';
import { ServiceService } from './../../../stores/service/service';
import { VanoValidationRenderer } from './../../../validation/vano-renderer';
import { Professional } from './../../../stores/professional/model';
import { ValidationRules, ValidationController } from "aurelia-validation";
import { NewService } from "../../../stores/service/model";
import { Router } from 'aurelia-router';

// Third Party
import * as swal from 'sweetalert';
import * as VMasker from 'vanilla-masker';

@inject(ServiceService, Router, NewInstance.of(ValidationController))
export class Simple{
  authProfessional: Professional;
  name: string;
  description: string;
  length: string;
  controller: ValidationController;
  price: string;
  isFetching:boolean;
  requirePhoneCall: boolean;

  constructor(private serviceService: ServiceService, private router: Router, controller: ValidationController) {
      this.authProfessional = JSON.parse(localStorage.getItem('professional'));
      this.controller = controller;
      this.controller.addRenderer(new VanoValidationRenderer());
  }

  attached() {
    VMasker(document.querySelector(".price-input").querySelector('input')).maskMoney({
      separator: '.',
      unit: '$'
    });
  }

  applyMask() {
    VMasker(document.querySelector(".price-input").querySelector('input')).maskMoney({
      separator: '.',
      unit: '$'
    });
  }

  validate() {
      this.controller.validate().then(res => {
          if (res.valid) {
              this.create();
          }
      });
  }

  handleKeyup(e) {
      if(e.which == 13 && !e.shiftKey) {  
          e.preventDefault();
          this.validate();
      }
  }

  create() {
      this.isFetching = true;
      let ns = new NewService();

      const priceEl: any = document.querySelector('.price-input')
      let price = this.price;
      price = price.split('$')[1];
      price = price.replace('.', '');


      ns.professional_id = this.authProfessional.id;
      ns.description = this.description;
      ns.name = this.name;
      ns.length = parseInt(this.length);
      ns.price = parseInt(price)
      ns.require_phone_call = this.requirePhoneCall;

      this.serviceService.create(ns).then(resp => {
          this.isFetching = false;
          if (resp.statusCode === 201) {
              swal("Awesome!", "Service was created!", "success");
              this.authProfessional.services.push(resp.content.service);
              localStorage.setItem('professional', JSON.stringify(this.authProfessional));
              this.router.navigateToRoute('app.services');
          }
      }).catch(err => {
          this.isFetching = false;
          if (err.statusCode === 400) {
              swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
              swal("This is emberassing!", "Could not process request at this time", "error");
          } else if (err.statusCode === 403) {
              swal("Oops!", "You do not have permission to do that!", "error");
          }
      });
  }

  clearInputs() {
      this.name = '';
      this.description = '';
      this.length = '';
      this.price = '';
  }
}

ValidationRules
.ensure('name').displayName('name').required().withMessage(`\${$displayName} cannot be blank.`)
.ensure('price').displayName('price').satisfies((x: string) => {
  console.log(x)
  if(!x) return false;
    let strPrice = x.split('$')[1];
    const price = parseFloat(strPrice);
    if (price > 0.00) {
        return true
    }

    return false
}).withMessage('price is required')
.ensure('length').displayName('length').required().withMessage(`\${$displayName} cannot be blank.`)
.on(Simple);
