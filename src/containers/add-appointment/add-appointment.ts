import { ManagedProfessional } from './../../stores/professional/model';
import { Professioanls } from './../professionals/professionals';
// Aurelia
import { inject, observable, NewInstance } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { ValidationRules, ValidationController, ValidateResult } from 'aurelia-validation';


// Third Party
import * as moment from 'moment';
import * as swal from 'sweetalert';
import * as Pikaday from 'pikaday';
import * as Hammer from 'hammerjs';
import * as algoliasearch from 'algoliasearch';

// Models
import { Professional } from '../../stores/professional/model';
import { Option } from '../../components/vano-forms/vano-search-dropdown/vano-search-dropdown';
import { VanoSelectOption } from '../../components/vano-forms/vano-select/vano-select';
import { Customer } from '../../stores/customer/model';
import { Company } from '../../stores/company/model';
import { Service } from '../../stores/service/model';
import { Appointment, NewAppointment } from '../../stores/appointment/model';

// First Part
import { capitalize, inArray } from '../../helpers/utils';
import { VanoValidationRenderer } from '../../validation/vano-renderer';
import env from '../../environment';
import { ALGOLIA_APP_ID, ALGOLIA_API_KEY } from '../../const/const';

// Services
import { AppointmentService } from '../../stores/appointment/service';
import { CompanyService } from '../../stores/company/service';
import { CustomerService } from '../../stores/customer/service';
import { ProfessionalService } from './../../stores/professional/service';
import { ServiceService } from './../../stores/service/service';

const NO_SERVICE_SELECTED_PLACEHOLDER = 'Select service(s)';
const SERVICE_SELECTED_PLACEHOLDER = 'Select another service';

@inject(AppointmentService, CustomerService, ProfessionalService, ServiceService, NewInstance.of(ValidationController), Router)
export class AddAppointment {
  authProfessional: Professional;
  customers: Customer[] = [];
  managedCustomers: Customer[] = [];
  company: Company;
  customerOptions: Option[] = [];
  managedCustomerOptions: Option[] = [];
  services: Service[];
  managedServices: Service[];
  serviceOptions: Option[] = [];
  managedServiceOptions: Option[] = [];
  @observable serviceSelected: string;
  @observable managedServiceSelected: string;
  router: Router;
  controller: ValidationController;
  invalidResults: ValidateResult[];
  @observable customerSelected: string;
  servicesSelected: Service[] = [];
  managedServicesSelected: Service[] = [];
  managedCustomerSelected: string;
  date: number;
  @observable stringDate: string;
  selectedTime: string;
  servicesSearchPlaceholder: string = NO_SERVICE_SELECTED_PLACEHOLDER;
  totalServicesLength: number;
  availableTimes: string[];
  timeOptions: VanoSelectOption[];
  timeDisabled: boolean = true;
  initialDate: number;
  notes: string;
  professionals: Professional[];
  professionalOpts: Option[];
  managedProfessionalOpts: Option[];
  @observable professionalSelected: string;
  @observable managedProfessionalSelected: string;
  @observable customerSearchQuery: string;
  @observable salonAppointment: boolean = true;
  managedProfessionals: Professional[];
  isFetching: boolean;
  customerServices: any[] = [];
  hasAvailableTimes: boolean = true;

  constructor(
    private appointmentService: AppointmentService,
    private customerService: CustomerService,
    private professionalService: ProfessionalService,
    private serviceService: ServiceService,
    controller: ValidationController,
    router: Router) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.company = JSON.parse(localStorage.getItem('company'));
    this.services = this.authProfessional.services;
    this.router = router;
    this.controller = controller;
    this.controller.addRenderer(new VanoValidationRenderer());
    this.removeService = this.removeService.bind(this);
    this.handleServiceLengthChange = this.handleServiceLengthChange.bind(this);
  }

  activate(params: any) {
    let selectedDate = false;
    this.initialDate = moment().unix();
    this.date = this.initialDate;
    this.stringDate = moment().format('MM/DD/YYYY');
    if (params.date) {
      selectedDate = true;
      this.initialDate = params.date;
      this.date = params.date;
      this.stringDate = moment(this.date * 1000).format('MM/DD/YYYY');
    }

    this.checkIsOpen(selectedDate);
    if (this.authProfessional.permission.manage_appointments) {
      return this.activateAdmin();
    }

    return this.activateProfessional();
  }

  activateAdmin() {
    return this.getCompanyCustomers().then(() => {
      this.generateCustomerOptions();
      return this.getCompanyProfessionals().then(() => {
        if(this.professionals.length === 1) {
          this.professionalSelected = this.professionals[0].id;
        }
        this.generateProfessionalsOptions();
      });
    });
  }

  activateProfessional() {
    this.professionalSelected = this.authProfessional.id;
    this.generateServiceOptions();
    return this.getCompanyCustomers().then(() => {
      this.generateCustomerOptions();
    });
  }

  attached() {
    window.addEventListener('vano-appointment-remove-service', this.removeService);
    window.addEventListener('vano-appointment-service-length-change', this.handleServiceLengthChange);
  }

  detached() {
    window.removeEventListener('vano-appointment-remove-service', this.removeService);
    window.removeEventListener('vano-appointment-service-length-change', this.handleServiceLengthChange);
  }

  handleServiceLengthChange(e) {
    const svc = e.detail.service;
    const newLength = e.detail.length;
    this.servicesSelected = this.servicesSelected.map(s => {
      if(s.id === svc.id) {
        s.length = parseInt(newLength);
      }
      return s;
    });

    this.getAvailableTimes();
  }

  customerSelectedChanged() {
    this.customerService.listCustomerServices(this.customerSelected).then(resp => {
      if(resp.statusCode === 200) {
        this.customerServices = resp.content.customer_services;
        this.checkForCustomerServicesLengths();
      }
    });
  }

  checkForCustomerServicesLengths() {
    if(this.servicesSelected.length > 0 && this.customerServices.length > 0) {
      this.servicesSelected = this.servicesSelected.map(svc => {
        this.customerServices.map(customerSvc => {
          if(customerSvc.service_id === svc.id) {
            svc.length = customerSvc.length;
          }
        });
        return svc;
      })
    }
  }

  validate() {
    this.invalidResults = [];
    this.controller.validate().then(result => {
      result.results.map(r => {
        if (!r.valid) {
          this.invalidResults.push(r);
        }
      });
      if (result.valid) {
        const names = this.authProfessional.services.map(s => s.name);
        let valid = true;
        // this.servicesSelected.map(ss => {
        //   if(!inArray(names, ss.name)){
        //     this.controller.addError('please select a service from your personal service list.', this, 'servicesSelected');
        //     valid = false;
        //   }
        // })
        if(valid) this.createAppointment();
      }
    });
  }

  stringDateChanged() {
    this.date = moment(this.stringDate).unix();
    if(this.managedProfessionalSelected){
      this.getManagedAvailableTimes();
      console.log('date', this.date)
      return
    }
    this.getAvailableTimes();
  }

  createAppointment() {
    this.isFetching = true;
    let a = new NewAppointment();
    a.customer_id = this.customerSelected;
    a.professional_id = this.professionalSelected;
    a.start_time = parseInt(this.selectedTime);
    a.end_time = (60 * this.totalServicesLength) + a.start_time;
    a.services = this.servicesSelected;
    a.notes = this.notes;
    if (this.managedProfessionalSelected) {
      a.customer_id = this.managedCustomerSelected;
      a.professional_id = this.managedProfessionalSelected;
      a.services = this.managedServicesSelected;
    }

    this.appointmentService.create(a).then(resp => {
      this.isFetching = false;
      if (resp.statusCode === 201) {
        swal("Awesome!", "Appointment was created!", "success");
        this.router.navigate(this.getBackRouteURL());
      }
    }).catch(err => {
      this.isFetching = false;
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  createNewClient() {
    this.router.navigate('/customers/new');
  }

  checkIsOpen(selectedDate: boolean) {
    this.timeDisabled = true;
    let day = moment(this.date * 1000).format('dddd').toLowerCase();
    let open;
    let close;
    switch (day) {
      case 'monday':
        open = this.company.hours.monday.open;
        close = this.company.hours.monday.close;
        if (!this.company.hours.monday.operating) {
          this.controller.addError(`salon closed on ${day}`, this, 'date');
        }
        break;
      case 'tuesday':
        open = this.company.hours.tuesday.open;
        close = this.company.hours.tuesday.close;
        if (!this.company.hours.tuesday.operating) {
          this.controller.addError(`salon closed on ${day}`, this, 'date');
        }
        break;
      case 'wednesday':
        open = this.company.hours.wednesday.open;
        close = this.company.hours.wednesday.close;
        if (!this.company.hours.wednesday.operating) {
          this.controller.addError(`salon closed on ${day}`, this, 'date');
        }
        break;
      case 'thursday':
        open = this.company.hours.thursday.open;
        close = this.company.hours.thursday.close;
        if (!this.company.hours.thursday.operating) {
          this.controller.addError(`salon closed on ${day}`, this, 'date');
        }
        break;
      case 'friday':
        open = this.company.hours.friday.open;
        close = this.company.hours.friday.close;
        if (!this.company.hours.friday.operating) {
          this.controller.addError(`salon closed on ${day}`, this, 'date');
        }
        break;
      case 'saturday':
        open = this.company.hours.saturday.open;
        close = this.company.hours.saturday.close;
        if (!this.company.hours.saturday.operating) {
          this.controller.addError(`salon closed on ${day}`, this, 'date');
        }
        break;
      case 'sunday':
        open = this.company.hours.sunday.open;
        close = this.company.hours.sunday.close;
        if (!this.company.hours.sunday.operating) {
          this.controller.addError(`salon closed on ${day}`, this, 'date');
        }
        break;
    }
  }

  getCompanyProfessionals() {
    return this.professionalService.getCompanyProfessionals(this.authProfessional.company_id).then(resp => {
      if (resp.statusCode === 200) {
        const p = resp.content.professionals;
        this.professionals = p.length > 0 ? p : [];
      }
    });
  }

  getAvailableTimes() {
    this.totalServicesLength = 0;
    if (this.servicesSelected.length > 0) {
      for (let i = 0; i < this.servicesSelected.length; i++) {
        this.totalServicesLength += this.servicesSelected[i].length;
      }

      this.appointmentService.listAvailableProfessionalAppointmentTimes(this.authProfessional.id, this.totalServicesLength, this.date).then(resp => {
        if (resp.statusCode === 200) {
          this.availableTimes = resp.content.times;
          this.generateTimeOptions();
        }
      });
    }
  }

  getBackRouteURL(): string {
    return `/appointments-v2?date=${this.initialDate}`;
  }

  getManagedAvailableTimes() {
    this.totalServicesLength = 0;
    if (this.managedServicesSelected.length > 0) {
      for (let i = 0; i < this.managedServicesSelected.length; i++) {
        this.totalServicesLength += this.managedServicesSelected[i].length;
      }

      this.appointmentService.listAvailableProfessionalAppointmentTimes(this.managedProfessionalSelected, this.totalServicesLength, this.date).then(resp => {
        if (resp.statusCode === 200) {
          this.availableTimes = resp.content.times;
          this.generateTimeOptions();
        }
      });
    }
  }

  generateTimeOptions() {
    this.timeOptions = [];
    this.availableTimes.map(h => {
      let opt = {
        value: h,
        label: moment(parseInt(h) * 1000).format('h:mm a'),
      }
      this.timeOptions.push(opt);
    });
    this.timeDisabled = false;
    if(this.availableTimes.length > 0) { 
      this.selectedTime = this.availableTimes[0];
      this.hasAvailableTimes = true;
      return
    }
    this.hasAvailableTimes = false;
  }

  handleKeyup(e) {
    if (e.which == 13 && !e.shiftKey) {
      e.preventDefault();
      this.validate();
    }
  }

  professionalSelectedChanged() {
    if (!this.authProfessional.permission.manage_appointments || !this.professionalSelected) return;
    this.services = [];
    this.servicesSelected = [];
    let prof = this.idToProf(this.professionalSelected);
    this.services = prof.services;
    this.generateServiceOptions();
  }

  idToProf(id: string): Professional {
    const p = this.professionals.filter(item => item.id === id);
    return p[0];
  }

  idToManagedProf(id: string): Professional {
    const p = this.managedProfessionals.filter(item => item.id === id);
    return p[0];
  }

  serviceSelectedChanged() {
    this.services.map(s => {
      if (s.id === this.serviceSelected) {
        this.servicesSelected.push(s);
        if(s.steps && s.steps.length > 0){
          this.servicesSelected = this.servicesSelected.concat(s.steps);
        }
        this.servicesSearchPlaceholder = SERVICE_SELECTED_PLACEHOLDER;
      }
    });
    this.serviceSelected = '';
    if (this.date) this.getAvailableTimes();
    this.checkForCustomerServicesLengths()
  }

  managedServiceSelectedChanged() {
    this.managedServices.map(s => {
      if (s.id === this.managedServiceSelected) {
        this.managedServicesSelected.push(s);
        this.servicesSearchPlaceholder = SERVICE_SELECTED_PLACEHOLDER;
      }
    });
    this.managedServiceSelected = '';
    if (this.date) this.getManagedAvailableTimes();
  }

  removeError(propertyName: string): void {
    const preCheckProperties = ['servicesSelected', 'selectedTime', 'managedServiceSelected']
    if(inArray(preCheckProperties, propertyName)) {
      this.checkIfInputReady(propertyName);
    }
    if (this.invalidResults) {
      const resLen = this.invalidResults.length;
      for (let i = 0; i < resLen; i++) {
        let r = this.invalidResults[i];
        if (r.propertyName === propertyName) {
          this.controller.removeError(r);
        }
      }
    }
  }

  checkIfInputReady(property) {
    switch(property) {
      case 'servicesSelected':
        if(this.authProfessional.permission.manage_appointments){
          if(!this.professionalSelected || this.professionalSelected == '') {
            swal('Oops!', 'You must selected a professional first', 'error');
          }
        }
        break;
      case 'managedServiceSelected':
        if(!this.managedProfessionalSelected || this.managedProfessionalSelected == '') {
          swal('Oops!', 'You must selected a professional first', 'error');
        }
        break;  
      case 'selectedTime':
        if(this.servicesSelected.length === 0){
          swal('Oops!', 'You must selected a service first', 'error');
        }
        break;
    }
  }

  removeService(e) {
    const service = e.detail.service;
    let selectedServicesLen = this.servicesSelected.length;
    for (let i = 0; i < selectedServicesLen; i++) {
      let s = this.servicesSelected[i];
      if (service.id == s.id) {
        this.servicesSelected.splice(i, 1);
        break;
      }
    }

    this.serviceSelected = '';

    if (this.servicesSelected.length === 0) {
      this.servicesSearchPlaceholder = NO_SERVICE_SELECTED_PLACEHOLDER;
    }

    this.getAvailableTimes();
  }

  getCompanyCustomers() {
    return this.customerService.getProfessionalCustomers(this.authProfessional.id).then(resp => {
      if (resp.statusCode === 200) {
        this.customers = resp.content.customers;
      }
    })
  }

  generateCustomerOptions() {
    this.customerOptions = [];
    let customersLen = this.customers.length;
    for (let i = 0; i < customersLen; i++) {
      let customer = this.customers[i];
      let opt = new Option();
      opt.value = customer.id;
      opt.label = capitalize(customer.first_name + ' ' + customer.last_name);
      this.customerOptions.push(opt);
    }
  }

  generateManagedCustomerOptions() {
    let customerOptions = [];
    let customersLen = this.managedCustomers.length;
    for (let i = 0; i < customersLen; i++) {
      let customer = this.managedCustomers[i];
      let opt = new Option();
      opt.value = customer.id;
      opt.label = capitalize(customer.first_name + ' ' + customer.last_name);
      customerOptions.push(opt);
    }

    this.managedCustomerOptions = customerOptions;
  }

  generateProfessionalsOptions() {
    let profOpts: Option[] = [];
    if (!this.professionals) return
    this.professionals.map(p => {
      let opt = {
        "label": capitalize(`${p.first_name} ${p.last_name}`),
        "value": p.id,
      }
      profOpts.push(opt);
    });

    this.professionalOpts = profOpts;
  }

  generateManagedProfessionalsOptions() {
    let profOpts: Option[] = [];
    if (!this.managedProfessionals) return
    this.managedProfessionals.map(p => {
      let opt = {
        "label": capitalize(`${p.first_name} ${p.last_name}`),
        "value": p.id,
      }
      profOpts.push(opt);
    });

    this.managedProfessionalOpts = profOpts;
  }

  generateServiceOptions() {
    let opts = [];
    let servicesLen = this.services.length;
    for (let i = 0; i < servicesLen; i++) {
      let service = this.services[i];
      let opt = new Option();
      opt.value = service.id;
      opt.label = service.name;
      opts.push(opt);
    }
    this.serviceOptions = opts;
  }

  generateManagedServiceOptions() {
    let opts = [];
    let servicesLen = this.managedServices.length;
    for (let i = 0; i < servicesLen; i++) {
      let service = this.managedServices[i];
      let opt = new Option();
      opt.value = service.id;
      opt.label = service.name;
      opts.push(opt);
    }
    this.managedServiceOptions = opts;
  }

  customerSearchQueryChanged() {
    if (!this.customerSearchQuery) return
    let client = algoliasearch(ALGOLIA_APP_ID, ALGOLIA_API_KEY);
    let indexName = 'vano_customers';
    if (env.debug) {
      indexName = 'staging_vano_customers';
    }
    if (env.debug && env.testing) {
      indexName = 'dev_vano_customers';
    }
    const index = client.initIndex(indexName);
    let companyID = this.authProfessional.company_id;
    if (this.managedProfessionalSelected) {
      companyID = this.idToManagedProf(this.managedProfessionalSelected).company_id;
    }
    index.search({
      query: this.customerSearchQuery,
      filters: `companies:${companyID} OR professionals:${this.authProfessional.id}`,
    }).then(resp => {
      if (this.managedProfessionalSelected) {
        this.managedCustomers = resp.hits.map(c => {
          c.id = c.objectID;
          return c;
        });
        this.generateManagedCustomerOptions();
        return;
      }
      this.customers = resp.hits.map(c => {
        c.id = c.objectID;
        return c;
      });
      this.generateCustomerOptions();
    });
  }

  salonAppointmentChanged(newVal, oldVal) {
    if (oldVal === false && newVal === true) {
      this.managedProfessionalSelected = ''
      return
    }

    if (oldVal === true && newVal === false) {
      this.professionalSelected = '';
    }
  }
}

ValidationRules
  .ensure('customerSelected').displayName('Customer').required()
  .ensure('professionalSelected').displayName('Professional').required()
  .ensure('servicesSelected').displayName('Service').satisfies((services: Service[]) => {
    if (services.length > 0) return true;
    return false;
  }).withMessage('Service is required')
  .ensure('stringDate').displayName('Date').required()
  .ensure('selectedTime').displayName('Time').required()
  .on(AddAppointment)
