import { Feature } from './../../stores/professional/model';
import { Professional } from '../../stores/professional/model';

export class Features{
  authProfessional: Professional;
  features: Feature[];

  constructor(){
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
  }

  attached() {
    this.features = this.authProfessional.features.sort(this.sort)
  }

  sort(a: Feature, b: Feature) {
    if (a.order < b.order) return -1;
    if (a.order > b.order) return 1;
    return 0;
  }
}
