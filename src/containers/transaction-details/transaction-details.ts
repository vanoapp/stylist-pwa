// Aurelia
import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

// Services
import { BillingService } from './../../stores/billing/service';
import { ProfessionalService } from './../../stores/professional/service';
import { CustomerService } from './../../stores/customer/service';
import { AppointmentService } from './../../stores/appointment/service';

// Models
import { Transaction } from './../../stores/billing/model';
import { Appointment } from './../checkout/appointment/appointment';
import { Professional } from './../../stores/professional/model';
import { Customer } from './../../stores/customer/model';

// Third Party
import * as swal from 'sweetalert';

@autoinject
export class TransactionDetails {
  transaction: Transaction;
  customer: Customer;
  professional: Professional;
  appointment: Appointment;
  router: Router;

  constructor(private billingService: BillingService, private customerService: CustomerService, private professionalService: ProfessionalService, private appointmentService: AppointmentService, router: Router) {
    this.router = router;
  }

  get customerFullName() {
    return `${this.customer.first_name} ${this.customer.last_name}`;
  }

  activate(params) {
    return this.getTransaction(params.id).then(() => {
      return this.getAppointment(this.transaction.appointment_id).then(() => {
        return this.getCustomer(this.transaction.customer_id).then(() => {
          return this.getProfessional(this.transaction.professional_id);
        })
      })
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  getTransaction(id: string) {
    return this.billingService.getTransaction(id).then(resp => {
      if (resp.statusCode === 200) {
        this.transaction = resp.content.transaction;
      }
    });
  }

  getAppointment(id: string) {
    return this.appointmentService.get(id).then(resp => {
      if (resp.statusCode === 200) {
        this.appointment = resp.content.appointment;
      }
    })
  }

  getCustomer(id: string) {
    return this.customerService.get(id).then(resp => {
      if (resp.statusCode === 200) {
        this.customer = resp.content.customer;
      }
    })
  }

  getProfessional(id: string) {
    return this.professionalService.get(id).then(resp => {
      if (resp.statusCode === 200) {
        this.professional = resp.content.professional;
      }
    })
  }

  refund() {
    let self = this;
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this transaction",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, refund it!",
      closeOnConfirm: false
    },
      () => {
        this.billingService.refund(this.transaction.id).then(resp => {
          if (resp.statusCode === 200) {
            swal('Awesome!', 'The transaction was refunded!', 'success');
            this.router.navigateBack()
          }
        }).catch(err => {
          if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
            swal("This is emberassing!", "Could not process request at this time", "error");
          }
        });
      });
  }

  navigateBack() {
    this.router.navigateBack();
  }
}
