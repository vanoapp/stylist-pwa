import { Company } from './../../../stores/company/model';
import { observable } from 'aurelia-framework';
// Aurelia
import { autoinject } from 'aurelia-framework';
import { ValidationController, ValidationControllerFactory, ValidationRules, ValidateResult } from 'aurelia-validation';

// Services
import { ProfessionalService } from '../../../stores/professional/service';

// Models
import { Professional, UpdateProfessional } from '../../../stores/professional/model';

// Third Party
import * as swal from 'sweetalert';

// First Party
import { VanoValidationRenderer } from './../../../validation/vano-renderer';

@autoinject
export class Personal {
  authProfessional: Professional;
  authCompany: Company;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  oldPassword: string;
  newPassword: string;
  serverErr: string;
  newPasswordErr: ValidateResult;
  oldPasswordErr: ValidateResult;
  controller: ValidationController;
  isFetching: boolean;
  isFetchingPassword: boolean;

  constructor(private professionalService: ProfessionalService, controller: ValidationControllerFactory) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.authCompany = JSON.parse(localStorage.getItem('company'));
    this.controller = controller.createForCurrentScope();
    this.controller.addRenderer(new VanoValidationRenderer());
  }

  attached() {
    this.firstName = this.authProfessional.first_name;
    this.lastName = this.authProfessional.last_name;
    this.email = this.authProfessional.email;
    this.phoneNumber = this.authProfessional.phone_number;
  }

  validate() {
    this.controller.validate().then(res => {
      if (res.valid) {
        this.updateInfo();
      }
    })
  }

  updateInfo() {
    this.isFetching = true;
    let updated = new UpdateProfessional();
    updated.id = this.authProfessional.id;
    updated.email = this.email;
    updated.phone_number = this.phoneNumber;
    updated.first_name = this.firstName;
    updated.last_name = this.lastName;
    updated.permission = this.authProfessional.permission;
    updated.notification = this.authProfessional.notification;

    this.professionalService.update(updated).then(resp => {
      this.isFetching = false
      if (resp.statusCode === 200) {
        swal("Awesome!", "Info was updated!", "success");
        this.authProfessional = resp.content.professional;
        localStorage.setItem('professional', JSON.stringify(this.authProfessional));
      }
    }).catch(err => {
      this.isFetching = false;
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    })
  }

  resetPassword() {
    this.controller.removeError(this.newPasswordErr);
    this.controller.removeError(this.oldPasswordErr);
    let hasErr = false
    if (!this.oldPassword) {
      this.controller.addError('old password is required', this, 'oldPassword');
      hasErr = true
    }
    if (!this.newPassword) {
     this.oldPasswordErr = this.controller.addError('new password is required', this, 'newPassword');
      hasErr = true
    }
    if (this.newPassword && this.newPassword.length < 6) {
      this.newPasswordErr = this.controller.addError('minimum 6 characters for new password', this, 'newPassword');
      hasErr = true
    }

    if (!hasErr) {
      this.isFetchingPassword = true;
      this.professionalService.resetPassword(this.authProfessional.id, this.oldPassword, this.newPassword).then(resp => {
        this.isFetchingPassword = false;
        if (resp.statusCode === 200) {
          swal('Awesome!', 'password has been updated', 'success');
          this.oldPassword = '';
          this.newPassword = '';
        }
      }).catch(err => {
        this.isFetchingPassword = false;
        if(err.statusCode === 404) {
          this.controller.addError('invalid password', this, 'serverErr');
        }
        if (err.statusCode === 400) {
          swal("Oops!", err.content.error, "error");
        } else if (err.statusCode === 500) {
          swal("This is emberassing!", "Could not process request at this time", "error");
        } else if (err.statusCode === 403) {
          swal("Oops!", "You do not have permission to do that!", "error");
        }
      });
    }
  }
}

ValidationRules
  .ensure('firstName').displayName('first name').required()
  .ensure('lastName').displayName('last name').required()
  .ensure('email').displayName('email').email().required()
  .ensure('phoneNumber').displayName('phone number').required()
  .on(Personal)
