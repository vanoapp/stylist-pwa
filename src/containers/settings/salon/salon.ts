// Aurelia
import { inject, NewInstance, observable } from 'aurelia-framework';
import { ValidationController, ValidationRules } from 'aurelia-validation';

// Services
import { CloudinaryService } from './../../../stores/cloudinary/service';
import { CompanyService } from './../../../stores/company/service';

// Models
import { Professional } from './../../../stores/professional/model';
import { Company, UpdateCompany, CompanyImage } from './../../../stores/company/model';

// Third Party
import * as moment from 'moment';
import * as swal from 'sweetalert';
import * as Dropzone from 'dropzone';

// First Party
import { VanoValidationRenderer } from './../../../validation/vano-renderer';
import env  from '../../../environment';

const CLOUDINARY_BASE_URL = 'https://api.cloudinary.com/v1_1/solabuddy/upload';

@inject(NewInstance.of(CloudinaryService), CompanyService, ValidationController)
export class Salon {
  authProfessional: Professional;
  displayDropImage: boolean = true;
  authCompany: Company;
  logoURL: string;
  description: string;
  phoneNumber: string;
  email: string;
  coverURL: string;
  controller: ValidationController;
  companyImages: CompanyImage[];
  uploadingLogo: boolean;
  uploadingCover: boolean;

  constructor(private cloudinaryService: CloudinaryService, private companyService: CompanyService, controller: ValidationController) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.authCompany = JSON.parse(localStorage.getItem('company'));

    this.handleFileUpload = this.handleFileUpload.bind(this);
    this.handleCloudinaryUpload = this.handleCloudinaryUpload.bind(this);
    this.controller = controller
    this.controller.addRenderer(new VanoValidationRenderer());
  }

  activate() {
    this.logoURL = this.authCompany.logo_url;
    this.coverURL = this.authCompany.cover_url;
    this.description = this.authCompany.description;
    this.email = this.authCompany.email;
    this.phoneNumber = this.authCompany.phone_number;
  }

  attached() {
    this.getCompanyImages();
    this.initDropzone();
    window.addEventListener('vano-input-file-upload', this.handleFileUpload)
  }

  detached() {
    window.removeEventListener('vano-input-file-upload', this.handleFileUpload)
  }

  viewPublicProfile() {
    let baseUrl = 'https://vanoapp.com'
    let fragmentUrl = `/salons/${this.authProfessional.company_id}`;
    if(env.debug === true && env.testing === false) {
      baseUrl = 'https://staging-clients.vanoapp.com'
    }
    if(env.debug === true && env.testing === true) {
      baseUrl = 'http://localhost:9001'
    }
    const url = `${baseUrl}${fragmentUrl}`
    window.open(url);
  }

  handleFileUpload(event) {
    const name = event.detail.name;
    if(event.detail.cropped_img) {
      if(name === 'logo') this.uploadingLogo = true;
      if(name === 'cover') this.uploadingCover = true;
      this.cloudinaryService.upload(event.detail.cropped_img).then(resp => {
        this.uploadingCover = false;
        this.uploadingLogo = false;
        if (resp.statusCode === 200) {
          switch (name) {
            case 'logo':
              this.logoURL = resp.content.secure_url;
              break;
            case 'cover':
              this.coverURL = resp.content.secure_url;
              break;
            case 'company':
              return this.uploadCompanyImage(resp.content.secure_url);
          }
        }
      }).catch(err => {
        this.uploadingCover = false;
        this.uploadingLogo = false;
        console.log(err);
      });
      return;  
    }
    const file = event.detail.file;
    if(name === 'logo') this.uploadingLogo = true;
    if(name === 'cover') this.uploadingCover = true;
    const reader = new FileReader();
    reader.onload = (event: any) => {
      this.cloudinaryService.upload(event.target.result).then(resp => {
        this.uploadingCover = false;
        this.uploadingLogo = false;
        if (resp.statusCode === 200) {
          switch (name) {
            case 'logo':
              this.logoURL = resp.content.secure_url;
              break;
            case 'cover':
              this.coverURL = resp.content.secure_url;
              break;
            case 'company':
              return this.uploadCompanyImage(resp.content.secure_url);
          }
        }
      }).catch(err => {
        this.uploadingCover = false;
        this.uploadingLogo = false;
        console.log(err);
      });
    };
    reader.readAsDataURL(file);
  }

  uploadCompanyImage(url) {
    return this.companyService.uploadImage(this.authProfessional.company_id, url).then(resp => {
      if(resp.statusCode === 201) {
        this.companyImages.push(resp.content.company_image);
      }
    })
  }

  validate() {
    this.controller.validate().then(res => {
      if(res.valid) {
        this.updateCompany();
      }
    })
  }

  updateCompany() {
    let uc = new UpdateCompany();
    uc.address_line1 = this.authCompany.address_line1;
    uc.address_line2 = this.authCompany.address_line2;
    uc.city = this.authCompany.city;
    uc.country = this.authCompany.country;
    uc.hours = this.authCompany.hours;
    uc.id = this.authCompany.id;
    uc.zip_code = this.authCompany.zip_code;
    uc.description = this.description;
    uc.logo_url = this.logoURL;
    uc.cover_url = this.coverURL;
    uc.merchant_integrated = this.authCompany.merchant_integrated;
    uc.state = this.authCompany.state;
    uc.name = this.authCompany.name;
    uc.phone_number = this.phoneNumber;
    uc.email = this.email;

    this.companyService.update(uc).then(resp => {
      if(resp.statusCode === 200) {
        swal('Awsome!', 'Salon profile updated successfully', 'success');
        localStorage.setItem('company', JSON.stringify(resp.content.company));
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("Well this is emberassing!", "Having trouble talking to our servers!", "error");
      }
    });
  }

  getCompanyImages() {
    return this.companyService.listCompanyImages(this.authCompany.id).then(resp => {
      if(resp.statusCode === 200) {
        this.companyImages = resp.content.company_images;
      }
    })
  }

  initDropzone() {
    let self = this;
    let options = {
      init() {
        this.on('success',  self.handleCloudinaryUpload)
      },
      parallelUploads: 1,
      url: "https://api.cloudinary.com/v1_1/solabuddy/upload",
      params: {
        tags: 'browser_upload',
        upload_preset: 'woxvsbgh'
      }
    }
    let zone = new Dropzone('div#dropzoneWrapper', options);
  }

  removeImage(id: string) {
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this photo",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
      () => {
        this.companyService.deleteImage(this.authCompany.id, id).then(resp => {
          if(resp.statusCode === 200) {
            swal('Awesome!', 'Image was deleted', 'success');
            this.companyImages = this.companyImages.filter(item => item.id !== id);
          }
        }).catch(err => {
          if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
            swal("This is emberassing!", "Could not process request at this time", "error");
          } else if (err.statusCode === 403) {
            swal("Oops!", "You do not have permission to do that!", "error");
          }
        });
      });
  }

  handleCloudinaryUpload(file, resp) {
    this.uploadCompanyImage(resp.secure_url);
  }
}


ValidationRules
.ensure('description').displayName('description').required().withMessage(`\${$displayName} cannot be blank.`)
.ensure('phoneNumber').displayName('phone number').required().withMessage(`\${$displayName} cannot be blank.`)
.ensure('email').displayName('email').required().withMessage(`\${$displayName} cannot be blank.`)
.on(Salon);
