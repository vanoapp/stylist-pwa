import { Professional } from './../../../../stores/professional/model';
import { autoinject } from 'aurelia-dependency-injection';
import { NewAppointment, Appointment } from './../../../../stores/appointment/model';
import { AppointmentService } from './../../../../stores/appointment/service';


import * as moment from 'moment';
import * as swal from 'sweetalert';

@autoinject
export class VacationDays {
  authProfessional: Professional;
  isFetchingVacationDays: boolean;
  blockoffs: Appointment[];

  constructor(private appointmentService: AppointmentService) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.handleDateRangeSelected = this.handleDateRangeSelected.bind(this);
  }

  attached() {
    window.addEventListener('vano-date-range-picker-selected', this.handleDateRangeSelected);
    this.getBlockoffs()
  }

  detached() {
    window.removeEventListener('vano-date-range-picker-selected', this.handleDateRangeSelected);
  }

  genDateRange(b: Appointment): string {
    return `${moment(b.start_time * 1000).format('MMM Do YYYY')} - ${moment(b.end_time * 1000).format('MMM Do YYYY')}`
  }

  getBlockoffs() {
    return this.appointmentService.listProfessionalBlockoffs(this.authProfessional.id).then(resp => {
      if (resp.statusCode === 200) {
        this.blockoffs = resp.content.appointments.filter(a => {
          const start = moment(a.start_time * 1000).unix()
          const end = moment(a.end_time * 1000).unix()
          if (end - start >= 86400) return true
          return false
        });
      }
    })
  }

  deleteBlockoff(id: string) {
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover these vacation days",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
      () => {
        this.appointmentService.cancel(id).then(resp => {
          if (resp.statusCode === 200) {
            swal('Awesome!', 'Vacation days were deleted successfully', 'success');
            this.blockoffs = this.blockoffs.filter(b => b.id !== id);
          }
        }).catch(err => {
          if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
            swal("Well this is emberassing!", "Having trouble talking to our servers!", "error");
          }
        });
      });

  }

  handleDateRangeSelected(e) {
    const dateRange = e.detail.dateRange;
    const start = dateRange.split(" - ")[0];
    const end = dateRange.split(" - ")[1];
    const startStamp = moment(start).startOf('day').unix()
    const endStamp = moment(end).startOf('day').unix();

    const svc = this.authProfessional.services.find(s => s.name === 'Vacation');

    this.isFetchingVacationDays = true;
    let a = new NewAppointment();
    a.services = [];
    a.professional_id = this.authProfessional.id;
    a.start_time = startStamp;
    a.end_time = endStamp;
    a.services.push(svc)
    a.notes = '';
    a.is_blockoff = true;

    this.appointmentService.create(a).then(resp => {
      this.isFetchingVacationDays = false;
      if (resp.statusCode === 201) {
        swal("Awesome!", "Vacation days were created!", "success");
        this.blockoffs.push(resp.content.appointment);
      }
    }).catch(err => {
      this.isFetchingVacationDays = false;
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error.toLowerCase().replace('appointment', 'vacation day'), "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }
}
