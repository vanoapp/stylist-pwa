// Aurelia
import { autoinject } from 'aurelia-framework';

// Services
import { CompanyService } from './../../../stores/company/service';

// Models
import { Company, Hours, UpdateCompany } from './../../../stores/company/model';
import { Professional } from './../../../stores/professional/model';

// Third Party
import * as swal from 'sweetalert';

// First Party
import { NewAppointment } from '../../../stores/appointment/model';

@autoinject
export class Schedules {
  authProfessional: Professional;
  company: Company;
  isFetchingVacationDays: boolean;

  constructor(private companyService: CompanyService) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
  }

  activate() {
    return this.getCompany().catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }


  updateCompany() {
    let uc = new UpdateCompany;
    uc.id = this.company.id;
    uc.name = this.company.name;
    uc.address_line1 = this.company.address_line1;
    uc.address_line2 = this.company.address_line2;
    uc.city = this.company.city;
    uc.state = this.company.state;
    uc.zip_code = this.company.zip_code
    uc.hours = this.company.hours;
    uc.country = this.company.country;
    uc.merchant_integrated = this.company.merchant_integrated;
    uc.logo_url = this.company.logo_url;
    uc.cover_url = this.company.cover_url;
    uc.phone_number = this.company.phone_number;
    uc.email = this.company.email;
    uc.description = this.company.description;

    this.companyService.update(uc).then(resp => {
      if (resp.statusCode === 200) {
        swal('Awesome!', 'Company was updated', 'success');
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  getCompany() {
    return this.companyService.get(this.authProfessional.company_id).then(resp => {
      if (resp.statusCode === 200) {
        this.company = resp.content.company;
      }
    });
  }
}
