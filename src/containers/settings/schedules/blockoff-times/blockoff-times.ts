// Aurelia
import { autoinject } from 'aurelia-dependency-injection';
import { ValidationController, ValidationControllerFactory, ValidationRules } from 'aurelia-validation';

// Models
import { Appointment, NewAppointment } from './../../../../stores/appointment/model';
import { Professional } from './../../../../stores/professional/model';

// Services
import { AppointmentService } from '../../../../stores/appointment/service';

// First Party Libs
import { VanoValidationRenderer } from './../../../../validation/vano-renderer';

// Third Party Libs
import * as moment from 'moment';

@autoinject
export class BlockoffTimes{
  authProfessional: Professional;
  blockoffs: Appointment[];
  controller: ValidationController;
  isFetching: boolean;
  date: string;
  startTime: string;
  endTime: string;

  constructor(private appointmentService: AppointmentService, controller: ValidationControllerFactory){
    this.controller = controller.createForCurrentScope();
    this.controller.addRenderer(new VanoValidationRenderer());
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
  }

  attached() {
    return this.getProfessionalBlockoffs();
  }

  getProfessionalBlockoffs() {
    return this.appointmentService.listProfessionalBlockoffs(this.authProfessional.id).then(resp => {
      if(resp.statusCode === 200) {
        this.blockoffs = resp.content.appointments.filter((a: Appointment) => {
          const start = moment(a.start_time * 1000).unix();
          const end = moment(a.end_time * 1000).unix();
          if(end - start < 86400) return true
          return false
        })
      }
    })
  }

  validate() {
    this.controller.validate().then(result => {
      if(result.valid) {
        this.createBlockoff();
      }
    })
  }

  createBlockoff() {
    this.isFetching = true;
    let a = new NewAppointment();

    const startHour = this.startTime.split(":")[0];
    const startMinute = this.startTime.split(":")[1];
    const endHour = this.endTime.split(":")[0];
    const endMinute = this.endTime.split(":")[1];
    const endTimestamp = moment(this.date).startOf('day').add(endHour, 'hour').add(endMinute, 'minute').unix();
    const startTimestamp = moment(this.date).startOf('day').add(startHour, 'hour').add(startMinute, 'minute').unix();

    if(startTimestamp > endTimestamp) {
      swal('Oops!', 'end time must be a time after the start time', 'warning');
      return
    }

    const svc = this.authProfessional.services.find(s => s.name === 'Blockoff');
    a.services = [];
    a.professional_id = this.authProfessional.id;
    a.start_time = startTimestamp;
    a.end_time = endTimestamp;
    a.services.push(svc)
    a.notes = '';
    a.is_blockoff = true;

    this.appointmentService.create(a).then(resp => {
      this.isFetching = false;
      if (resp.statusCode === 201) {
        swal("Awesome!", "blockoff were created!", "success");
        this.blockoffs.push(resp.content.appointment);
      }
    }).catch(err => {
      this.isFetching = false;
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error.toLowerCase().replace('appointment', 'blockoff'), "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  genRangeTimes(b :Appointment): string {
    return `${moment(b.start_time * 1000).format('MMM Do hh:mm a')} - ${moment(b.end_time * 1000).format('hh:mm a')}`
  }

  deleteBlockoff(id: string) {
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this block off",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
      () => {
        this.appointmentService.cancel(id).then(resp => {
          if (resp.statusCode === 200) {
            swal('Awesome!', 'Block off was deleted successfully', 'success');
            this.blockoffs = this.blockoffs.filter(b => b.id !== id);
          }
        }).catch(err => {
          if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
            swal("Well this is emberassing!", "Having trouble talking to our servers!", "error");
          }
        });
      });
  }
}

ValidationRules
  .ensure('date').displayName('date').required()
  .ensure('startTime').displayName('start time').required()
  .ensure('endTime').displayName('end time').required()
  .on(BlockoffTimes)
