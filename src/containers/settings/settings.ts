// libs
import { Router, RouterConfiguration } from 'aurelia-router';

// models
import { Customer } from '../../stores/customer/model';
import { Professional } from './../../stores/professional/model';

export class Settings {
    authProfessional: Professional;
    router: Router;

    constructor() {
      this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    }

    configureRouter(config: RouterConfiguration, router: Router) {
        config.map([
            { route: [''], redirect: 'personal' },
            { route: ['/personal'], name: 'app.settings.personal', moduleId: './personal/personal', title: 'Personal' },
            { route: ['/salon'], name: 'app.settings.salon', moduleId: './salon/salon', title: 'Salon' },
            { route: ['/company'], name: 'app.settings.company', moduleId: './company/company', title: 'Company' },
            { route: ['/schedules'], name: 'app.settings.schedules', moduleId: './schedules/schedules', title: 'Schedules' },
        ]);

        config.mapUnknownRoutes('not-found');

        this.router = router;
    }

    goTo(route: string): void {
        this.router.navigateToRoute(route);
    }
}
