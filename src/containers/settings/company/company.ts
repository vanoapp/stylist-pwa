// Aurelia
import { autoinject } from 'aurelia-framework';
import { ValidationController, ValidationControllerFactory, ValidationRules } from 'aurelia-validation';

// Services
import { CompanyService } from './../../../stores/company/service';

// Models
import { Company as CompanyModel, Hours, UpdateCompany } from './../../../stores/company/model';
import { Professional } from './../../../stores/professional/model';

// Third Party
import * as swal from 'sweetalert';

// First Party
import { VanoValidationRenderer } from './../../../validation/vano-renderer';

@autoinject
export class Company {
  authProfessional: Professional;
  company: CompanyModel;
  name: string;
  address1: string;
  address2: string;
  city: string;
  state: string;
  zip: string;
  hours: Hours;
  controller: ValidationController;

  constructor(private companyService: CompanyService, controller: ValidationControllerFactory) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.controller = controller.createForCurrentScope();
    this.controller.addRenderer(new VanoValidationRenderer());
  }

  activate() {
    return this.getCompany().catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  validate() {
    this.controller.validate().then(res => {
      if (res.valid) return this.updateCompany()
      window.scrollTo(0, 0);
    })
  }

  updateCompany() {
    let uc = new UpdateCompany;
    uc.id = this.company.id;
    uc.name = this.name;
    uc.address_line1 = this.address1;
    uc.address_line2 = this.address2;
    uc.city = this.city;
    uc.state = this.state;
    uc.zip_code = parseInt(this.zip);
    uc.hours = this.company.hours;
    uc.country = this.company.country;
    uc.merchant_integrated = this.company.merchant_integrated;
    uc.logo_url = this.company.logo_url;
    uc.cover_url = this.company.cover_url;
    uc.phone_number = this.company.phone_number;
    uc.email = this.company.email;
    uc.description = this.company.description;

    this.companyService.update(uc).then(resp => {
      if (resp.statusCode === 200) {
        swal('Awesome!', 'Company was updated', 'success');
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  getCompany() {
    return this.companyService.get(this.authProfessional.company_id).then(resp => {
      if (resp.statusCode === 200) {
        this.company = resp.content.company;
        this.setUpdateableProps();
      }
    });
  }

  setUpdateableProps() {
    this.name = this.company.name;
    this.address1 = this.company.address_line1;
    this.address2 = this.company.address_line2;
    this.city = this.company.city;
    this.state = this.company.state;
    this.zip = this.company.zip_code.toString();
    this.hours = this.company.hours;
  }
}


ValidationRules
  .ensure('name').displayName('Name').required()
  .ensure('address1').displayName('Address Line 1').required()
  .ensure('city').displayName('City').required()
  .ensure('state').displayName('State').required()
  .ensure('zip').displayName('Zip code').required()
  .on(Company)
