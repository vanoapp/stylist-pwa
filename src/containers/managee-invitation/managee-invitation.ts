// Aurelia
import {autoinject} from 'aurelia-framework';

// Services
import { ProfessionalService } from './../../stores/professional/service';

// Models 
import { Professional, ManagedProfessional } from './../../stores/professional/model';
import { capitalize } from '../../helpers/utils';

@autoinject
export class ManageeInvitation{
  authProfessional: Professional;
  invitations: ManagedProfessional[];
  professionals: Professional[];
  dataLoaded: boolean;

  constructor(private professionalService: ProfessionalService){
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.handleInvitationChange = this.handleInvitationChange.bind(this);
  }

  attached() {
    this.getSPInvitations().then(() => {
      if(this.invitations.length === 0) {
        this.dataLoaded = true;
        return;
      }
      return this.getProfessionals(this.invitations);
    });
    this.addEventListeners();
  }

  detached() {
    window.removeEventListener('vano-invitation-status-changed', this.handleInvitationChange)
  }

  addEventListeners() {
    window.addEventListener('vano-invitation-status-changed', this.handleInvitationChange)
  }
  
  handleInvitationChange(e) {
    const invitation = this.idToInvitation(e.detail.invitationId);
    if(e.detail.accepted){
      this.acceptInvitation(invitation);
      return;
    }
    this.declineInvitation(invitation);
    return;
  }

  idToInvitation(id: string): ManagedProfessional {
    const i = this.invitations.filter(i => i.id === id);
    return i[0];
  }

  getSPInvitations() {
    return this.professionalService.getManageeInvitations(this.authProfessional.id).then(resp => {
      if(resp.statusCode === 200) {
        this.invitations = resp.content.managed_professionals;
      }
    });
  }

  fullName(id: string): string {
    const p = this.professionals.filter(p => p.id === id);
    return capitalize(`${p[0].first_name} ${p[0].last_name}`);
  }

  invitationStatus(status: string): boolean {
    if(status === 'accepted') return true;
    return false
  }

  declinePendingInvitations() {
    this.invitations.map(i => {
      if(i.status === 'pending') {
        this.declineInvitation(i)
      }
    })
  }

  declineInvitation(i: ManagedProfessional) {
    let m = {
      id: i.id,
      managee: i .managee,
      manager: i.manager,
      status: 'declined',
      permission: i.permission,
    }
    this.professionalService.updateManagedProfessional(m).then(resp => {
      if(resp.statusCode === 200) {
        
      }
    })
  }

  acceptInvitation(i: ManagedProfessional) {
    let m = {
      id: i.id,
      managee: i .managee,
      manager: i.manager,
      status: 'accepted',
      permission: i.permission,
    }
    this.professionalService.updateManagedProfessional(m).then(resp => {
      if(resp.statusCode === 200) {
        
      }
    })
  }

  getProfessionals(invitations: ManagedProfessional[]) {
    return new Promise((resolve, reject) => {
      const iLen = invitations.length;
      let pIndex = 0;
      this.professionals = [];
      invitations.map(i => {
        this.professionalService.get(i.manager).then(resp => {
          if(resp.statusCode === 200) {
            pIndex++
            this.professionals.push(resp.content.professional);
            if(pIndex === iLen) {
              this.dataLoaded = true;
              resolve()
            }
          }
        }).catch(err => {
          reject(err);
        })
      })
    })
  }
}
