// Aurelia
import { autoinject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

// Services
import { ProfessionalService } from '../../stores/professional/service';
import { AuthService } from '../../stores/auth/service';

// Models
import { Professional } from '../../stores/professional/model';

// Third Party
import * as swal from 'sweetalert';

@autoinject
export class Activate {
    closeEl = undefined;
    closeElDisplay: string;
    email: string;
    professional: Professional;
    displayAddPassword: boolean = false;
    router: Router;
    password: string;

    constructor(private professionalService: ProfessionalService, router: Router, private authService: AuthService) {
        this.router = router;
    }

    attached() {
        this.closeEl = document.querySelector('.vano-close-component');
        this.closeElDisplay = this.closeEl.style.display;
        this.closeEl.style.display = 'none';
    }

    detached() {
        this.closeEl.style.display = this.closeElDisplay;
    }

    submit() {
        if(this.displayAddPassword) {
            this.setPassword();
            return;
        }
        this.continue();
    }

    setPassword() {
        if (this.password === '') {
            swal("Oops!", "password is required", "error");
            return;
        }

        this.professionalService.activate(this.professional.id, this.password).then(resp => {
            if (resp.statusCode === 200) {
                return this.authService.login(this.email, this.password);
            }
        }).catch(err => {
            if (err.statusCode === 400) {
                swal("Oops!", err.content.error, "error");
            } else if (err.statusCode === 500) {
                swal("This is emberassing!", "Could not process request at this time", "error");
            } else if (err.statusCode === 404) {
                swal("Oops!", "Could not find email or phone number", "error");
            }
        });
    }

    continue() {
        if (this.email === '') {
            swal("Oops!", "Email is required", "error");
            return;
        }

        this.professionalService.checkExists(this.email, '00000000000').then(resp => {
            if (resp.statusCode === 200) {
                this.professional = resp.content.professional;
                this.displayPassword();
            }
        }).catch(err => {
            if (err.statusCode === 400) {
                swal("Oops!", err.content.error, "error");
            } else if (err.statusCode === 500) {
                swal("This is emberassing!", "Could not process request at this time", "error");
            } else if (err.statusCode === 404) {
                swal("Oops!", "Could not find email or phone number", "error");
            }
        });
    }

    displayPassword() {
        if (this.professional.password_set) {
            swal("Your account is already activated!", "You are being redirected to the login page.", "warning");
            this.router.navigate('/login');
            return;
        }
        this.displayAddPassword = true;
    }

    handleKeyup(e) {
        if (e.which == 13 && !e.shiftKey) {
            e.preventDefault();
            if(this.displayAddPassword) {
                this.setPassword();
                return;
            }
            this.continue();
        }
    }
}