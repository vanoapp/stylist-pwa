// Aurelia
import {autoinject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {ValidationController, ValidationControllerFactory, ValidationRules} from 'aurelia-validation';

// Services
import { ProfessionalService } from './../../stores/professional/service';

// Models
import { Professional, UpdateProfessional } from './../../stores/professional/model';

// First Party
import { VanoValidationRenderer } from './../../validation/vano-renderer';

// Third Party
import * as swal from 'sweetalert';

@autoinject
export class ProfessionalDetails{
  authProfessional: Professional;
  professional: Professional;
  controller: ValidationController;
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  router: Router;
  isFetching: boolean;
  serverError: string;

  constructor(controller: ValidationControllerFactory, private professionalService: ProfessionalService, router: Router) {
    this.router = router;
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.controller = controller.createForCurrentScope();
    this.controller.addRenderer(new VanoValidationRenderer());
  }

  activate(params) {
    if(!this.authProfessional.permission.manage_professionals) {
      this.router.navigateBack();
      return;
    }
    this.professionalService.get(params.id).then(resp => {
      if(resp.statusCode === 200) {
        this.professional = resp.content.professional;
        this.setUpdateableProps();
      }
    })
  }

  validate() {
    this.controller.validate().then(res => {
      if(res.valid) {
        this.update();
      }
    })
  }

  delete() {
    let self = this;
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this professional",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
      () => {
        self.professionalService.delete(self.professional.id).then(resp => {
          if(resp.statusCode === 200) {
            swal("Awesome!", "Professional was deleted!", "success");
            this.router.navigateBack();
          }
        }).catch(err => {
          if (err.statusCode === 400) {
              swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
              swal("This is emberassing!", "Could not process request at this time", "error");
          } else if (err.statusCode === 403) {
              swal("Oops!", "You do not have permission to do that!", "error");
          }
      });
      });
  }

  update() {
    this.isFetching = true;
    let phoneNumber = this.phoneNumber.replace(/\D/g, '');
    this.professionalService.checkExists(this.email, phoneNumber).then(resp => {
        this.isFetching = false;
        if(resp.statusCode === 200) {
            const p: Professional = resp.content.professional;
            let shouldUpdate = true;
            if(p.email === this.email) {
              if(this.professional.email !== p.email) {
                this.controller.addError('email address is already taken', this, 'serverError');
                shouldUpdate = false;
              }
            }
            if(p.phone_number === phoneNumber) {
              if(this.professional.phone_number !== p.phone_number) {
                this.controller.addError('phone number is already taken', this, 'serverError');
                shouldUpdate = false;
              }
            }
            if(shouldUpdate) {
              this.runUpdate();
            }
            return;
        }
    }).catch(resp => {
        if (resp.statusCode === 400) {
            this.isFetching = false;
            swal("Oops!", resp.content.error, "error");
        } else if (resp.statusCode === 500) {
            this.isFetching = true;
            swal("This is emberassing!", "Could not process request at this time", "error");
        }
        if(resp.statusCode === 404) {
          this.runUpdate();
        }
    })    
  }

  runUpdate() {
    let updated = new UpdateProfessional();
    updated.id = this.professional.id;
    updated.email = this.email;
    updated.phone_number = this.phoneNumber;
    updated.first_name = this.firstName;
    updated.last_name = this.lastName;
    updated.permission = this.professional.permission;
    updated.notification = this.professional.notification;

    this.professionalService.update(updated).then(resp => {
        if (resp.statusCode === 200) {
            swal("Awesome!", "Info was updated!", "success");
            this.router.navigateBack();
        }
    }).catch(err => {
        if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
        } else if (err.statusCode === 500) {
            swal("This is emberassing!", "Could not process request at this time", "error");
        } else if (err.statusCode === 403) {
            swal("Oops!", "You do not have permission to do that!", "error");
        }
    });
  }

  setUpdateableProps() {
    this.firstName = this.professional.first_name;
    this.lastName = this.professional.last_name;
    this.email = this.professional.email;
    this.phoneNumber = this.professional.phone_number;
  }
}

ValidationRules
.ensure('firstName').displayName('first name').required().withMessage(`\${$displayName} cannot be blank.`)
.ensure('lastName').displayName('last name').required().withMessage(`\${$displayName} cannot be blank.`)
.ensure('email').displayName('email address').email().required().withMessage(`\${$displayName} cannot be blank.`)
.ensure('phoneNumber').displayName('phone number').matches(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/).withMessageKey('invalid phone number').required().withMessage(`\${$displayName} cannot be blank.`)
.on(ProfessionalDetails);
