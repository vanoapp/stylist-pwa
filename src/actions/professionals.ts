import { Professional } from './../stores/professional/model';
import { apiHost } from "../helpers/utils";
import 'whatwg-fetch'

export const professionalTypes = {
  REQUEST_PROFESSIONAL: 'REQUEST_PROFESSIONAL',
  SUCCESS_REQUEST_PROFESSIONAL: 'SUCCESS_REQUEST_PROFESSIONAL',
  FAILURE_REQUEST_PROFESSIONAL: 'FAILURE_REQUEST_PROFESSIONAL',
}

export const getProfessional = (id: string) => {
  return (dispatch, getState) => {
    
        dispatch(requestProfessional());
    
        const url = `${apiHost()}/professionals/${id}`;
    
        const config = {
          method: 'GET',
        }
    
        fetch(url, config).then(resp => {
          return resp.json().then(data => {
            dispatch(successProfessional(data.professional));
          })
        }).catch(err => {
          dispatch(failureProfessional(err))
        })
      }
}

const requestProfessional = () => {
  return {
    type: professionalTypes.REQUEST_PROFESSIONAL,
  }
}

const successProfessional = (professional: Professional) => {
  return {
    type: 'SUCCESS_REQUEST_PROFESSIONAL',
    payload: professional
  }
}

const failureProfessional = (err: any) => {
  return {
    type: professionalTypes.FAILURE_REQUEST_PROFESSIONAL,
    payload: err
  }
}
