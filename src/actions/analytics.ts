import { Analytics } from './../stores/analytics/model';
import { apiHost } from "../helpers/utils";
import 'whatwg-fetch'

export const analyticsTypes = {
  REQUEST_PROFESSIONAL_ANALYTICS: 'REQUEST_PROFESSIONAL_ANALYTICS',
  SUCCESS_REQUEST_PROFESSIONAL_ANALYTICS: 'SUCCESS_REQUEST_PROFESSIONAL_ANALYTICS',
  FAILURE_REQUEST_PROFESSIONAL_ANALYTICS: 'FAILURE_REQUEST_PROFESSIONAL_ANALYTICS',
}

export const getProfessionalAnalytics = (professionalID: string) => {
  return (dispatch, getState) => {

    dispatch(requestProfessionalAnalytics());

    const url = `${apiHost()}/professionals/${professionalID}/analytics`;

    let headers = new Headers();
    headers.append("Authorization", `Bearer ${localStorage.getItem('token')}`);

    const config = {
      method: 'GET',
      headers: headers,
    }

    fetch(url, config).then(resp => {
      return resp.json().then(data => {
        dispatch(successProfessionalAnalytics(data.analytics));
      })
    }).catch(err => {
      dispatch(failureProfessionalAnalytics(err))
    })
  }
}

const requestProfessionalAnalytics = () => {
  return {
    type: analyticsTypes.REQUEST_PROFESSIONAL_ANALYTICS,
  }
}

const successProfessionalAnalytics = (analytics: Analytics) => {
  return {
    type: analyticsTypes.SUCCESS_REQUEST_PROFESSIONAL_ANALYTICS,
    payload: analytics
  }
}

const failureProfessionalAnalytics = (err: any) => {
  return {
    type: analyticsTypes.FAILURE_REQUEST_PROFESSIONAL_ANALYTICS,
    payload: err
  }
}
