import { Router, RouterConfiguration } from 'aurelia-router';
import { AuthorizeStep, PreActivateStep } from './pipelines/pipeline';


export class App {
  router: Router;

  configureRouter(config: RouterConfiguration, router: Router): void {
    config.title = 'VANO';
    config.addPipelineStep('authorize', AuthorizeStep);
    config.addPipelineStep('preActivate', PreActivateStep);
    config.options.pushState = true;
    

    config.map([
      { route: '', redirect: 'login' },
      { route: 'login', moduleId: 'containers/login/login', title: 'Login', name: 'login'},
      { route: 'forgot-password', moduleId: 'containers/forgot-password/forgot-password', title: 'Forgot Password', name: 'forgot.password'},
      { route: 'reset-password', moduleId: 'containers/reset-password/reset-password', title: 'Reset Password', name: 'reset.password'},
      { route: 'logout', moduleId: 'containers/logout/logout', title: 'Logout', name: 'app.logout', layoutView: './layouts/site.html' },
      { route: 'activate', moduleId: './containers/activate/activate', title: 'Activate', name: 'site.activate'},
      { route: '/appointments/new', moduleId: 'containers/add-appointment/add-appointment', title: 'New Appointment', name: 'app.add.appointment', auth: true },{ route: '/appointments/:id/no-show', moduleId: 'containers/no-show-appointment/no-show-appointment', title: 'No Show', name: 'app.no.show.appointment', auth: true },
      { route: '/appointments/:id', moduleId: 'containers/appointment-details/appointment-details', title: 'Appointment Details', name: 'app.appointment.details', auth: true },
      { route: '/assistant', moduleId: 'containers/vano-assistant/vano-assistant', title: 'Assistant', name: 'app.assistant', auth: true },
      { route: '/services/new', moduleId: 'containers/add-service/add-service', title: 'New Service', name: 'app.add.service', auth: true },
      { route: '/services/:id', moduleId: 'containers/service-details/service-details', title: 'Service Details', name: 'app.service.details', auth: true },
      { route: '/reminders/new', moduleId: 'containers/add-reminder/add-reminder', title: 'New Reminder', name: 'app.add.reminder', auth: true },
      { route: ['/customers/new'], name: 'app.add.customer', moduleId: 'containers/add-customer/add-customer', title: 'New Customer', auth: true },
      { route: ['/customers/:id/update'], name: 'app.update.customer', moduleId: 'containers/update-customer/update-customer', title: 'Update Customer', auth: true },
      { route: ['/professionals/new'], name: 'app.add.professional', moduleId: 'containers/add-professional/add-professional', title: 'New Professional', auth: true },
      { route: ['/professionals/:id/details'], name: 'app.professional.details', moduleId: 'containers/professional-details/professional-details', title: 'Professional', auth: true },
      { route: '/*app', moduleId: 'containers/vano-app/vano-app', title: '', name: 'vano.app', auth: true },{ route: 'reminders/:id', moduleId: 'containers/reminder-details/reminder-details', title: 'Reminder Details', name: 'app.reminder.details', auth: true },
    ]);

    config.fallbackRoute('login');

    this.router = router;
  }
}
