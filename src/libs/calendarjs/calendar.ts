import * as moment from 'moment';

export class Calendar {
  element: Element;
  config: CalendarOptions;
  maxEvents: number = 5;
  date: moment.Moment;
  events: CalendarEvent[];
  monthEvents: CalendarEvent[];
  view: string;
  weekHourBlockHeight: number = 49;
  backgrounds: string[] = ['#6091f5', 'rgba(247, 123, 97, .8)', 'rgba(125, 210, 126, .8)', 'rgba(249, 176, 73, .8)'];
  agendaColumns: number = 7;

  constructor(element: Element, config?: CalendarOptions) {
    this.element = element;
    this.config = config;
    this.date = moment();
    if (window.innerWidth < 600) {
      this.agendaColumns = 3;
    }
    if (window.innerWidth > 600 && window.innerWidth < 960) {
      this.agendaColumns = 4;
    }
  }

  renderMonthCalendar() {
    this.view = 'month';
    this.element.innerHTML = '';
    const wrapper = this.createElement('div', 'vano-calendar');
    this.element.appendChild(wrapper);
    const monthCalendarWrapperDiv = this.createElement('div', 'vano-month-calendar-wrapper');
    wrapper.appendChild(monthCalendarWrapperDiv);
    const monthHeaderEl = this.createElement('div', 'vano-month-header');
    monthCalendarWrapperDiv.appendChild(monthHeaderEl);
    this.createMonthHeader(monthHeaderEl)
    const monthBodyEl = this.createElement('div', 'vano-month-body');
    monthCalendarWrapperDiv.appendChild(monthBodyEl);
    this.createMonthRows(monthBodyEl);
  }

  private createMonthHeader(headerEl: Element) {
    const labels = ['Sun', 'Mon', 'Tue', "Wed", 'Thur', 'Fri', 'Sat'];
    for (let i = 0; i < 7; i++) {
      const dayLabelEl = this.createElement('div', 'vano-month-header-label');
      dayLabelEl.innerHTML = `${labels[i]}`
      headerEl.appendChild(dayLabelEl);
    }
  }

  renderAgendaView(rows?: number) {
    if(rows) this.agendaColumns = rows;
    this.view = 'week';
    this.element.innerHTML = '';
    const wrapper = this.createElement('div', 'vano-calendar');
    this.element.appendChild(wrapper);
    const weelCalendarWrapperDiv = this.createElement('div', 'vano-week-calendar-wrapper');
    wrapper.appendChild(weelCalendarWrapperDiv);
    this.createWeekLayout(weelCalendarWrapperDiv);
    document.getElementsByClassName('vano-week-body')[0].scrollTop = this.calcScrollTop();
  }

  setEvents(events: CalendarEvent[]) {
    this.events = events
    this.monthEvents = this.filterMonthEvents();
    this.render();
  }

  next() {
    switch (this.view) {
      case 'month':
        this.date = moment(this.date).add(1, 'month');
        break
      case 'week':
        if (this.agendaColumns === 7) {
          this.date = moment(this.date).startOf('week').add(8, 'days');
          break
        }
        this.date = moment(this.date).add(this.agendaColumns, 'days');
        break
      default:
        this.date = moment(this.date).add(1, 'month');
        break
    }
    this.monthEvents = this.filterMonthEvents()
    this.render();
  }

  prev() {
    switch (this.view) {
      case 'month':
        this.date = moment(this.date).subtract(1, 'month');
        break
      case 'week':
      if (this.agendaColumns === 7) {
        this.date = moment(this.date).endOf('week').subtract(8, 'days');
        break
      }
      this.date = moment(this.date).subtract(this.agendaColumns, 'days');
        break
      default:
        this.date = moment(this.date).subtract(1, 'month');
        break
    }
    this.monthEvents = this.filterMonthEvents()
    this.render();
  }

  getTitle(): string {
    switch (this.view) {
      case 'month':
        return `${moment(this.date).format('MMMM YYYY')}`;
      case 'week':
        return this.genAgendaTitle();
      default:
        return this.genAgendaTitle();
    }
  }

  render() {
    switch (this.view) {
      case 'week':
        this.renderAgendaView();
        break;
      case 'month':
        this.renderMonthCalendar();
        break;
      default:
        this.renderMonthCalendar();
    }
  }

  private filterMonthEvents(): CalendarEvent[] {
    if (!this.events || this.events.length === 0) return [];
    return this.events.filter(e => {
      const beginingOfMonth = moment(this.date).startOf('month').subtract(7, 'days').unix();
      const endOfMonth = moment(this.date).endOf('month').add(7, 'days').unix();
      const eventdate = e.start
      if (eventdate >= beginingOfMonth && eventdate <= endOfMonth) {
        return true

      }
      return false
    })
  }

  private createMonthRows(monthCalWrapperEl: Element): void {
    let totalRows: number = 5;
    const totalMonthDays = moment(this.date).endOf('month').date();
    const firstDayOfMonthWeekDate = moment(this.date).startOf('month').day();

    // In a 5 row calendar we can get 35 days. So if we need more than 35 days to display we know we will need 6 rows.
    if ((totalMonthDays + firstDayOfMonthWeekDate) > 35) {
      totalRows = 6
    }

    const initialDate = moment(this.date).startOf('month').startOf('week').subtract(1, 'days');
    for (let r = 1; r <= totalRows; r++) {
      let rowDiv = this.createElement('div', 'vano-month-row');
      for (let d = 1; d <= 7; d++) {
        const date = initialDate.add(1, 'days');
        this.createMonthDay(rowDiv, date);
      }
      monthCalWrapperEl.appendChild(rowDiv);
    }
  }

  private createMonthDay(rowEL: Element, date: moment.Moment): void {
    const dayEl = this.createElement('div', 'vano-month-day');
    if (moment(date).format('MM/DD/YYYY') === moment().format('MM/DD/YYYY')) {
      dayEl.classList.add('vano-today');
    }
    if (moment(date).format('MMM') !== moment(this.date).format('MMM')) {
      dayEl.classList.add('vano-other-month');
    }
    const dayFormatted = date.format('MM/DD/YYYY')
    dayEl.setAttribute('vano-date', dayFormatted);
    dayEl.addEventListener('click', e => this.handleDayClick(e, dayFormatted));

    const numEl = this.createElement('div', 'vano-month-day-number');
    numEl.innerHTML = `${date.format('D')}`;
    dayEl.appendChild(numEl);

    const eventsDayWrapperEl = this.createElement('div', 'vano-month-day-events');
    if (this.monthEvents && this.monthEvents.length > 0) {
      const events = this.monthEvents.filter(e => moment(e.start * 1000).format('MM/DD/YYYY') === dayFormatted);
      this.createMonthEvents(eventsDayWrapperEl, events);
    }
    dayEl.appendChild(eventsDayWrapperEl);
    rowEL.appendChild(dayEl);
  }

  private calcScrollTop(): number {
    const scroll = (this.weekHourBlockHeight * 8.5);
    return scroll
  }

  private createWeekLayout(weekCalWrapperEl: Element): void {
    const weekHeaderWrapperEl = this.createElement('div', 'vano-week-header');
    const hourBlockPaddingEl = this.createElement('div', 'vano-week-header-hour-block-padding')
    weekHeaderWrapperEl.appendChild(hourBlockPaddingEl);
    weekCalWrapperEl.appendChild(weekHeaderWrapperEl);
    const weekBodyWrapperEl = this.createElement('div', 'vano-week-body')
    weekCalWrapperEl.appendChild(weekBodyWrapperEl);
    const timeWrapperEl = this.createElement('div', 'vano-week-hours-wrapper');
    weekBodyWrapperEl.appendChild(timeWrapperEl);
    this.createWeekHours(timeWrapperEl);
    const bodyWrapperEl = this.createElement('div', 'vano-week-body-wrapper');
    weekBodyWrapperEl.appendChild(bodyWrapperEl);
    let initialDate = moment(this.date).subtract(1, 'days');
    if (this.agendaColumns === 7) {
      initialDate = moment(this.date).startOf('week').subtract(1, 'days');
    }

    for (let r = 0; r < this.agendaColumns; r++) {
      const date = initialDate.add(1, 'day');
      this.createWeekHeader(weekHeaderWrapperEl, date)
      this.createWeekRow(bodyWrapperEl, date);
    }
  }

  private createWeekHeader(weekHeaderEl: Element, date: moment.Moment) {
    const rowDiv = this.createElement('div', 'vano-week-header-row');
    rowDiv.setAttribute('vano-date', date.format('MM/DD/YYYY'));
    rowDiv.innerHTML = `
            <div class="vano-header-date-name">${date.format('ddd')}</div>
            <div class="vano-header-date-number">${date.format('DD')}</div>
        `
    weekHeaderEl.appendChild(rowDiv)
  }

  private createWeekRow(weekBodyEl: Element, date: moment.Moment): void {
    const formattedDate = moment(date).format('MM/DD/YYYY')
    const rowDiv = this.createElement('div', 'vano-week-row');
    rowDiv.setAttribute('vano-date', formattedDate);
    rowDiv.addEventListener('click', e => this.handleDayClick(e, formattedDate));
    if (this.monthEvents && this.monthEvents.length > 0) {
      const events = this.monthEvents.filter(e => moment(e.start * 1000).format('MM/DD/YYYY') === date.format('MM/DD/YYYY'));
      if (events.length > 0) this.createWeekEvents(rowDiv, events);
    }
    for (let h = 0; h < 24; h++) {
      const rowHourEl = this.createElement('div', 'vano-week-row-hour-block');
      rowDiv.appendChild(rowHourEl)
    }
    weekBodyEl.appendChild(rowDiv)
  }

  private createWeekEvents(dayRowEl: Element, events: CalendarEvent[]) {
    const eventsWrapper = this.createElement('div', 'vano-events-wrapper')
    dayRowEl.appendChild(eventsWrapper);

    let eventHeights: number[] = [];
    events.map(e => {
      const eventEl: any = this.createElement('div', 'vano-event');
      eventEl.setAttribute('vano-event', e.id);
      eventEl.addEventListener('click', (event: Event) => this.handleEventClick(event, e))
      const heightPerMinute = this.weekHourBlockHeight / 60;
      const eventDuration = (e.end - e.start) / 60;
      const elHeight = heightPerMinute * eventDuration;
      eventEl.style.height = `${elHeight}px`;
      eventEl.innerHTML = `<div class="vano-event-title">${e.title}</div>`;
      const eventDayMinutes = ((e.start - moment(e.start * 1000).startOf('day').unix()) / 60);
      const elTop = eventDayMinutes * heightPerMinute;
      eventEl.style.left = `1%`;
      if (inArray(eventHeights, elTop)) {
        const matchingHeightsLen = eventHeights.filter(h => h === elTop).length;
        eventEl.style.left = `${randomIntFromInterval(10, 42)}%`;
      }
      eventHeights.push(elTop)
      eventEl.style.top = `${elTop}px`;
      eventEl.style.borderLeft = `solid 3px ${this.backgrounds[randomIntFromInterval(0, this.backgrounds.length - 1)]}`
      eventsWrapper.appendChild(eventEl)
    })
  }

  private createWeekHours(timeWrapperEl: Element): void {
    for (let i = 0; i < 24; i++) {
      const hourEl = this.createElement('div', 'vano-hour-block');
      hourEl.innerHTML = `<span>${formatIndexToTime(i)}</span>`;
      timeWrapperEl.appendChild(hourEl);
    }
  }

  private genAgendaTitle(): string {
    const firstDay = moment(this.date).startOf('week');
    const firstDayTitle = moment(firstDay).format('MMMM YYYY');
    const lastDay = moment(firstDay).add(this.agendaColumns, 'days');
    const lastDayTitle = moment(lastDay).format('MMMM YYYY');

    if (firstDayTitle === lastDayTitle) {
      return `${firstDayTitle}`;
    }

    const firstDayYear = firstDayTitle.split(' ')[1];
    const lastDayYear = lastDayTitle.split(' ')[1];

    if (firstDayYear === lastDayYear) {
      return `${moment(firstDay).format('MMM')} - ${moment(lastDay).format('MMM YYYY')}`
    }
    return `${moment(firstDay).format('MMM YYYY')} - ${moment(lastDay).format('MMM YYYY')}`
  }

  private createMonthEvents(eventsWrapperEl: Element, events: CalendarEvent[]): void {
    const eventsLen = events.length;
    let maxEvents = eventsLen;
    let remainingEvents = 0;
    if (eventsLen > this.maxEvents) {
      maxEvents = this.maxEvents;
      remainingEvents = eventsLen - maxEvents;
    }

    for (let e = 0; e < maxEvents; e++) {
      const event = events[e];
      const eventEl: any = this.createElement('div', 'vano-event');
      eventEl.addEventListener('click', e => this.handleEventClick(e, event));
      eventEl.setAttribute('vano-event-id', event.id);
      eventEl.innerHTML = event.title;
      eventEl.style.borderLeft = `solid 2px ${this.backgrounds[randomIntFromInterval(0, this.backgrounds.length - 1)]}`
      eventsWrapperEl.appendChild(eventEl);
    }

    if (remainingEvents > 0) {
      const loadMoreEvents = this.createElement('div', 'vano-more-events');
      loadMoreEvents.innerHTML = `${remainingEvents} more`;
      eventsWrapperEl.appendChild(loadMoreEvents);
    }
  }

  private createElement(type: string, ...classes: string[]): Element {
    const el = document.createElement(type);
    el.classList.add(...classes);
    return el;
  }

  private handleEventClick(e: any, event: CalendarEvent) {
    const eventID = e.target.getAttribute('vano-event');
    window.dispatchEvent(new CustomEvent('vano-calendar-event-click', {
      detail: {
        e: e,
        event,
      }
    }))

  }

  private handleDayClick(e: any, date: string) {
    console.log(e);
    
    const classes = e.target.classList;
    if (!classes.contains('vano-week-row-hour-block') && !classes.contains('vano-month-day-events') && !classes.contains('vano-month-day-number')) return
    window.dispatchEvent(new CustomEvent('vano-calendar-day-click', {
      detail: {
        e: e,
        date,
      }
    }))

  }
}

const randomIntFromInterval = (min: number, max: number): number => {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

const formatIndexToTime = (index: number): string => {
  if (index < 12) {
    if (index === 0) return '';
    return `${index} am`
  }
  if (index === 12) return '12 pm';
  return `${index - 12} pm`
}

const inArray = (list: any[], item: any): boolean => {
  return (list.indexOf(item) != -1);
}


export class CalendarOptions {
  view: string;
}

export class CalendarEvent {
  id: string;
  title: string;
  start: number;
  end: number;
  payload: any;
}

