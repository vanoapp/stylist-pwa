export default {
  success(msg) {
    console.log('yes')
    const toastrWrapperDiv = this.createElement('div', 'vano-toastr-wrapper');
    toastrWrapperDiv.innerHTML = msg;
    toastrWrapperDiv.classList.add('visible');
    document.querySelector('body').appendChild(toastrWrapperDiv);
    setTimeout(() => {
      document.querySelector('body').removeChild(toastrWrapperDiv);
    }, 5000)
  },

  createElement(type: string, klass: string): Element {
    const elem =  document.createElement('div')
    elem.classList.add(klass);
    return elem;
  }
}
