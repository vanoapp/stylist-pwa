import * as moment from 'moment';

export class CapitalizeValueConverter {
    toView(value) {
        let words = value.split(' ');
        if (words.length > 1) {
            let res = '';
            for (let i = 0; i < words.length; i++) {
                if (i != 0) {
                    res += ' ';
                }
                res += words[i].charAt(0).toUpperCase() + words[i].slice(1)
            }
            return res;
        }
        return value.charAt(0).toUpperCase() + value.slice(1)
    }
}

export class DateFormatValueConverter {
    toView(value, format) {
        if (isNaN(value)) {
            return moment(value).format(format);
        }
        return moment(value * 1000).format(format);
    }
}

export class FromNowValueConverter {
    toView(value, timestamp: boolean = true) {
        if(timestamp) {
            return moment(value * 1000).fromNow();
        }
        return moment(value).fromNow();
    }
}

export class CurrencyValueConverter {
    toView(value: any, dollarSign: boolean = true) {
        if(dollarSign) return `$${(value / 100).toFixed(2)}`;
        return `${(value / 100).toFixed(2)}`;
    }
}

export class PhoneNumberValueConverter {
    toView(value) {
        console.log('value', value);
        return value.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3')
    }
}

export class StringLengthValueConverter {
    toView(value, length) {
        if (value.length <= length) {
            return value;
        }
        return value.substring(0, length) + '...';
    }
}
