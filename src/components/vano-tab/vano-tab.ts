import {bindable} from 'aurelia-framework';

export class VanoTab {
    @bindable selected: string;
    tabBodyEls = undefined;

    constructor() {
        this.handleTabHeaderItemClick = this.handleTabHeaderItemClick.bind(this);
    }

    attached() {
        this.addEventListeners();
        this.tabBodyEls = document.getElementsByTagName('vano-tab-body');
        this.updateTabBody();
    }
    
    detached() {
        this.removeEventListeners();
    }

    addEventListeners() {
        window.addEventListener('vano-tab-header-item-click', this.handleTabHeaderItemClick);
    }

    removeEventListeners() {
        window.removeEventListener('vano-tab-header-item-click', this.handleTabHeaderItemClick);
    }

    handleTabHeaderItemClick(e) {
        this.selected = e.detail.name;
        this.updateTabBody();
    }

    updateTabBody() {
        const bodyLen = this.tabBodyEls.length;
        for(let i = 0; i < bodyLen; i ++) {
            let el = this.tabBodyEls[i];
            el.style.display = 'none';
            if(this.selected === el.getAttribute('name')) {
                el.style.display = 'block';
            }
        }
    }
}