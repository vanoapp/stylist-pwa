import {bindable} from 'aurelia-framework';

export class VanoTabHeaderItem{
    @bindable selected: boolean = false;
    @bindable title: string;
    @bindable name: string;

    handleClick() {
        window.dispatchEvent(new CustomEvent('vano-tab-header-item-click', {detail: {name: this.name}}));
    }
}