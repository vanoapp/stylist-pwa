// Aurelia
import {bindable} from 'aurelia-framework';

export class VanoTransactionCard{
  @bindable customer: string;
  @bindable date: string;
  @bindable total: number;
}
