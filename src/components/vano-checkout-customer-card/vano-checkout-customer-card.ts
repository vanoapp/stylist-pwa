// Aurelia
import {bindable} from 'aurelia-framework';

export class VanoCheckoutCustomerCard{
    @bindable customer: string;
}