import {bindable} from 'aurelia-framework';

export class VanoMonthCalendarHeader{
    @bindable title: string;

    next() {
        window.dispatchEvent(new CustomEvent('vano-calendar-next-click'));
    }

    prev() {
        window.dispatchEvent(new CustomEvent('vano-calendar-prev-click'));
    }
}