// Aurelia
import {bindable} from 'aurelia-framework';

export class VanoStepIndicator{
    @bindable length: number;
    @bindable active: number;
    @bindable color: string;
    @bindable progress: Boolean;   
}