import {bindable} from 'aurelia-framework';

export class VanoStatCard {
    @bindable description: string;
    @bindable value: number;
    @bindable color: string;
    @bindable direction: string;
    @bindable percentage: boolean;
    @bindable money: boolean;
}
