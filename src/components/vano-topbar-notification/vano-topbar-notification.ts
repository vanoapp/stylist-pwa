// Aurelia
import {bindable, inject} from 'aurelia-framework';

@inject(Element)
export class VanoTopbarNotification{
  @bindable text: string;
  element: Element;

  constructor(element: Element){
    this.element = element;
    this.handleClick = this.handleClick.bind(this);
  }

  attached() {
    this.element.addEventListener('click', this.handleClick);
  }

  detached() {
    this.element.removeEventListener('click', this.handleClick);
  }

  handleClick(e) {
    window.dispatchEvent(new CustomEvent('vano-topbar-notification-click', {detail: {
      event: e,
    }}))
  }
}
