// Aurelia
import {bindable} from 'aurelia-framework';

export class VanoHourDisplayCard{
  @bindable date: string;
  @bindable openTime: string;
  @bindable closeTime: string;
  @bindable operating: boolean;
}
