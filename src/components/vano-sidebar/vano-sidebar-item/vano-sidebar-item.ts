// Aurelia
import {bindable} from 'aurelia-framework';
import {Router} from 'aurelia-router';

export class VanoSidebarItem{
    @bindable name: string;
    @bindable route: string;
    @bindable icon: string = '';
    @bindable active: boolean;
    @bindable router: Router;

    navigate() {
        this.router.navigateToRoute(this.route);
        if (window.innerWidth < 960) {
            window.dispatchEvent(new CustomEvent('close-vano-sidebar'));
        }
    }
}