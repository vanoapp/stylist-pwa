// Aurelia
import { bindable } from 'aurelia-framework';

// Third Party
import * as Hammer from 'hammerjs';

export class VanoSidebar {
    @bindable opened: boolean;
    @bindable backgroundColor: string = '#ffffff';
    supportsPassive: boolean = undefined;
    sideBarEl = undefined;
    sideBarContainerEl = undefined;
    startX: number;
    currentX: number;
    touchingSideBar: boolean = false;
    scrimEl = undefined;
    hammerScrimEl: Hammer = undefined;

    constructor() {
        this.onTouchEnd = this.onTouchEnd.bind(this);
        this.onTouchMove = this.onTouchMove.bind(this);
        this.onTouchStart = this.onTouchStart.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
    }

    attached() {
        this.sideBarEl = document.getElementById('vano-sidebar');
        this.sideBarContainerEl = document.getElementById('vano-sidebar-container');
        this.scrimEl = document.getElementById('vano-scrim');
        this.hammerScrimEl = new Hammer(this.scrimEl);
        this.addEventListeners();
    }

    detached() {
        this.removeEventListeners();
    }

    addEventListeners() {
        window.addEventListener('close-vano-sidebar', this.closeMenu);
        this.hammerScrimEl.on('tap', this.closeMenu);
        this.sideBarEl.addEventListener('touchstart', this.onTouchStart, this.applyPassive());
        this.sideBarEl.addEventListener('touchmove', this.onTouchMove, this.applyPassive());
        this.sideBarEl.addEventListener('touchend', this.onTouchEnd, this.applyPassive());
    }

    removeEventListeners() {
        window.removeEventListener('close-vano-sidebar', this.closeMenu);
        this.hammerScrimEl.off('tap', this.closeMenu);
        this.sideBarEl.removeEventListener('touchstart', this.onTouchStart, this.applyPassive());
        this.sideBarEl.removeEventListener('touchmove', this.onTouchMove, this.applyPassive());
        this.sideBarEl.removeEventListener('touchend', this.onTouchEnd, this.applyPassive());
    }

    closeMenu(e) {
        this.opened = false;
    }

    applyPassive() {
        if (this.supportsPassive !== undefined) {
            return this.supportsPassive ? { passive: true } : false;
        }
        let isSupported = false;
        var opts = Object.defineProperty && Object.defineProperty({}, 'passive', { get: function () { isSupported = true } });
        // create a throwaway element & event and (synchronously) test out our options
        document.addEventListener('test', function () { }, opts);
        this.supportsPassive = isSupported;
        return this.applyPassive();
    }

    onTouchStart(evt) {
        if (!this.sideBarEl || !this.sideBarEl.classList.contains('opened'))
            return;

        this.startX = evt.touches[0].pageX;
        this.currentX = this.startX;

        this.touchingSideBar = true;
        requestAnimationFrame(this.update.bind(this));
    }

    onTouchMove(evt) {
        if (!this.touchingSideBar)
            return;

        this.currentX = evt.touches[0].pageX;
    }

    onTouchEnd(evt) {
        if (!this.touchingSideBar)
            return;

        this.touchingSideBar = false;

        const translateX = Math.min(0, this.currentX - this.startX);
        this.sideBarContainerEl.style.transform = '';

        if (translateX < -50) {
            this.closeMenu(evt);
        }
    }

    update() {
        if (!this.touchingSideBar)
            return;

        requestAnimationFrame(this.update.bind(this));

        const translateX = Math.min(0, this.currentX - this.startX);
        this.sideBarContainerEl.style.transform = `translateX(${translateX}px)`;
    }
}