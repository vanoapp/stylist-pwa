// Aurelia
import {bindable} from 'aurelia-framework';

export class VanoAnalyticsStatCard {
  @bindable title: string;
  @bindable subTitle: string;
  @bindable value: string;
  @bindable graphColor: string;
}
