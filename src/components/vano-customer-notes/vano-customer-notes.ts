// Aurelia
import {bindable, autoinject} from 'aurelia-framework';
import {ValidationController, ValidationControllerFactory, ValidationRules} from 'aurelia-validation';

//Models
import {CustomerNote} from '../../stores/customer/model';

// FIrst Party
import {VanoCardInputValidationRenderer} from '../../validation/vano-renderer';

@autoinject
export class VanoCustomerNotes{
    @bindable notes: CustomerNote[] = [];
    @bindable value: string;
    newNoteValue: string;
    controller: ValidationController;

    constructor(controller: ValidationControllerFactory) {
        this.controller = controller.createForCurrentScope();
        this.controller.addRenderer(new VanoCardInputValidationRenderer());
        this.handleInputSubmit = this.handleInputSubmit.bind(this);
    }

    attached() {
      window.addEventListener('vano-card-input-submit', this.handleInputSubmit)
    }

    detached() {
      window.removeEventListener('vano-card-input-submit', this.handleInputSubmit)
    }

    handleInputSubmit() {
      this.submit();
    }

    submit() {
        this.controller.validate().then(result => {
            if(result.valid) {
                this.value = this.newNoteValue;
                this.newNoteValue = '';
            }
        })
    }

    validate() {
        if(!this.newNoteValue) return;
        this.controller.validate();
    }
}


ValidationRules
.ensure('newNoteValue').displayName('note').required()
.on(VanoCustomerNotes)
