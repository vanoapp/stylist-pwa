import { bindable } from 'aurelia-framework';

import * as swal from 'sweetalert';

export class VanoCustomerNote {
  @bindable text: string;
  @bindable id: string
  isEditing: boolean;
  initialText: string;

  constructor() {
    this.handleWindowClick = this.handleWindowClick.bind(this);
  }

  attached() {
    window.addEventListener('click', this.handleWindowClick);
  }

  detached() {
    window.removeEventListener('click', this.handleWindowClick);
  }

  handleWindowClick(e) {
    if (!this.isEditing) return;
    const self = this;
    const trgt = e.target;
    console.log(trgt);

    if (trgt.classList.contains('icon-pencil')) return;

    const elem = trgt.localName;
    switch (elem) {
      case 'input':
        break;
      case 'button':
        break;
      case 'path':
        break;
      case 'svg':
        break;
      default:
        self.text = self.initialText;
        self.isEditing = false;
    }

  }

  toggleEdit(e) {
    this.isEditing = !this.isEditing;
    if (this.isEditing) {
      this.initialText = this.text;
    }
  }

  update(e) {
    if (!this.text || this.text == '') {
      swal('Oops!', 'note value is required', 'error');
      return;
    }
    window.dispatchEvent(new CustomEvent('vano-edit-customer-note', {
      detail: {
        event: e,
        id: this.id,
        value: this.text,
      }
    }));
    this.isEditing = false;
  }

  delete(e) {
    window.dispatchEvent(new CustomEvent('vano-delete-customer-note', {
      detail: {
        event: e,
        id: this.id,
      }
    }));
    this.isEditing = false;
  }
}
