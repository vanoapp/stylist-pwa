// Aurelia
import { bindable } from 'aurelia-framework';

// Third Party
import * as swal from 'sweetalert';
import * as VMasker from 'vanilla-masker';

export class VanoCheckoutServiceCard {
  @bindable service: string;
  @bindable price: string;
  @bindable editing: boolean;

  constructor() {
    this.handleWindowClick = this.handleWindowClick.bind(this);
  }

  attached() {
    window.addEventListener('vano-checkout-price-editing-change', this.handleWindowClick);
  }

  applyMask() {
    VMasker(document.querySelector(".edit-price-input")).maskMoney({
      separator: '.',
      unit: '$'
    });
  }

  toggleEditing() {
    this.editing = !this.editing;
    console.log('editing', this.editing);
  }

  handleWindowClick(e) {
    const localName = e.detail.event.target.localName;
    if(localName === 'svg' || localName === 'path') return;
    
    if(!this.editing) return;
    if(!e.detail.event.target.classList.contains('edit-price-input')) {
      this.editing = false;
    }
  }

  confirmPrice() {
    let self = this;
    console.log(self.price);
    
    swal({
      title: "Confirm Update Price",
      text: "Please confirm you wish to update this service price",
      showCancelButton: true,
      confirmButtonColor: "#97f13c",
      confirmButtonText: "Confirm!",
      closeOnConfirm: false,
      imageUrl: 'https://res.cloudinary.com/solabuddy/image/upload/v1499454181/thumbs-up_c2bgve.jpg'
    }, () => {
      let cleanPrice: any = (parseInt(self.price) / 100);
      if (self.price.toString().includes('$')) {
        cleanPrice = self.price.split(' ')[1].trim();
      }
      self.price = (parseInt(cleanPrice) * 100).toString();
      self.toggleEditing();
      swal("Updated!", "The service price has been updated", "success");
      window.dispatchEvent(new CustomEvent('vano-checkout-service-price-change', { detail: { price: self.price}}));
    });
  }
}
