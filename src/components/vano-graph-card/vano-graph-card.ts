// Aurelia
import { bindable, inject } from 'aurelia-framework';

// Third Party
import * as Hammer from 'hammerjs'

@inject(Element)
export class VanoGraphCard {
  @bindable displayDay: boolean;;
  @bindable displayWeek: boolean;;
  @bindable displayMonth: boolean;;
  @bindable displaySixMonth: boolean;;
  @bindable displayThreeMonth: boolean;
  @bindable displayYear: boolean;
  @bindable selected: string;
  @bindable isFetching: boolean;
  element: Element;
  hammerFooterEl: Hammer;
  footerEl;

  constructor(element: Element) {
    this.element = element;
    this.handleFooterSwipeLeft = this.handleFooterSwipeLeft.bind(this);
    this.handleFooterSwipeRight = this.handleFooterSwipeRight.bind(this);
  }

  attached() {
    if(this.isTouchDevice()) return

    this.footerEl = this.element.children[0].children[1];
    this.hammerFooterEl = new Hammer(this.footerEl);
    this.hammerFooterEl.on('panleft', this.handleFooterSwipeLeft)
    this.hammerFooterEl.on('panright', this.handleFooterSwipeRight)
  }

  detached() {
    if(this.isTouchDevice()) return

    this.hammerFooterEl.off('swipeleft', this.handleFooterSwipeLeft)
    this.hammerFooterEl.off('swiperight', this.handleFooterSwipeRight)
  }

  handleItemClick(e, item: string): void {
    this.selected = item;
  }

  handleFooterSwipeLeft(e) {
    requestAnimationFrame(() => {
      this.footerEl.scrollLeft = e.distance
    })
  }

  handleFooterSwipeRight(e) {
    requestAnimationFrame(() => {
      this.footerEl.scrollLeft = -e.distance
    })
  }

  isTouchDevice() {
    return 'ontouchstart' in window        // works on most browsers 
        || navigator.maxTouchPoints;       // works on IE10/11 and Surface
  };
}
