import {bindable} from 'aurelia-framework';

import {Appointment} from '../../stores/appointment/model';
import {Customer} from '../../stores/customer/model';

export class VanoCheckoutAppointment{
    @bindable customers: Customer[];
    @bindable appointment: Appointment;
    
    
    customerIdToFullName(id: string) {
        let customersLen = this.customers.length;
        for (let i = 0; i < customersLen; i++) {
            if (id == this.customers[i].id) {
                return `${this.customers[i].first_name} ${this.customers[i].last_name}`
            }
        }
    }
}