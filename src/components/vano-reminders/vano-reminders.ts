import { bindable } from 'aurelia-framework';

import {Reminder} from '../../stores/reminder/model';

export class VanoReminders{
    @bindable reminders: Reminder[]; 
    @bindable newReminder: string; 

    toggleReminder(r: Reminder) {
        r.completed = !r.completed;
        window.dispatchEvent(new CustomEvent('vano-reminder-change', {detail: {'checked': r.completed, 'reminder': r}}));
    }
}
