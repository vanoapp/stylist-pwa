import {bindable} from 'aurelia-framework';

export class VanoButton{
    @bindable color;
    @bindable rounded: boolean;
    @bindable loading: boolean;
    @bindable disabled: boolean;
}
