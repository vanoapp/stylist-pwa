import {bindable} from 'aurelia-framework';

import {Reminder} from '../../stores/reminder/model';

export class VanoCheckbox{
    @bindable checked;
    @bindable reminder: Reminder;

    toggleChecked() {
        this.checked = !this.checked;
        window.dispatchEvent(new CustomEvent('vano-reminder-change', {detail: {'checked': this.checked, 'reminder': this.reminder}}));
    }
}
