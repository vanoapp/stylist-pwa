import {bindable} from 'aurelia-framework';

export class VanoCheckoutCard{
    @bindable color: string;
    @bindable center: boolean = true;
    @bindable icon: string;
    @bindable title: string;
}