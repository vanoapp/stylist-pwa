// Aurelia
import {bindable} from 'aurelia-framework';

export class VanoPermissionCard{
    @bindable checked: boolean = false;
    @bindable title: string;
    @bindable description: string;
}