import {bindable} from 'aurelia-framework';

export class VanoCcCard{
    @bindable id: string;
    @bindable type: string;
    @bindable lastFour: string;
    @bindable selectable: boolean;
    @bindable selected: boolean;
    @bindable icon: string;

    delete() {
      window.dispatchEvent(new CustomEvent('vano-delete-cc', {detail: {id: this.id}}));
    }
}
