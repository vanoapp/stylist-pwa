// Aurelia
import {bindable} from 'aurelia-framework';

export class VanoToggle{
  @bindable checked: boolean;

  checkedChanged() {
    console.log('checked changed', this.checked);
  }
}
