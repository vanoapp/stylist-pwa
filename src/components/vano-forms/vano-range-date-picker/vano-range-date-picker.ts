import { autoinject } from 'aurelia-framework';
// Aurelia
import {bindable} from 'aurelia-framework';

// First Party
import { randomClass } from './../../../helpers/utils';
import {VanoDateRangePicker} from '../../../libs/vano-daterange-picker/vano-daterange-picker';

@autoinject
export class VanoRangeDatePicker{
  randomID: string;
  element: Element;

  constructor(element: Element) {
    this.randomID = randomClass();
    this.element = element;
  }

  attached() {
    console.log(this.element.querySelector('input'))
    new VanoDateRangePicker(this.element.querySelector('input'), {});
  }
}
