// Auerelia
import { bindable, observable } from 'aurelia-framework';

// First Party
import { randomClass } from '../../../helpers/utils';

// Models 
import { VanoSelectOption } from '../vano-select/vano-select';

const OPTION_LEN_THRESHOLD = 20;

export class VanoSearchDropdown {
  @bindable initialSelected: string;
  @bindable selected: string;
  @bindable placeholder: string = 'search...';
  @bindable options: Option[] = [];
  @bindable filterable: boolean = true;
  @bindable search: string;
  filteredOpts: Option[] = [];
  randomClass: string;
  resultsOpen: boolean;
  inputEl;
  selectedLabel: string;

  constructor() {
    this.randomClass = randomClass();
    this.handleFocusInput = this.handleFocusInput.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }

  attached() {
    console.log(this.selected,this.options);
    
    this.filteredOpts = this.options;
    this.inputEl = document.querySelector(`.${this.randomClass} input`);
    this.inputEl.addEventListener('focus', this.handleFocusInput);
    window.addEventListener('click', this.handleClick);
  }

  detached() {
    this.resultsOpen = false;
    this.inputEl.removeEventListener('focus', this.handleFocusInput);
    window.removeEventListener('click', this.handleClick);
  }

  searchChanged() {
    if(this.search !== this.selectedLabel) this.selected = '';
    if (!this.filterable || !this.search) return;
    this.filteredOpts = this.options.filter(opt => {
      return opt.label.toLocaleLowerCase().includes(this.search.toLocaleLowerCase());
    });
    if(this.search !== this.selectedLabel) this.resultsOpen = true;
  }

  handleFocusInput(e) {
    this.resultsOpen = true;
    this.filteredOpts = this.options;
  }

  handleClick(e) {
    const clickedElement = e.target.classList.contains(`${this.randomClass}`)
    const clickedInput = e.target.localName === 'input'
    if(clickedInput) {
      const elemWrapper = e.target.parentNode.parentNode.parentNode;
      if(!elemWrapper.classList.contains(`${this.randomClass}`)){
        this.resultsOpen = false;
        return;
      }
    }
    const clickedResult = e.target.classList.contains('vano-search-dropdown-result');
    if (!clickedInput && !clickedResult && !clickedElement) this.resultsOpen = false;
    
  }

  selectedChanged(newVal: string, oldVal: string) {
    console.log('selected changed', this.selected, this.options);
    
    if(this.selected === '') this.search = '';
    // First time the value is set
    if(oldVal === undefined && newVal != '') {
      setTimeout(() => {
        this.options.map(o => {
          if(o.value === this.selected) {
            this.search = o.label
            this.selectedLabel = o.label
          }
        })
      }, 500)
    }
  }

  setSelected(opt: Option) {
    this.search = opt.label;
    this.selectedLabel = this.search;
    this.selected = opt.value;
    this.resultsOpen = false;
  }

  optionsChanged() {
    this.filteredOpts = this.options;
    if (!this.options || !this.selected) return;
    const o = this.options.filter(opt => opt.value === this.selected);
    if(o.length === 0) return
    this.search = o[0].label;
  }
}

export class Option {
  value: string;
  label: string;
}
