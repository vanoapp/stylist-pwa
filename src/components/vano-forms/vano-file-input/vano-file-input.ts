import { randomClass } from './../../../helpers/utils';
// Aurelia
import { bindable } from 'aurelia-framework';

// Third Party
import * as Cropper from 'cropperjs';
import * as swal from 'sweetalert';

export class VanoFileInput {
  @bindable name: string;
  @bindable fileName: string;
  @bindable uploading: boolean;
  @bindable uploadProgress: string;
  @bindable isCroppable: boolean;
  progressBarEl;
  croppableImgSrc: string;
  modalOpened: boolean;
  randomID: string;
  cropper: Cropper;
  imgType: string;

  constructor() {
    this.randomID = randomClass();
  }

  handleFileUpload(event) {
    if (this.isCroppable) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.croppableImgSrc = e.target.result;
        this.modalOpened = true;
        if(this.cropper) {
          this.cropper.replace(this.croppableImgSrc);
          return
        }
        const img = document.getElementById(this.randomID);
        setTimeout(() => {
          this.cropper = new Cropper(img, {
            aspectRatio: 960 / 295,
          });
        }, 300);
      }
      switch (event.target.files[0].type) {
        case 'image/png':
          this.imgType = 'image/png'
          break
        case 'image/jpeg':
          this.imgType = 'image/jpeg';
          break
        default:
          swal('Oops!', 'Only .jpg and .png files are allowed', 'warning');
          return
      }

      reader.readAsDataURL(event.target.files[0]);
      return
    }
    window.dispatchEvent(new CustomEvent('vano-input-file-upload', {
      detail: {
        event: event,
        file: event.target.files[0],
        name: this.name,
      }
    }));
  }

  attached() {
    this.progressBarEl = document.querySelector('.vano-file-input-progress-bar');
    if (this.fileName) {
      this.fileName = this.trimFileName(this.fileName);
    }
  }

  cropAndSave() {
    const imgEncoded = this.cropper.getCroppedCanvas().toDataURL(this.imgType);
    window.dispatchEvent(new CustomEvent('vano-input-file-upload', {
      detail: {
        cropped_img: imgEncoded,
        name: this.name,
      }
    }));
    this.modalOpened = false;
    this.cropper.clear()
  }

  fileNameChanged() {
    if (!this.fileName) return
    this.fileName = this.trimFileName(this.fileName);
  }

  trimFileName(name: string): string {
    const trimmed = name.split('/')
    return trimmed[trimmed.length - 1];
  }

  uploadProgressChanged() {
    this.progressBarEl.style.width = `${this.uploadProgress}%`
  }
}
