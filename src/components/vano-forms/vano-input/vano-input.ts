// Aurelia
import { bindable } from 'aurelia-framework';

// First Party
import { randomClass } from '../../../helpers/utils';

// Third Party
import * as Pikaday from 'pikaday';
import * as VMasker from 'vanilla-masker';


export class VanoInput {
    @bindable placeholder: string = '';
    @bindable value: string = '';
    @bindable type: string = 'text';
    @bindable mask: string;
    @bindable disabled: boolean;
    @bindable limit: number = 524288;
    @bindable required: boolean;
    @bindable pattern: string = '/*./';
    randomClass: string;
    inputEL = undefined;
    readOnly: boolean;
    initialType: string;

    constructor() {
        this.randomClass = randomClass();
    }

    lol() {
      if(this.mask) {
        const el = document.querySelector(`.${this.randomClass}`);
        const vEl = VMasker(el);
        vEl.maskPattern(this.mask)
      }
    }

    attached() {
        const initialType = this.type;
        this.initialType = initialType
        
        if(this.type === 'date') {
            this.readOnly = true;
            this.type = 'text';
        }
        if(this.type == 'search'){
          this.type = 'text'
        }
        this.inputEL = document.querySelector(`.${this.randomClass}`);

        let self = this;
        switch (initialType) {
            case 'date':
                let picker = new Pikaday({
                    field: this.inputEL,
                    format: 'MM/DD/YYYY',
                    onSelect() {
                       self.value = this.getMoment().format('MM/DD/YYYY');
                    }
                });
                picker.setDate(self.value);
                break;
        }
    }

    togglePasswordType() {
      if(this.type === 'text') return this.type = 'password';
      return this.type = 'text'
    }
}
