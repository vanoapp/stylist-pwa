// Aurelia
import {bindable} from 'aurelia-framework';

export class VanoLabel{
    @bindable fontSize: string;
    labelEl = undefined;

    attached() {
        if(this.fontSize) {
            this.labelEl = document.querySelector('.vano-label-component');
            this.labelEl.style.fontSize = this.fontSize;
        }
    }
}