// Aurelia
import {bindable} from 'aurelia-framework';

// First Party
import { randomClass } from '../../../helpers/utils';

// Third Party
import * as creditcardutils from 'creditcardutils';

export class VanoCcInput{
    @bindable placeholder: string = '';
    @bindable value: string = '';
    @bindable type: string = 'text';
    @bindable mask: string;
    @bindable disabled: boolean;
    @bindable limit: number;
    randomClass: string;
    inputEL = undefined;

    constructor() {
        this.randomClass = randomClass();
    }

    format() {
      this.value = creditcardutils.formatCardNumber(this.value)
    }
}
