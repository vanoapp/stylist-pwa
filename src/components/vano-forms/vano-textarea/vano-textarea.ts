import {bindable} from 'aurelia-framework';

export class VanoTextarea {
    @bindable placeholder: string = '';
    @bindable value: any;
}