import {bindable} from 'aurelia-framework';

export class VanoInputGroup{
    @bindable multi: boolean = false;
    @bindable first: boolean = false;
}