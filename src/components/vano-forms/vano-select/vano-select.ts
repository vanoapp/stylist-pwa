// Aurelia
import {bindable} from 'aurelia-framework';

export class VanoSelect{
    @bindable selected: string;
    @bindable placeholder: string;
    @bindable disabled: boolean;
    @bindable isState: boolean;
    @bindable options: VanoSelectOption[];
}

export class VanoSelectOption {
    value: string;
    label: string;
}