import {bindable} from 'aurelia-framework';

export class VanoCardAddInput{
    @bindable placeholder: string;
    @bindable value: string;

    submit() {
      window.dispatchEvent(new CustomEvent('vano-card-input-submit'));
    }
}
