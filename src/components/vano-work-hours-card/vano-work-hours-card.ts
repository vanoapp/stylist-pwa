import { ClockIns } from './../../stores/professional/model';
import { bindable } from 'aurelia-framework';

import * as moment from 'moment';

export class VanoWorkHoursCard {
  @bindable clockIns: ClockIns[];
  sanitizedClockIns: ClockIns[];
  totalHours: string = '0';
  displayBody: boolean;

  constructor() {
    this.handleDateRangeSelected = this.handleDateRangeSelected.bind(this);
  }

  attached() {
    this.sanitizedClockIns = []
    window.addEventListener('vano-date-range-picker-selected', this.handleDateRangeSelected);
  }

  detached() {
    window.removeEventListener('vano-date-range-picker-selected', this.handleDateRangeSelected);
  }

  handleDateRangeSelected(e) {
    const start = e.detail.dateRange.split('-')[0].trim();
    const end = e.detail.dateRange.split('-')[1].trim();
    const from = moment(start).unix();
    
    const to = moment(end).unix()
    this.sanitizeClockIns(from, to);
    this.calcTotalHours();
  }

  sanitizeClockIns(from: number, to: number) {
    if(this.clockIns.length === 0) {
      this.sanitizedClockIns = [];
      return;
    }
    this.sanitizedClockIns = this.clockIns.filter(c => {
      const created_at = moment(c.created_at).unix();      
      if(created_at > from && created_at < to) return true
      return false;
    });
  }

  calcTotalHours() {
    let total = 0;
    this.sanitizedClockIns.map(s => {
      let clockOutTime = moment(s.cloc_out_time).unix();
      const clockInTime = moment(s.cloc_in_time).unix();
      if(clockOutTime < 0){
        clockOutTime = moment().unix();
      }

      total += clockOutTime - clockInTime;
    });
    this.totalHours = (total / 3600).toFixed(1);
  }

  showBody() {
    this.displayBody = !this.displayBody;
  }
}
