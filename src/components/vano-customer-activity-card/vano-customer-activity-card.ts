import { PostAppointment, Appointment } from './../../stores/appointment/model';
import { AppointmentService } from './../../stores/appointment/service';
import { VanoValidationRenderer } from './../../validation/vano-renderer';
import { autoinject } from 'aurelia-dependency-injection';
// Aurelia 
import { bindable } from 'aurelia-framework';
import { ValidationController, ValidationControllerFactory, ValidationRules } from 'aurelia-validation';

import {Service} from '../../stores/service/model';

import * as swal from 'sweetalert';

@autoinject
export class VanoCustomerActivityCard {
    @bindable services: Service[] = [];
    @bindable date: string;
    @bindable postAppointment: PostAppointment;
    @bindable appointment: Appointment;
    hasPostAppointment: boolean;
    bubbleText: string = 'View Notes';
    errMsg: string;
    controller: ValidationController;
    highlightFormula: string = '';
    colorFormula: string = '';
    totalServiceLength: string = '';
    notes: string = '';
    isFetching: boolean;
    validationErr;
    showNotes: boolean;
    bubbleColor: string = 'blue';

    constructor(controller: ValidationControllerFactory, private appointmentService: AppointmentService){
      this.controller = controller.createForCurrentScope();
      this.controller.addRenderer(new VanoValidationRenderer());
    }

    attached() {
      if(this.postAppointment.appointment_id === "") {
        this.hasPostAppointment = false;
        this.bubbleText = 'Write Notes'
        return
      }
      this.setUpdateableProps()
      this.bubbleColor = 'green';
      this.hasPostAppointment = true;
    }

    setUpdateableProps() {
      this.totalServiceLength = this.postAppointment.total_service_length;
      this.notes = this.postAppointment.notes;
      this.colorFormula = this.postAppointment.color_formula;
      this.highlightFormula = this.postAppointment.highlight_formula;
    }

    submit() {
      if(!this.hasPostAppointment) {
        this.validateCreate();
        return
      }
      this.validateUpdate();
    }

    validateUpdate() {
      if(this.highlightFormula === '' && this.colorFormula === '' && this.totalServiceLength === '' && this.notes == ''){
        this.validationErr = this.controller.addError('At least one field has to be completed to submit the form', this, 'errMsg');
        return
      }
      if(this.validationErr) {
        this.controller.removeError(this.validationErr);
      }
      this.updatePostAppointment();
    }

    validateCreate() {
      if(this.highlightFormula === '' && this.colorFormula === '' && this.totalServiceLength === '' && this.notes == ''){
        this.validationErr = this.controller.addError('At least one field has to be completed to submit the form', this, 'errMsg');
        return
      }
      if(this.validationErr) {
        this.controller.removeError(this.validationErr);
      }
      this.createPostAppointment();
    }

    createPostAppointment() {
      this.isFetching = true;
      let pa = new PostAppointment();
      pa.appointment_id = this.appointment.id;
      pa.total_service_length = this.totalServiceLength;
      pa.color_formula = this.colorFormula;
      pa.highlight_formula = this.highlightFormula;
      pa.notes = this.notes;

      this.appointmentService.createPostAppointment(pa).then(resp => {
        this.isFetching = false;
        if(resp.statusCode === 201) {
          const pa = resp.content.post_appointment;
          swal('Awesome!', 'Post appointment notes were created', 'success');
          this.hasPostAppointment = true;
          this.bubbleText = 'View Notes';
          this.showNotes = false;
          this.postAppointment = pa;
          this.bubbleColor = 'green';
        }
      }).catch(err => {
        this.isFetching = false;
        if (err.statusCode === 400) {
          swal("Oops!", err.content.error, "error");
        } else if (err.statusCode === 500) {
          swal("This is emberassing!", "Could not process request at this time", "error");
        }
      })
    }

    updatePostAppointment() {
      this.isFetching = true;
      let pa = new PostAppointment();
      pa.id = this.postAppointment.id;
      pa.appointment_id = this.postAppointment.appointment_id;
      pa.total_service_length = this.totalServiceLength;
      pa.color_formula = this.colorFormula;
      pa.highlight_formula = this.highlightFormula;
      pa.notes = this.notes;

      this.appointmentService.updatePostAppointment(pa).then(resp => {
        this.isFetching = false;
        if(resp.statusCode === 200) {
          swal('Awesome!', 'Post appointment notes were created', 'success');
          this.showNotes = false;
        }
      }).catch(err => {
        this.isFetching = false;
        if (err.statusCode === 400) {
          swal("Oops!", err.content.error, "error");
        } else if (err.statusCode === 500) {
          swal("This is emberassing!", "Could not process request at this time", "error");
        }
      })
    }

    displayServices () {
        let resp: string = '';
        const servicesLen = this.services.length;
        for(let i = 0; i < servicesLen; i++) {
            let s = this.services[i];
            resp += `${s.name}, `;
        }

        const cleanResp = resp.slice(0, -2);
        return cleanResp;
    }

    hasColorAppointment(): boolean {
        const s = this.services.filter(item => item.name.toLowerCase().includes('color') || item.name.toLowerCase().includes('highlight') || item.name.toLowerCase().includes('paint') );
        if(s.length > 0) return true
        return false
    }
}
