// Aurelia
import { bindable } from 'aurelia-framework';

export class VanoTitle {
    @bindable title: string;
    @bindable subTitle: string;
    @bindable description: string;
    @bindable logoFont: boolean;
    titleEL = undefined;
    subTitleEL = undefined;
    descriptionEL = undefined;

    attached() {
        if (this.description) {
            this.titleEL = document.querySelector('.title');
            this.subTitleEL = document.querySelector('.sub-title');
            this.descriptionEL = document.querySelector('.description');
            
            this.setFullTitleStyles();
            return;
        }
    }

    setFullTitleStyles() {
        this.titleEL.style.fontSize = '2.5625rem';
        this.subTitleEL.style.fontSize = '1.3125rem';
        this.descriptionEL.style.fontSize = '.8125rem';
    }
}
