import {bindable} from 'aurelia-framework';

export class VanoSettingsTabsHeaderItem{
    @bindable title: string;
    @bindable selected: string;
}