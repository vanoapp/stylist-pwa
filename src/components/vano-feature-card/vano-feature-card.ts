import { Company } from './../../stores/company/model';
// Aurelia
import { autoinject, observable, bindable } from 'aurelia-framework';

// Services
import { ProfessionalService } from '../../stores/professional/service';

// Models
import { Professional, UpdateProfessional, Feature } from '../../stores/professional/model';

// Third Party
import * as swal from 'sweetalert';
import { capitalize } from '../../helpers/utils';

@autoinject
export class VanoFeatureCard {
  display: boolean = true;
  authProfessional: Professional;
  authCompany: Company;
  @bindable feature: Feature;
  @observable enabled: boolean;

  constructor(private professionalService: ProfessionalService) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.authCompany = JSON.parse(localStorage.getItem('company'));
  }

  attached() {
    if(this.feature.name === 'require_credit_card'){
      if(this.authProfessional.independent) {
        if(!this.authProfessional.merchant_integrated) {
          return this.enabled = false;
        }
      } else {
        if(!this.authCompany.merchant_integrated) {
          return this.enabled = false;
        }
      }
    }
    this.enabled = this.feature.enabled;
    this.checkShouldDisplay()
  }

  checkShouldDisplay() {
    if(this.feature.name === 'clock_in_clock_out') {
      if(!this.authProfessional.permission.manage_professionals) {
        this.display = false;
      }
    }

    if(this.feature.name === 'manage_professionals') {
      if(!this.authProfessional.permission.manage_professionals) {
        this.display = false;
      }
    }
  }

  cleanTitle(title: string): string {
    return capitalize(title.replace(/_/g, ' '));
  }

  enabledChanged(newVal, oldVal) {
    if(oldVal === undefined) return;

    const prof = this.authProfessional

    if(this.feature.name === 'require_credit_card' && this.enabled === true) {
      if(prof.independent) {
        if(!prof.merchant_integrated) {
          swal('Hey!', 'You must first enable the credit card processing feature and complete the merchant onboarding to use this feature.');
          setTimeout(() => {
            this.enabled = false;
          }, 1000);
          return
        }
      } else {
        if(!this.authCompany.merchant_integrated) {
          swal('Hey!', 'You must first enable the credit card processing feature and complete the merchant onboarding to use this feature.');
          setTimeout(() => {
            this.enabled = false;
          }, 1000);
          return
        }
      }
    }

    if(this.feature.name === 'require_credit_card' && newVal === false) {
      if(prof.independent) {
        if(!prof.merchant_integrated) {
          return
        }
      } else {
        if(!this.authCompany.merchant_integrated) {
          return
        }
      }
    }

    this.professionalService.updateProfessionalFeature(this.authProfessional.id, this.feature.id, newVal).then(resp => {
      if (resp.statusCode === 200) {
        prof.features.find(i => i.name === this.feature.name).enabled = newVal;
        localStorage.setItem('professional', JSON.stringify(prof));
        this.fireFeatureChangedEvent(prof);
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      } else if (err.statusCode === 403) {
        swal("Oops!", "You do not have permission to do that!", "error");
      }
    })
  }

  fireFeatureChangedEvent(p: Professional) {
    window.dispatchEvent(new CustomEvent('vano-feature-changed', {
      detail: {
        professional: p,
      }
    }));
  }
}
