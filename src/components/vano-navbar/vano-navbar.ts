import { ProfessionalService } from './../../stores/professional/service';
// Aurelia
import {bindable, autoinject} from 'aurelia-framework';
import {Router} from 'aurelia-router';

// Models
import { Professional } from '../../stores/professional/model';
import { Company } from './../../stores/company/model';

// First Party
import env from '../../environment';

// Third Party
import * as moment from 'moment';
 
@autoinject
export class VanoNavbar {
    authProfessional: Professional;
    authCompany: Company;
    displayProfileMenu: boolean;
    @bindable sidebarOpen: boolean;
    @bindable router: Router;
    profileUrl: string;
    @bindable hasNotification: boolean;
    showClocInBtn: boolean;
    currentTime: string;
    isClockedIn: boolean;
    lastClocIn;
    showClockedInMenu: boolean;

    constructor(private professionalService: ProfessionalService) {
        this.authProfessional = JSON.parse(localStorage.getItem('professional'));
        this.authCompany = JSON.parse(localStorage.getItem('company'));
        this.handleWindowClick = this.handleWindowClick.bind(this);
        this.handleVanoFeatureChanged = this.handleVanoFeatureChanged.bind(this);
        this.handleResize = this.handleResize.bind(this);
        this.handleClockInChange = this.handleClockInChange.bind(this);
    }

    attached() {
      this.profileUrl = `http://clients.vanoapp.com/app/salons/${this.authCompany.id}` 
      if(env.debug && env.testing) this.profileUrl = `http://localhost:9001/app/salons/${this.authCompany.id}`;
      if(env.debug && !env.testing) this.profileUrl = `https://staging-clients.vanoapp.com/app/salons/${this.authCompany.id}`;

      window.addEventListener('click', this.handleWindowClick);
      window.addEventListener('vano-feature-changed', this.handleVanoFeatureChanged);
      window.addEventListener('resize', this.handleResize)
      window.addEventListener('vano-clock-in-changed', this.handleClockInChange)
      if(window.innerWidth > 765) {
        if(this.feature.cloc_in) {
          this.showClocInBtn = true;
        }
        setTimeout(() => {
          this.adjustLogoCentering();
        }, 300)
      }
      this.checkIsClockedIn();
    }

    detached() {
      window.removeEventListener('vano-feature-changed', this.handleVanoFeatureChanged)
      window.removeEventListener('click', this.handleWindowClick)
      window.removeEventListener('resize', this.handleResize)
      window.removeEventListener('vano-clock-in-changed', this.handleClockInChange)
    }

    handleResize() {
      this.adjustLogoCentering();
      if(window.innerWidth > 765 && this.isClockedIn && this.feature.cloc_in) {
          this.showClocInBtn = true;
      }
      if(window.innerWidth < 765) {
        this.showClocInBtn = false
      }
    }

    adjustLogoCentering() {
      const elem: any = document.getElementsByClassName('profile-wrapper')[0];
      let elemWidth = elem.offsetWidth;
      
      if(this.showClocInBtn) {
        elemWidth = parseInt(elemWidth) + 80;
      }
      const logoEl: any = document.getElementById('logo');
      logoEl.style.marginLeft = `${elemWidth}px`;
    }

    checkIsClockedIn() {
      this.professionalService.listProfessionalClocIns(this.authProfessional.id).then(resp => {
        if(resp.statusCode === 200) {
          const c = resp.content.cloc_ins.filter(item => item.cloc_out_time === '0001-01-01T00:00:00Z');
          if(c.length > 0) {
            this.lastClocIn = c[0];
            this.isClockedIn = true;
          }
        }
      });
    }

    handleVanoFeatureChanged() {
      this.authProfessional = JSON.parse(localStorage.getItem('professional'));
      this.showClocInBtn = false;
      if(this.feature.cloc_in && window.innerWidth > 765) {
        this.showClocInBtn = true;
      }
      this.adjustLogoCentering();
    }

    navigate(url: string): void {
      this.router.navigate(url);
      this.displayProfileMenu = false;
    }

    viewPublicProfile() {
      let baseUrl = 'https://vanoapp.com'
      let fragmentUrl = `/salons/${this.authProfessional.company_id}`;
      if(env.debug === true && env.testing === false) {
        baseUrl = 'https://staging-clients.vanoapp.com'
      }
      if(env.debug === true && env.testing === true) {
        baseUrl = 'http://localhost:9001'
      }
      const url = `${baseUrl}${fragmentUrl}`
      window.open(url);
      this.displayProfileMenu = false;
    }

    handleClockInChange(e) {
      console.log(e)
      if (e.detail.source === 'navbar') return
      const type = e.detail.type;
      if (type === 'clock-out') {
        this.isClockedIn = false;
        return;
      }
  
      this.isClockedIn = true;
      this.lastClocIn = e.detail.clockIn;
    }

    dispatchClockInEvent(type: string) {
      const details =  {
        type: type,
        clockIn: undefined,
        source: 'navbar'
      }
      if(type === 'clock-in') {
        details.clockIn = this.lastClocIn
      }
      window.dispatchEvent(new CustomEvent('vano-clock-in-changed', {
        detail: details
      }))
    }

    clocIn() {
      if(this.isClockedIn) return;
      this.professionalService.clocIn(this.authProfessional.id).then(resp => {
        if(resp.statusCode === 201) {
          this.lastClocIn = resp.content.cloc_in;
          this.isClockedIn = true;
          this.showClockedInMenu = false;
          this.dispatchClockInEvent('clock-in')
        }
      }).catch(err => {
        if (err.statusCode === 400) {
          swal("Oops!", err.content.error, "error");
        } else if (err.statusCode === 500) {
          swal("This is emberassing!", "Could not process request at this time", "error");
        }
      });
    }

    clocOut() {
      if(!this.isClockedIn) return;
      this.professionalService.clocOut(this.authProfessional.id, this.lastClocIn.id).then(resp => {
        if(resp.statusCode === 200) {
          this.isClockedIn = false;
          this.showClockedInMenu = false;
          this.dispatchClockInEvent('clock-out')
        }
      }).catch(err => {
        if (err.statusCode === 400) {
          swal("Oops!", err.content.error, "error");
        } else if (err.statusCode === 500) {
          swal("This is emberassing!", "Could not process request at this time", "error");
        }
      });
    }

    handleWindowClick(e) {
      const trgt = e.target;
      
      const isMenu = trgt.classList.contains('vano-navbar-menu') || trgt.classList.contains('vano-navbar-menu-item');
      const isProfile = trgt.classList.contains('profile-wrapper') || trgt.localName === 'img' || trgt.localName === 'h5';
      const isClocInBtn = trgt.classList.contains('cloc-in') || trgt.localName == 'svg' || trgt.localName == 'path';
      const isClocInMenu = trgt.classList.contains('vano-cloc-in-menu-section') || trgt.classList.contains('label') || trgt.classList.contains('time') || trgt.id === 'clocInMenu';
      if(isMenu || isProfile || isClocInMenu || isClocInBtn) return;
      this.displayProfileMenu = false;
      this.showClockedInMenu = false;
    }

    openAssistant() {
        this.router.navigateToRoute('app.assistant');
    }

    displayClocMenu() {
      this.showClockedInMenu = !this.showClockedInMenu;
      if(this.showClockedInMenu){
        this.currentTime = moment().format('h:mm a');
      }
    }

    openProfileMenu() {
      this.displayProfileMenu = true;
    }

    get feature() {
      return {
        merchant_services: this.authProfessional.features.filter(i => i.name === 'credit_card_processing')[0].enabled,
        manage_employees: this.authProfessional.features.filter(i => i.name === 'manage_professionals')[0].enabled,
        require_credit_card: this.authProfessional.features.filter(i => i.name === 'require_credit_card')[0].enabled,
        cloc_in: this.authProfessional.features.filter(i => i.name === 'clock_in_clock_out')[0].enabled,
      }
    }
}
