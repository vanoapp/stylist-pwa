import {bindable} from 'aurelia-framework';
import {Router} from 'aurelia-router';

import {Service} from '../../stores/service/model';

export class VanoServices { 
    @bindable services: Service[];
    @bindable router: Router;

    randomColor() {
        const colors = ['red', 'blue', 'green'];
        const color = colors[Math.floor(Math.random() * 3) + 0];
        switch (color) {
            case 'red':
                return 'red';
            case 'blue':
                return 'blue';
            case 'green':
                return 'green';
        }
        return 'blue';
    }

    clickService(id: string) {
      this.router.navigateToRoute('app.service.details.v2', {id});
    }
}
