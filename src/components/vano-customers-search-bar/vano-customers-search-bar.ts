import {bindable} from 'aurelia-framework';

export class VanoCustomersSearchBar{
    @bindable search: string;
}