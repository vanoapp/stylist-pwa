// Aurelia
import {bindable} from 'aurelia-framework';
import {Router} from 'aurelia-router';

export class VanoCanvasLayout{
    @bindable title: string;
    @bindable subTitle: string;
    @bindable router: Router;
    @bindable route: string; // full path not route name

    navigateBack() {
        if(this.route) {
          this.router.navigate(this.route);
          return;
        }
        this.router.navigateBack();
    }
}
