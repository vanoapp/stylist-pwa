import { VanoValidationRenderer } from './../../../validation/vano-renderer';
// Aurelia
import { bindable, autoinject } from 'aurelia-framework';
import { ValidationController, ValidationControllerFactory } from 'aurelia-validation';

import * as swal from 'sweetalert';

@autoinject
export class VanoAppointmentSelectedService {
  @bindable service: any;
  newLength: number;
  controller: ValidationController;
  updatingServiceLength: boolean;

  constructor(controller:ValidationControllerFactory ) {
    this.controller = controller.createForCurrentScope();
    this.controller.addRenderer(new VanoValidationRenderer());
  }

  cancel(e) {
    e.preventDefault()
    this.updatingServiceLength = false;
  }

  openModal() {
    this.updatingServiceLength = true;
  }

  removeService() {
    window.dispatchEvent(new CustomEvent('vano-appointment-remove-service', {
      detail: { service: this.service }
    }));
  }

  updateServiceLength() {
    if(!this.newLength || this.newLength === 0) {
      this.controller.addError('length is required', this, 'newLength');
      return
    }

    window.dispatchEvent(new CustomEvent('vano-appointment-service-length-change', {
      detail: { service: this.service, length: this.newLength }
    }));
    swal('Awesome!', `${this.service.name} was updated!`, 'success');
    this.updatingServiceLength = false;
  }
}
