// Aurelia
import {bindable} from 'aurelia-framework';

export class VanoNoResultsCard{
    @bindable text: string;
}