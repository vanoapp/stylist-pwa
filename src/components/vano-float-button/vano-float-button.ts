import { bindable } from 'aurelia-framework';

export class VanoFloatButton {
    @bindable position: string = 'bottom-right';
    @bindable color: string;
    @bindable zIndex: string = '51';
    thisEls = undefined;
    thisFloatBtn: HTMLElement = undefined;

    attached() {
        this.thisEls = document.getElementsByClassName('vano-float-button-component');
        this.thisFloatBtn = document.getElementById('vano-float-button');
        const elsLength = this.thisEls.length;
        for (let i = 0; i < elsLength; i++) {
            let el: HTMLElement = this.thisEls[i];
            el.style.zIndex = this.zIndex;
        }

        window.addEventListener('vano-fab-menu-style-change', e => this.handlefabMenuStyleChange(e));
    }


    handlefabMenuStyleChange(e) {
        this.thisFloatBtn.style.display = e.detail.display;
    }
}