import {bindable} from 'aurelia-framework';
import {Router} from 'aurelia-router';

export class VanoComponentLabel{
    @bindable title: string;
    @bindable link: string;
    @bindable router: Router;
    @bindable route: string;
    @bindable fontSize: string;
    labelEl = undefined;

    attached() {
        if(this.fontSize) {
            this.labelEl = document.querySelector('.vano-component-label-component');
            this.labelEl.style.fontSize = this.fontSize;
        }
    }

    linkClick() {
        this.router.navigateToRoute(this.route);
    }
}