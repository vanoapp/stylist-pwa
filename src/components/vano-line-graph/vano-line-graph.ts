// Aurelia
import { bindable } from 'aurelia-framework';

import 'chartjs';

export class VanoLineGraph {
  chartId: string;
  @bindable data: number[];
  @bindable labels: string[];
  @bindable label: string;
  @bindable legend: boolean = false;
  @bindable backgroundColor: string = '#FFF';
  @bindable lineColor: string = '#4786FF';
  chartEl;
  isChartAttached: boolean;

  constructor() {
    this.genId();
  }

  attached() {
    this.createChart();
  }

  genId() {
    this.chartId = Math.random().toString(36).substring(7);
  }

  createChart() {
    let self = this;
    let ctx: any = document.getElementById(this.chartId);
    const scales: any = {
      xAxes: [{
        gridLines: {
          display: false,
          drawBorder: false,
        }
      }],
      yAxes: [{
        gridLines: {
          display: false,
          drawBorder: false,
        }
      }]
    }
    this.chartEl = new Chart(ctx, {
      type: 'line',
      data: {
        labels: self.labels,
        datasets: [
          {
            label: null,
            fill: false,
            lineTension: 0,
            backgroundColor: '#fff',
            borderColor: self.lineColor,
            borderCapStyle: 'butt',
            borderDash: [],
            borderWidth: 0,
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: self.lineColor,
            pointBackgroundColor: self.lineColor,
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: self.data,
            spanGaps: true,
            responsive: true,
            maintainAspectRatio: true
          }
        ]
      },
      options: {
        legend: {
          display: self.legend
        },
        scales: scales,
        responsiveAnimationDuration: 500,
      }
    });
    this.isChartAttached = true;
  }

  dataChanged() {
    if (!this.isChartAttached) return
    this.chartEl.data.datasets.forEach((set) => {
      set.data = this.data;
    });
    this.chartEl.update();
  }

  labelsChanged() {
    if (!this.isChartAttached) return
    this.chartEl.data.labels = this.labels
    this.chartEl.update();
    
  }
}
