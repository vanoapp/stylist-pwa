// Aurelia
import {bindable} from 'aurelia-framework';
import {Router} from 'aurelia-router';

// Models 
import {Professional} from '../../../stores/professional/model';

export class VanoProfessionalsAccordionHeader{
    @bindable professional: Professional;
    @bindable opened: boolean = false;
    @bindable router: Router;
    showEdit: boolean;

    get fullName(): string {
        if(!this.professional) return
        return `${this.professional.first_name} ${this.professional.last_name}`
    }

    navigateToDetails(e) {
      this.router.navigateToRoute('app.professional.details', {id: this.professional.id});
    }
}
