// Aurelia
import {bindable} from 'aurelia-framework';
import {Router} from 'aurelia-router';

// Models 
import {Professional} from '../../stores/professional/model';

export class VanoProfessionalsAccordion{
    @bindable professional: Professional; 
    @bindable router: Router; 
}
