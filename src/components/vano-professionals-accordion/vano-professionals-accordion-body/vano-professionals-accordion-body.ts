// Aurelia
import {bindable} from 'aurelia-framework';

// Models
import {Professional} from '../../../stores/professional/model';

export class VanoProfessionalsAccordionBody{
    @bindable professional: Professional;
    @bindable opened: boolean = false;

    submit() {
      window.dispatchEvent(new CustomEvent('vano-professional-permission-change', {detail: {professional: this.professional}}));
    }
}
