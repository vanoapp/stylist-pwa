// Aurelia
import {bindable} from 'aurelia-framework';

export class VanoMessageBubble{
    @bindable text: string;
    @bindable fromBot: boolean;
}