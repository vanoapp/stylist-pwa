export class VanoOfflineInfoCard{
    randomColor() {
        const colors = ['red', 'blue', 'green'];
        const color = colors[Math.floor(Math.random() * 3) + 0];
        switch (color) {
            case 'red':
                return 'red';
            case 'blue':
                return 'blue';
            case 'green':
                return 'green';
        }
        return 'blue';

    }
}