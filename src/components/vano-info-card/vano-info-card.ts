import { bindable } from 'aurelia-framework';

export class VanoInfoCard {
    @bindable bubbleText: string;
    @bindable title: string;
    @bindable subTitle: string;
    @bindable color: string;
}