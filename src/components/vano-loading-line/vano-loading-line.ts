import { bindable } from 'aurelia-framework';

export class VanoLoadingLine {
    @bindable width: string = '100%';
    lineEl = undefined;
    randomClass: string;

    constructor() {
        this.generateRandomClass();
    }

    attached() {
        this.lineEl = document.querySelector(`.${this.randomClass}`);
        this.addStyles();
    }

    addStyles(): void {
        this.lineEl.style.width = this.width;
    }

    generateRandomClass() {
        let text = "";
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        for (let i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        this.randomClass = text;
    }
}