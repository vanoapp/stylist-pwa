import { bindable, autoinject } from "aurelia-framework";
import { ProfessionalService } from "../../stores/professional/service";
import { Professional } from "../../stores/professional/model";

import * as moment from 'moment';

@autoinject
export class VanoClockInCard{
  authProfessional: Professional;
  isClockedIn: boolean;
  isFetching: boolean;
  currentTime: string;
  lastClockIn: any;

  constructor(private professionalService: ProfessionalService){
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.handleClockInChange = this.handleClockInChange.bind(this);
  }

  attached() {
    this.checkIsClockedIn();
    this.currentTime = moment().format('h:mm a');
    window.addEventListener('vano-clock-in-changed', this.handleClockInChange)
  }

  detached() {
    window.removeEventListener('vano-clock-in-changed', this.handleClockInChange);
  }

  handleClockInChange(e) {
    if(e.detail.source === 'dashboard') return
    const type = e.detail.type;
    if(type === 'clock-out') {
      this.isClockedIn = false;
      return;
    }

    this.isClockedIn = true;
    this.lastClockIn = e.detail.clockIn;
  }

  checkIsClockedIn() {
    this.professionalService.listProfessionalClocIns(this.authProfessional.id).then(resp => {
      if(resp.statusCode === 200) {
        const c = resp.content.cloc_ins.filter(item => item.cloc_out_time === '0001-01-01T00:00:00Z');
        if(c.length > 0) {
          this.lastClockIn = c[0];
          this.isClockedIn = true;
        }
      }
    });
  }

  getProfessionalHours() {
    this.isFetching = true;
    this.professionalService
  }

  toggleClockIn() {
    if(!this.isClockedIn) return this.clocIn();
    this.clocOut();
  }

  clocIn() {
    this.professionalService.clocIn(this.authProfessional.id).then(resp => {
      if(resp.statusCode === 201) {
        this.lastClockIn = resp.content.cloc_in;
        this.isClockedIn = true;
        this.dispatchClockInEvent('clock-in')
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("Error on our end!", "Could not process request at this time", "error");
      }
    });
  }

  clocOut() {
    this.professionalService.clocOut(this.authProfessional.id, this.lastClockIn.id).then(resp => {
      if(resp.statusCode === 200) {
        this.isClockedIn = false;
        this.dispatchClockInEvent('clock-out')
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  dispatchClockInEvent(type: string) {
    const details =  {
      type: type,
      clockIn: undefined,
      source: 'dashboard'
    }
    if(type === 'clock-in') {
      details.clockIn = this.lastClockIn
    }
    window.dispatchEvent(new CustomEvent('vano-clock-in-changed', {
      detail: details
    }))
  }
}
