// Aurelia
import {bindable, autoinject} from 'aurelia-framework';

@autoinject
export class VanoModal{
  @bindable modalId: string;
  @bindable opened: boolean;
  @bindable static: boolean; // if true won't close on overlay click
  element: Element;

  constructor(element: Element){
    this.element = element;
    this.handleComponentClick = this.handleComponentClick.bind(this)
  }

  attached() {
    this.element.addEventListener('click', this.handleComponentClick);
  }

  detached() {
    this.element.removeEventListener('click', this.handleComponentClick);
  }

  handleComponentClick(e) {
    const isOverlay: boolean = e.target.classList.contains('vano-modal-component') || e.target.classList.contains('vano-modal-body');
    if(!isOverlay || this.static) return
    this.opened = false;
  }
}
