import {bindable} from 'aurelia-framework';

export class VanoAccordion {
   @bindable opened: boolean = false;
}