import {bindable} from 'aurelia-framework';

export class VanoAccordionHeader{
    @bindable opened: boolean = false;
    @bindable height: string;
    headerEl = undefined;

    attached() {
        if(this.height) {
            this.headerEl = document.querySelector('.vano-accordion-header-component');
            this.headerEl.style.height = this.height;
        }
    }
}