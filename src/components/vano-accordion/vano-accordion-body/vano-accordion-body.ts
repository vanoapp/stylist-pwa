import {bindable} from 'aurelia-framework';

export class VanoAccordionBody{
    @bindable opened: boolean = false;
}