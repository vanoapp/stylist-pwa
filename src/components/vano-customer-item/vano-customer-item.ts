import {bindable} from 'aurelia-framework';

export class VanoCustomerItem{
    @bindable customer;
    @bindable letter;
}