// Aurelia
import {bindable} from 'aurelia-framework';

export class VanoOnboardingCardHeader{
    @bindable title: string;
}