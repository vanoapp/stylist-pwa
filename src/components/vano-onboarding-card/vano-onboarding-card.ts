// Aurelia
import {bindable} from 'aurelia-framework';

export class VanoOnboardingCard{
    @bindable state: string;
}