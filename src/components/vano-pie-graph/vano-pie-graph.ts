import { shuffleArray } from './../../helpers/utils';
// Aurelia
import { bindable } from 'aurelia-framework';

import 'chartjs';
import PieceLabel from '../../helpers/chart';

export class VanoPieGraph {
  chartId: string;
  @bindable data: number[];
  @bindable labels: string[];
  @bindable label: string;
  @bindable legend: boolean = false;
  @bindable backgroundColor: string = '#FFF';
  @bindable lineColor: string = '#4786FF';
  @bindable money: boolean;
  chartEl;
  isChartAttached: boolean;

  constructor() {
    this.genId();
  }

  attached() {
    this.createChart();
  }

  genId() {
    this.chartId = Math.random().toString(36).substring(7);
  }

  createChart() {
    let  self = this;
    let  ctx: any = document.getElementById(this.chartId);

    let  plugins: any = [{
      beforeInit: function (chartInstance) {
        chartInstance.pieceLabel = new PieceLabel();
      },
      beforeDatasetsUpdate: function (chartInstance) {
        chartInstance.pieceLabel.beforeDatasetsUpdate(chartInstance);
      },
      afterDatasetsDraw: function (chartInstance) {
        chartInstance.pieceLabel.afterDatasetsDraw(chartInstance);
      }
    }];

    this.chartEl = new Chart(ctx, {
      plugins: plugins,
      type: 'doughnut',
      data: {
        labels: self.labels,
        datasets: [{
          data: self.data,
          backgroundColor: self.backgroundColor,
        }],
      },
      options: {
        legend: {
          display: false
        },
        tooltips: {
          callbacks: {
            label (tooltipItem, data) {
              let allData = data.datasets[tooltipItem.datasetIndex].data;
              let tooltipLabel = data.labels[tooltipItem.index];
              let tooltipData = allData[tooltipItem.index];
              let total = 0;
              for (let i in allData) {
                total += allData[i];
              }
              let tooltipPercentage = Math.round((tooltipData / total) * 100);
              if(self.money) {
                tooltipData = `$${tooltipData}`;
              }
              return tooltipLabel + ': ' + tooltipData + ' (' + tooltipPercentage + '%)';
            }
          }
        },
        pieceLabel: {
          render: 'percentage',
          fontColor: '#fff',
          fontFamily: "'Nunito', sans-serif",
        },
        responsiveAnimationDuration: 500,
      }
    });
    this.isChartAttached = true;
  }

  dataChanged() {
    if (!this.isChartAttached) return
    
    this.chartEl.data.datasets.forEach((set) => {
      set.data = this.data;
    });
    
    this.chartEl.update();
  }

  labelsChanged() {
    if (!this.isChartAttached) return
    this.chartEl.data.labels = this.labels
    this.chartEl.update();
  }
}
