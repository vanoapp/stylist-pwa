import {bindable} from 'aurelia-framework';

export class VanoAppointmentsHeader{
    @bindable selected: string = 'month';

    setSelected(s: string): void {
        this.selected = s;
    }
}