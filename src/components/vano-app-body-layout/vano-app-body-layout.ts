import { bindable } from 'aurelia-framework';

export class VanoAppBodyLayout {
    @bindable sidebarOpen: boolean;
    @bindable hasNotification: boolean;

    sidebarOpenChanged() {
        let bodyEl = document.getElementsByTagName('body')[0];
        if(this.sidebarOpen && window.innerWidth < 960) {
            bodyEl.style.overflowY = 'hidden';
            return;
        }
        bodyEl.style.overflowY = 'auto';
    }
}
