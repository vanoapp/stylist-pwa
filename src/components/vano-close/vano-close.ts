import {bindable} from 'aurelia-framework';

export class VanoClose {
    @bindable color: string = 'blue';
    @bindable size: string;
}
