// Aurelia
import {bindable} from 'aurelia-framework';
import { capitalize } from '../../helpers/utils';

export class VanoManageeInvitationCard{
  @bindable accepted: boolean;
  @bindable name: string;
  @bindable permission;
  @bindable invitationId: string;
  hasAttached: boolean

  attached() {
    this.hasAttached = true;
  }

  acceptedChanged() {
    if(!this.hasAttached) return
    window.dispatchEvent(new CustomEvent('vano-invitation-status-changed', {detail: {
      accepted: this.accepted,
      invitationId: this.invitationId,
    }}));
  }
}
