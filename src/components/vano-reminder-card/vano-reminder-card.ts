import { Reminder } from './../../stores/reminder/model';
// Aurelia
import { bindable } from 'aurelia-framework';
import { Router } from 'aurelia-router';

export class VanoReminderCard {
  @bindable reminderId: string;
  @bindable title: string;
  @bindable completed: boolean;
  @bindable router: Router;

  toggleReminder() {
    this.completed = !this.completed;
    window.dispatchEvent(new CustomEvent('vano-reminder-toggle', {
      detail: {
        checked: this.completed,
        reminderId: this.reminderId,
      }
    }));
  }

  edit() {
    this.router.navigateToRoute('app.reminder.details', { id: this.reminderId });
  }

  delete() {
    window.dispatchEvent(new CustomEvent('vano-delete-reminder', {
      detail: {
        reminderId: this.reminderId,
      }
    }))
  }
}
