import {bindable, autoinject} from 'aurelia-framework';
import {Router} from 'aurelia-router';

import {Appointment} from '../../stores/appointment/model';
import {Customer} from '../../stores/customer/model';

@autoinject
export class VanoAppointmentsTable{
    @bindable appointments: Appointment[];
    @bindable customers: Customer[];
    router: Router;

    constructor(router: Router) {
        this.router = router;
    }

    viewAppointment(id: string):void {
        this.router.navigate(`/appointments/${id}`);
    }

    customerIdToFullName(id: string) {
        let customersLen = this.customers.length;
        for (let i = 0; i < customersLen; i++) {
            if (id == this.customers[i].id) {
                return `${this.customers[i].first_name} ${this.customers[i].last_name}`
            }
        }
        return 'name unavailable'
    }
}
