import { ProfessionalService } from './../../stores/professional/service';
import { VanoToggle } from './../vano-forms/vano-toggle/vano-toggle';
// Aurelia
import { bindable } from 'aurelia-framework';

// First Party
import { inArray } from '../../helpers/utils';
import { Calendar, CalendarEvent } from './../../libs/calendarjs/calendar';

// Third Party
import * as moment from 'moment';
import { Professional } from '../../stores/professional/model';

export class CalendarOption {
  label: string;
  value: string;
}

export class VanoCalendar {
  authProfessional: Professional;
  @bindable calendarDisplay: string = 'month';
  @bindable events: CalendarEvent[];
  @bindable options: CalendarOption[];
  @bindable selectedOption: string;
  selectedLabel: string = 'My Calendar';
  monthCalendarTitle: string;
  doneDates: string[] = [];
  supportsPassive: boolean;
  calendarEl: Element;
  startX: number;
  currentX: number;
  touchingCalendar: boolean = false;
  title: string;
  isMobileView: boolean;
  calendar: Calendar;
  showMenu: boolean;
  showSpMenu: boolean;
  showViewMenu: boolean;
  @bindable loading: boolean;
  @bindable initialDate: number;

  constructor(private professioanlService: ProfessionalService) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.handleWindowClick = this.handleWindowClick.bind(this);
  }

  attached() {
    this.initCalendar();
    this.displayEvents();
    if (window.innerWidth < 768) {
      this.isMobileView = true;
    }
    window.addEventListener('click', this.handleWindowClick);
  }

  detached() {
    window.removeEventListener('click', this.handleWindowClick)
  }

  private handleWindowClick(e) {
    if (e.target.classList.contains('vano-calendar-dont-close')) return;
    this.closeMenu();
  }

  toggleSpMenu() {
    this.showViewMenu = false;
    this.showMenu = !this.showMenu;
    this.showSpMenu = this.showMenu;
  }

  toggleViewMenu() {
    this.showSpMenu = false;
    this.showMenu = !this.showMenu;
    this.showViewMenu = this.showMenu;
  }

  selectProfessional(o: CalendarOption) {
    this.selectedLabel = o.label;
    this.closeMenu();
    window.dispatchEvent(new CustomEvent('vano-calendar-selected-professional', {
      detail: o,
    }))
  }

  closeMenu() {
    this.showMenu = false;
    this.showSpMenu = false;
    this.showViewMenu = false;
  }

  selectView(view: string) {
    switch (view) {
      case 'mo':
        sessionStorage.setItem('appointment-view', 'mo');
        this.calendar.renderMonthCalendar();
        break;
      case '3d':
        sessionStorage.setItem('appointment-view', '3d');
        this.calendar.renderAgendaView(3)
        break;
      case '4d':
        sessionStorage.setItem('appointment-view', '4d');
        this.calendar.renderAgendaView(4)
        break;
      case 'we':
        sessionStorage.setItem('appointment-view', 'we');
        this.calendar.renderAgendaView(7)
        break;
      default:
        this.calendar.renderAgendaView(7)
    }
    this.closeMenu();
  }

  initCalendar() {
    this.calendar = new Calendar(document.getElementById('calendar'));
    if (this.initialDate) {
      this.calendar.date = moment(this.initialDate * 1000);
    }
    this.renderInitialView()
    this.title = this.calendar.getTitle();
  }

  renderInitialView() {
    const initialView = sessionStorage.getItem('appointment-view');
    if (!initialView || initialView === '') return this.calendar.renderAgendaView();
    this.selectView(initialView);
  }

  eventsChanged() {
    this.displayEvents();
  }

  displayEvents() {
    if (!this.events) return;
    this.calendar.setEvents(this.events);
    this.calendar.render()
  }

  next() {
    this.calendar.next();
    this.title = this.calendar.getTitle();
  }

  prev() {
    this.calendar.prev();
    this.title = this.calendar.getTitle();
  }
}

export class VanoCalendarEvent {
  id: string;
  title: string;
  start: string;
  end: string;
  color: string;
  textColor: string;
  className: string;
}

const randomIntFromInterval = (min: number, max: number): number => {
  return Math.floor(Math.random() * (max - min + 1) + min);
}
