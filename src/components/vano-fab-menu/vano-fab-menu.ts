import { bindable } from 'aurelia-framework';

export class VanoFabMenu {
    fabButtonEL: HTMLElement;
    fabMenuEL: HTMLElement;
    fabMenuWrapperEl = undefined;
    TouchingFabMenu: boolean;
    opened: boolean = false;
    supportsPassive: boolean = undefined;
    startX: number;
    currentX: number;
    @bindable sidebarOpen: boolean = false;

    attached() {
        this.fabButtonEL = document.getElementById('vano-fab-button');
        this.fabMenuEL = document.getElementById('vano-fab-menu');
        this.fabMenuWrapperEl = document.getElementsByClassName('vano-fab-menu-wrapper')[0];
        this.addEventListeners();
    }

    addEventListeners() {
        this.fabButtonEL.addEventListener('click', (e) => this.handleFabClick(e));
        window.addEventListener('vano-fab-menu-change', (e) => this.handleMenuChange(e));
        // this.fabMenuWrapperEl.addEventListener('touchstart', e => this.onTouchStart(e), this.applyPassive());
        // this.fabMenuWrapperEl.addEventListener('touchmove', e => this.onTouchMove(e), this.applyPassive());
        // this.fabMenuWrapperEl.addEventListener('touchend', e => this.onTouchEnd(e), this.applyPassive());
    }

    applyPassive() {
        if (this.supportsPassive !== undefined) {
            return this.supportsPassive ? { passive: true } : false;
        }
        let isSupported = false;
        var opts = Object.defineProperty && Object.defineProperty({}, 'passive', { get: function () { isSupported = true } });
        // create a throwaway element & event and (synchronously) test out our options
        document.addEventListener('test', function () { }, opts);
        this.supportsPassive = isSupported;
        return this.applyPassive();
    }

    onTouchStart(evt) {
        if (!this.fabMenuWrapperEl || !this.fabButtonEL.classList.contains('opened'))
            return;

        this.startX = evt.touches[0].pageX;
        this.currentX = this.startX;

        this.TouchingFabMenu = true;
        requestAnimationFrame(this.update.bind(this));
    }

    onTouchMove(evt) {
        if (!this.TouchingFabMenu)
            return;

        this.currentX = evt.touches[0].pageX;
    }

    onTouchEnd(evt) {
        if (!this.TouchingFabMenu)
            return;

        this.TouchingFabMenu = false;

        const translateX = Math.max(0, this.currentX - this.startX);
        this.fabMenuWrapperEl.style.transform = '';

        if (translateX > 0) {
            this.opened = false;
        }
    }

    update() {
        if (!this.TouchingFabMenu)
            return;

        requestAnimationFrame(this.update.bind(this));

        const translateX = Math.max(0, this.currentX - this.startX);
        this.fabMenuWrapperEl.style.transform = `translateX(${translateX}px)`;
    }

    handleFabClick(e: MouseEvent) {
        this.opened = !this.opened;
        let bodyEl = document.getElementsByTagName('body')[0];
        let components: any = document.getElementsByClassName('au-animate')[0];
        if (this.opened) {
            const maxHeight = window.innerHeight - 80;
            components.style.maxHeight = `${maxHeight}px`;
            components.style.overflowY = 'hidden';
            return;
        }
        components.style.overflowY = 'auto';
        components.style.maxHeight = 'none';
    }

    handleMenuChange(e) {
        this.opened = e.detail.open;
    }

    sidebarOpenChanged() {
        if (this.fabMenuEL) {
            if (this.sidebarOpen) {
                this.fabMenuEL.classList.add('sidebar-open');
                return;
            }
            this.fabMenuEL.classList.remove('sidebar-open');
        }
    }

    sidebarDynamicClass() {
        if (this.sidebarOpen) return 'sidebar-open';
        return ''
    }
}