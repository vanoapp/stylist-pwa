// Aurelia
import {bindable} from 'aurelia-framework';

export class VanoFabMenuItem {
    @bindable title: string;
    @bindable route: string;
    itemEl = undefined;
    randomClass: string;

    constructor() {
        this.generateRandomClass();
    }

    attached() {
        this.itemEl = document.querySelector(`.${this.randomClass}`);
        this.addEventListeners();
    }

    addEventListeners() {
        this.itemEl.addEventListener('click', e => this.handleClick(e));
    }

    handleClick(e: MouseEvent) {
        window.dispatchEvent(new CustomEvent('vano-fab-menu-change', {detail: {open: false}}));
    }

    generateRandomClass() {
        let text = "";
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        for (let i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        this.randomClass = text;
    }
}