import {bindable, bindingMode} from 'aurelia-framework';

export class VanoDialPad{
    @bindable({ defaultBindingMode: bindingMode.twoWay }) amount: number = 0.00;
    @bindable loading: boolean;
    UIAmount: string = '0';

    pressPad(e, amount: string) {
        let elem = e.srcElement;
        if (e.srcElement.toString() === '[object HTMLHeadingElement]') {
            elem = e.srcElement.parentElement
        }
        elem.classList.add('clicked');
        setTimeout(() => {
            elem.classList.remove('clicked');
        }, 200);

        if(this.UIAmount === '0' && amount != '<') {
            this.UIAmount = amount;
            this.amount = parseFloat(this.UIAmount);
            return;
        }

        if(amount === '<') {
            this.UIAmount = this.UIAmount.slice(0, -1);
            this.amount = parseFloat(this.UIAmount);
            if(this.UIAmount === '') {
                this.UIAmount = '0';
                this.amount = parseFloat(this.UIAmount);
            }
            return;
        }

        this.UIAmount += amount;
        this.amount = parseFloat(this.UIAmount);
    }

    submit() {
        window.dispatchEvent(new CustomEvent('vano-dial-pad-submit', {detail: {total: this.amount}}));
    }
}