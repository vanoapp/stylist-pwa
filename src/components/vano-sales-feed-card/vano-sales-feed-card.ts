import {bindable} from 'aurelia-framework';

export class VanoSalesFeedCard{
    @bindable type: string;
    @bindable amount: string;
    @bindable customer: string;
    @bindable when: string;
}