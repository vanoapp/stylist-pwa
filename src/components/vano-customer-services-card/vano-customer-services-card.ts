import { Customer } from './../../stores/customer/model';
// Aurelia
import { autoinject, bindable } from 'aurelia-framework';
import { ValidationController, ValidationRules, ValidationControllerFactory } from 'aurelia-validation';

// Services
import { CustomerService } from './../../stores/customer/service';

// Models
import { Service } from './../../stores/service/model';
import { Professional } from './../../stores/professional/model';
import { inArray } from '../../helpers/utils';

// First Party
import { Option } from './../vano-forms/vano-search-dropdown/vano-search-dropdown';
import { VanoValidationRenderer } from './../../validation/vano-renderer';

// third party 
import * as swal from 'sweetalert';

@autoinject
export class VanoCustomerServicesCard {
  authProfessional: Professional;
  isFetching: boolean;
  @bindable customer: Customer;
  services: Service[];
  filteredServices: Service[];
  customerServices: any[];
  serviceOptions: Option[];
  selectedService: string;
  controller: ValidationController;
  duration: string;

  constructor(private customerService: CustomerService, controller: ValidationControllerFactory) {
    this.authProfessional = JSON.parse(localStorage.getItem('professional'));
    this.services = this.authProfessional.services;
    this.controller = controller.createForCurrentScope();
    this.controller.addRenderer(new VanoValidationRenderer());
  }

  attached() {
    this.isFetching = true;
    this.getCustomerServices().then(() => {
      this.filteredServices = this.filterServices();
      this.generateServiceOptions(this.filteredServices);
    });
  }

  customerChanged() {
    this.isFetching = true;
    this.getCustomerServices().then(() => {
      this.filteredServices = this.filterServices();
      this.generateServiceOptions(this.filteredServices);
    });
  }

  validate() {
    this.controller.validate().then(res => {
      if (res.valid) {
        this.createCustomerService();
      }
    })
  }

  createCustomerService() {
    this.customerService.addCustomerService(this.customer.id, this.selectedService, parseInt(this.duration)).then(resp => {
      if (resp.statusCode === 201) {
        swal('Awesome!', 'Custom customer service duration was created.', 'success');
        this.duration = '';
        this.selectedService = '';
        this.customerServices.push(resp.content.customer_service);
        this.filteredServices = this.filterServices()
        this.generateServiceOptions(this.filteredServices)
      }
    }).catch(err => {
      if (err.statusCode === 400) {
        swal("Oops!", err.content.error, "error");
      } else if (err.statusCode === 500) {
        swal("This is emberassing!", "Could not process request at this time", "error");
      }
    });
  }

  getCustomerServices() {
    return this.customerService.listCustomerServices(this.customer.id).then(resp => {
      if (resp.statusCode === 200) {
        this.customerServices = resp.content.customer_services.filter(s => {
          const svc = this.idToService(s.service_id);
          if (!svc) return false
          return true
        });
      }
    })
  }

  deleteCustomerService(id: string) {
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this customer service length",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false
    },
      () => {
        this.customerService.removeCustomerService(this.customer.id, id).then((resp) => {
          if (resp.statusCode === 200) {
            swal('Awesome!', 'custom customer service length was deleted', 'success');
            this.customerServices = this.customerServices.filter(i => i.id !== id);
            this.filteredServices = this.filterServices()
            this.generateServiceOptions(this.filteredServices)
          }
        }).catch(err => {
          if (err.statusCode === 400) {
            swal("Oops!", err.content.error, "error");
          } else if (err.statusCode === 500) {
            swal("This is emberassing!", "Could not process request at this time", "error");
          }
        });
      });
  }

  idToServiceName(id: string): string {
    return this.services.filter(i => i.id === id)[0].name;
  }

  idToService(id: string): Service {
    return this.services.filter(i => i.id === id)[0];
  }

  filterServices(): Service[] {
    if (this.customerServices.length === 0) return this.services;
    const customerServicesIds = this.customerServices.map(s => {
      return s.service_id;
    })
    return this.services.filter(s => {
      if (!inArray(customerServicesIds, s.id)) return true;
      return false
    })
  }

  generateServiceOptions(services: Service[]): void {
    this.serviceOptions = this.filteredServices.map(s => {
      const o = {
        label: s.name,
        value: s.id,
      }
      return o
    });
  }
}

ValidationRules
  .ensure('duration').displayName('duration').required()
  .ensure('selectedService').displayName('service').required()
  .on(VanoCustomerServicesCard)
