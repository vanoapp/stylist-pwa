import { Professional } from './../../stores/professional/model';
// Aurelia
import {bindable} from 'aurelia-framework';

// Models
import { Option } from './../vano-forms/vano-search-dropdown/vano-search-dropdown';

// First Party
import {capitalize} from '../../helpers/utils'

export class VanoAdminCalendarHeader{
    @bindable professionals: Professional[];
    @bindable selected;
    @bindable selectedProfessional;
    @bindable title: string;
    professionalOpts: Option[];

    attached() {
      this.generateProfessionalsOptions();
    }

    professionalsChanged() {
      this.generateProfessionalsOptions();
    }
  
    generateProfessionalsOptions() {
      let profOpts: Option[] = [];
      if(!this.professionals) return
      const pLen = this.professionals.length;
      this.professionals.map(p => {
        let opt = {
          "label": capitalize(`${p.first_name} ${p.last_name}`),
          "value": p.id,
        }
        profOpts.push(opt);
      });

      let opt = {
        "value": "all",
        "label": "All"
      }

      profOpts.unshift(opt);
  
      this.professionalOpts = profOpts;
    }
}
