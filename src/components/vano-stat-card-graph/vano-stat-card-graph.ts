import { bindable } from 'aurelia-framework';

export class VanoStatCardGraph {
    @bindable color: string;
    @bindable direction: string;
    @bindable value: number;
    @bindable percentage: boolean;
    @bindable money: boolean;

    attached() {        
        this.generateCircleSize();
        window.addEventListener('resize', e => this.resizeScreen(e));
    }

    resizeScreen(e) {
        let elems = document.getElementsByClassName('percentage-circle-fill');
        
        if (window.innerWidth > 960) {
            this.setSmallCircle(elems);
            return
        }

        this.setBigCircle(elems);
    }

    generateCircleSize() {
        if (window.innerWidth > 960) {
            let elems = document.getElementsByClassName('percentage-circle-fill');
            this.setSmallCircle(elems);
        }
    }

    setBigCircle(elems) {
        const elemLength = elems.length;
        for (let i = 0; i < elemLength; i++) {
            let elem = elems[i];
            elem.setAttribute('cx', '53');
            elem.setAttribute('cy', '53');
            elem.setAttribute('r', '52');
        }
    }

    setSmallCircle(elems) {
        const elemLength = elems.length;
        for (let i = 0; i < elemLength; i++) {
            let elem = elems[i];
            elem.setAttribute('cx', '40');
            elem.setAttribute('cy', '40');
            elem.setAttribute('r', '39');
        }
    }
}
