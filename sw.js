importScripts('workbox-sw.prod.v2.0.0.js');

/**
 * DO NOT EDIT THE FILE MANIFEST ENTRY
 *
 * The method precache() does the following:
 * 1. Cache URLs in the manifest to a local cache.
 * 2. When a network request is made for any of these URLs the response
 *    will ALWAYS comes from the cache, NEVER the network.
 * 3. When the service worker changes ONLY assets with a revision change are
 *    updated, old cache entries are left as is.
 *
 * By changing the file manifest manually, your users may end up not receiving
 * new versions of files because the revision hasn't changed.
 *
 * Please use workbox-build or some other tool / approach to generate the file
 * manifest which accounts for changes to local files and update the revision
 * accordingly.
 */
const fileManifest = [
  {
    "url": "index.html",
    "revision": "e1b92939ecf0905b02d4152c6a534ab6"
  },
  {
    "url": "scripts/app-bundle-3c421f3e71.js",
    "revision": "ef542ed2d1c59838e2f6a8dddf0089d4"
  },
  {
    "url": "scripts/vendor-bundle-a719819adc.js",
    "revision": "3083729647d507f52b5ac9ec7c186adb"
  },
  {
    "url": "/",
    "revision": "4a6ed62be46901d3c56f6a9de57cd64f"
  }
];

const workboxSW = new self.WorkboxSW();
workboxSW.precache(fileManifest);
